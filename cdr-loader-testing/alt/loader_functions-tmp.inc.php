<?php

// {{{ file_lines

function file_lines ($file) {
	return trim(shell_exec ("wc -l < $file"));
}

// }}}
// {{{ cdr_finder_main_loop
	
	function cdr_finder_main_loop ($cdr_file_paths,$mask,$callback) {

		foreach ($cdr_file_paths as $prfx => $path) {
			
			$dir = opendir ($path);
			
			while (1) {
				$file = readdir ($dir);
				if (!is_string($file)) break; // TODO: check if there's a correctable problem or just the end of the direcotry

				if (!preg_match($mask,$file)) continue;
				if (!is_file("$path/$file")) continue;
				if (!is_readable("$path/$file")) continue; // TODO: whine about this !!!!
				$data =Array(
					"path" => $path,
					"filename" => $file,
					"filepath" => "$path/$file",
					"prefix" => $prfx,
					"fileid" => "$prfx$file",
					"newfile" => false,
				);
				$callback ($data);
			}
		}

	}

// }}}
// {{{ cdr_load_file
	
	function cdr_load_file_info (&$c) {
		global $billing;
		$fileid = $c['fileid'];
		$sq_fileid = addslashes ($fileid);
		
		if ($billing->getone("SELECT count(*) FROM ldr_files WHERE filename = '$sq_fileid'")==1) {
			return;
		}
		
		$md5 = md5_file($c['filepath']);
		$sha1 = sha1_file($c['filepath']);
		$size = filesize($c['filepath']);
		$lines = file_lines($c['filepath']);
		$ins = Array (
			"filename" => "$fileid",
			"md5sum" => "$md5",
			"sha1sum" => "$sha1",
			"filesize" => "$size",
			"lines" => "$lines",
			"loaded" => "0",
			"locked" => "0",
		);
		$cols="";
		$vals="";
		foreach ($ins as $col => $v) {
			$sq_v = addslashes ($v);
			if ($cols!="") $cols.=",";
			if ($vals!="") $vals.=",";
			$cols.="$col";
			$vals.="'$sq_v'";
		}
		$sql = "INSERT INTO ldr_files ($cols) VALUES ($vals)";

		$res = $billing->query($sql);
		if (DB::isError($res)) {
			// TODO: freak out properly
			echo "SQL error\nQuery: $sql\nError: ".$res->getmessage()."\n";
			exit();
		}
		$c['newfile']=TRUE;
	}

// }}}
// {{{ fix_cdr_file

	function fix_cdr_file ($file_name,&$info) {
		global $billing;
		global $cdr_table;

		$lines = Array ();
		$lines_found = 0;
		
		echo "Fixing (trying to) file {$info['filename']}\n";
		sleep (2);
		
		$sql = "SELECT cdr_file_line FROM $cdr_table WHERE cdr_file_id = '{$info['id']}' ";
		$res = $billing->query($sql);
		if (DB::isError($res)) {
			echo "DB error\n";
			return TRUE;
		}
		while (1) {
			$row = $res->fetchrow();
			if (!is_array($row)) break;
			$lines_found++;
			$l = $row['cdr_file_line'];
			if ($l<1 || $l>$info['lines']) {
				echo "Unexpected: invalid line number ($l) expecting >= 1 AND <= {$info['lines']}\n";
				exit();
			}
			if ($lines[$l]>0) {
				// TODO: handle duplicated line
				echo "Unexpected: duplicated CDR entries\n";
				return TRUE;
			}
			$lines[$l]=1;
		}
		if ($lines_found!=$info['count']) {
			// TODO:
			echo "Unexpected: lines found != lines previously found, WEIRD\n";
			exit();
		}
		
		if ($info['loaded']!=$lines_found) {
			echo "Updating [loaded]\n";
			$sql = "UPDATE ldr_files SET loaded = '$lines_found' WHERE id = '{$info['id']}'";
			$res = $billing->query($sql);
			if (DB::isError($res)) {
				echo "DB Error #1\n";
				return TRUE;
			}
		}
		
	}

// }}}
// {{{ check_file_fixes
//
//	Assumes file is locked and doesn't unlock the file
//

	function check_file_fixes (&$info) {
		global $billing;
		global $cdr_table;
		if ($info['loaded']>$info['lines']) {
			echo "Fixing $file_name (loaded>lines)\n";
			return fix_cdr_file ($file_name,$info);
		}
		
		if ($info['count']==$info['lines']) {
			echo "Fixing $file_name (count==lines != loaded)\n";
			return fix_cdr_file ($file_name,$info);
		}
		
		
	}

// }}}
// {{{ fetch_cdr_info

	function fetch_cdr_info ($file_name) {
		global $billing;
		global $cdr_table;
		
		$file_id = $billing->getone ("select id from ldr_files where filename = '".addslashes($file_name)."'");
		
		$cdr_cnt = $billing->getone ("select count(*) from $cdr_table where cdr_file_id = '$file_id'");
		
		if ($cdr_cnt==0) {
			$sql = "select id,filename,lines,load_first,loaded,0 as count,-1 as min_line, -1 as max_line from ldr_files WHERE id = '$file_id'";
		} else {
			$sql = "select id,filename,lines,load_first,loaded,count from ldr_files,"
			."(	select cdr_file_id,count(*) as count,min(cdr_file_line) as min_line, "
			."	max(cdr_file_line) as max_line from $cdr_table group by cdr_file_id"
			.") as summary"
			." where summary.cdr_file_id = ldr_files.id AND ldr_files.id = '$file_id';";
		}
		$info = $billing->getrow($sql);
		if (!is_array($info)) {
			echo __FILE__.":".__LINE__." ".__FUNCTION__."(): unexpected error\n";
			return FALSE;
		}
		return $info;
	}

// }}}
// {{{ clean_number

	// performs longest match with known prefixes removed

	function clean_number ($cdr) {
		$txt['call_party_after_src_calling_plan'] = $cdr['call_party_after_src_calling_plan'];
		$txt['called_party_on_src'] = $cdr['called_party_on_src'];
		$txt['called_party_on_dst'] = $cdr['called_party_on_dst'];
		
		$hold="";
		foreach ($txt as $i => $t) {
			$t = preg_replace ('/^.*#/','',$t);	// remove anything before #
			$t = preg_replace ('/^00/','',$t);
//			$txt[$i]=$t;
			if ($hold=="") {
				$hold=$t;
				continue;
			}
			$l1 = strlen($hold);
			$l2 = strlen($t);
			$new="";
			for ($i=0;$i<$l1 && $i < $l2;$i++) {
				$j=$i+1;
				if (substr($hold,-$j)==substr($t,-$j)) $new=substr($t,-$j);
			}
			$hold=$new;
		}
//		$hold = preg_replace ('/^.*#/','',$hold);
//		$hold = preg_replace ('/^00/','',$hold);
		$txt['result']=$hold;
		$old = $txt['clean_number'];
		if ($old!=$hold) {
#			echo "   [$old] => [$hold]   \n";
		}
		return $hold;

	}
// }}}
// {{{ load_cdr_file

	function load_cdr_file ($file_name,$force_careful = 0) {
		global $billing;
		global $cdr_table;
		global $cdr_field_map;
		global $cdr_field_map_empty;
		global $user_termination;
		
		while ($user_termination) return;

		if (!lock_file ($file_name)) {
			// TODO: process reason
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";
			return;
		}
		
		$line_counter = 0;
		$load_counter = 0;
		$careful = ($force_careful ? 1 : 0);
		
		$info = fetch_cdr_info ($file_name);
		if ($info===FALSE) {
			echo "Failed to fetch CDR info\n";
			unlock_file ($file_name);
			return;
		}
		
		if ($info['lines']==0) {
//			print_r($info);
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";
			// skipping empty file
			unlock_file ($file_name);
			return;
		}
		
		$r = check_file_fixes ($info);
		if ($r === TRUE) {
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";
			unlock_file($file_name);
			return;
		}

		// {{{ abort if already loaded
		
		if ($info['loaded']==$info['lines'] and $info['lines']==$info['count']) {
			// Correctly detecting that all is well, still this should not happen in reality
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";
			unlock_file ($file_name);
			return;
		}
		
		// }}}
		
		if ($info['load_first'] > 0) {
			$prio=" Requested";
			echo "Requested file: $file_name\n";
		} else {
			$prio="";
		}
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";
		
		if ($info['count']>0 || $info['loaded']>0) {
			$careful|=2;
		}
		
		$file_path = "/var/billing/cdr/$file_name"; // TODO: fix to automagically detect
		
		$f = fopen ($file_path,"r");
		if (!$f) {
			echo "Error opening file ($file_path)\n";
			unlock_file ($file_name);
			return ;
		}
		if ($careful) {
			echo "Being careful ($file_name)".($careful&2 ? "" : " forced")."\n";
			$sql = "SELECT cdr_file_line,callid,start_time_int FROM $cdr_table WHERE cdr_file_id = '{$info['id']}'";
			$cdrinfo = $billing->getAssoc($sql,TRUE,array(),DB_FETCHMODE_ASSOC);
//			print_r ($cdrinfo);
		}

		while (!$user_termination) {
			$line = trim(fgets($f));
			if ($line=='') break;
			$field = split (";",$line);
			$line_counter++;
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";

			unset($new_cdr);
			$new_cdr=Array();
			$new_cdr['cdr_file_id'] = $info['id'];
			$new_cdr['cdr_file_line'] = $line_counter;
			foreach ($cdr_field_map as $fno => $name) $new_cdr[$name]=$field[$fno-1];
			$new_cdr['clean_number'] = clean_number($new_cdr);
			
			
			if ($careful && isset($cdrinfo[$line_counter])) {
				
				if ($cdrinfo[$line_counter]['callid'] == $new_cdr['callid']) {
					continue;
				}
				// TODO:
				echo "Unexpected error ".__FILE__.":".__LINE__."\n";
				unlock_file ($file_name);
				return ;
			}
			
		// {{{ prepare the SQL statement
		
			$cols = "";
			$vals = "";
			$sep="";
			//foreach ($cdr_field_map as $fno => $name) {
			foreach ($new_cdr as $col => $val) {
				global $cdr_field_convert;
				$conv = $cdr_field_convert[$col];
				
				if ($val=='' && $conv == 'skipempty') continue;
				if ($val=='' && $conv == 'zeroempty') $val='0';

				$cols .= "$sep$col";
				$vals .= "$sep'".addslashes ($val)."' -- $col \n";
//			echo __FILE__.":".__LINE__."  ".__FUNCTION__."\n";
				$sep=",";
			}
			$sql = "INSERT INTO $cdr_table ($cols) VALUES ($vals)";
			
		// }}}
			
			$res = $billing->query($sql);
			if (DB::isError($res)) {
				echo "===\n$sql\n===\n".$res->getmessage()."\n===\n";
				if ($new_cdr['cdr_file_id']==21694 && (
				  $new_cdr['cdr_file_line'] == 6006 ||
				  $new_cdr['cdr_file_line'] == 6056 ) ) {

					$load_counter--;
				} else {
					unlock_file ($file_name);
					exit();
				}
			}
			echo "       \r$file_name ($line_counter) ".((int)(100*$line_counter/$info['lines']))."%";
//			echo $row['filename']."\n";
			$load_counter++;
			if ($careful || $load_counter==$info['lines'] || $user_termination) {
//				echo "\rUpdating load counter                      $load_counter {$info['id']}\n";
				echo " <counter> ";
				$sql="UPDATE ldr_files SET loaded=loaded+'$load_counter' WHERE id = '{$info['id']}'";
				$billing->query($sql);
				if (DB::isError($res)) {
					// TODO: consider, plan, reconsider, implement
					echo "Error updating loaded counter !!!\n=====\nSQL = ".$sql."\n".$res->getmessage()."\n====\n";
				} else {
					$load_counter=0;
				}
			}
		}
		if ($load_counter) {
			echo "{__FILE__}:{__LINE__} {__FUNCTIONS__}(): unexpected error\n";
		}
		echo "\r$file_name loaded.                                                           \n";
		fclose ($f);
		unlock_file ($file_name);
	}
	
// }}}
// {{{ locking	

	function ll_lock_file ($file_id,$from,$to) {
		global $billing;
		
		$sq_file_id = addslashes ($file_id);

		$sql = "UPDATE ldr_files SET locked = '$to' WHERE locked = '$from' AND filename = '$sq_file_id'";
		
		$res = $billing->query ($sql);
		if (DB::isError ($res)) {
			return -1;
		}
		if ($billing->affectedRows()==1) return 1;
		return 0;
	}
	function lock_file ($file_id) {
		$ret = ll_lock_file ($file_id,0,1);
		if ($ret == -1) {
			echo "Error locking file ($file_id)\n";
			return FALSE;
		}
		return ($ret==1);
	}
	function unlock_file ($file_id) {
		$ret = ll_lock_file ($file_id,1,0);
		if ($ret == -1) {
			echo "Error unlocking file ($file_id)\n";
			return FALSE;
		}
		return ($ret==1);
	}

// }}}
	$batch = Array();

	function batch_register ($name) {
		global $batch;
		
//$batch
	}
	
	function batch_unregister () {
		
	}

?>
