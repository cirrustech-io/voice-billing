<?php 

	include ('page_variable.php');
	include ("functions-inc.php");
	
	#Cust Info Page 
	$page_longtitle = 'Customer Details';
	$page_title =  $cust_info_page_title;
	
	session_start();

	if (!isset($_SESSION[$sess_cust_id]) || $_SESSION[$sess_cust_id] == "") {
		session_write_close(); 
		header("Location: {$login_page_title}.php");
		exit(0);
	}
	
	if ((isset($_POST["hdn_clear"])) && isset($_POST["btn_clearform"])) {
		if($_POST["hdn_clear"] == "yes2"){
			$clean = true;	
			clear_full_form();
			$_SESSION[$sess_service_order_id] = ''; 
			$_SESSION[$sess_ordercomplete] = ''; 
			
		} else {
			$goback=true;
		}
		
	}
	
	if ((isset($_POST["hdn_clear"]))  && isset($_POST["btn_clear"])) {
		if($_POST["hdn_clear"] == "yes1"){
			$clean = true;	
		} else {
			$goback=true;
		}
	}
	
	if((isset($_POST["hdn_posted"])) && ($_POST["hdn_posted"] == "yes") && isset($_POST["btn_default"])) {
		$default = true;	
	}
	
	
	if ((isset($_POST["hdn_posted"])) && ($_POST["hdn_posted"] == "yes") && isset($_POST["btn_save"])) {
		
			
		#Validate Mandatory
		if(validate_mandatory($page_title,$_POST)){
		
			$save_result=save_fields($page_title);					
			if ($save_result){
					$_SESSION[$sess_service_order_id] = $save_result; 
					#$error_message = "The Customer Details section of your order form has been saved successfully.";		
					header("Location: {$service_details_page_title}.php");
			} else {
					$goback = true;
					$error_message = "Save failed. Please try again.";
			}
		} else {
			$goback = true;
			$error_message = "Mandatory fields must be filled.";
		}
		session_write_close();
	
	} 
	
	if((isset($_POST["hdn_posted"])) && ($_POST["hdn_posted"] == "reload")) {
		$_SESSION['order_type'] = $_POST["cmb_ordertype"];
		
		switch($_SESSION['order_type']){
			case 1: $_SESSION[$sess_service_order_id] = '';
				break;
			default:
					if($_POST['cmb_orderlist'] == 'None'){
						$error_message = "No existing orders to modify";
					}
					$_SESSION[$sess_service_order_id] = ($_POST['cmb_orderlist'] <> 'None')?$_POST['cmb_orderlist']:'';
			
		}
		
		$_SESSION[$sess_ordercomplete] = '';
		
		
	} else {
		$_SESSION['order_type'] = '';
		
	}
	

?>
<?php 


$page_script ="
<script type='text/javascript'>


	function doOnBodyLoad(){
		countryIdx = document.getElementById('cmb_billcountry').selectedIndex;
		countryName = document.getElementById('cmb_billcountry').options[countryIdx].text;
		if(countryName == 'BAHRAIN'){
			document.getElementById('cmb_paymentmethod').options[1].disabled = false;
		} else {
			document.getElementById('cmb_paymentmethod').options[1].disabled = true;
		}
	}

	function verify(){
	}

	function loadDefault(){
	}
	
	function clearText(textobj){
		textobj.value = '';
	}

	function reloadCombo(selobj){
		selobj.selectedIndex = 0;
	}

	function cmb_orderlistChanged(){
		
		document.cust_info_form.submit();
		
	}
	
	
	function cmb_ordertypeChanged(){
		var order_type_idx = document.getElementById('cmb_ordertype').value;
		
		//if  not new order , then let the order list appear, else hide
		if(order_type_idx != 1){
			document.getElementById('div_title_cmb_orderlist').className='title';
			document.getElementById('div_cmb_orderlist').className='control';
		} else {
			document.getElementById('div_title_cmb_orderlist').className='hide';
			document.getElementById('div_cmb_orderlist').className='hide';
		}
		
		document.getElementById('hdn_posted').value = 'reload';
		
		document.cust_info_form.submit();
	}
	
	function cmb_accexecutiveChanged(){
	}
	
	function cmb_companycountryChanged(){

	}
	
	function cmb_billcountryChanged(){
		doOnBodyLoad();
		
	
	}
	
	function cmb_initcontracttermChanged(){
	}
	
	function cmb_billingmediaChanged(){
	}
	
	function cmb_billingcurrencyChanged(){
	}
	
	function cmb_paymentmethodChanged(){
	}
	
	function clearFields(){
		if(confirm('Are you sure you want to refresh the Customer Details section?')){
			document.getElementById('hdn_clear').value = 'yes1';
				
		} else {
			document.getElementById('hdn_clear').value = 'no';
		}
	}
	
	function clearOrder(){
		if(confirm('Are you sure you want to purge the contents of the entire order form? Your order details will be purged and you cannot revert this step if you proceed.')){
			document.getElementById('hdn_clear').value = 'yes2';
		} else {
			document.getElementById('hdn_clear').value = 'no';
		}
	
	
	}
	
	
	


</script>";


include("render-page.php"); 

?>
	
	
	
	







