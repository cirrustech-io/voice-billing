<?php

#page variables
$page_longtitle = "";
$page_title = ""; #Login,Cust_Info,Service_Details,Order_Summary
$page_content = "";
$page_script = "";
$site_name = "2Connect W.L.L. Service Order Form";
$error_message = "";
$control_blab = "";
$site_url = "";
$company_url = "http://www.2connectbahrain.com";

$proceed = true;
$clean = false;
$default = false;
$goback = false;
$page_blob = '';

require "datepicker/class.datepicker.php";
$dt_control = new datepicker();
$dt_control->firstDayOfWeek = 1;
$dt_control->dateFormat = "Y-m-d";


 #page_titles
 $login_page_title = "login";
 $instructions_page_title = "instructions";
 $cust_info_page_title = "cust_info";
 $service_details_page_title = "service_detail";
 $order_summary_page_title = "order_summary";
 $logout_page_title = "logout";
 $print_page_title ="print_order";
 
 #session variable names
 $sess_cust_id = 'cust_id';
 $sess_service_order_id = 'service_order_id';
 $sess_ordercomplete = 'order_complete';
 
#build form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");


?>