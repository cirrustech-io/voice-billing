<?php
	include('db_accessor.php');
	
	include("controls-inc.php");

	function authenticate_user($username,$password){
		$login_result = checkPassword($username,$password);
		
		if ($login_result){
			if(pg_num_rows($login_result) < 1){
				return false;
			}
			else{
				return pg_result($login_result,0,0); #custid from login result
			}
		}
		else {
			return false;
		}
	}
	
	function vomit_controls($page_name,&$page_script,&$dt_control,$clean,$default,$goback){
	
		$fields = load_fields($page_name,$clean,$default,$goback);
		#print_r($fields);
		
		$bunch = $GLOBALS['page_control']["{$page_name}"];
			
		$regular_types = array('text','password','select','textarea','label');
		$special_type1 = array('hidden','submit','button');
		$special_type2 = array('textarea');
		$special_type3 = array('span');
		
		$htags_bunch = array();
	
		#returns false in case of error or clean up
		if($fields === false)
			return $htags_bunch;
		
	
		foreach($bunch as $section=>$ctrls){
			$htags = array();
			foreach($ctrls as $ctrl) {
				#title,html-tag,type,id/name,size,text value/default-value,read-only(0/1),action 
				
				
				$title = $ctrl[0]; #title
				$tag = $ctrl[1]; #html-tag
				$type = $ctrl[2]; #tag-type
				$id = $ctrl[3]; #id/name
				$size = $ctrl[4]; #size
				$value = $ctrl[5]; #text-val/default-val
				$read_only = $ctrl[6]; #read-only
				$action = $ctrl[7]; #action 
				$class = $ctrl[8]; #class
				$populate = $ctrl[9]; #populate-field
				$mandatory = $ctrl[10]; #mandatory
				$visible = $ctrl[11]; #visible
				$tip = $ctrl[12]; #tip
				
				$htag = "";
			
				$htag .= "<div name='div_title_{$id}' id='div_title_{$id}' ";
				
				if(in_array($type,$special_type2)){
					$htag .= ($visible)?" class='title_large'>":" class='hide'>";
				}elseif(in_array($type,$special_type1)){
					$htag .= ($visible)?" class='title_clear'>":" class='hide'>"; 
				}elseif(in_array($type,$special_type3)){
					$htag .= ($visible)?" class='show'>":" class='hide'>"; 
				}else {
					$htag .= ($visible)?" class='title'>":" class='hide'>";  	
				}
		
		
				if(in_array($type,$special_type1) || in_array($type,$special_type3)){
					$htag .= "</div>";
				}else {
					$htag .= "{$title}</div>";	
				}
				
		
				$htag .= "<div name='div_{$id}' id='div_{$id}' ";
			
				if(in_array($type,$special_type1)){
					$htag .= ($visible)?" class='btn'>":" class='hide'>"; 	
				}elseif(in_array($type,$special_type2)){
					$htag .= ($visible)?" class='control_large'>":" class='hide'>"; 
				}elseif(in_array($type,$special_type3)){
					$htag .= ($visible)?" class='show'>":" class='hide'>";
				}else {
					$htag .= ($visible)?" class='control'>":" class='hide'>"; 		
				}
			
				
					
				$htag .= "<{$tag} ";
			
				switch($tag){
				
					case "input":
						$htag .= " type='{$type}'  name='{$id}' id='{$id}' maxlength='{$size}' ";
						switch($type){
							
							case "text":
								if($populate <> null){
									switch($populate){
														 #for date texts, action is used to store database field key
										case 'date': 	$htag .= (array_key_exists($action,$fields))? " value='" . date('Y-m-d',strtotime($fields[$action])) . "' " : " value='" . date('Y-m-d') . "'";
														break;
										default:
														$htag .= (array_key_exists($populate,$fields))? " value='" .  $fields[$populate] . "' " : " value='{$value}' "; 													
									}
									
								} else {
									$htag .= " value='{$value}'";
								}
								$htag .= ($read_only == 1)?" readonly ":"";
								break;
								
							case "password": 
								$htag .= " value='{$value}'";
								$htag .= ($read_only == 1)?" readonly ":"";
								break;
							case "hidden": 
								$htag .= ($populate <> null && array_key_exists($populate,$fields))? "value = '{$fields[$populate]}' " :"value= '{$value}'" ;						
								$htag .= ($read_only == 1)?" readonly ":"";
								#$htag .= ($visible)? " class='show' ":" class='hide' "; 
								break;
								
							case "submit":
								$htag .= " value='{$value}' class='blue-button' ";
								#$htag .= ($visible)?" class='btn' ":" class='hide' ";
								break;
							case "checkbox":
								$htag .= " value='{$value}' onChange='{$id}Changed()'  ";
								#$htag .= " value='{$value}'  ";
								if($populate <> null && array_key_exists($populate,$fields)){
									$htag .= ($fields[$populate])?" checked ": "";
								}
								break;
							
							
							
								
							default:
								break;
						}
						
						$htag .= " />";
						break;
					
					case "button":
						$htag .= " name='{$id}' id='{$id}' class='blue-button' onClick='{$action}();' ";
						#$htag .= ($visible)?" class='btn' ":" class='hide' ";
						$htag .= ">{$value}</button>";
						break;
					
					case "select":
						$chosen_item = ($type <> '' && array_key_exists($type,$fields))? $fields[$type]: ''; #for select, type is used to store database field key
						$options = "";
						$htag .= " name='{$id}' id='{$id}' onChange='{$id}Changed()'>";
						$htag .= $options = ($populate <> null)?fill_options($populate,$chosen_item,$type):""; 
						$htag .= "</select>";
						break;
						
					case "textarea":
						$htag .= " name='{$id}' id='{$id}' class='{$class}'cols=50 rows=3 >";
						$htag .= ($populate <> null && array_key_exists($populate,$fields))? "{$fields[$populate]}" :"{$value}" ;
						$htag .= "</textarea>";
						
						break;
						
					case "span":
						$htag .= " name='{$id}' id='{$id}'>";
						$htag .= ($populate <> null && array_key_exists($populate,$fields))? "{$fields[$populate]}" :"{$value}" ;
						$htag .= "</span>";
						
						break;
						
					case "label":
						$htag .= " name='{$id}' id='{$id}'>";
						$htag .= ($populate <> null && array_key_exists($populate,$fields))? "{$fields[$populate]}" :"{$value}" ;
						$htag .= "</label>";
						
						break;
				}
				
				
				#Add GUI for date
				if($populate == 'date'){
					$htag .= "<input type='button' class='blue-button' value='+' onClick=\"" . $dt_control->show("$id") . "\" >";
					
				}
				
				$htag .= ($mandatory == 1)?"<font color='red'>*</font>&nbsp;":"";
				
				
				
				$htag .= "</div>";
				$htag .= ($tip)?"<div class='tip'>$tip</div>":"";
				
				$htags["{$title}"] = $htag;	
			}
			$htags_bunch[$section] = $htags;
		}
		return $htags_bunch;
	}
	
	function draw_controls($htags_bunch,&$control_blab,&$error_message){
	
		if(count($htags_bunch) > 0){
			foreach($htags_bunch as $section=>$htags){
				
				$control_blab .= ($section <> 'Buttons' && substr_compare($section,'No_Title',0,strlen('No_Title')) <> 0 )?"<h3>{$section}</h3>":"<br>";
				
				foreach($htags as $title=>$htag) {		
					#$control_blab .= ($section == 'Buttons')?"{$htag} ":"{$htag} <br><br>\n";
					$control_blab .= "{$htag} <br><br>\n";
				}
				
				if($section == 'Buttons'){ $control_blab .= "<br><br>\n"; }
			}
		} else {
			$error_message = "Could not load controls";
		}
		
		return;
	}
	
	#type was introduced to take care of combos that dont directly have a master list to populate
	# eg Order List which is populated with customer's existing orders
	function fill_options($populate,$chosen_item,$type){
		
		$options = "";
		$alpha = false;
		if($populate == 'country'){ $alpha = true; } #order alphabetically if country list, else based on id
		
		switch($populate){
				case "yes_no":
					$options .= ($chosen_item == false)?"<option value=0 selected>No</option>" : "<option value=0>No</option>";
					$options .= ($chosen_item == true)? "<option value=1 selected>Yes</option>" : "<option value=1>Yes</option>";
					break;
				default:
					$combo_result = ($type <> 'special')? extractComboData($populate,$alpha): extractCustOrders($_SESSION['cust_id']);
					#$combo_result = extractComboData($populate);
					if(isset($combo_result) && pg_num_rows($combo_result) > 0){
						while($line = pg_fetch_array($combo_result, NULL, PGSQL_ASSOC)){
						
							$idx = intval($line['id']);
							$title = $line['title'];
							$default = intval($line['def']);
							
							$options .= "<option value={$idx} ";
							if($chosen_item){
								$options .= ($chosen_item == $idx)?" selected":"";
							} else {
								$options .= ($default)?" selected":"";
							}
							$options .= " >{$title}</option> ";
						}
			
					} else {
						$options .= "<option value='None' selected >None</option>";
					}
					break;
		}
		return $options;
	}
		
	function clean_session(){
		session_start();
		$_SESSION = array();
		if (isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time()-42000, '/');
		}
		session_destroy();
	}

	function load_fields($page_title,$clean,$default,$goback){
	
		
		 #page_titles
		 $instructions_page_title = "instructions";
		 $login_page_title = "login";
		 $cust_info_page_title = "cust_info";
		 $service_details_page_title = "service_detail";
		 $order_summary_page_title = "order_summary";
		 $print_page_title ="print_order";
		 $logout_page_title = "logout";
		
		if(!$goback){
			switch($page_title){
			
				case "{$instructions_page_title}":
												return load_instructions($clean,$default); 
												break;
			
				case "{$login_page_title}":
												return load_login($clean,$default); 
												break;
			
				case "{$cust_info_page_title}":
												return load_cust_info($clean,$default); 
												break;
				case "{$service_details_page_title}":
												return load_service_details($clean,$default); 
												break;
				case "{$order_summary_page_title}":
												
												return load_order_summary($clean,$default); 
												break;
				case "{$print_page_title}":
												
												return load_order_summary($clean,$default); 
												break;
				default: 						return false; 
			}
		} else {
			switch($page_title){
				
				case "{$cust_info_page_title}":
					return load_cust_info_goback(); 
					break;
				case "{$service_details_page_title}":
					return load_service_details_goback(); 
					break;
				default:
					return false;
			}
		}

	}
	
	function save_fields($page_title){
		#page_titles
		 $login_page_title = "login";
		 $cust_info_page_title = "cust_info";
		 $service_details_page_title = "service_detail";
		 $order_summary_page_title = "order_summary";
		 $logout_page_title = "logout";
	
	
		switch($page_title){
		
			case "{$cust_info_page_title}":
											return save_cust_info(); 
											break;
			case "{$service_details_page_title}":
											return save_service_details(); 
											break;
			
			default: 						return false; 
	
		}
	}

	function load_instructions($clean=false){
		$fields = array();

		#Refresh Required, return empty fields
		if($clean){ return $fields;}
		
		$instruction_data = "";
		$instruction_data .= "<b>WELCOME TO 2CONNECT SERVICE ORDER FORM</b><br /><br /><br />";
		$instruction_data .= "Please submit you order details in the following sequence:<br /><br />";
		$instruction_data .= "<b>1. Customer Details :</b> Standard customer details, billing and order information. <br /><br />";
		$instruction_data .= "<b>2. Service Details :</b> Product features being ordered and service specification. <br /><br />";
		$instruction_data .= "<b>3. Order Summary :</b> Complete summary of your service order, with a provision to print the order form. <br /><br /><br /><br />";
		
		$instruction_data .= "<u><b>Guidelines</b></u><br /><br />";
		$instruction_data .= "<ul><li>Fields marked with <font color='red'>*</font> are mandatory.</li><br />";
		$instruction_data .= "<li>You are adviced to save section details before you move to another part of the form, else changes made would not be retained.</li><br />";
		#$instruction_data .= "<li>Note that a successful save message after the completion of section indicates that you may proceed to the next step of your order.</li><br />";
		$instruction_data .= "<li>You will automatically be taken to the next step of your order after the current section has been submitted successfully.</li><br />";
		$instruction_data .= "<li>You may use the navigation menu to switch between sections should you want to review/modify your order details.</li><br />";
		$instruction_data .= "<li>Orders can be filled in part, and you may return later on provided you saved the section you are currently working on. Select 'Modification' option in the order type list to resume incomplete orders.</li><br />";
		$instruction_data .= "<li>Kindly print the order form once you have satisfactorily filled your entire order.</li><br />";
		$instruction_data .= "<li>Customer signature is required in the designated section of the service order form.</li></ul><br />";
	

		
		$fields['instructions'] = $instruction_data;
		
		return $fields;
	}
		
	function load_login($clean=false,$default=false) {
		$fields = array();
		return $fields;
	}
	
	function load_cust_info($clean=false,$default=false){
	
		$fields = array();
		
		#Refresh Required, return empty fields
		if($clean){ return $fields;}
		
		
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		
		$cust_result = array();
		$service_result = array();
		
		if(!empty($_SESSION[$sess_cust_id]) && !empty($_SESSION[$sess_service_order_id])){ #saved earlier
			$service_result = fetchServiceDetails($_SESSION[$sess_service_order_id]);
			if($service_result && pg_num_rows($service_result) > 0){
				
				#echo "Entered one now";
			
				$line = pg_fetch_array($service_result, NULL, PGSQL_ASSOC);	
				
				#do not attempt to refresh order type combo
				#$fields['order_id'] =  intval($line['order_id']);
				
				$fields['order_id'] =  ($_SESSION['order_type'] <> '')?$_SESSION['order_type']:intval($line['order_id']);
			
				switch($fields['order_id']){
					case 1: #New 
							$GLOBALS['page_control']['cust_info']['Order Details']['sp5'] = array('Existing Orders','select','special','cmb_orderlist',50,0,0,'','','order_list',0,0,null);
							break;
							
							#All other order types
					default:$GLOBALS['page_control']['cust_info']['Order Details']['sp5'] = array('Existing Orders','select','special','cmb_orderlist',50,0,0,'','','order_list',0,1,null);
							
					
				}
				
				$fields['special'] = $_SESSION[$sess_service_order_id];
				
				$fields['order_date'] =  $line['order_date'];
				$fields['required_date'] =  $line['required_date'];
				$fields['agreement_number'] =  $line['agreement_number'];
				$fields['agreement_date'] =  $line['agreement_date'];
				#$fields['account_executive_id'] =  intval($line['account_executive_id']);
				$fields['account_executive'] =  $line['account_executive_name'];
				
				$fields['p_o_number'] =  $line['p_o_number'];
				
				$fields['company_legal_name'] =  ($line['cust_company_legal_name']);
				$fields['building'] =  $line['building'];
				$fields['address'] = $line['address'];
				$fields['town_city'] = $line['town_city'];
				$fields['postcode'] =  $line['postcode'];
				$fields['province'] =  $line['province'];
				$fields['country_id'] =  intval($line['country_id']);
				$fields['contact_name'] =  $line['contact_name'];
				$fields['contact_title'] =  $line['contact_title'];
				$fields['contact_email'] = $line['contact_email'];
				$fields['contact_telephone'] =  $line['contact_telephone'];
				$fields['contact_mobile'] =  $line['contact_mobile'];
				$fields['contact_fax'] =  $line['contact_fax'];
				$fields['tech_contact_name'] = $line['tech_contact_name'];
				$fields['tech_contact_email'] =  $line['tech_contact_email'];
				$fields['tech_contact_telephone'] =  $line['tech_contact_telephone'];	
				
				$fields['bill_company_name'] = ($line['bill_company_name']);
				$fields['bill_building'] = $line['bill_building'];
				$fields['bill_address'] =  $line['bill_address'];
				$fields['bill_town_city'] = $line['bill_town_city'];
				$fields['bill_postcode'] = $line['bill_postcode'];
				$fields['bill_province'] = $line['bill_province'];
				$fields['bill_country_id'] = intval($line['bill_country_id']);
				$fields['bill_contact_name'] = $line['bill_contact_name'];
				$fields['bill_title'] = $line['bill_title'];
				$fields['bill_email'] = $line['bill_email'];
				$fields['bill_telephone'] =  $line['bill_telephone'];
				$fields['bill_contact_mobile'] =  $line['bill_contact_mobile'];
				$fields['bill_contact_fax'] =  $line['bill_contact_fax'];
				
				$fields['vat_number'] =  $line['vat_number'];
				$fields['initial_contract_term_id'] =  $line['initial_contract_term_id'];
				$fields['billing_media_id'] =  $line['billing_media_id'];
				$fields['billing_currency_id'] =  $line['billing_currency_id'];
				$fields['payment_method_id'] =  $line['payment_method_id'];
				#$fields['payment_frequency'] =  $line['payment_frequency'];
			} 
			
			$service_add_result = fetchServiceAddrDetails($_SESSION[$sess_service_order_id],true);
			if($service_result && pg_num_rows($service_add_result) > 0){
				$line = pg_fetch_array($service_add_result, NULL, PGSQL_ASSOC);
				$_SESSION[$sess_ordercomplete] = intval($line['id']);
			} else {
				$_SESSION[$sess_ordercomplete] = '';
			}
			
		
		} elseif (!empty($_SESSION[$sess_cust_id]) && $default == true){ 
			
			#load default data
			$cust_result = fetchCustDetails($_SESSION[$sess_cust_id]);
			if($cust_result && pg_num_rows($cust_result) > 0){
				
				$line = pg_fetch_array($cust_result, NULL, PGSQL_ASSOC);	
				$fields['bill_company_name'] = $fields['company_legal_name'] =  ($line['company_legal_name']);
				$fields['bill_building'] = $fields['building'] =  $line['building'];
				$fields['bill_address'] = $fields['address'] = $line['address'];
				$fields['bill_town_city'] =$fields['town_city'] = $line['town_city'];
				$fields['bill_postcode'] = $fields['postcode'] =  $line['postcode'];
				$fields['bill_province'] = $fields['province'] =  $line['province'];
				$fields['bill_country_id'] = $fields['country_id'] =  intval($line['country_id']);
				$fields['bill_contact_name'] =$fields['contact_name'] =  $line['contact_name'];
				$fields['bill_title'] =$fields['contact_title'] =  $line['contact_title'];
				$fields['bill_email'] =$fields['contact_email'] = $line['contact_email'];
				$fields['bill_telephone'] =$fields['contact_telephone'] =  $line['contact_telephone'];
				$fields['bill_contact_mobile'] =$fields['contact_mobile'] =  $line['contact_mobile'];
				$fields['bill_contact_fax'] =$fields['contact_fax'] =  $line['contact_fax'];
				$fields['tech_contact_name'] = $line['tech_contact_name'];
				$fields['tech_contact_email'] =  $line['tech_contact_email'];
				$fields['tech_contact_telephone'] =  $line['tech_contact_telephone'];		
				#print_r($fields); exit(0);
			} 
			
	
		} 
		return $fields;

	}
	
	function load_service_details($clean=false,$default=false){	
	
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		$fields = array();
	
		
		#Refresh Required, return empty fields
		if($clean){ return $fields;}
	
		$service_result = array();
		
		if(!empty($_SESSION[$sess_service_order_id])){ 
			
				$service_result = fetchServiceDetails($_SESSION[$sess_service_order_id]);	
				if($service_result && pg_num_rows($service_result) > 0){
					$line = pg_fetch_array($service_result, NULL, PGSQL_ASSOC);	
					
					$fields['topology_id'] = intval($line['topology_id']);					
					
				
					switch($fields['topology_id']){
						case 1:				
							$GLOBALS['page_control']['service_detail']['No_Title1']['sp1'] = array('Type of site','select','type_of_site_id','cmb_typesite',50,0,0,'','','site_type_detail',0,0,null);
							$GLOBALS['page_control']['service_detail']['No_Title1']['sp2'] = array('Site Status','select','site_status_id','cmb_sitestatus',50,0,0,'','','site_status',0,0,null);
							break;
						case 2:							
							$GLOBALS['page_control']['service_detail']['No_Title1']['sp1'] = array('Type of site','select','type_of_site_id','cmb_typesite',50,0,0,'','','site_type_detail',0,1,null);
							$GLOBALS['page_control']['service_detail']['No_Title1']['sp2'] = array('Site Status','select','site_status_id','cmb_sitestatus',50,0,0,'','','site_status',0,1,null);
							break;
						default:
							break;
					}
					
					$fields['type_of_site_id'] = intval($line['type_of_site_id']);
					$fields['site_status_id'] = intval($line['site_status_id']);
					$fields['multiport'] = ($line['multiport'] == 'f')?0:1;
					$fields['access_bundle'] = ($line['access_bundle']=='f')?0:1;
					$fields['cos'] = ($line['cos']=='f')?0:1;
					$fields['bandwidth_id'] = intval($line['bandwidth_id']);
					
					
					switch($fields['bandwidth_id']){
						case 10:				
							$GLOBALS['page_control']['service_detail']['No_Title1']['sp3'] = array('Bandwidth Details','input','text','txt_bdetail',100,'',0,'','','bandwidth_description',0,1,null);					
							break;
						default:							
							$GLOBALS['page_control']['service_detail']['No_Title1']['sp3'] = array('Bandwidth Details','input','text','txt_bdetail',100,'',0,'','','bandwidth_description',0,0,null);			
							break;
			
					}
					
					
					$fields['bandwidth_description'] = $line['bandwidth_description'];
					$fields['additional_information'] = $line['additional_information'];
				}
				
				if(!empty($_SESSION[$sess_ordercomplete]) ) {
					$service_addr_result = fetchServiceAddrDetails($_SESSION[$sess_ordercomplete]);	#read using service end address id
				} else {
					$service_addr_result = fetchServiceAddrDetails($_SESSION[$sess_service_order_id],true);	#read using service order id
				}
				if($service_addr_result && pg_num_rows($service_addr_result) > 0){
						$line = pg_fetch_array($service_addr_result, NULL, PGSQL_ASSOC);
						$_SESSION[$sess_ordercomplete] = $line['id'];
						$fields['a_company_name'] = $line['a_company_name'];
						$fields['a_address'] = $line['a_address'];
						$fields['a_road'] = $line['a_road'];
						$fields['a_building'] = $line['a_building'];
						$fields['a_area'] = $line['a_area'];
						$fields['a_block'] = $line['a_block'];
						$fields['a_country_id'] = intval($line['a_country_id']);
						$fields['a_pobox'] = $line['a_pobox'];
						$fields['a_province'] = $line['a_province'];
						$fields['a_towncity'] = $line['a_towncity'];						
						
							$fields['a_telehouse'] =($line['a_telehouse']=='f')?0:1;
							$fields['a_suite'] = $line['a_suite'];
							$fields['a_row'] = $line['a_row'];
							$fields['a_rack'] = $line['a_rack'];
							$fields['a_vert'] = $line['a_vert'];
						
						
						$fields['b_company_name'] = $line['b_company_name'];
						$fields['b_address'] = $line['b_address'];
						$fields['b_road'] = $line['b_road'];
						$fields['b_building'] = $line['b_building'];
						$fields['b_area'] = $line['b_area'];
						$fields['b_block'] = $line['b_block'];
						$fields['b_country_id'] = intval($line['b_country_id']);
						$fields['b_pobox'] = $line['b_pobox'];
						$fields['b_province'] = $line['b_province'];
						$fields['b_towncity'] = $line['b_towncity'];
						
							$fields['b_telehouse'] =($line['b_telehouse']=='f')?0:1;
							$fields['b_suite'] = $line['b_suite'];
							$fields['b_row'] = $line['b_row'];
							$fields['b_rack'] = $line['b_rack'];
							$fields['b_vert'] = $line['b_vert'];
												
				
				}
		
				return $fields;
			} else {
					return false;
			}
		
		
		
	}
	
	function load_order_summary($clean=false,$default=false){
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		$fields = array();

		#Refresh Required, return empty fields
		if($clean){ return $fields;}
		
		if(!empty($_SESSION[$sess_service_order_id])){ 
			
			$order_result = fetchOrderSummary($_SESSION[$sess_service_order_id]);
			
			if($order_result && pg_num_rows($order_result) > 0){
				
				$line = pg_fetch_array($order_result, NULL, PGSQL_ASSOC);
				
				$order_main = array(); $order_main_data = '';
				$cust_bill = array(); $cust_bill_data = '';
				$service = array(); $service_data = '';
				$service_address = array(); $service_address_data = '';
				$charges = array(); $charges_data = ''; 
				$additional_data = '';
				
				$order_main['order_number'] = $line['id'];
				$order_main['order_type'] = $line['order_type'];
				$order_main['order_date'] = date('Y-m-d',strtotime($line['order_date']));
				$order_main['required_date'] = date('Y-m-d',strtotime($line['required_date']));
				$order_main['agreement_date'] = date('Y-m-d',strtotime($line['agreement_date']));
				$order_main['agreement_number'] = $line['agreement_number'];
				$order_main['executive_name'] = $line['executive_name'];
				$order_main['p_o_number'] = $line['p_o_number'];
				$order_main_data = create_ordermain_summary($order_main);
				
				$cust_bill['company'] = $line['cust_company_legal_name'];
				$cust_bill['building'] = $line['building'];
				$cust_bill['address'] = $line['address'];
				$cust_bill['town_city'] = $line['town_city'];
				$cust_bill['postcode'] = $line['postcode'];
				$cust_bill['country_name'] = $line['country_name'];
				$cust_bill['province'] = $line['province'];
				$cust_bill['contact_name'] = $line['contact_name'];
				$cust_bill['contact_title'] = $line['contact_title'];
				$cust_bill['contact_email'] = $line['contact_email'];
				$cust_bill['contact_telephone'] = $line['contact_telephone'];
				$cust_bill['contact_mobile'] = $line['contact_mobile'];
				$cust_bill['contact_fax'] = $line['contact_fax'];
				$cust_bill['billing_media'] = $line['billing_media'];
				$cust_bill['contract_term'] = $line['init_contract_term'];
				$cust_bill['payment_frequency'] = $line['payment_frequency'];
				$cust_bill_data = create_custbill_summary($cust_bill);
					
				$charges['code'] = $line['code'];
				$charges['multiple'] = $line['multiple'];
				$charges['rate_installation'] = $line['rate_installation'];
				$charges['rate_monthly'] = $line['rate_monthly'];
				$charges_data = create_charges_summary($charges);
			
				$service['site_topology_name'] = $line['site_topology_name'];
				$service['type_of_site_name'] = $line['type_of_site_name'];
				$service['multiport'] = $line['multiport'];
				$service['access_bundle'] = $line['access_bundle'];
				$service['cos'] = $line['cos'];
				$service['bandwidth_name'] = $line['bandwidth_name'];
				$service['bandwidth_description'] = $line['bandwidth_description'];
				$service_data = create_service_summary($service);
				
				$service_address['a_company_name'] = $line['a_company_name'];
				$service_address['a_address'] = $line['a_address'];
				$service_address['a_road'] = $line['a_road'];
				$service_address['a_building'] = $line['a_building'];
				$service_address['a_area'] = $line['a_area'];
				$service_address['a_block'] = $line['a_block'];
				$service_address['a_province'] = $line['a_province'];
				$service_address['a_towncity'] = $line['a_towncity'];
				
				$service_address['a_country_name'] = $line['a_country_name'];
				$service_address['a_pobox'] = $line['a_pobox'];
				$service_address['a_telehouse'] = $line['a_telehouse'];
				$service_address['a_suite'] = $line['a_suite'];
				$service_address['a_row'] = $line['a_row'];
				$service_address['a_rack'] = $line['a_rack'];
				$service_address['a_vert'] = $line['a_vert'];
				
				$service_address['b_company_name'] = $line['b_company_name'];
				$service_address['b_address'] = $line['b_address'];
				$service_address['b_road'] = $line['b_road'];
				$service_address['b_building'] = $line['b_building'];
				$service_address['b_area'] = $line['b_area'];
				$service_address['b_block'] = $line['b_block'];
				$service_address['b_province'] = $line['b_province'];
				$service_address['b_towncity'] = $line['b_towncity'];
				$service_address['b_country_name'] = $line['b_country_name'];
				$service_address['b_pobox'] = $line['b_pobox'];
				$service_address['b_telehouse'] = $line['b_telehouse'];
				$service_address['b_suite'] = $line['b_suite'];
				$service_address['b_row'] = $line['b_row'];
				$service_address['b_rack'] = $line['b_rack'];
				$service_address['b_vert'] = $line['b_vert'];
				$service_address_data = create_service_address_summary($service_address);
				
				$additional_data = create_additional_summary($line['additional_information']);
				
				$fields['summary'] = $order_main_data . $cust_bill_data . $charges_data . $service_data . $service_address_data . $additional_data;
				$fields['service_order_no'] = intval($line['id']);
				
			} else {
				return false;
			}
		
		}
	
		return $fields;
	}

	function save_cust_info(){
		
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		$data = array();
		
		if((!empty($_SESSION[$sess_cust_id]))){
			$data['cust_id']= load_value($_SESSION,$sess_cust_id);
		}
		
		
		
		//read data from $_POST
		$data['order_id']= load_value($_POST,'cmb_ordertype');
		$data['order_date'] = date('Y-m-d',strtotime(load_value($_POST,'txt_orderdate')));
		$data['required_date'] = date('Y-m-d',strtotime(load_value($_POST,'txt_requireddate')));
		$data['agreement_no']= load_value($_POST,'txt_agreementnumber');
		$data['agreement_date'] = date('Y-m-d',strtotime(load_value($_POST,'txt_agreementdate')));
		$data['acc_executive']= load_value($_POST,'txt_accexecutive');
		$data['ponumber']= load_value($_POST,'txt_ponumber');
		
		$data['co_legal_name']= load_value($_POST,'txt_companylegalname');
		$data['co_address']= load_value($_POST,'txt_companyaddress');
		$data['co_building']= load_value($_POST,'txt_companybuilding');
		$data['co_towncity']= load_value($_POST,'txt_companytowncity');
		$data['co_province']= load_value($_POST,'txt_companyprovince');
		$data['co_postcode']= load_value($_POST,'txt_companypostcode');
		$data['co_country_id']= load_value($_POST,'cmb_companycountry');
		
		$data['contact_name']= load_value($_POST,'txt_contactname');
		$data['contact_title']= load_value($_POST,'txt_contacttitle');
		$data['contact_email']= load_value($_POST,'txt_contactemail');
		$data['contact_telephone']= load_value($_POST,'txt_contacttelephone');
		$data['contact_mobile']= load_value($_POST,'txt_contactmobile');
		$data['contact_fax']= load_value($_POST,'txt_contactfax');
		
		$data['tech_contact_name']= load_value($_POST,'txt_techcontact');
		$data['tech_contact_email']= load_value($_POST,'txt_techemail');
		$data['tech_contact_telephone']= load_value($_POST,'txt_techcontacttelephone');
	
		$data['bill_co_name']= load_value($_POST,'txt_billcompanyname');
		$data['bill_address']= load_value($_POST,'txt_billaddress');
		$data['bill_building']= load_value($_POST,'txt_billbuilding');
		$data['bill_towncity']= load_value($_POST,'txt_billtowncity');
		$data['bill_province']= load_value($_POST,'txt_billprovince');
		$data['bill_postcode']= load_value($_POST,'txt_billpostcode');
		$data['bill_country_id']= load_value($_POST,'cmb_billcountry');
		
		$data['bill_contact_name']= load_value($_POST,'txt_billcontactname');
		$data['bill_contact_title']= load_value($_POST,'txt_billcontacttitle');
		$data['bill_contact_email']= load_value($_POST,'txt_billcontactemail');
		$data['bill_contact_telephone']= load_value($_POST,'txt_billcontacttel');
		$data['bill_contact_mobile']= load_value($_POST,'txt_billcontactmobile');
		$data['bill_contact_fax']= load_value($_POST,'txt_billcontactfax');
		
		$data['bill_vatno']= load_value($_POST,'txt_billvatnumber');
		$data['billing_cycle']= load_value($_POST,'txt_billingcycle');
		$data['init_contract_term_id']= load_value($_POST,'cmb_initcontractterm');
		$data['billing_media_id']= load_value($_POST,'cmb_billingmedia');
		$data['billing_currency_id']= load_value($_POST,'cmb_billingcurrency');
		$data['payment_method_id']= load_value($_POST,'cmb_paymentmethod');
		
		#print_r($data);
		#exit(0);
		if((!empty($_SESSION[$sess_service_order_id]))){
			$doit = "UPDATE";
			if($_POST['cmb_orderlist'] <> 'None'){
				return saveCustDetails($data,$doit,$_SESSION[$sess_service_order_id]);
			} else {
				return false;
			}
		} else {
			$doit = "INSERT";
			return saveCustDetails($data,$doit,null);
		}
			

	}
	
	function save_service_details(){
	
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		$data = array();
		
		if((!empty($_SESSION[$sess_service_order_id]))){
			$data['service_order_id']= load_value($_SESSION,$sess_service_order_id);
		}	
		
		$data['topology_id']= load_value($_POST,'cmb_topology');
		$data['type_of_site_id']= load_value($_POST,'cmb_typesite');
		$data['site_status_id']= load_value($_POST,'cmb_sitestatus');
		$data['multiport']= load_value($_POST,'cmb_multiport');
		$data['access_bundle']= load_value($_POST,'cmb_accessbundle');
		$data['cos']= load_value($_POST,'cmb_cos');
		$data['bandwidth_id']= load_value($_POST,'cmb_bandwidth');
		$data['additional_information']= load_value($_POST,'txt_additionaldetails');
		$data['bandwidth_description']= load_value($_POST,'txt_bdetail');
		
		$data['a_company_name']= load_value($_POST,'txt_acompany');
		$data['a_address']= load_value($_POST,'txt_aaddress');
		$data['a_building'] = load_value($_POST,'txt_abuilding');
		$data['a_road']= load_value($_POST,'txt_aroad');
		$data['a_area']= load_value($_POST,'txt_aarea');
		$data['a_block']= load_value($_POST,'txt_ablock');
		$data['a_province']= load_value($_POST,'txt_aprovince');
		$data['a_towncity']= load_value($_POST,'txt_atowncity');
		$data['a_country_id']= load_value($_POST,'cmb_acountry');
		$data['a_pobox']= load_value($_POST,'txt_apobox');
		$data['a_telehouse']= load_value($_POST,'chk_atelehouse');
		$data['a_suite']= load_value($_POST,'txt_asuite');
		$data['a_row']= load_value($_POST,'txt_arow');
		$data['a_rack']= load_value($_POST,'txt_arack');
		$data['a_vert']= load_value($_POST,'txt_avert');
		
		$data['b_company_name']= load_value($_POST,'txt_bcompany');
		$data['b_address']= load_value($_POST,'txt_baddress');
		$data['b_building'] = load_value($_POST,'txt_bbuilding');
		$data['b_road']= load_value($_POST,'txt_broad');
		$data['b_area']= load_value($_POST,'txt_barea');
		$data['b_block']= load_value($_POST,'txt_bblock');
		$data['b_province']= load_value($_POST,'txt_bprovince');
		$data['b_towncity']= load_value($_POST,'txt_btowncity');
		$data['b_country_id']= load_value($_POST,'cmb_bcountry');
		$data['b_pobox']= load_value($_POST,'txt_bpobox');
		$data['b_telehouse']= load_value($_POST,'chk_btelehouse');
		$data['b_suite']= load_value($_POST,'txt_bsuite');
		$data['b_row']= load_value($_POST,'txt_brow');
		$data['b_rack']= load_value($_POST,'txt_brack');
		$data['b_vert']= load_value($_POST,'txt_bvert');
			
		return saveServiceDetails($data);	
	}
	
	function load_value($source,$key){
		if (array_key_exists($key,$source)){
			#echo "$source[$key]<br>";	
			return $source[$key];
		} else {
			return 0;
		}
	}
	
	function create_ordermain_summary($order_main){
		$data = "";
		if(count($order_main) > 0){
			$data .= fix_header('Order Details');
			$data .= add_newline(1);
			$data .= fix_title('Order No.:');
			$data .= fix_data($order_main['order_number']);
			$data .= add_newline(1);
			$data .= fix_title('Type:');
			$data .= fix_data($order_main['order_type']);
			$data .= add_newline(1);
			$data .= fix_title('Order Date:');
			$data .= fix_data($order_main['order_date']);
			$data .= add_newline(1);
			$data .= fix_title('Required Date:');
			$data .= fix_data($order_main['required_date']);
			$data .= add_newline(1);
			$data .= fix_title('Agreement Date:');
			$data .= fix_data($order_main['agreement_date']);
			$data .= add_newline(1);
			$data .= fix_title('Agreement No.:');
			$data .= fix_data($order_main['agreement_number']);
			$data .= add_newline(1);
			$data .= fix_title('2Connect Account Executive:');
			$data .= fix_data($order_main['executive_name']);
			$data .= add_newline(1);
			$data .= fix_title('Purchase Order No.:');
			$data .= fix_data($order_main['p_o_number']);
			$data .= add_newline(3);	
		}		
		return $data;
		

		
	}
	
	function create_custbill_summary($cust_bill){
		$data = "";
		
		if(count($cust_bill) > 0){
			$data .= fix_header('Customer Details');
			$data .= add_newline(1);
			$data .= fix_title('Company:');
			$data .= fix_data($cust_bill['company']);
			$data .= add_newline(1);
			$data .= fix_title('Building:');
			$data .= fix_data($cust_bill['building']);
			$data .= add_newline(1);
			$data .= fix_title('Address:');
			$data .= fix_data($cust_bill['address']);
			$data .= add_newline(1);
			$data .= fix_title('Town/City:');
			$data .= fix_data($cust_bill['town_city']);
			$data .= add_newline(1);
			$data .= fix_title('Province:');
			$data .= fix_data($cust_bill['province']);
			$data .= add_newline(1);
			$data .= fix_title('Post Code:');
			$data .= fix_data($cust_bill['postcode']);
			$data .= add_newline(1);
			$data .= fix_title('Country:');
			$data .= fix_data($cust_bill['country_name']);
			$data .= add_newline(2);
			$data .= fix_title('Contact Name:');
			$data .= fix_data($cust_bill['contact_name']);
			$data .= add_newline(1);
			$data .= fix_title('Title:');
			$data .= fix_data($cust_bill['contact_title']);
			$data .= add_newline(1);
			$data .= fix_title('Email:');
			$data .= fix_data($cust_bill['contact_email']);
			$data .= add_newline(2);
			$data .= fix_title('Telephone:');
			$data .= fix_data($cust_bill['contact_telephone']);
			$data .= add_newline(1);
			$data .= fix_title('Mobile');
			$data .= fix_data($cust_bill['contact_mobile']);
			$data .= add_newline(1);
			$data .= fix_title('Fax:');
			$data .= fix_data($cust_bill['contact_fax']);
			$data .= add_newline(3);
			$data .= fix_header('Billing Details');
			$data .= add_newline(1);
			$data .= fix_title('Payment:');
			$data .= fix_data($cust_bill['payment_frequency']);
			$data .= add_newline(1);
			$data .= fix_title('Media:');
			$data .= fix_data($cust_bill['billing_media']);
			$data .= add_newline(1);
			$data .= fix_title('Contract Term:');
			$data .= fix_data($cust_bill['contract_term']);
			$data .= add_newline(3);
			#echo $data;
			#exit(0);
		}
		
		return $data;
		
	}
	
	function create_charges_summary($charges){
		$data = "";
		
		if(count($charges) > 0){
			$rate_install = floatval($charges['rate_installation']) * floatval($charges['multiple']);
			$rate_monthly = floatval($charges['rate_monthly']) * floatval($charges['multiple']);
			$currency_code = $charges['code'] ;
			
			$data .= fix_header('Pricing Details');
			$data .= add_newline(1);
			/*
			if($rate_monthly <> 0 && $rate_install <> 0 ){
				$data .= fix_title('Installation:');
				$data .= fix_data($currency_code . add_space(2) . $rate_install);
				$data .= add_newline(1);
				$data .= fix_title('Monthly:');
				$data .= fix_data($currency_code . add_space(2) .$rate_monthly);
				$data .= add_newline(3);
			} else {
				$data .= fix_data('Subject to Service Bandwidth');
				$data .= add_newline(3);
			}
			*/
			
			#Temp until pricing has been decided
			$data .= fix_title('Installation & Annual Charges:');
			$data .= fix_data('Subject to 2Connect Confirmation');
			$data .= add_newline(3);
		}
		
		return $data;
		
		
			
			
	}
	
	function create_service_summary($service){
		$data = "";
		
		if(count($service) > 0){
			
			
			
			$multiport 				= ($service['multiport'] == 'f')?'NO':'YES';
			$accessbundle 			= ($service['access_bundle'] == 'f')?'NO':'YES';
			$cos 					= ($service['cos'] == 'f')?'NO':'YES';
			
			$data .= fix_header('Service Details');
			$data .= add_newline(1);
			$data .= fix_title('Topology:');
			$data .= fix_data($service['site_topology_name']);
			$data .= add_newline(1);
			$data .= fix_title('Type Of Site:');
			$data .= fix_data($service['type_of_site_name']);
			$data .= add_newline(1);
			$data .= fix_title('Multiport Access:');
			$data .= fix_data($multiport);
			$data .= add_newline(1);
			$data .= fix_title('Internet Access Bundle:');
			$data .= fix_data($accessbundle);
			$data .= add_newline(1);
			$data .= fix_title('CoS Active on Service:');
			$data .= fix_data($cos);
			$data .= add_newline(1);
			$data .= fix_title('Bandwidth:');
			$data .= fix_data($service['bandwidth_name']);
			
			
			if(strtoupper($service['bandwidth_name']) == 'OTHER'){
				$data .= add_newline(1);
				$data .= fix_title('Bandwidth Details:');
				$data .= fix_data($service['bandwidth_description']);
				$data .= add_newline(3);
			} else {
				$data .= add_newline(3);
			}
		
		}
		return $data;
	}
		
	function create_service_address_summary($service_address){
		$data = "";
		
		if(count($service_address) > 0){
		
				#print_r($service_address);
				$is_a_telehouse = ($service_address['a_telehouse'] == 'f')?'NO':'YES';
				$is_b_telehouse = ($service_address['b_telehouse'] == 'f')?'NO':'YES';
		
				$data .= fix_header('A End Address');
				$data .= add_newline(1);
				$data .= fix_title('Company Name:');
				$data .= fix_data($service_address['a_company_name']);
				$data .= add_newline(1);
				$data .= fix_title('Address:');
				$data .= fix_data($service_address['a_address']);
				$data .= add_newline(1);
				$data .= fix_title('Building:');
				$data .= fix_data($service_address['a_building']);
				$data .= add_newline(1);
				$data .= fix_title('Road | Area | Block:');
				
				$service_address['a_road'] = ($service_address['a_road'] == '')?'NA':$service_address['a_road'];
				$service_address['a_area'] = ($service_address['a_area'] == '')?'NA':$service_address['a_area'];
				$service_address['a_block'] = ($service_address['a_block'] == '')?'NA':$service_address['a_block'];
				
				$data .= fix_data("{$service_address['a_road']} | {$service_address['a_area']} | {$service_address['a_block']}");
				$data .= add_newline(1);
				$data .= fix_title('Zip/Postal Code:');
				$data .= fix_data($service_address['a_pobox']);
				$data .= add_newline(1);
				$data .= fix_title('Town/City:');
				$data .= fix_data($service_address['a_towncity']);
				$data .= add_newline(1);
				$data .= fix_title('State/Province:');
				$data .= fix_data($service_address['a_province']);
				$data .= add_newline(1);
				$data .= fix_title('Country:');
				$data .= fix_data($service_address['a_country_name']);
				$data .= add_newline(1);
				
				if($is_a_telehouse == 'YES'){
					$service_address['a_suite'] = ($service_address['a_suite'] == '')?'NA':$service_address['a_suite'];
					$service_address['a_row'] = ($service_address['a_row'] == '')?'NA':$service_address['a_row'];
					$service_address['a_rack'] = ($service_address['a_rack'] == '')?'NA':$service_address['a_rack'];
					$service_address['a_vert'] = ($service_address['a_vert'] == '')?'NA':$service_address['a_vert'];
					$data .= fix_title('Suite | Row | Rack | Vert');
					$data .= fix_data("{$service_address['a_suite']} | {$service_address['a_row']} | {$service_address['a_rack']} | {$service_address['a_vert']}");
					$data .= add_newline(1);
				
				}
				
				
				$data .= add_newline(2);
				
				$data .= fix_header('B End Address');
				$data .= add_newline(1);
				$data .= fix_title('Company Name:');
				$data .= fix_data($service_address['b_company_name']);
				$data .= add_newline(1);
				$data .= fix_title('Address:');
				$data .= fix_data($service_address['b_address']);
				$data .= add_newline(1);
				$data .= fix_title('Building:');
				$data .= fix_data($service_address['b_building']);
				$data .= add_newline(1);
				$data .= fix_title('Road | Area | Block:');
				
				$service_address['b_road'] = ($service_address['b_road'] == '')?'NA':$service_address['b_road'];
				$service_address['b_area'] = ($service_address['b_area'] == '')?'NA':$service_address['b_area'];
				$service_address['b_block'] = ($service_address['b_block'] == '')?'NA':$service_address['b_block'];
				
				
				
				$data .= fix_data("{$service_address['b_road']} | {$service_address['b_area']} | {$service_address['b_block']}");
				$data .= add_newline(1);
				$data .= fix_title('Zip/Postal Code:');
				$data .= fix_data($service_address['b_pobox']);
				$data .= add_newline(1);
				$data .= fix_title('Town/City:');
				$data .= fix_data($service_address['b_towncity']);
				$data .= add_newline(1);
				$data .= fix_title('State/Province:');
				$data .= fix_data($service_address['b_province']);
				$data .= add_newline(1);
				$data .= fix_title('Country:');
				$data .= fix_data($service_address['b_country_name']);
				$data .= add_newline(1);
				
				if($is_b_telehouse == 'YES'){
					$service_address['b_suite'] = ($service_address['b_suite'] == '')?'NA':$service_address['b_suite'];
					$service_address['b_row'] = ($service_address['b_row'] == '')?'NA':$service_address['b_row'];
					$service_address['b_rack'] = ($service_address['b_rack'] == '')?'NA':$service_address['b_rack'];
					$service_address['b_vert'] = ($service_address['b_vert'] == '')?'NA':$service_address['b_vert'];
					$data .= fix_title('Suite | Row | Rack | Vert');
					$data .= fix_data("{$service_address['b_suite']} | {$service_address['b_row']} | {$service_address['b_rack']} | {$service_address['b_vert']}");
					$data .= add_newline(1);
				
				}
				
				
				$data .= add_newline(2);		
		}
		
		return $data;
		
	}

	function create_additional_summary($additional){
	
		$data = "";
		$data .= fix_header('Additional Information');
		$data .= fix_data($additional);
		$data .= add_newline(2);
		return $data;
	
	}

	function fix_header($str){
		#1. trim
		$str = trim($str);
		
		#2. make pretty
		$str = "<div class='print_text_header'><b>{$str}</b></div>";
		
		return $str;
		
	}
	
	function fix_title($str){
		#1. trim
		$str = trim($str);
		
		#2. make pretty
		$str = "<div class='print_text_title'>{$str}</div>";
		
		return $str;
		
	}
	
	function fix_data($str){
		#1. trim
		$str = trim($str);
		
		#Fix blanks with 'NA'
		$str = ($str == '')?'NA':$str;
		
		#2. make pretty
		$str = "<div class='print_text_data'>{$str}</div>";
		return $str;
		
	}
	
	function add_space($count){
	
		$spaces = '';
		while($count-- > 0){
			$spaces .= '&nbsp;';
		}
		return $spaces;
		
	}
	
	function add_newline($count){
		$newlines = '';
		while($count-- > 0){
			$newlines .= '<br>';
		}
		return $newlines;
	}
	
	function validate_mandatory($page_title,$form_fields){
		$bunch = $GLOBALS['page_control']["{$page_title}"];
		
		foreach($bunch as $section=>$ctrls){
			foreach($ctrls as $ctrl) {
				#title,html-tag,type,id/name,size,text value/default-value,read-only(0/1),action 
				
				$id = $ctrl[3]; #id/name
				$mandatory = $ctrl[10]; #mandatory
				if($mandatory && $form_fields[$id] == ''){
					return false;
				}
			}
		}
		return true;
		
		
	}
	
	function clear_full_form(){
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		if(!empty($_SESSION[$sess_service_order_id])){
			return wipeoutOrder($_SESSION[$sess_service_order_id]);
		} else {
			return false;
		}
		
	}
	
	function load_cust_info_goback(){
		$fields = array();
		
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
	
		$fields['order_id'] =  ($_SESSION['order_type'] <> '')?$_SESSION['order_type']:load_value($_POST,'cmb_ordertype');
		
		switch($fields['order_id']){
			case 1: #New 
					$GLOBALS['page_control']['cust_info']['Order Details']['sp5'] = array('Existing Orders','select','special','cmb_orderlist',50,0,0,'','','order_list',0,0,null);
					break;
					
					#All other order types
			default:$GLOBALS['page_control']['cust_info']['Order Details']['sp5'] = array('Existing Orders','select','special','cmb_orderlist',50,0,0,'','','order_list',0,1,null);
					
			
		}
		
		$fields['special'] = (array_key_exists($sess_service_order_id,$_SESSION))?$_SESSION[$sess_service_order_id]:'';
		
		$fields['order_date'] =  date('Y-m-d',strtotime(load_value($_POST,'txt_orderdate')));
		$fields['required_date'] = date('Y-m-d',strtotime(load_value($_POST,'txt_requireddate')));
		$fields['agreement_number'] =  load_value($_POST,'txt_agreementnumber');
		$fields['agreement_date'] =  date('Y-m-d',strtotime(load_value($_POST,'txt_agreementdate')));
		$fields['account_executive'] =  load_value($_POST,'txt_accexecutive');		
		$fields['p_o_number'] =  load_value($_POST,'txt_ponumber');
			
		
		$fields['company_legal_name'] =  load_value($_POST,'txt_companylegalname');
		$fields['building'] = load_value($_POST,'txt_companybuilding');
		$fields['address'] =  load_value($_POST,'txt_companyaddress');
		$fields['town_city'] = load_value($_POST,'txt_companytowncity');
		$fields['postcode'] =   load_value($_POST,'txt_companypostcode');
		$fields['province'] =  load_value($_POST,'txt_companyprovince');
		$fields['country_id'] =  load_value($_POST,'cmb_companycountry');
		
		$fields['contact_name'] = load_value($_POST,'txt_contactname');
		$fields['contact_title'] =  load_value($_POST,'txt_contacttitle');
		$fields['contact_email'] = load_value($_POST,'txt_contactemail');
		$fields['contact_telephone'] =  load_value($_POST,'txt_contacttelephone');
		$fields['contact_mobile'] =  load_value($_POST,'txt_contactmobile');
		$fields['contact_fax'] =  load_value($_POST,'txt_contactfax');
		$fields['tech_contact_name'] = load_value($_POST,'txt_techcontact');
		$fields['tech_contact_email'] =  load_value($_POST,'txt_techemail');
		$fields['tech_contact_telephone'] =  load_value($_POST,'txt_techcontacttelephone');
	
		$fields['bill_company_name'] = load_value($_POST,'txt_billcompanyname');
		$fields['bill_building'] =  load_value($_POST,'txt_billbuilding');
		$fields['bill_address'] = load_value($_POST,'txt_billaddress');
		$fields['bill_town_city'] = load_value($_POST,'txt_billtowncity');
		$fields['bill_postcode'] = load_value($_POST,'txt_billpostcode');
		$fields['bill_province'] =load_value($_POST,'txt_billprovince');
		$fields['bill_country_id'] = load_value($_POST,'cmb_billcountry');
		$fields['bill_contact_name'] = load_value($_POST,'txt_billcontactname');
		$fields['bill_title'] = load_value($_POST,'txt_billcontacttitle');
		$fields['bill_email'] = load_value($_POST,'txt_billcontactemail');
		$fields['bill_telephone'] =  load_value($_POST,'txt_billcontacttel');
		$fields['bill_contact_mobile'] =  load_value($_POST,'txt_billcontactmobile');
		$fields['bill_contact_fax'] =  load_value($_POST,'txt_billcontactfax');

		$fields['vat_number'] =  load_value($_POST,'txt_billvatnumber');
		$fields['initial_contract_term_id'] =  load_value($_POST,'cmb_initcontractterm');
		$fields['billing_media_id'] =  load_value($_POST,'cmb_billingmedia');
		$fields['billing_currency_id'] =  load_value($_POST,'cmb_billingcurrency');
		$fields['payment_method_id'] =  load_value($_POST,'cmb_paymentmethod');
		$fields['payment_frequency'] =  load_value($_POST,'txt_billingcycle');
		
		return $fields;
	}

	function load_service_details_goback(){
		$fields = array();
		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';
		
		
		$fields['topology_id'] = load_value($_POST,'cmb_topology');					
					
				
		switch($fields['topology_id']){
			case 1:				
				$GLOBALS['page_control']['service_detail']['No_Title1']['sp1'] = array('Type of site','select','type_of_site_id','cmb_typesite',50,0,0,'','','site_type_detail',0,0,null);
				$GLOBALS['page_control']['service_detail']['No_Title1']['sp2'] = array('Site Status','select','site_status_id','cmb_sitestatus',50,0,0,'','','site_status',0,0,null);
				break;
			case 2:							
				$GLOBALS['page_control']['service_detail']['No_Title1']['sp1'] = array('Type of site','select','type_of_site_id','cmb_typesite',50,0,0,'','','site_type_detail',0,1,null);
				$GLOBALS['page_control']['service_detail']['No_Title1']['sp2'] = array('Site Status','select','site_status_id','cmb_sitestatus',50,0,0,'','','site_status',0,1,null);
				break;
			default:
				break;
		}
		
		$fields['type_of_site_id'] = load_value($_POST,'cmb_typesite');
		$fields['site_status_id'] = load_value($_POST,'cmb_sitestatus');
		$fields['multiport'] = load_value($_POST,'cmb_multiport');
		$fields['access_bundle'] = load_value($_POST,'cmb_accessbundle');
		$fields['cos'] = load_value($_POST,'cmb_cos');
		$fields['bandwidth_id'] = load_value($_POST,'cmb_bandwidth');
		
		
		switch($fields['bandwidth_id']){
			case 10:				
				$GLOBALS['page_control']['service_detail']['No_Title1']['sp3'] = array('Bandwidth Details','input','text','txt_bdetail',100,'',0,'','','bandwidth_description',0,1,null);					
				break;
			default:							
				$GLOBALS['page_control']['service_detail']['No_Title1']['sp3'] = array('Bandwidth Details','input','text','txt_bdetail',100,'',0,'','','bandwidth_description',0,0,null);			
				break;

		}
		
		$fields['bandwidth_description'] = load_value($_POST,'txt_additionaldetails');
		$fields['additional_information'] = load_value($_POST,'txt_bdetail');
		
		$fields['a_company_name'] = load_value($_POST,'txt_acompany');
		$fields['a_address'] =load_value($_POST,'txt_aaddress');
		$fields['a_road'] = load_value($_POST,'txt_aroad');
		$fields['a_building'] = load_value($_POST,'txt_abuilding');
		$fields['a_area'] = load_value($_POST,'txt_aarea');
		$fields['a_block'] = load_value($_POST,'txt_ablock');
		$fields['a_country_id'] = load_value($_POST,'cmb_acountry');
		$fields['a_pobox'] = load_value($_POST,'txt_apobox');
		$fields['a_province'] = load_value($_POST,'txt_aprovince');
		$fields['a_towncity'] = load_value($_POST,'txt_atowncity');					
		
		$fields['a_telehouse'] =load_value($_POST,'chk_atelehouse');
		$fields['a_suite'] = load_value($_POST,'txt_asuite');
		$fields['a_row'] = load_value($_POST,'txt_arow');
		$fields['a_rack'] = load_value($_POST,'txt_arack');
		$fields['a_vert'] = load_value($_POST,'txt_avert');
				
		$fields['b_company_name'] = load_value($_POST,'txt_bcompany');
		$fields['b_address'] = load_value($_POST,'txt_baddress');
		$fields['b_road'] = load_value($_POST,'txt_broad');
		$fields['b_building'] = load_value($_POST,'txt_bbuilding');
		$fields['b_area'] = load_value($_POST,'txt_barea');
		$fields['b_block'] = load_value($_POST,'txt_bblock');
		$fields['b_country_id'] = load_value($_POST,'cmb_bcountry');
		$fields['b_pobox'] = load_value($_POST,'txt_bpobox');
		$fields['b_province'] = load_value($_POST,'txt_bprovince');
		$fields['b_towncity'] = load_value($_POST,'txt_btowncity');
		
		$fields['b_telehouse'] =load_value($_POST,'chk_btelehouse');
		$fields['b_suite'] = load_value($_POST,'txt_bsuite');
		$fields['b_row'] = load_value($_POST,'txt_brow');
		$fields['b_rack'] = load_value($_POST,'txt_brack');
		$fields['b_vert'] = load_value($_POST,'txt_bvert');
			
		return $fields;
	}
	
	?>