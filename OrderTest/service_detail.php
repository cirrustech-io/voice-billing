<?php 

	include('functions-inc.php');
	include('page_variable.php');
	
	#Service Page 
	$page_longtitle = 'Service Details';
	$page_title = $service_details_page_title;

	session_start();
	
	if (empty($_SESSION[$sess_cust_id])) {
		session_write_close(); 
		header("Location: {$login_page_title}.php");
	} elseif (empty($_SESSION[$sess_service_order_id])) {
		$proceed= false;
		$error_message = "Please submit Customer Details first.";
	}
	
	
	if ((isset($_POST["hdn_clear"])) && isset($_POST["btn_clearform"])) {
		if($_POST["hdn_clear"] == "yes2"){
			$clean = true;	
			clear_full_form();
			$_SESSION[$sess_service_order_id] = ''; 
			$_SESSION[$sess_ordercomplete] = ''; 
			header("Location: {$cust_info_page_title}.php");
		} else {
			$goback = true;
		}
	}
	
	if ((isset($_POST["hdn_clear"])) && isset($_POST["btn_clear"])) {
		if($_POST["hdn_clear"] == "yes1"){
			$clean = true;
		} else {
			$goback = true;
		}
		
	}
	
	if ((isset($_POST["hdn_posted"])) && ($_POST["hdn_posted"] == "yes") && isset($_POST["btn_save"])) {
				
		#Validate Mandatory
		if(validate_mandatory($page_title,$_POST)){
			$save_result=save_fields($page_title);
		
			if ($save_result){
					$_SESSION[$sess_ordercomplete] = $save_result; 
					#$error_message = "The Service Details section of your order form has been saved successfully.";
					header("Location: {$order_summary_page_title}.php");
			} else {
					$goback = true;
					$error_message = "Save failed. Please try again.";
			}
		} else {
			$goback = true;
			$error_message = "Mandatory fields must be filled.";
		}
		session_write_close();
	}
	
?>


<?php 
$page_script ="
<script type='text/javascript'>

	function verify(){
		var idx = document.getElementById('cmb_topology').selectedIndex;
		
		if(idx == 0){ 
			document.getElementById('div_title_cmb_typesite').className='hide';
			document.getElementById('div_cmb_typesite').className='hide';
			document.getElementById('div_title_cmb_sitestatus').className='hide';
			document.getElementById('div_cmb_sitestatus').className='hide';
		}

		if(idx == 1){ 
			document.getElementById('div_title_cmb_typesite').className='title';
			document.getElementById('div_cmb_typesite').className='control';
			document.getElementById('div_title_cmb_sitestatus').className='title';
			document.getElementById('div_cmb_sitestatus').className='control';
		}
	}
	
	function doOnBodyLoad(){
		chk_atelehouseChanged();
		chk_btelehouseChanged();
		
	}
		
	function chk_atelehouseChanged(){
		
		var valA = document.getElementById('chk_atelehouse').checked;
		
		if(valA){
			document.getElementById('chk_atelehouse').value = '1';
			document.getElementById('txt_asuite').disabled = false;
			document.getElementById('txt_arow').disabled = false;
			document.getElementById('txt_arack').disabled = false;
			document.getElementById('txt_avert').disabled = false;
			
		} else {
			document.getElementById('chk_atelehouse').value = '0';
			document.getElementById('txt_asuite').value = '';
			document.getElementById('txt_arow').value = '';
			document.getElementById('txt_arack').value = '';
			document.getElementById('txt_avert').value = '';
			
			document.getElementById('txt_asuite').disabled = true;
			document.getElementById('txt_arow').disabled = true;
			document.getElementById('txt_arack').disabled = true;
			document.getElementById('txt_avert').disabled = true;
		}
		
	}
	
	function chk_btelehouseChanged(){
		
		var valB = document.getElementById('chk_btelehouse').checked;
		
		if(valB){
			document.getElementById('chk_btelehouse').value = '1';
			document.getElementById('txt_bsuite').disabled = false;
			document.getElementById('txt_brow').disabled = false;
			document.getElementById('txt_brack').disabled = false;
			document.getElementById('txt_bvert').disabled = false;
		
		} else {
			document.getElementById('chk_btelehouse').value = '0';
			document.getElementById('txt_bsuite').value = '';
			document.getElementById('txt_brow').value = '';
			document.getElementById('txt_brack').value = '';
			document.getElementById('txt_bvert').value = '';
			
			document.getElementById('txt_bsuite').disabled = true;
			document.getElementById('txt_brow').disabled = true;
			document.getElementById('txt_brack').disabled = true;
			document.getElementById('txt_bvert').disabled = true;		
		}
		
	}
	
	
	function cmb_topologyChanged(){
		verify();
	}
	
	function cmb_bandwidthChanged(){
		var val = document.getElementById('cmb_bandwidth').value;
		
		document.getElementById('txt_bdetail').value = '';
		
		//value for Other is 12
		if(val == 12){ 
			document.getElementById('div_title_txt_bdetail').className='title';
			document.getElementById('div_txt_bdetail').className='control';
		} else {
			document.getElementById('div_title_txt_bdetail').className='hide';
			document.getElementById('div_txt_bdetail').className='hide';
		}
	
	}
	
	function cmb_typesiteChanged(){}
	
	function cmb_multiportChanged(){}
	
	function cmb_cosChanged(){}
	
	function cmb_sitestatusChanged(){}
	
	function cmb_accessbundleChanged() {}
	
	function cmb_acountryChanged() {}
	
	function cmb_bcountryChanged() {}
	
	
	function clearText(textobj){
	textobj.value = '';
	}

	function reloadCombo(selobj){
		selobj.selectedIndex = 0;
	}

	function clearFields(){
			
		if(confirm('Are you sure you want to refresh the Service Details section?')){
			document.getElementById('hdn_clear').value = 'yes1';
				
		} else {
			document.getElementById('hdn_clear').value = 'no';
		}
		
	}
	
	function clearOrder(){
		if(confirm('Are you sure you want to purge the contents of the entire order form? Your order details will be purged and you cannot revert this step if you proceed.')){
			document.getElementById('hdn_clear').value = 'yes2';
		} else {
			document.getElementById('hdn_clear').value = 'no';
		}
	
	
	}
	
	


</script>";

include("render-page.php"); 

?>



	

	
	
	
	







