<?php
	#DB Accessor
	
	include("db-inc.php");
	require_once('includes/functions.inc.php');

	function checkPassword($username,$password){
		
		connectDB();
		$sql = "SELECT id from customer_detail where user_name = '{$username}' and pwd = '{$password}'";
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());		
		return $result;
	}
	
	function extractComboData($table_name,$alpha=false){
		connectDB();
		if(!$alpha) {
						$sql = "SELECT id,title,def from {$table_name} order by id ASC";
		} else {
						$sql = "SELECT id,title,def from {$table_name} order by title ASC";
		}
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		return $result;
	}

	function extractCustOrders($cust_id){
		connectDB();
		$sql = "SELECT id,id as title,0 as def from service_order_detail where customer_id={$cust_id}";
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		return $result;
	
	}
	
	function fetchCustDetails($cust_id) {
		connectDB();
		$sql = "select * from customer_detail where id = $cust_id";
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		return $result;
	}
	
	function fetchServiceDetails($service_order_id){
		connectDB();
		$sql = "select * from service_order_detail where id = $service_order_id";
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		return $result;	
	}
	
	function fetchServiceAddrDetails($id,$use_service_order_id=false){
		connectDB();
		if(!$use_service_order_id){
			$sql = "select * from service_end_address where id = $id";
		} else {
			$sql = "select * from service_end_address where service_order_id = $id";
		}
		
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		return $result;	
	}
	
	function saveCustDetails($data,$doit,$sess_service_order_id){
		#print_r($data);
		#exit(0);
		connectDB();
		$serial_id = "";
		
		if(count($data) > 0){
			
			if($doit == 'INSERT'){ 
				#fetch next serial id for service_order_detail table
				$serialSQL = "SELECT nextval('service_order_detail_id_seq'::text) as id";
				$serialResult = pg_query($serialSQL)  or die ('Query failed: ' . pg_last_error());
				
				if(isset($serialResult) && pg_num_rows($serialResult) > 0){
					$line = pg_fetch_array($serialResult, NULL, PGSQL_ASSOC);
					$sess_service_order_id = $line['id'];
				}
							
			
				$sql = "INSERT INTO service_order_detail(id,
														order_id,
														order_date,
														required_date,
														agreement_number,
														agreement_date,
														account_executive_name,
														p_o_number,
														
														customer_id,
														cust_company_legal_name,
														building,
														address,
														town_city,
														postcode,
														country_id,
														province,
														contact_name,
														contact_title,
														contact_email,
														contact_telephone,
														contact_mobile,
														contact_fax,
														tech_contact_name,
														tech_contact_email,
														tech_contact_telephone,
														bill_company_name,
														bill_building,
														bill_address,
														bill_town_city,
														bill_postcode,
														bill_country_id,
														bill_province,
														bill_contact_name,
														bill_title,
														bill_email,
														bill_telephone,
														bill_contact_mobile,
														bill_contact_fax,
														vat_number,
														initial_contract_term_id,
														billing_media_id,
														billing_currency_id,
														payment_method_id,
														payment_frequency)
													values(" . GetSQLValueString($sess_service_order_id,'int') . ","
															 . GetSQLValueString($data['order_id'],'int') . ","
															 . GetSQLValueString($data['order_date'],'date') . ","
															 . GetSQLValueString($data['required_date'],'date') . ","
															 . GetSQLValueString($data['agreement_no'],'text') . ","
															 . GetSQLValueString($data['agreement_date'],'date') . ","
															 . GetSQLValueString($data['acc_executive'],'text') . ","
															 . GetSQLValueString($data['ponumber'],'text') . ","
															 
															 . GetSQLValueString($data['cust_id'],'int') . ","
															 . GetSQLValueString($data['co_legal_name'],'text') . ","
															 . GetSQLValueString($data['co_building'],'text') . ","
															 . GetSQLValueString($data['co_address'],'text') . ","
															 . GetSQLValueString($data['co_towncity'],'text') . ","														
															. GetSQLValueString($data['co_postcode'],'text') . ","
															. GetSQLValueString($data['co_country_id'],'int') . ","
															. GetSQLValueString($data['co_province'],'text') . ","
															. GetSQLValueString($data['contact_name'],'text') . ","
															. GetSQLValueString($data['contact_title'],'text') . ","
															. GetSQLValueString($data['contact_email'],'text') . ","
															. GetSQLValueString($data['contact_telephone'],'text') . ","
															. GetSQLValueString($data['contact_mobile'],'text') . ","
															. GetSQLValueString($data['contact_fax'],'text') . ","
															. GetSQLValueString($data['tech_contact_name'],'text') . ","
															. GetSQLValueString($data['tech_contact_email'],'text') . ","
															. GetSQLValueString($data['tech_contact_telephone'],'text') . ","
															. GetSQLValueString($data['bill_co_name'],'text') . ","
															. GetSQLValueString($data['bill_building'],'text') . ","
															. GetSQLValueString($data['bill_address'],'text') . ","
															. GetSQLValueString($data['bill_towncity'],'text') . ","													
															. GetSQLValueString($data['bill_postcode'],'text') . ","
															. GetSQLValueString($data['bill_country_id'],'int') . ","
															. GetSQLValueString($data['bill_province'],'text') . ","
															. GetSQLValueString($data['bill_contact_name'],'text') . ","
															. GetSQLValueString($data['bill_contact_title'],'text') . ","
															. GetSQLValueString($data['bill_contact_email'],'text') . ","
															. GetSQLValueString($data['bill_contact_telephone'],'text') . ","
															. GetSQLValueString($data['bill_contact_mobile'],'text') . ","
															. GetSQLValueString($data['bill_contact_fax'],'text') . ","
															. GetSQLValueString($data['bill_vatno'],'text') . ","
															. GetSQLValueString($data['init_contract_term_id'],'int') . ","
															. GetSQLValueString($data['billing_media_id'],'int') . ","
															. GetSQLValueString($data['billing_currency_id'],'int') . ","
															. GetSQLValueString($data['payment_method_id'],'int') . ","
															. GetSQLValueString($data['billing_cycle'],'text') 
															. ");";
															
					
			} elseif($doit == 'UPDATE'){
				$sql = "UPDATE service_order_detail set order_id =" . GetSQLValueString($data['order_id'],'int') . ",
														order_date =" . GetSQLValueString($data['order_date'],'date') . ",
														required_date =" . GetSQLValueString($data['required_date'],'date') . ",
														agreement_number =" . GetSQLValueString($data['agreement_no'],'text') . ",
														agreement_date =" .  GetSQLValueString($data['agreement_date'],'date') . ",
														account_executive_name =" . GetSQLValueString($data['acc_executive'],'text') . ",
														p_o_number =" . GetSQLValueString($data['ponumber'],'text') . ",
														
														customer_id =" . GetSQLValueString($data['cust_id'],'int') . ",
														cust_company_legal_name =" . GetSQLValueString($data['co_legal_name'],'text') . ",
														building =" . GetSQLValueString($data['co_building'],'text') . ",
														address =" . GetSQLValueString($data['co_address'],'text') . ",
														town_city =" . GetSQLValueString($data['co_towncity'],'text') . ",
														postcode =" . GetSQLValueString($data['co_postcode'],'text') . ",
														country_id =" . GetSQLValueString($data['co_country_id'],'int') . ",
														province =" . GetSQLValueString($data['co_province'],'text') . ",
														contact_name =" . GetSQLValueString($data['contact_name'],'text') . ",
														contact_title =" . GetSQLValueString($data['contact_title'],'text') . ",
														contact_email =" . GetSQLValueString($data['contact_email'],'text') . ",
														contact_telephone =" . GetSQLValueString($data['contact_telephone'],'text') . ",
														contact_mobile =" . GetSQLValueString($data['contact_mobile'],'text') . ",
														contact_fax =" .  GetSQLValueString($data['contact_fax'],'text') . ",
														tech_contact_name =" . GetSQLValueString($data['tech_contact_name'],'text') . ",
														tech_contact_email =" . GetSQLValueString($data['tech_contact_email'],'text') . ",
														tech_contact_telephone =" . GetSQLValueString($data['tech_contact_telephone'],'text') . ",
														bill_company_name =" . GetSQLValueString($data['bill_co_name'],'text') . ",
														bill_building =" . GetSQLValueString($data['bill_building'],'text') . ",
														bill_address =" . GetSQLValueString($data['bill_address'],'text') . ",
														bill_town_city =" . GetSQLValueString($data['bill_towncity'],'text') . ",
														bill_postcode =" . GetSQLValueString($data['bill_postcode'],'text') . ",
														bill_country_id =" . GetSQLValueString($data['bill_country_id'],'int') . ",
														bill_province =" . GetSQLValueString($data['bill_province'],'text') . ",
														bill_contact_name =" . GetSQLValueString($data['bill_contact_name'],'text') . ",
														bill_title =" . GetSQLValueString($data['bill_contact_title'],'text') . ",
														bill_email =" . GetSQLValueString($data['bill_contact_email'],'text') . ",
														bill_telephone =" . GetSQLValueString($data['bill_contact_telephone'],'text') . ",
														bill_contact_mobile =" . GetSQLValueString($data['bill_contact_mobile'],'text') . ",
														bill_contact_fax =" . GetSQLValueString($data['bill_contact_fax'],'text') . ",
														vat_number =" . GetSQLValueString($data['bill_vatno'],'text') . ",
														initial_contract_term_id =" . GetSQLValueString($data['init_contract_term_id'],'int') . ",
														billing_media_id =" . GetSQLValueString($data['billing_media_id'],'int') . ",
														billing_currency_id =" . GetSQLValueString($data['billing_currency_id'],'int') . ",
														payment_method_id =" .  GetSQLValueString($data['payment_method_id'],'int') . ",
														payment_frequency = " . GetSQLValueString($data['billing_cycle'],'text')
													. " where id =" . GetSQLValueString($sess_service_order_id,'int') . ";";
				
			}
			#echo $sql;
			#exit(0);
			$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			if($result){
				return $sess_service_order_id;
			}
		}
		return false;
	}
	
	function saveServiceDetails($data){
						
			connectDB();
			$serial_id = "";
			
			#exit false  if no data available
			if(count($data) < 1){
				return false;
			}
			
			$sql = "UPDATE service_order_detail SET "
					. " topology_id = " . GetSQLValueString($data['topology_id'],'int') . ","
					. " type_of_site_id = " . GetSQLValueString($data['type_of_site_id'],'int') . ","
					. " site_status_id = " . GetSQLValueString($data['site_status_id'],'int') . ","
					. " multiport = " . GetSQLValueString($data['multiport'],'text') . ","
					. " access_bundle = " . GetSQLValueString($data['access_bundle'],'text') . ","
					. " cos = " . GetSQLValueString($data['cos'],'text') . ","
					. " bandwidth_id = " . GetSQLValueString($data['bandwidth_id'],'int') . ","
					. " bandwidth_description = " . GetSQLValueString($data['bandwidth_description'],'text') . ","
					. " additional_information = " . GetSQLValueString($data['additional_information'],'text') 
				. " WHERE id = " . GetSQLValueString($data['service_order_id'],'int') . ";"; 
			
			
			$result1 = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			
			#exit false if service order detail could not be updated
			if(!$result1){
				return false;
			}
			
			#check if service end address has been inserted, if yes only update
			$sql = "SELECT id from service_end_address where service_order_id = " . GetSQLValueString($data['service_order_id'],'int') . ";";
			
			$result2 = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			
			#exit false if pg error
			if(!$result2){
				return false;
			}
			
			if(pg_num_rows($result2) > 0) { #update required
					$line = pg_fetch_array($result2, NULL, PGSQL_ASSOC);
					$serial_id = $line['id'];
					
					$sql = "UPDATE service_end_address SET "
							. " a_company_name = " . GetSQLValueString($data['a_company_name'],'text') . ","
							. " a_address = " . GetSQLValueString($data['a_address'],'text') . ","
							. " a_road = " . GetSQLValueString($data['a_road'],'text') . ","
							. " a_building = " . GetSQLValueString($data['a_building'],'text') . ","
							. " a_area = " . GetSQLValueString($data['a_area'],'text') . ","
							. " a_block = " . GetSQLValueString($data['a_block'],'text') . ","
							. " a_country_id = " . GetSQLValueString($data['a_country_id'],'int') . ","
							. " a_pobox = " . GetSQLValueString($data['a_pobox'],'text') . ","
							. " a_province = " . GetSQLValueString($data['a_province'],'text') . ","
							. " a_towncity = " . GetSQLValueString($data['a_towncity'],'text') . ","
							. " a_telehouse = " . GetSQLValueString($data['a_telehouse'],'text') . ","
							. " a_suite = " . GetSQLValueString($data['a_suite'],'text') . ","
							. " a_row = " . GetSQLValueString($data['a_row'],'text') . ","
							. " a_rack = " . GetSQLValueString($data['a_rack'],'text') . ","
							. " a_vert = " . GetSQLValueString($data['a_vert'],'text') . ","
							. " b_company_name = " . GetSQLValueString($data['b_company_name'],'text') . ","
							. " b_address = " . GetSQLValueString($data['b_address'],'text') . ","
							. " b_road = " . GetSQLValueString($data['b_road'],'text') . ","
							. " b_building = " . GetSQLValueString($data['b_building'],'text') . ","
							. " b_area = " . GetSQLValueString($data['b_area'],'text') . ","
							. " b_block = " . GetSQLValueString($data['b_block'],'text') . ","
							. " b_country_id = " . GetSQLValueString($data['b_country_id'],'int') . ","
							. " b_pobox = " . GetSQLValueString($data['b_pobox'],'text') . ","
							. " b_province = " . GetSQLValueString($data['b_province'],'text') . ","
							. " b_towncity = " . GetSQLValueString($data['b_towncity'],'text') . ","
							. " b_telehouse = " . GetSQLValueString($data['b_telehouse'],'text') . ","
							. " b_suite = " . GetSQLValueString($data['b_suite'],'text') . ","
							. " b_row = " . GetSQLValueString($data['b_row'],'text') . ","
							. " b_rack = " . GetSQLValueString($data['b_rack'],'text') . ","
							. " b_vert = " . GetSQLValueString($data['b_vert'],'text') 
						. " WHERE id = " . GetSQLValueString($serial_id,'int') . ";";
				
				
				
			} else { #insert required
				
					#fetch next serial id for service_order_detail table
					$serialSQL = "SELECT nextval('service_end_address_id_seq'::text) as id";
					$serialResult = pg_query($serialSQL)  or die ('Query failed: ' . pg_last_error());
					
					if(isset($serialResult) && pg_num_rows($serialResult) > 0){
						$line = pg_fetch_array($serialResult, NULL, PGSQL_ASSOC);
						$serial_id = $line['id'];
					} else {
						return false;
					}
					
					$sql = "INSERT INTO service_end_address(id,
															service_order_id,
															a_company_name,
															a_address,
															a_road,
															a_building,
															a_area,
															a_block,
															a_country_id,
															a_pobox,
															a_towncity,
															a_province,
															a_telehouse,
															a_suite,
															a_row,
															a_rack,
															a_vert,
															b_company_name,
															b_address,
															b_road,
															b_building,
															b_area,
															b_block,
															b_country_id,
															b_pobox,
															b_towncity,
															b_province,
															b_telehouse,
															b_suite,
															b_row,
															b_rack,
															b_vert) 
												values(" 	. GetSQLValueString($serial_id,'int') . ","
															. GetSQLValueString($data['service_order_id'],'int') . ","
															. GetSQLValueString($data['a_company_name'],'text') . ","
															. GetSQLValueString($data['a_address'],'text') . ","
															. GetSQLValueString($data['a_road'],'text') . ","
															. GetSQLValueString($data['a_building'],'text') . ","
															. GetSQLValueString($data['a_area'],'text') . ","
															. GetSQLValueString($data['a_block'],'text') . ","
															. GetSQLValueString($data['a_country_id'],'int') . ","
															. GetSQLValueString($data['a_towncity'],'text') . ","
															. GetSQLValueString($data['a_province'],'text') . ","
															. GetSQLValueString($data['a_pobox'],'text') . ","
															. GetSQLValueString($data['a_telehouse'],'text') . ","
															. GetSQLValueString($data['a_suite'],'text') . ","
															. GetSQLValueString($data['a_row'],'text') . ","
															. GetSQLValueString($data['a_rack'],'text') . ","
															. GetSQLValueString($data['a_vert'],'text') . ","
															. GetSQLValueString($data['b_company_name'],'text') . ","
															. GetSQLValueString($data['b_address'],'text') . ","
															. GetSQLValueString($data['b_road'],'text') . ","
															. GetSQLValueString($data['b_building'],'text') . ","
															. GetSQLValueString($data['b_area'],'text') . ","
															. GetSQLValueString($data['b_block'],'text') . ","
															. GetSQLValueString($data['b_country_id'],'int') . ","
															. GetSQLValueString($data['b_pobox'],'text') . ","
															. GetSQLValueString($data['b_towncity'],'text') . ","
															. GetSQLValueString($data['b_province'],'text') . ","
															. GetSQLValueString($data['b_telehouse'],'text') . ","
															. GetSQLValueString($data['b_suite'],'text') . ","
															. GetSQLValueString($data['b_row'],'text') . ","
															. GetSQLValueString($data['b_rack'],'text') . ","
															. GetSQLValueString($data['b_vert'],'text')
															. ");";
			}
				
			
			$result3 = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			
			#exit false if insert or update failed
			if(!$result3){
				return false;
			}
			
			return $serial_id;
			
	}
	
	function fetchBandwidthPricingDetails(){
		connectDB();
		$sql = "select * from bandwidth_pricing";
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		return $result;
	}
	
	function fetchOrderSummary($service_order_id){
	
			connectDB();
			$sql = 	"select s.id,
					ot.title as order_type,
					s.order_date,
					s.required_date,
					s.agreement_date,
					s.agreement_number,
					s.account_executive_name as executive_name,
					s.p_o_number,
					s.cust_company_legal_name,
					s.building,
					s.address,
					s.town_city,
					s.postcode,
					c.title as country_name,
					s.province,
					s.contact_name,
					s.contact_title,
					s.contact_email,
					s.contact_telephone,
					s.contact_mobile,
					s.contact_fax,
					bm.title as billing_media,
					ct.title as init_contract_term,
					s.payment_frequency,
					bc.code,
					bc.multiple,
					st.title as site_topology_name,
					ts.title as type_of_site_name,
					s.multiport,
					s.access_bundle,
					s.cos,
					b.title as bandwidth_name,
					s.bandwidth_description,
					s.additional_information,
					bp.rate_installation,
					bp.rate_monthly,
					sea.a_company_name,
					sea.a_address,
					sea.a_road,
					sea.a_building,
					sea.a_area,
					sea.a_block,
					c1.title as a_country_name,
					sea.a_pobox,
					sea.a_province,
					sea.a_towncity,
					sea.a_telehouse,
					sea.a_suite,
					sea.a_row,
					sea.a_rack,
					sea.a_vert,
					sea.b_company_name,
					sea.b_address,
					sea.b_road,
					sea.b_building,
					sea.b_area,
					sea.b_block,
					c2.title as b_country_name,
					sea.b_pobox,
					sea.b_province,
					sea.b_towncity,
					sea.b_telehouse,
					sea.b_suite,
					sea.b_row,
					sea.b_rack,
					sea.b_vert

			from 	service_order_detail s,
					order_type ot,
					country c,
					billing_media bm,
					service_topology st,
					site_type_detail ts,
					bandwidth_detail b,
					bandwidth_pricing bp,
					billing_currency bc,
					contract_term ct,
					service_end_address sea,
					country c1,
			        country c2

 
			where		c.id = s.country_id
				and 	ot.id = s.order_id
				and		bm.id = s.billing_media_id
				and		st.id = s.topology_id
				and     b.id = s.bandwidth_id
				and 	bp.bandwidth_id = b.id
				and 	bc.id = s.billing_currency_id
				and     ts.id = s.type_of_site_id
				and 	ct.id = s.initial_contract_term_id
				and 	sea.service_order_id = s.id
				and 	c1.id = sea.a_country_id
				and 	c2.id = sea.b_country_id
				and 	s.id = " .  GetSQLValueString($service_order_id,'int') 
			. ";";
			
			$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			return $result;
	}

	function wipeoutOrder($service_order_id){
		connectDB();
		
		$sql = "DELETE FROM service_end_address where service_order_id = {$service_order_id}";
		
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		
		$sql = "DELETE FROM service_order_detail where id = {$service_order_id}";
		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		
		return true;
		
		
		
	}
	?>