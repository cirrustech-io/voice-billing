<?
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "XHTML1-s.dtd">';
?>
<html xmlns="http://www.w3.org/TR/1999/REC-html-in-xml"
	  xml:lang="en" lang="en">
<head>
	  <title><?php echo "{$site_name} | " . strtoupper($page_title); ?></title>
	  
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF8" /><!-- UTF-8 -->
	 
	  <link rel="stylesheet" href="assets/templates/2connect/2c_general-pages.css" type="text/css" media="screen" />
	  <link rel="stylesheet" href="assets/templates/2connect/2connect-common.css" type="text/css" media="screen" />
	  <link rel="stylesheet" href="assets/templates/2connect/print.css" type="text/css" media="print" />

	  <!--[if IE 6]>
	  <link rel="stylesheet" href="assets/templates/2connect/2c_general-pages_ie.css" type="text/css" media="screen" />
	  <link rel="stylesheet" href="assets/templates/2connect/2c_common_ie.css" type="text/css" media="screen" />
	  <![endif]-->
  
	  <!--[if lt IE 7]>
		<script type="text/javascript" src="assets/js/select-option-disabled-emulation.js"></script>
	  <![endif]-->

	
	
</head>
<body onLoad='doOnBodyLoad();'>
<div id="wrapper">
	<div id="layout_header_strip">
		<div id="layout_header_bar_left">&nbsp;</div>
		<div id="layout_header_bar_right">&nbsp;</div>
	</div>
	<div id="minHeight"></div>
	<div id="outer">
		<div id="inner">
			<div id="left">
				<div id="left-inner">
					<div id="content">
						<div class="post">
							<?php if(isset($_SESSION[$sess_cust_id])) {include("menu.php");} ?>
							<h2><?php echo "{$page_longtitle}"; ?></h2>
							 <div id='error-text'><?php echo "{$error_message}"; ?> </div>
							 <?php 
								if (1) { # Copyright (C) 2009 Martin Papik <martin.papik@ipsec.info>
								    #ob_start();
									echo "{$page_content}";
									#$_SESSION['html_content'] = ob_get_contents(); ob_end_flush();
									echo "{$page_script}"; 
								 }
							 ?>
						</div>
						<div class="clearBoth">&nbsp;</div>
						<!-- close .post (main column content) -->
					</div>
					<!-- close #content -->
				</div>
				<!-- end left-inner -->
			</div><!-- end left -->
		<div id="right">
			<div id="right-inner"></div><!-- end right inner-->
		</div> <!-- end right -->
		</div> <!-- end inner -->
		<div id="clearfooter"></div>
		<div style="clear:both;height:0;font-size: 0px" >&nbsp;</div>
		<div id="header">
		<a href="http://www.2connectbahrain.com" title="2Connect Bahrain W.L.L. Home" id="logo"><img src="assets/templates/2connect/images/2C-logo.gif" width="60px" height="65px" /></a>
		<!-- end topmenu -->
		</div> <!-- end header -->
		<div style="clear:both;height:0;font-size: 1px" >&nbsp;</div>
	</div> <!-- end outer div -->

	<div id="layout_footer_strip">

	    <div id="footer"><!--<div class="footerFixer"></div>--></div>
		<div id="layout_footer_bar_left">&nbsp;</div>
		<div id="layout_footer_bar_right">&nbsp;</div>
		<div class="footerStripFixer"></div>
	</div>
    <!-- end footer -->

</div>
<!-- end wrapper -->

<div id="blank"></div>
</body>
</html>
