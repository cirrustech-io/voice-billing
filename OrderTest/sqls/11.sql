-- Table: service_end_address

-- DROP TABLE service_end_address;

CREATE TABLE service_end_address
(
  id serial NOT NULL,
  service_order_id integer,
  a_company_name character varying(100),
  a_address character varying(200),
  a_road character varying(100),
  a_building character varying(50),
  a_area character varying(50),
  a_block character varying(25),
  a_country_id integer,
  a_pobox character varying(25),
  b_company_name character varying(100),
  b_address character varying(200),
  b_road character varying(100),
  b_building character varying(50),
  b_area character varying(50),
  b_block character varying(25),
  b_country_id integer,
  b_pobox character varying(25),
  CONSTRAINT service_end_address_pkey PRIMARY KEY (id),
  CONSTRAINT service_end_address_service_order_id_fkey FOREIGN KEY (service_order_id)
      REFERENCES service_order_detail (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITHOUT OIDS;
ALTER TABLE service_end_address OWNER TO postgres;
GRANT ALL ON TABLE service_end_address TO postgres;
GRANT SELECT, UPDATE, INSERT ON TABLE service_end_address TO web_order;




