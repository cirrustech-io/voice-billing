-- Table: site_type_detail

-- DROP TABLE site_type_detail;

CREATE TABLE site_type_detail
(
  id serial NOT NULL,
  title character varying(50),
  description character varying(100),
  def smallint DEFAULT 0,
  CONSTRAINT site_type_detail_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE site_type_detail OWNER TO postgres;
GRANT ALL ON TABLE site_type_detail TO postgres;
GRANT SELECT ON TABLE site_type_detail TO web_order;




