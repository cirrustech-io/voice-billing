-- Table: customer_detail

-- DROP TABLE customer_detail;

CREATE TABLE customer_detail
(
  id serial NOT NULL,
  user_name character varying(50),
  pwd character varying(50),
  company_legal_name character varying(100),
  building character varying(100),
  address character varying(500),
  town_city character varying(100),
  postcode character varying(25),
  province character varying(100),
  country_id integer,
  contact_name character varying(200),
  contact_title character varying(100),
  contact_email character varying(100),
  contact_telephone character varying(50),
  contact_mobile character varying(50),
  contact_fax character varying(50),
  tech_contact_name character varying(200),
  tech_contact_email character varying(100),
  tech_contact_telephone character varying(50),
  CONSTRAINT customer_detail_pkey PRIMARY KEY (id),
  CONSTRAINT customer_detail_country_id_fkey FOREIGN KEY (country_id)
      REFERENCES country (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITHOUT OIDS;
ALTER TABLE customer_detail OWNER TO postgres;
GRANT ALL ON TABLE customer_detail TO postgres;
GRANT SELECT ON TABLE customer_detail TO web_order;




