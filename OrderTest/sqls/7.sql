-- Table: country

-- DROP TABLE country;

CREATE TABLE country
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(100),
  def smallint DEFAULT 0,
  CONSTRAINT country_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE country OWNER TO postgres;
GRANT ALL ON TABLE country TO postgres;
GRANT SELECT ON TABLE country TO web_order;




