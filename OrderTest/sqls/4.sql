-- Table: billing_currency

-- DROP TABLE billing_currency;

CREATE TABLE billing_currency
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(250),
  code character varying(10),
  def smallint DEFAULT 0,
  CONSTRAINT billing_currency_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE billing_currency OWNER TO postgres;
GRANT ALL ON TABLE billing_currency TO postgres;
GRANT SELECT ON TABLE billing_currency TO web_order;




