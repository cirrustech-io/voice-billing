-- Table: account_executive

-- DROP TABLE account_executive;

CREATE TABLE account_executive
(
  id serial NOT NULL,
  name character varying(100),
  contact_email character varying(50),
  contact_telephone character varying(25),
  contact_mobile character varying(25),
  def smallint DEFAULT 0,
  title character varying(100),
  CONSTRAINT account_executive_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE account_executive OWNER TO postgres;
GRANT ALL ON TABLE account_executive TO postgres;
GRANT SELECT ON TABLE account_executive TO web_order;




