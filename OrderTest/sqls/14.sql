-- Table: site_status

-- DROP TABLE site_status;

CREATE TABLE site_status
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(200),
  def smallint DEFAULT 0,
  CONSTRAINT site_status_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE site_status OWNER TO alicia;
GRANT ALL ON TABLE site_status TO alicia;
GRANT SELECT ON TABLE site_status TO web_order;




