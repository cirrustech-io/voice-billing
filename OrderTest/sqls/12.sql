-- Table: service_order_detail

-- DROP TABLE service_order_detail;

CREATE TABLE service_order_detail
(
  id serial NOT NULL,
  order_id integer,
  order_date timestamp without time zone,
  required_date timestamp without time zone,
  agreement_number bigint,
  agreement_date timestamp without time zone,
  account_executive_id integer,
  p_o_number bigint,
  customer_id integer,
  cust_company_legal_name character varying(200),
  building character varying(100),
  address character varying(500),
  town_city character varying(100),
  postcode character varying(25),
  country_id integer,
  province character varying(100),
  contact_name character varying(200),
  contact_title character varying(100),
  contact_email character varying(100),
  contact_telephone character varying(50),
  contact_mobile character varying(50),
  contact_fax character varying(50),
  tech_contact_name character varying(200),
  tech_contact_email character varying(100),
  tech_contact_telephone character varying(50),
  bill_company_name character varying(200),
  bill_building character varying(100),
  bill_address character varying(500),
  bill_town_city character varying(100),
  bill_postcode character varying(50),
  bill_country_id integer,
  bill_province character varying(100),
  bill_contact_name character varying(200),
  bill_title character varying(100),
  bill_email character varying(100),
  bill_telephone character varying(50),
  bill_contact_mobile character varying(50),
  bill_contact_fax character varying(50),
  vat_number character varying(100),
  initial_contract_term_id integer,
  billing_media_id integer,
  billing_currency_id integer,
  payment_method_id integer,
  payment_frequency character varying(50),
  topology_id integer,
  type_of_site_id integer,
  multiport boolean,
  access_bundle boolean,
  cos boolean,
  bandwidth_id integer,
  additional_information text,
  site_status_id integer,
  bandwidth_detail character varying(200),
  CONSTRAINT service_order_detail_pkey PRIMARY KEY (id),
  CONSTRAINT service_order_detail_account_executive_id_fkey FOREIGN KEY (account_executive_id)
      REFERENCES account_executive (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_bandwidth_id_fkey FOREIGN KEY (bandwidth_id)
      REFERENCES bandwidth_detail (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_bill_country_id_fkey FOREIGN KEY (bill_country_id)
      REFERENCES country (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_billing_currency_id_fkey FOREIGN KEY (billing_currency_id)
      REFERENCES billing_currency (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_billing_media_id_fkey FOREIGN KEY (billing_media_id)
      REFERENCES billing_media (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_country_id_fkey FOREIGN KEY (country_id)
      REFERENCES country (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_customer_id_fkey FOREIGN KEY (customer_id)
      REFERENCES customer_detail (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_initial_contract_term_id_fkey FOREIGN KEY (initial_contract_term_id)
      REFERENCES contract_term (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES order_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_payment_method_id_fkey FOREIGN KEY (payment_method_id)
      REFERENCES payment_method (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_site_status_id_fkey FOREIGN KEY (site_status_id)
      REFERENCES site_status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_topology_id_fkey FOREIGN KEY (topology_id)
      REFERENCES service_topology (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_order_detail_type_of_site_id_fkey FOREIGN KEY (type_of_site_id)
      REFERENCES site_type_detail (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITHOUT OIDS;
ALTER TABLE service_order_detail OWNER TO postgres;
GRANT ALL ON TABLE service_order_detail TO postgres;
GRANT SELECT, UPDATE, INSERT ON TABLE service_order_detail TO web_order;




