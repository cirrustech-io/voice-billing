-- Table: bandwidth_pricing

-- DROP TABLE bandwidth_pricing;

CREATE TABLE bandwidth_pricing
(
  id serial NOT NULL,
  bandwidth_id integer,
  rate_installation numeric,
  rate_monthly numeric,
  CONSTRAINT bandwidth_pricing_pkey PRIMARY KEY (id),
  CONSTRAINT bandwidth_pricing_bandwidth_id_fkey FOREIGN KEY (bandwidth_id)
      REFERENCES bandwidth_detail (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITHOUT OIDS;
ALTER TABLE bandwidth_pricing OWNER TO postgres;
GRANT ALL ON TABLE bandwidth_pricing TO postgres;
GRANT SELECT ON TABLE bandwidth_pricing TO web_order;




