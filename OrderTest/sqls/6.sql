-- Table: contract_term

-- DROP TABLE contract_term;

CREATE TABLE contract_term
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(250),
  def smallint DEFAULT 0,
  CONSTRAINT contract_term_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE contract_term OWNER TO postgres;
GRANT ALL ON TABLE contract_term TO postgres;
GRANT SELECT ON TABLE contract_term TO web_order;




