-- Table: bandwidth_detail

-- DROP TABLE bandwidth_detail;

CREATE TABLE bandwidth_detail
(
  id serial NOT NULL,
  title character varying(25),
  description character varying(100),
  def smallint DEFAULT 0,
  CONSTRAINT bandwidth_detail_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE bandwidth_detail OWNER TO postgres;
GRANT ALL ON TABLE bandwidth_detail TO postgres;
GRANT SELECT ON TABLE bandwidth_detail TO web_order;




