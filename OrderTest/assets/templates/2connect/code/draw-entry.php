<?php
// Process Input from the Draw Entry form
function checkFieldValidity( $field, $value, &$errors )
{
	switch( $field )
	{
		case 'name':
			if( $value=='' ){ 
				array_push( $errors, "Invalid Name" ); 
			}
			break;
			
		case 'email':
			$pattern = '[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)';
			ereg( $pattern, $value, $regxp_result );
			
			if( $value=='' || $regxp_result == null ){
				array_push( $errors, "Invalid Email" );
			}
			break;
			
		case 'city':
			
			break;
			
		case 'mobile':
			if( $value == '' || strlen( $value )!=8 || ( substr( $value, 0, 2 )!='39' && substr( $value, 0, 2 )!='36' ) ){
				array_push( $errors, "Invalid Mobile Number" );
			}
			break;
			
		default:
			break;
	}
	return true;
}

if( isset( $_POST ) )
{
	$errors = array();
	$output = '';
	
	// print_r( $_POST );
	$input = array(
		'name' => $modx->db->escape( $_POST['name'] ),
		'email' => $modx->db->escape( $_POST['email'] ),
		'city' => $modx->db->escape( $_POST['city'] ),
		'mobile' => $modx->db->escape( $_POST['mobile'] )
	);
	
	foreach( $input as $field => $value ){
		checkFieldValidity( $field, $value, $errors );
	}
	
	// print_r( $errors );
		
	if( count( $errors ) > 0 ){
		foreach( $errors as $key => $value ){
			$output .= $value . ", ";
		}
		$output = substr( $output, 0, strlen($output)-2 ) . ".";
		echo '<div class="draw_error">' . $output . '</div>';
	}else{
		/*
		 * Write database code here.
		 */
		
		$name = $input['name'];
		$email = $input['email'];
		$mobile = $input['mobile'];
		$city = $input['city'];
		
		$entry_sql = "INSERT INTO contest_entries VALUES( null, '$name', '$email', $mobile, '$city', 0, NOW() );";
		$entry_result = $modx->db->query( $entry_sql );
		if( $entry_result == 1 ){
			echo '<div class="draw_success">ACCEPTED.</div>';
		}else{
			echo '<div class="draw_error">DB PROBLEM (contact admin).</div>';
		}
	}
}
else
{
	echo('No input');
}
?>