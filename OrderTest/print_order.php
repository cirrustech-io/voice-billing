<?php
#session variable names
$sess_cust_id = 'cust_id';
$sess_service_order_id = 'service_order_id';
$sess_ordercomplete = 'order_complete';
session_start();

if(!empty($_GET[$sess_service_order_id])){
	if(!empty($_SESSION[$sess_service_order_id]) && ($_SESSION[$sess_service_order_id] <> $_GET[$sess_service_order_id]) ){
		echo "Cannot produce order form. Please try again.";
		exit(0);
	}
}
session_write_close();

require('fpdf16/fpdf.php');

class PDF extends FPDF
{
	#Page header
	function Header()
	{
		#Banner
		$this->Image('assets/templates/2connect/images/order_pdf/Header_PDF-new.png',10,8,190);

		#Date,Arial bold 6
		$this->SetFont('Arial','B',6);
		$this->SetTextColor(0,0,0);
		$this->SetX(187);
		$date = date('d-M-Y');
		$this->Cell(0,55,$date);
		$this->Ln(10);
		$this->SetX(0);
	}

	#Page footer
	function Footer()
	{
		$this->Image('assets/templates/2connect/images/order_pdf/Footer_PDF-new.png',10,275,190);
	}
}

	$filename = 'Error.pdf';
	$pdf=new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$pdf->SetX(12);
	$pdf->SetY(25);

	$posMainX = 0;
	$posMainY = 25;

	$pdf->SetFillColor(200,200,200);
	$pdf->SetTextColor(0,0,0);#black
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell($posMainX,$posMainY,'ORDER SUMMARY',0,0);
	$pdf->Ln(15);

	order_summary($pdf);

	add_signature($pdf);
	#$pdf->Ln(10);

	
	$pdf->Output($filename,'D');


?>

<?


	function order_summary(&$pdf){

		#session variable names
		$sess_cust_id = 'cust_id';
		$sess_service_order_id = 'service_order_id';
		$sess_ordercomplete = 'order_complete';

		if(!empty($_GET[$sess_service_order_id])){


			$order_result = fetchOrderSummary($_GET[$sess_service_order_id]);

			if($order_result && pg_num_rows($order_result) > 0){

				$line = pg_fetch_array($order_result, NULL, PGSQL_ASSOC);

				$order_main = array(); 
				$cust_bill = array();
				$service = array();
				$charges = array();
				$service_address = array();
				
			
				$order_number = $order_main['order_number'] = $line['id'];
				$order_main['order_type'] = $line['order_type'];
				$order_main['order_date'] = date('Y-m-d',strtotime($line['order_date']));
				$order_main['required_date'] = date('Y-m-d',strtotime($line['required_date']));
				$order_main['agreement_date'] = date('Y-m-d',strtotime($line['agreement_date']));
				$order_main['agreement_number'] = $line['agreement_number'];
				$order_main['executive_name'] = $line['executive_name'];
				$order_main['p_o_number'] = $line['p_o_number'];
				create_ordermain_summary($pdf,$order_main);

				$cust_bill['company'] = $line['cust_company_legal_name'];
				$cust_bill['building'] = $line['building'];
				$cust_bill['address'] = $line['address'];
				$cust_bill['town_city'] = $line['town_city'];
				$cust_bill['postcode'] = $line['postcode'];
				$cust_bill['country_name'] = $line['country_name'];
				$cust_bill['province'] = $line['province'];
				$cust_bill['contact_name'] = $line['contact_name'];
				$cust_bill['contact_title'] = $line['contact_title'];
				$cust_bill['contact_email'] = $line['contact_email'];
				$cust_bill['contact_telephone'] = $line['contact_telephone'];
				$cust_bill['contact_mobile'] = $line['contact_mobile'];
				$cust_bill['contact_fax'] = $line['contact_fax'];
				$cust_bill['billing_media'] = $line['billing_media'];
				$cust_bill['contract_term'] = $line['init_contract_term'];
				$cust_bill['payment_frequency'] = $line['payment_frequency'];
				create_custbill_summary($pdf,$cust_bill);

				$service['site_topology_name'] = $line['site_topology_name'];
				$service['type_of_site_name'] = $line['type_of_site_name'];
				$service['multiport'] = $line['multiport'];
				$service['access_bundle'] = $line['access_bundle'];
				$service['cos'] = $line['cos'];
				$service['bandwidth_name'] = $line['bandwidth_name'];
				$service['bandwidth_description'] = $line['bandwidth_description'];
				create_service_summary($pdf,$service);
				
				$service_address['a_company_name'] = $line['a_company_name'];
				$service_address['a_address'] = $line['a_address'];
				$service_address['a_road'] = $line['a_road'];
				$service_address['a_building'] = $line['a_building'];
				$service_address['a_area'] = $line['a_area'];
				$service_address['a_block'] = $line['a_block'];
				$service_address['a_country_name'] = $line['a_country_name'];
				$service_address['a_telehouse'] = $line['a_telehouse'];
				$service_address['a_suite'] = $line['a_suite'];
				$service_address['a_row'] = $line['a_row'];
				$service_address['a_rack'] = $line['a_rack'];
				$service_address['a_vert'] = $line['a_vert'];
				$service_address['a_pobox'] = $line['a_pobox'];
				$service_address['a_province'] = $line['a_province'];
				$service_address['a_towncity'] = $line['a_towncity'];
				$service_address['b_company_name'] = $line['b_company_name'];
				$service_address['b_address'] = $line['b_address'];
				$service_address['b_road'] = $line['b_road'];
				$service_address['b_building'] = $line['b_building'];
				$service_address['b_area'] = $line['b_area'];
				$service_address['b_block'] = $line['b_block'];
				$service_address['b_country_name'] = $line['b_country_name'];
				$service_address['b_telehouse'] = $line['b_telehouse'];
				$service_address['b_suite'] = $line['b_suite'];
				$service_address['b_row'] = $line['b_row'];
				$service_address['b_rack'] = $line['b_rack'];
				$service_address['b_vert'] = $line['b_vert'];
				$service_address['b_pobox'] = $line['b_pobox'];
				$service_address['b_province'] = $line['b_province'];
				$service_address['b_towncity'] = $line['b_towncity'];
				create_service_address_summary($pdf,$service_address);

				$charges['code'] = $line['code'];
				$charges['multiple'] = $line['multiple'];
				$charges['rate_installation'] = $line['rate_installation'];
				$charges['rate_monthly'] = $line['rate_monthly'];
				create_charges_summary($pdf,$charges);

				$additional_summary = $line['additional_information'];
				create_additional_summary($pdf,$additional_summary);

				$coname= str_replace('','-',$line['cust_company_legal_name']);
				$GLOBALS['filename'] = "2Connect_Service_Order_Form_{$coname}_No_{$order_number}.pdf";
			} else {
				$pdf->Cell(100,100,'Could not retrieve database records',0,0);
				return;
			}

		} else {
			$pdf->Cell(100,100,'Could not retrieve session data',0,0);
			return;
		}


	}

	function fetchOrderSummary($service_order_id){
			$conn_string = "host=80.88.242.8 dbname=db_order user=web_order password=n7g3B4J2xQso";

			$conn = pg_connect($conn_string);

			$sql = 	"select s.id,
					ot.title as order_type,
					s.order_date,
					s.required_date,
					s.agreement_date,
					s.agreement_number,
					s.account_executive_name as executive_name,
					s.p_o_number,
					s.cust_company_legal_name,
					s.building,
					s.address,
					s.town_city,
					s.postcode,
					c.title as country_name,
					s.province,
					s.contact_name,
					s.contact_title,
					s.contact_email,
					s.contact_telephone,
					s.contact_mobile,
					s.contact_fax,
					bm.title as billing_media,
					ct.title as init_contract_term,
					s.payment_frequency,
					bc.code,
					bc.multiple,
					st.title as site_topology_name,
					ts.title as type_of_site_name,
					s.multiport,
					s.access_bundle,
					s.cos,
					b.title as bandwidth_name,
					s.bandwidth_description,
					s.additional_information,
					bp.rate_installation,
					bp.rate_monthly,
					sea.a_company_name,
					sea.a_address,
					sea.a_road,
					sea.a_building,
					sea.a_area,
					sea.a_block,
					sea.a_telehouse,
					sea.a_suite,
					sea.a_row,
					sea.a_rack,
					sea.a_vert,
					c1.title as a_country_name,
					sea.a_pobox,
					sea.a_province,
					sea.a_towncity,
					sea.b_company_name,
					sea.b_address,
					sea.b_road,
					sea.b_building,
					sea.b_area,
					sea.b_block,
					c2.title as b_country_name,
					sea.b_pobox,
					sea.b_province,
					sea.b_towncity,
					sea.b_telehouse,
					sea.b_suite,
					sea.b_row,
					sea.b_rack,
					sea.b_vert

			from 	service_order_detail s,
					order_type ot,
					country c,
					billing_media bm,
					service_topology st,
					site_type_detail ts,
					bandwidth_detail b,
					bandwidth_pricing bp,
					billing_currency bc,
					contract_term ct,
					service_end_address sea,
					country c1,
			        country c2

 
			where		c.id = s.country_id
				and 	ot.id = s.order_id
				and		bm.id = s.billing_media_id
				and		st.id = s.topology_id
				and     b.id = s.bandwidth_id
				and 	bp.bandwidth_id = b.id
				and 	bc.id = s.billing_currency_id
				and     ts.id = s.type_of_site_id
				and 	ct.id = s.initial_contract_term_id
				and 	sea.service_order_id = s.id
				and 	c1.id = sea.a_country_id
				and 	c2.id = sea.b_country_id
				and 	s.id = " .  $service_order_id 	. ";";

			$result = pg_query($conn,$sql) or die ('Query failed: ' . pg_last_error());
			return $result;

	}

	function create_ordermain_summary(&$pdf,$order_main){
		$posTitleX = 10;
		$posDataX = 50;
		$posLine = 3;

		if(count($order_main) > 0){
			#order main section data
			$pdf->SetFont('Arial','',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Order Reference Number:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['order_number'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Type:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['order_type'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Order Date:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['order_date'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Agreement Date:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['agreement_date'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Agreement Number:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['agreement_number'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'2Connect Account Executive:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['executive_name'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Purchase Order Number:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$order_main['p_o_number'],0,0);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			
		
		}

	}

	function create_custbill_summary(&$pdf,$cust_bill){
		$posTitleX = 10;  $r_posTitleX = 110;
		$posDataX = 50;   $r_posDataX = 150; 
		$posLine = 3;

		if(count($cust_bill) > 0){
			#cust section header									#billing section header
			$pdf->SetFont('Arial','B',8);
			$pdf->SetX($posTitleX);									
			$pdf->Cell(0,5,'Customer Details',0,0,'L',true);        
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Billing Details',0,0,'L',true);
			
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);

				
			
			#cust section data										#billing section data
			$pdf->SetFont('Arial','',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Company:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Payment:',0,0);
			
			
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['company'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$cust_bill['payment_frequency'],0,0);
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Building:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Media:',0,0);
					
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['building'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$cust_bill['billing_media'],0,0);
			
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Address:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Contract Term:',0,0);	
			
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['address'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$cust_bill['contract_term'],0,0);
			
			$pdf->Ln($posLine);
			
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Town/City:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['town_city'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Province:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['province'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Post Code:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['postcode'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Country:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['country_name'],0,0);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);

			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Contact Name:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contact_name'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Title:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contact_title'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Email:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contact_email'],0,0);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);


			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Telephone:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contact_telephone'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Mobile:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contact_mobile'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Fax:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contact_fax'],0,0);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			

			/*
			#billing section header
			$pdf->SetFont('Arial','B',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Billing Details',0,0,'L',true);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);

			#billing section data
			$pdf->SetFont('Arial','',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Payment:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['payment_frequency'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Media:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['billing_media'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Contract Term:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cust_bill['contract_term'],0,0);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			*/
			
		}


	}

	function create_charges_summary(&$pdf,$charges){

		$posTitleX = 10;
		$posDataX = 50;
		$posLine = 3;


		if(count($charges) > 0){
			$rate_install = floatval($charges['rate_installation']) * floatval($charges['multiple']);
			$rate_monthly = floatval($charges['rate_monthly']) * floatval($charges['multiple']);
			$currency_code = $charges['code'] ;



			#charges section header
			$pdf->SetFont('Arial','B',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Pricing Details',0,0,'L',true);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);


			#charges section data
			$pdf->SetFont('Arial','',8);
/*
			if($rate_monthly <> 0 && $rate_install <> 0 ){
				$pdf->SetX($posTitleX);
				$pdf->Cell(0,5,'Installation:',0,0);
				$pdf->SetX($posDataX);
				$pdf->Cell(0,5,"{$currency_code}   {$rate_install}",0,0);
				$pdf->Ln($posLine);
				$pdf->Cell(0,5,'Monthly:',0,0);
				$pdf->SetX($posDataX);
				$pdf->Cell(0,5,"{$currency_code}   {$rate_monthly}",0,0);
				$pdf->Ln($posLine);

			} else {
				$pdf->SetX($posTitleX);
				$pdf->Cell(0,5,'Subject to bandwidth requirements',0,0);
			}
*/
			#Temp until pricing is decided
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Installation & Annual Charges:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,"Subject to 2Connect Confirmation.",0,0);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);

		}

	}

	function create_service_summary(&$pdf,$service){
		$posTitleX = 10;
		$posDataX = 50;
		$posLine = 3;

		#service section header
		$pdf->SetFont('Arial','B',8);
		$pdf->SetX($posTitleX);
		$pdf->Cell(0,5,'Service Details',0,0,'L',true);
		$pdf->Ln($posLine);
		$pdf->Ln($posLine);

		if(count($service) > 0){


			$multiport 				= ($service['multiport'] == 'f')?'NO':'YES';
			$accessbundle 			= ($service['access_bundle'] == 'f')?'NO':'YES';
			$cos 					= ($service['cos'] == 'f')?'NO':'YES';

			#service section data
			$pdf->SetFont('Arial','',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Topology:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service['site_topology_name'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Type Of Site:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service['type_of_site_name'],0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Multiport Access:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$multiport,0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Internet Access Bundle:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$accessbundle,0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'CoS Active on Service:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$cos,0,0);
			$pdf->Ln($posLine);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Bandwidth:',0,0);
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service['bandwidth_name'],0,0);


			if(strtoupper($service['bandwidth_name']) == 'OTHER'){

				$pdf->Ln($posLine);
				$pdf->SetX($posTitleX);
				$pdf->Cell(0,5,'Bandwidth Details:',0,0);
				$pdf->SetX($posDataX);
				$pdf->Cell(0,5,$service['bandwidth_description'],0,0);
			}

			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);

		}
	}

	function create_service_address_summary(&$pdf,$service_address){
		$posTitleX = 10;  $r_posTitleX = 110;
		$posDataX = 50;   $r_posDataX = 150; 
		$posLine = 3;
		
		if(count($service_address) > 0){
		
			$is_a_telehouse = ($service_address['a_telehouse'] == 'f')?'NO':'YES';
			$is_b_telehouse = ($service_address['b_telehouse'] == 'f')?'NO':'YES';
		
		
			#A end address section header							#B end address section header
			$pdf->SetFont('Arial','B',8);
			$pdf->SetX($posTitleX);									
			$pdf->Cell(0,5,'A End Address',0,0,'L',true);        
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'B End Address',0,0,'L',true);
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);

			#A end address data										#B end address data
			$pdf->SetFont('Arial','',8);
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Company:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Company:',0,0);
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service_address['a_company_name'],0,0);
			
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$service_address['b_company_name'],0,0);
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Address:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Address:',0,0);
			
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service_address['a_address'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$service_address['b_address'],0,0);
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Building:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Building:',0,0);
		
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service_address['a_building'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$service_address['b_building'],0,0);
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Road | Area | Block:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Road | Area | Block::',0,0);
		
			
			$pdf->SetX($posDataX);
			$service_address['a_road'] = ($service_address['a_road'] == '')?'NA':$service_address['a_road'];
			$service_address['a_area'] = ($service_address['a_area'] == '')?'NA':$service_address['a_area'];
			$service_address['a_block'] = ($service_address['a_block'] == '')?'NA':$service_address['a_block'];
			$pdf->Cell(0,5,"{$service_address['a_road']} | {$service_address['a_area']} | {$service_address['a_block']}"
			,0,0);
															
																	$pdf->SetX($r_posDataX);
																	$service_address['b_road'] = ($service_address['b_road'] == '')?'NA':$service_address['b_road'];
																	$service_address['b_area'] = ($service_address['b_area'] == '')?'NA':$service_address['b_area'];
																	$service_address['b_block'] = ($service_address['b_block'] == '')?'NA':$service_address['b_block'];
																	$pdf->Cell(0,5,"{$service_address['b_road']} | {$service_address['b_area']} | {$service_address['b_block']}"
																	,0,0);
																	
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Zip/Postal Code:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Zip/Postal Code:',0,0);
			
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service_address['a_pobox'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$service_address['b_pobox'],0,0);
			$pdf->Ln($posLine);
			
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Country:',0,0);
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Country:',0,0);
			
			
			$pdf->SetX($posDataX);
			$pdf->Cell(0,5,$service_address['a_country_name'],0,0);
																	$pdf->SetX($r_posDataX);
																	$pdf->Cell(0,5,$service_address['b_country_name'],0,0);
			$pdf->Ln($posLine);
			
			if($is_a_telehouse == 'YES'){
			$pdf->SetX($posTitleX);
			$pdf->Cell(0,5,'Suite | Row | Rack | Suite:',0,0);
			}
																	if($is_b_telehouse == 'YES'){
																	$pdf->SetX($r_posTitleX);
																	$pdf->Cell(0,5,'Suite | Row | Rack | Suite:',0,0);
																	}
		
			if($is_a_telehouse == 'YES'){
			$pdf->SetX($posDataX);
			$service_address['a_suite'] = ($service_address['a_suite'] == '')?'NA':$service_address['a_suite'];
			$service_address['a_row'] = ($service_address['a_row'] == '')?'NA':$service_address['a_row'];
			$service_address['a_rack'] = ($service_address['a_rack'] == '')?'NA':$service_address['a_rack'];
			$service_address['a_vert'] = ($service_address['a_vert'] == '')?'NA':$service_address['a_vert'];
			$pdf->Cell(0,5,"{$service_address['a_suite']} | {$service_address['a_row']} | {$service_address['a_rack']} | {$service_address['a_vert']}"
			,0,0);
			}
																	if($is_b_telehouse == 'YES'){	
																	$pdf->SetX($r_posDataX);
																	$service_address['b_suite'] = ($service_address['b_suite'] == '')?'NA':$service_address['b_suite'];
																	$service_address['b_row'] = ($service_address['b_row'] == '')?'NA':$service_address['b_row'];
																	$service_address['b_rack'] = ($service_address['b_rack'] == '')?'NA':$service_address['b_rack'];
																	$service_address['b_vert'] = ($service_address['b_vert'] == '')?'NA':$service_address['b_vert'];
																	$pdf->Cell(0,5,"{$service_address['b_suite']} | {$service_address['b_row']} | {$service_address['b_rack']} | {$service_address['b_vert']}"
																	,0,0);
																	}
			$pdf->Ln($posLine);
			
			$pdf->Ln($posLine);
			$pdf->Ln($posLine);
													
		}
	}
	
	function create_additional_summary(&$pdf,$additional_summary){
		$posTitleX = 10;
		$posDataX = 50;
		$posLine = 3;

		$additional_summary = trim($additional_summary);
		$additional_summary = ($additional_summary == '')?'NA':$additional_summary;

		#additional section header
		$pdf->SetFont('Arial','B',8);
		$pdf->SetX($posTitleX);
		$pdf->Cell(0,5,'Additional Details:',0,0,'L',true);
		$pdf->Ln($posLine);
		$pdf->Ln($posLine);


		#additional section data
		$pdf->SetFont('Arial','',8);
		$pdf->SetX($posTitleX);
		$pdf->Cell(0,5,$additional_summary,0,0);
		$pdf->Ln($posLine);
		$pdf->Ln($posLine);
		$pdf->Ln($posLine);

	}

	function add_signature(&$pdf){
		$pdf->Image('assets/templates/2connect/images/order_pdf/Signature_PDF-new.png',10,250,190);
	}

?>
