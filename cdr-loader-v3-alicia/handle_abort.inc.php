<?php

	$user_termination=0;
	declare(ticks = 1);
	function user_abort($sig) {
		global $user_termination;
		$user_termination = 1;
		echo "Abort\n";
	}

	pcntl_signal(SIGTERM,"user_abort");
	pcntl_signal(SIGHUP,"user_abort");
	pcntl_signal(SIGINT,"user_abort");

?>
