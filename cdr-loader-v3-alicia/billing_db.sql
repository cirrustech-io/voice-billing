--
-- PostgreSQL database dump
--

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 2 (OID 0)
-- Name: billing; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE billing WITH TEMPLATE = template0 ENCODING = 'UNICODE';


\connect billing postgres

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;

--
-- TOC entry 4 (OID 2200)
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


SET SESSION AUTHORIZATION 'alicia';

SET search_path = public, pg_catalog;

--
-- TOC entry 7 (OID 264489476)
-- Name: carrier; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE carrier (
    carrier_id serial NOT NULL,
    carrier_name character varying(50) NOT NULL,
    "type" character(1)
);


--
-- TOC entry 9 (OID 264489476)
-- Name: carrier; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE carrier FROM PUBLIC;
REVOKE ALL ON TABLE carrier FROM alicia;
SET SESSION AUTHORIZATION postgres;
GRANT ALL ON TABLE carrier TO postgres WITH GRANT OPTION;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION postgres;
GRANT ALL ON TABLE carrier TO hameeda;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION postgres;
GRANT ALL ON TABLE carrier TO alicia;
RESET SESSION AUTHORIZATION;
GRANT ALL ON TABLE carrier TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 80 (OID 264489476)
-- Name: carrier_carrier_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE carrier_carrier_id_seq FROM PUBLIC;
GRANT ALL ON TABLE carrier_carrier_id_seq TO hameeda;
GRANT ALL ON TABLE carrier_carrier_id_seq TO alicia;
GRANT ALL ON TABLE carrier_carrier_id_seq TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 10 (OID 264489483)
-- Name: update; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "update" (
    update_id serial NOT NULL,
    received_date timestamp without time zone,
    carrier_id integer NOT NULL
);


--
-- TOC entry 11 (OID 264489483)
-- Name: update; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE "update" FROM PUBLIC;
GRANT ALL ON TABLE "update" TO hameeda;
GRANT ALL ON TABLE "update" TO alicia;
GRANT ALL ON TABLE "update" TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 81 (OID 264489483)
-- Name: update_update_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE update_update_id_seq FROM PUBLIC;
GRANT ALL ON TABLE update_update_id_seq TO hameeda;
GRANT ALL ON TABLE update_update_id_seq TO alicia;
GRANT ALL ON TABLE update_update_id_seq TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 12 (OID 264489494)
-- Name: region; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE region (
    region_id serial NOT NULL,
    update_id integer NOT NULL,
    region_name character varying(250) NOT NULL,
    region_code character varying(64) NOT NULL,
    rate numeric(10,6),
    effective_date timestamp without time zone,
    end_date timestamp without time zone
);


--
-- TOC entry 13 (OID 264489494)
-- Name: region; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE region FROM PUBLIC;
GRANT ALL ON TABLE region TO hameeda;
GRANT ALL ON TABLE region TO alicia;
GRANT ALL ON TABLE region TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 82 (OID 264489494)
-- Name: region_region_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE region_region_id_seq FROM PUBLIC;
GRANT ALL ON TABLE region_region_id_seq TO hameeda;
GRANT ALL ON TABLE region_region_id_seq TO alicia;
GRANT ALL ON TABLE region_region_id_seq TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 14 (OID 264489505)
-- Name: code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE code (
    code_id serial NOT NULL,
    region_id integer,
    calling_code character varying(15) NOT NULL
);


--
-- TOC entry 15 (OID 264489505)
-- Name: code; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE code FROM PUBLIC;
GRANT ALL ON TABLE code TO hameeda;
GRANT ALL ON TABLE code TO alicia;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 83 (OID 264489505)
-- Name: code_code_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE code_code_id_seq FROM PUBLIC;
GRANT ALL ON TABLE code_code_id_seq TO hameeda;
GRANT ALL ON TABLE code_code_id_seq TO alicia;
GRANT ALL ON TABLE code_code_id_seq TO haifa;


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 16 (OID 289141229)
-- Name: master; Type: TABLE; Schema: public; Owner: hameeda
--

CREATE TABLE master (
    master_id serial NOT NULL,
    region_name text,
    calling_code text,
    "type" character varying DEFAULT 'I'::character varying
) WITHOUT OIDS;


--
-- TOC entry 18 (OID 289141229)
-- Name: master; Type: ACL; Schema: public; Owner: hameeda
--

REVOKE ALL ON TABLE master FROM PUBLIC;
GRANT ALL ON TABLE master TO alicia;
GRANT ALL ON TABLE master TO haifa;


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 84 (OID 289141229)
-- Name: master_master_id_seq; Type: ACL; Schema: public; Owner: hameeda
--

REVOKE ALL ON TABLE master_master_id_seq FROM PUBLIC;
GRANT ALL ON TABLE master_master_id_seq TO alicia;
GRANT ALL ON TABLE master_master_id_seq TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 19 (OID 304159026)
-- Name: rate; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE rate (
    rate_id serial NOT NULL,
    carrier_id bigint,
    master_id bigint,
    rate numeric,
    effective_date timestamp without time zone,
    billing_increment integer,
    connection_charge numeric,
    minimum_duration integer,
    customer_carrier_id bigint,
    validity character(1),
    effective_end_date time without time zone
) WITHOUT OIDS;


--
-- TOC entry 25 (OID 304159026)
-- Name: rate; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE rate FROM PUBLIC;
REVOKE ALL ON TABLE rate FROM alicia;
SET SESSION AUTHORIZATION hameeda;
GRANT ALL ON TABLE rate TO hameeda WITH GRANT OPTION;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION hameeda;
GRANT ALL ON TABLE rate TO alicia;
RESET SESSION AUTHORIZATION;
GRANT ALL ON TABLE rate TO haifa;


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 85 (OID 304159026)
-- Name: rate_rate_id_seq; Type: ACL; Schema: public; Owner: hameeda
--

REVOKE ALL ON TABLE rate_rate_id_seq FROM PUBLIC;
GRANT ALL ON TABLE rate_rate_id_seq TO alicia;
GRANT ALL ON TABLE rate_rate_id_seq TO haifa;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 26 (OID 313989337)
-- Name: ldr_files; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ldr_files (
    id serial NOT NULL,
    filename character varying(250) NOT NULL,
    md5sum character varying(35),
    sha1sum character varying(45),
    filesize integer NOT NULL,
    lines integer,
    loaded integer DEFAULT 0,
    locked integer DEFAULT 0,
    load_first integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 27 (OID 313989337)
-- Name: ldr_files; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE ldr_files FROM PUBLIC;
GRANT ALL ON TABLE ldr_files TO cdr_loader;
GRANT ALL ON TABLE ldr_files TO hameeda;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 86 (OID 313989337)
-- Name: ldr_files_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE ldr_files_id_seq FROM PUBLIC;
GRANT ALL ON TABLE ldr_files_id_seq TO cdr_loader;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 28 (OID 314089507)
-- Name: new_cdr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE new_cdr (
    start_time timestamp without time zone NOT NULL,
    start_time_int integer NOT NULL,
    call_duration interval NOT NULL,
    originator_ip character varying(100) NOT NULL,
    terminator_ip character varying(100) NOT NULL,
    call_source_custid character varying(100) NOT NULL,
    called_party_on_dst character varying(100) NOT NULL,
    called_party_on_src character varying(100) NOT NULL,
    call_type character(2) NOT NULL,
    disconnect_error_type character(1) NOT NULL,
    call_error_int integer NOT NULL,
    call_error_str character varying(100) NOT NULL,
    ani character varying(100) NOT NULL,
    cdr_seq_no integer NOT NULL,
    callid character varying(100) NOT NULL,
    call_hold_time interval NOT NULL,
    call_source_regid character varying(100) NOT NULL,
    call_source_uport integer NOT NULL,
    call_dest_regid character varying(100) NOT NULL,
    call_dest_uport integer NOT NULL,
    isdn_cause_code character varying(100) NOT NULL,
    call_party_after_src_calling_plan character varying(100) NOT NULL,
    call_error_dest_int integer,
    call_error_dest_str character varying(100) NOT NULL,
    new_ani character varying(100) NOT NULL,
    call_duration_int integer NOT NULL,
    incoming_leg_callid character varying(100) NOT NULL,
    protocol character varying(6) NOT NULL,
    cdr_type character varying(10) NOT NULL,
    hunting_attempts integer NOT NULL,
    caller_trunk_group character varying(100) NOT NULL,
    call_pdd integer NOT NULL,
    h323_dest_ras_error integer,
    h323_dest_h225_error integer,
    sip_dest_respcode integer,
    dest_trunk_group character varying(100) NOT NULL,
    call_duration_fractional numeric(21,3) NOT NULL,
    timezone character varying(100) NOT NULL,
    msw_name character varying(100) NOT NULL,
    price numeric(25,5),
    rate_id integer,
    cdr_file_id integer NOT NULL,
    cdr_file_line integer NOT NULL,
    clean_number character varying(100) DEFAULT ''::character varying,
    "zone" integer
);


--
-- TOC entry 29 (OID 314089507)
-- Name: new_cdr; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE new_cdr FROM PUBLIC;
GRANT ALL ON TABLE new_cdr TO cdr_loader;
GRANT ALL ON TABLE new_cdr TO hameeda;
GRANT ALL ON TABLE new_cdr TO alicia;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 30 (OID 320223638)
-- Name: tmp__error_type_and_description; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tmp__error_type_and_description (
    error_description character varying(100),
    error_type integer,
    count bigint
);


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 31 (OID 340880866)
-- Name: endpoint; Type: TABLE; Schema: public; Owner: hameeda
--

CREATE TABLE endpoint (
    endpoint_id serial NOT NULL,
    carrier_id bigint,
    endpoint text,
    port integer
) WITHOUT OIDS;


--
-- TOC entry 32 (OID 340880866)
-- Name: endpoint; Type: ACL; Schema: public; Owner: hameeda
--

REVOKE ALL ON TABLE endpoint FROM PUBLIC;
GRANT ALL ON TABLE endpoint TO alicia;
GRANT ALL ON TABLE endpoint TO haifa;


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 87 (OID 340880866)
-- Name: endpoint_endpoint_id_seq; Type: ACL; Schema: public; Owner: hameeda
--

REVOKE ALL ON TABLE endpoint_endpoint_id_seq FROM PUBLIC;
GRANT ALL ON TABLE endpoint_endpoint_id_seq TO alicia;
GRANT ALL ON TABLE endpoint_endpoint_id_seq TO haifa;


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 33 (OID 384491670)
-- Name: tempbatelco; Type: TABLE; Schema: public; Owner: hameeda
--

CREATE TABLE tempbatelco (
    start_time timestamp without time zone,
    start_time_int integer,
    call_duration interval,
    originator_ip character varying(100),
    terminator_ip character varying(100),
    call_source_custid character varying(100),
    called_party_on_dst character varying(100),
    called_party_on_src character varying(100),
    call_type character(2),
    disconnect_error_type character(1),
    call_error_int integer,
    call_error_str character varying(100),
    ani character varying(100),
    cdr_seq_no integer,
    callid character varying(100),
    call_hold_time interval,
    call_source_regid character varying(100),
    call_source_uport integer,
    call_dest_regid character varying(100),
    call_dest_uport integer,
    isdn_cause_code character varying(100),
    call_party_after_src_calling_plan character varying(100),
    call_error_dest_int integer,
    call_error_dest_str character varying(100),
    new_ani character varying(100),
    call_duration_int integer,
    incoming_leg_callid character varying(100),
    protocol character varying(6),
    cdr_type character varying(10),
    hunting_attempts integer,
    caller_trunk_group character varying(100),
    call_pdd integer,
    h323_dest_ras_error integer,
    h323_dest_h225_error integer,
    sip_dest_respcode integer,
    dest_trunk_group character varying(100),
    call_duration_fractional numeric(21,3),
    timezone character varying(100),
    msw_name character varying(100),
    price numeric(25,5),
    rate_id integer,
    cdr_file_id integer,
    cdr_file_line integer,
    clean_number character varying(100)
);


--
-- TOC entry 34 (OID 384496690)
-- Name: tempbatelco2; Type: TABLE; Schema: public; Owner: hameeda
--

CREATE TABLE tempbatelco2 (
    start_time timestamp without time zone,
    start_time_int integer,
    call_duration interval,
    originator_ip character varying(100),
    terminator_ip character varying(100),
    call_source_custid character varying(100),
    called_party_on_dst character varying(100),
    called_party_on_src character varying(100),
    call_type character(2),
    disconnect_error_type character(1),
    call_error_int integer,
    call_error_str character varying(100),
    ani character varying(100),
    cdr_seq_no integer,
    callid character varying(100),
    call_hold_time interval,
    call_source_regid character varying(100),
    call_source_uport integer,
    call_dest_regid character varying(100),
    call_dest_uport integer,
    isdn_cause_code character varying(100),
    call_party_after_src_calling_plan character varying(100),
    call_error_dest_int integer,
    call_error_dest_str character varying(100),
    new_ani character varying(100),
    call_duration_int integer,
    incoming_leg_callid character varying(100),
    protocol character varying(6),
    cdr_type character varying(10),
    hunting_attempts integer,
    caller_trunk_group character varying(100),
    call_pdd integer,
    h323_dest_ras_error integer,
    h323_dest_h225_error integer,
    sip_dest_respcode integer,
    dest_trunk_group character varying(100),
    call_duration_fractional numeric(21,3),
    timezone character varying(100),
    msw_name character varying(100),
    price numeric(25,5),
    rate_id integer,
    cdr_file_id integer,
    cdr_file_line integer,
    clean_number character varying(100)
);


--
-- TOC entry 35 (OID 386211509)
-- Name: prepaid; Type: TABLE; Schema: public; Owner: hameeda
--

CREATE TABLE prepaid (
    prepaid_id serial NOT NULL
) WITHOUT OIDS;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 36 (OID 388432150)
-- Name: stat_load_month; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW stat_load_month AS
    SELECT t1.date, t1.files_t, (SELECT count(*) AS count FROM ldr_files WHERE ((substr((ldr_files.filename)::text, 7, 6) = t1.date) AND (ldr_files.loaded = ldr_files.lines))) AS files_c, (SELECT count(*) AS count FROM ldr_files WHERE ((substr((ldr_files.filename)::text, 7, 6) = t1.date) AND (ldr_files.loaded <> ldr_files.lines))) AS files_i, (SELECT count(*) AS count FROM ldr_files WHERE ((substr((ldr_files.filename)::text, 7, 6) = t1.date) AND (ldr_files.locked > 0))) AS files_l, t1.cdr_t, t1.cdr_c, t1.cdr_i FROM (SELECT substr((ldr_files.filename)::text, 7, 6) AS date, count(*) AS files_t, sum(ldr_files.lines) AS cdr_t, sum(ldr_files.loaded) AS cdr_c, (sum(ldr_files.lines) - sum(ldr_files.loaded)) AS cdr_i FROM ldr_files GROUP BY substr((ldr_files.filename)::text, 7, 6) ORDER BY substr((ldr_files.filename)::text, 7, 6)) t1;


--
-- TOC entry 37 (OID 388432150)
-- Name: stat_load_month; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE stat_load_month FROM PUBLIC;
GRANT ALL ON TABLE stat_load_month TO alicia;
GRANT ALL ON TABLE stat_load_month TO hameeda;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 38 (OID 388442907)
-- Name: stat_load_month_percent; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW stat_load_month_percent AS
    SELECT stat_load_month.date, stat_load_month.files_t, stat_load_month.files_c, stat_load_month.files_i, stat_load_month.files_l, stat_load_month.cdr_t, stat_load_month.cdr_c, stat_load_month.cdr_i, ((((100 * stat_load_month.cdr_c) / stat_load_month.cdr_t))::text || '%'::text) AS complete FROM stat_load_month WHERE (stat_load_month.cdr_t > 0);


--
-- TOC entry 39 (OID 388442907)
-- Name: stat_load_month_percent; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE stat_load_month_percent FROM PUBLIC;
GRANT ALL ON TABLE stat_load_month_percent TO hameeda;
GRANT ALL ON TABLE stat_load_month_percent TO alicia;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 40 (OID 390683449)
-- Name: tmp_find_bug; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tmp_find_bug (
    filename character varying(250),
    lines integer,
    loaded integer,
    really_there bigint
);


--
-- TOC entry 41 (OID 392203712)
-- Name: cdrs_idt_sep2007; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cdrs_idt_sep2007 (
    start_time timestamp without time zone,
    call_duration interval,
    clean_number character varying(100),
    called_party_on_dst character varying(100),
    callid character varying(100),
    terminator_ip character varying(100),
    ani character varying(100),
    rate_id integer,
    call_dest_regid character varying(100),
    call_dest_uport integer
);


--
-- TOC entry 42 (OID 392203712)
-- Name: cdrs_idt_sep2007; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE cdrs_idt_sep2007 FROM PUBLIC;
GRANT ALL ON TABLE cdrs_idt_sep2007 TO alicia;
GRANT ALL ON TABLE cdrs_idt_sep2007 TO hameeda;


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 43 (OID 392251955)
-- Name: cdrs_idt_sep2007_2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cdrs_idt_sep2007_2 (
    start_time timestamp without time zone,
    call_duration interval,
    clean_number character varying(100),
    called_party_on_dst character varying(100),
    callid character varying(100),
    terminator_ip character varying(100),
    ani character varying(100),
    rate_id integer,
    call_dest_regid character varying(100),
    call_dest_uport integer
);


--
-- TOC entry 44 (OID 392251955)
-- Name: cdrs_idt_sep2007_2; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE cdrs_idt_sep2007_2 FROM PUBLIC;
GRANT ALL ON TABLE cdrs_idt_sep2007_2 TO hameeda;
GRANT ALL ON TABLE cdrs_idt_sep2007_2 TO alicia;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 45 (OID 392284653)
-- Name: cdrs_idt_sep2007_3; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE cdrs_idt_sep2007_3 (
    start_time timestamp without time zone,
    call_duration interval,
    call_duration_int integer,
    clean_number character varying(100),
    called_party_on_dst character varying(100),
    callid character varying(100),
    terminator_ip character varying(100),
    ani character varying(100),
    rate_id integer,
    call_dest_regid character varying(100),
    call_dest_uport integer,
    price numeric(25,5)
);


--
-- TOC entry 46 (OID 392284653)
-- Name: cdrs_idt_sep2007_3; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE cdrs_idt_sep2007_3 FROM PUBLIC;
REVOKE ALL ON TABLE cdrs_idt_sep2007_3 FROM alicia;
SET SESSION AUTHORIZATION postgres;
GRANT ALL ON TABLE cdrs_idt_sep2007_3 TO postgres WITH GRANT OPTION;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION postgres;
GRANT ALL ON TABLE cdrs_idt_sep2007_3 TO hameeda;
RESET SESSION AUTHORIZATION;
SET SESSION AUTHORIZATION postgres;
GRANT ALL ON TABLE cdrs_idt_sep2007_3 TO alicia;
RESET SESSION AUTHORIZATION;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 47 (OID 392314183)
-- Name: country; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE country (
    country_id serial NOT NULL,
    country_code text,
    country_name text
) WITHOUT OIDS;


--
-- TOC entry 48 (OID 392314183)
-- Name: country; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE country FROM PUBLIC;
GRANT ALL ON TABLE country TO hameeda;
GRANT ALL ON TABLE country TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 88 (OID 392314183)
-- Name: Country_country_id_seq; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE "Country_country_id_seq" FROM PUBLIC;
GRANT ALL ON TABLE "Country_country_id_seq" TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 49 (OID 392359160)
-- Name: master_rsm; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE master_rsm (
    master_id integer,
    region_name text,
    calling_code text,
    "type" character varying
);


--
-- TOC entry 50 (OID 392359160)
-- Name: master_rsm; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE master_rsm FROM PUBLIC;
GRANT ALL ON TABLE master_rsm TO hameeda;
GRANT ALL ON TABLE master_rsm TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 51 (OID 399066231)
-- Name: master_premium; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE master_premium (
    master_id integer,
    region_name text,
    calling_code text,
    "type" character varying
);


--
-- TOC entry 52 (OID 399066231)
-- Name: master_premium; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE master_premium FROM PUBLIC;
GRANT ALL ON TABLE master_premium TO hameeda;
GRANT ALL ON TABLE master_premium TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 53 (OID 403108612)
-- Name: liverate_login; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE liverate_login (
    id serial NOT NULL,
    username text,
    "password" text
) WITHOUT OIDS;


--
-- TOC entry 54 (OID 403108612)
-- Name: liverate_login; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE liverate_login FROM PUBLIC;
GRANT ALL ON TABLE liverate_login TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 55 (OID 403127691)
-- Name: customer; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE customer (
    customer_id serial NOT NULL,
    name character varying(100) NOT NULL,
    address text,
    contact_name character varying(100),
    contact_no character varying(50),
    email character varying(100),
    carrier_id integer
);


--
-- TOC entry 56 (OID 403127691)
-- Name: customer; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE customer FROM PUBLIC;
GRANT ALL ON TABLE customer TO hameeda;
GRANT ALL ON TABLE customer TO postgres;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 89 (OID 403127691)
-- Name: customer_customer_id_seq; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE customer_customer_id_seq FROM PUBLIC;
GRANT ALL ON TABLE customer_customer_id_seq TO postgres;
GRANT ALL ON TABLE customer_customer_id_seq TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 57 (OID 403127701)
-- Name: number; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE number (
    number_id serial NOT NULL,
    src character varying(50) NOT NULL,
    dst character varying(50) NOT NULL,
    customer_id integer NOT NULL,
    note character varying(100),
    consolidate integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 58 (OID 403127701)
-- Name: number; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE number FROM PUBLIC;
GRANT ALL ON TABLE number TO hameeda;
GRANT ALL ON TABLE number TO postgres;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 90 (OID 403127701)
-- Name: number_number_id_seq; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE number_number_id_seq FROM PUBLIC;
GRANT ALL ON TABLE number_number_id_seq TO postgres;
GRANT ALL ON TABLE number_number_id_seq TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 59 (OID 403127715)
-- Name: timeslice_master; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE timeslice_master (
    timeslice integer NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL
);


--
-- TOC entry 60 (OID 403127715)
-- Name: timeslice_master; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE timeslice_master FROM PUBLIC;
GRANT ALL ON TABLE timeslice_master TO hameeda;
GRANT ALL ON TABLE timeslice_master TO postgres;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 61 (OID 403127717)
-- Name: ani_bill_tracker; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE ani_bill_tracker (
    ani_bill_id integer,
    cdr_file_id integer,
    cdr_line_no integer,
    dirty integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 62 (OID 403127717)
-- Name: ani_bill_tracker; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE ani_bill_tracker FROM PUBLIC;
GRANT ALL ON TABLE ani_bill_tracker TO hameeda;
GRANT ALL ON TABLE ani_bill_tracker TO postgres;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 63 (OID 403127722)
-- Name: ani_bill; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE ani_bill (
    ani_bill_id serial NOT NULL,
    number_id integer NOT NULL,
    seconds integer NOT NULL,
    timeslice integer,
    first_call_time timestamp without time zone,
    last_call_time timestamp without time zone,
    num_calls integer,
    retail_price numeric(16,4),
    wholesale_price numeric(16,4),
    dirty integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 64 (OID 403127722)
-- Name: ani_bill; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE ani_bill FROM PUBLIC;
GRANT ALL ON TABLE ani_bill TO hameeda;
GRANT ALL ON TABLE ani_bill TO postgres;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 91 (OID 403127722)
-- Name: ani_bill_ani_bill_id_seq; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE ani_bill_ani_bill_id_seq FROM PUBLIC;
GRANT ALL ON TABLE ani_bill_ani_bill_id_seq TO postgres;
GRANT ALL ON TABLE ani_bill_ani_bill_id_seq TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 65 (OID 408365291)
-- Name: master_admin; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE master_admin (
    master_id integer,
    region_name text,
    calling_code text,
    "type" character varying
);


--
-- TOC entry 66 (OID 408365291)
-- Name: master_admin; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE master_admin FROM PUBLIC;
GRANT ALL ON TABLE master_admin TO hameeda;
GRANT ALL ON TABLE master_admin TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 67 (OID 409418764)
-- Name: ani_rate; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE ani_rate (
    ani_rate_id serial NOT NULL,
    number_id bigint,
    master_id bigint,
    effective_start_date timestamp without time zone,
    effective_end_date timestamp without time zone,
    rate numeric(15,3)
) WITHOUT OIDS;


--
-- TOC entry 68 (OID 409418764)
-- Name: ani_rate; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE ani_rate FROM PUBLIC;
GRANT ALL ON TABLE ani_rate TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 92 (OID 409418764)
-- Name: ani_rate_ani_rate_id_seq; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE ani_rate_ani_rate_id_seq FROM PUBLIC;
GRANT ALL ON TABLE ani_rate_ani_rate_id_seq TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 69 (OID 411201833)
-- Name: master_wholesale; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE master_wholesale (
    master_id integer,
    region_name text,
    calling_code text,
    "type" character varying
);


--
-- TOC entry 70 (OID 411201833)
-- Name: master_wholesale; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE master_wholesale FROM PUBLIC;
GRANT ALL ON TABLE master_wholesale TO hameeda;
GRANT ALL ON TABLE master_wholesale TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 71 (OID 420533319)
-- Name: master_temp; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE master_temp (
    master_id integer,
    region_name text,
    calling_code text,
    "type" character varying
);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 72 (OID 424044850)
-- Name: tmp_phone_numbers_80089987; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tmp_phone_numbers_80089987 (
    num character varying(100)
);


--
-- TOC entry 73 (OID 424202474)
-- Name: tmp_phone_numbers_80081004; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tmp_phone_numbers_80081004 (
    num character varying(100)
);


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 74 (OID 424374684)
-- Name: master_rsm_premium; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE master_rsm_premium (
    master_id integer,
    region_name text,
    calling_code text,
    "type" character varying
);


--
-- TOC entry 75 (OID 424374684)
-- Name: master_rsm_premium; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE master_rsm_premium FROM PUBLIC;
GRANT ALL ON TABLE master_rsm_premium TO hameeda;
GRANT ALL ON TABLE master_rsm_premium TO haifa;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 76 (OID 445164732)
-- Name: rsm_2con_wholesale_hunt; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE rsm_2con_wholesale_hunt (
    calling_code text,
    first_hunt integer,
    second_hunt integer,
    third_hunt integer,
    remark text,
    hunt_id integer DEFAULT nextval('public.rsm_2con_wholesale_hunt_hunt_id_seq'::text) NOT NULL
) WITHOUT OIDS;


--
-- TOC entry 77 (OID 445164732)
-- Name: rsm_2con_wholesale_hunt; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE rsm_2con_wholesale_hunt FROM PUBLIC;
GRANT ALL ON TABLE rsm_2con_wholesale_hunt TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 5 (OID 445164740)
-- Name: rsm_2con_wholesale_hunt_hunt_id_seq; Type: SEQUENCE; Schema: public; Owner: alicia
--

CREATE SEQUENCE rsm_2con_wholesale_hunt_hunt_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 6 (OID 445164740)
-- Name: rsm_2con_wholesale_hunt_hunt_id_seq; Type: ACL; Schema: public; Owner: alicia
--

REVOKE ALL ON TABLE rsm_2con_wholesale_hunt_hunt_id_seq FROM PUBLIC;
GRANT ALL ON TABLE rsm_2con_wholesale_hunt_hunt_id_seq TO hameeda;


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 78 (OID 450791829)
-- Name: ani_rate_temp; Type: TABLE; Schema: public; Owner: alicia
--

CREATE TABLE ani_rate_temp (
    ani_rate_id integer,
    number_id bigint,
    master_id bigint,
    effective_start_date timestamp without time zone,
    effective_end_date timestamp without time zone,
    rate numeric(15,3)
);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 79 (OID 452722843)
-- Name: temporary_xx1; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE temporary_xx1 (
    id integer,
    filename character varying(250),
    lines integer,
    loaded integer,
    locked integer,
    missing integer,
    counted bigint,
    really_missing bigint
);


--
-- TOC entry 107 (OID 314103606)
-- Name: new_cdr_idx_cdr_file_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_cdr_file_id ON new_cdr USING btree (cdr_file_id);


--
-- TOC entry 111 (OID 314103607)
-- Name: new_cdr_idx_start_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_start_time ON new_cdr USING btree (start_time);


--
-- TOC entry 112 (OID 314103608)
-- Name: new_cdr_idx_start_time_int; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_start_time_int ON new_cdr USING btree (start_time_int);


--
-- TOC entry 108 (OID 314103609)
-- Name: new_cdr_idx_cdr_type; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_cdr_type ON new_cdr USING btree (cdr_type);


--
-- TOC entry 105 (OID 314103610)
-- Name: new_cdr_idx_call_source_regid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_call_source_regid ON new_cdr USING btree (call_source_regid);


--
-- TOC entry 104 (OID 314103611)
-- Name: new_cdr_idx_call_dest; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_call_dest ON new_cdr USING btree (call_dest_regid);


--
-- TOC entry 109 (OID 320257348)
-- Name: new_cdr_idx_file_id_file_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_file_id_file_name ON new_cdr USING btree (cdr_file_id, cdr_file_line);


--
-- TOC entry 106 (OID 346797988)
-- Name: new_cdr_idx_callid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_callid ON new_cdr USING btree (callid);


--
-- TOC entry 110 (OID 346799592)
-- Name: new_cdr_idx_originator_and_noerror; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX new_cdr_idx_originator_and_noerror ON new_cdr USING btree (originator_ip, call_error_int) WHERE ((call_error_int = 0) AND ((originator_ip)::text = '80.88.247.115'::text));


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 98 (OID 349253777)
-- Name: master_id_index; Type: INDEX; Schema: public; Owner: hameeda
--

CREATE INDEX master_id_index ON master USING btree (master_id);


--
-- TOC entry 97 (OID 349264564)
-- Name: master_id_idx; Type: INDEX; Schema: public; Owner: hameeda
--

CREATE INDEX master_id_idx ON master USING btree (master_id);


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 100 (OID 349265000)
-- Name: rate_master_id_idx; Type: INDEX; Schema: public; Owner: alicia
--

CREATE INDEX rate_master_id_idx ON rate USING btree (master_id);


--
-- TOC entry 119 (OID 403127732)
-- Name: ani_bill_idx; Type: INDEX; Schema: public; Owner: alicia
--

CREATE INDEX ani_bill_idx ON ani_bill USING btree (timeslice);


--
-- TOC entry 93 (OID 264489479)
-- Name: carrier_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY carrier
    ADD CONSTRAINT carrier_pkey PRIMARY KEY (carrier_id);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 94 (OID 264489486)
-- Name: update_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "update"
    ADD CONSTRAINT update_pkey PRIMARY KEY (update_id);


--
-- TOC entry 95 (OID 264489497)
-- Name: region_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pkey PRIMARY KEY (region_id);


--
-- TOC entry 96 (OID 264489508)
-- Name: code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY code
    ADD CONSTRAINT code_pkey PRIMARY KEY (code_id);


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 99 (OID 289141238)
-- Name: master_pkey; Type: CONSTRAINT; Schema: public; Owner: hameeda
--

ALTER TABLE ONLY master
    ADD CONSTRAINT master_pkey PRIMARY KEY (master_id);


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 101 (OID 304159032)
-- Name: rate_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY rate
    ADD CONSTRAINT rate_pkey PRIMARY KEY (rate_id);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 103 (OID 313989342)
-- Name: ldr_files_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ldr_files
    ADD CONSTRAINT ldr_files_pkey PRIMARY KEY (id);


--
-- TOC entry 102 (OID 313989344)
-- Name: ldr_files_filename_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ldr_files
    ADD CONSTRAINT ldr_files_filename_key UNIQUE (filename);


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 113 (OID 340880875)
-- Name: endpoint_pkey; Type: CONSTRAINT; Schema: public; Owner: hameeda
--

ALTER TABLE ONLY endpoint
    ADD CONSTRAINT endpoint_pkey PRIMARY KEY (endpoint_id);


--
-- TOC entry 114 (OID 386211512)
-- Name: prepaid_pkey; Type: CONSTRAINT; Schema: public; Owner: hameeda
--

ALTER TABLE ONLY prepaid
    ADD CONSTRAINT prepaid_pkey PRIMARY KEY (prepaid_id);


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 115 (OID 392314189)
-- Name: Country_PK; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY country
    ADD CONSTRAINT "Country_PK" PRIMARY KEY (country_id);


--
-- TOC entry 116 (OID 403108618)
-- Name: liverate_login_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY liverate_login
    ADD CONSTRAINT liverate_login_pkey PRIMARY KEY (id);


--
-- TOC entry 117 (OID 403127697)
-- Name: customer_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (customer_id);


--
-- TOC entry 118 (OID 403127705)
-- Name: number_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY number
    ADD CONSTRAINT number_pkey PRIMARY KEY (number_id);


--
-- TOC entry 120 (OID 403127726)
-- Name: ani_bill_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY ani_bill
    ADD CONSTRAINT ani_bill_pkey PRIMARY KEY (ani_bill_id);


--
-- TOC entry 121 (OID 409418767)
-- Name: ani_rate_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY ani_rate
    ADD CONSTRAINT ani_rate_pkey PRIMARY KEY (ani_rate_id);


--
-- TOC entry 122 (OID 445164743)
-- Name: rsm_2con_wholesale_hunt_pkey; Type: CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY rsm_2con_wholesale_hunt
    ADD CONSTRAINT rsm_2con_wholesale_hunt_pkey PRIMARY KEY (hunt_id);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 123 (OID 264489488)
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "update"
    ADD CONSTRAINT "$1" FOREIGN KEY (carrier_id) REFERENCES carrier(carrier_id);


--
-- TOC entry 124 (OID 264489499)
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY region
    ADD CONSTRAINT "$1" FOREIGN KEY (update_id) REFERENCES "update"(update_id);


--
-- TOC entry 125 (OID 264489510)
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY code
    ADD CONSTRAINT "$1" FOREIGN KEY (region_id) REFERENCES region(region_id);


--
-- TOC entry 126 (OID 314089512)
-- Name: $1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY new_cdr
    ADD CONSTRAINT "$1" FOREIGN KEY (cdr_file_id) REFERENCES ldr_files(id);


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 127 (OID 403127707)
-- Name: f1; Type: FK CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY number
    ADD CONSTRAINT f1 FOREIGN KEY (customer_id) REFERENCES customer(customer_id);


--
-- TOC entry 128 (OID 403127728)
-- Name: f1; Type: FK CONSTRAINT; Schema: public; Owner: alicia
--

ALTER TABLE ONLY ani_bill
    ADD CONSTRAINT f1 FOREIGN KEY (number_id) REFERENCES number(number_id);


SET SESSION AUTHORIZATION 'postgres';

--
-- TOC entry 3 (OID 2200)
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 8 (OID 264489476)
-- Name: COLUMN carrier."type"; Type: COMMENT; Schema: public; Owner: alicia
--

COMMENT ON COLUMN carrier."type" IS 'C means Carrier
O means Other';


SET SESSION AUTHORIZATION 'hameeda';

--
-- TOC entry 17 (OID 289141229)
-- Name: COLUMN master."type"; Type: COMMENT; Schema: public; Owner: hameeda
--

COMMENT ON COLUMN master."type" IS 'National or International calling code. Partial (only region) for national codes.
I= International, N= National';


SET SESSION AUTHORIZATION 'alicia';

--
-- TOC entry 20 (OID 304159026)
-- Name: COLUMN rate.billing_increment; Type: COMMENT; Schema: public; Owner: alicia
--

COMMENT ON COLUMN rate.billing_increment IS 'in seconds';


--
-- TOC entry 21 (OID 304159026)
-- Name: COLUMN rate.minimum_duration; Type: COMMENT; Schema: public; Owner: alicia
--

COMMENT ON COLUMN rate.minimum_duration IS 'in seconds';


--
-- TOC entry 22 (OID 304159026)
-- Name: COLUMN rate.customer_carrier_id; Type: COMMENT; Schema: public; Owner: alicia
--

COMMENT ON COLUMN rate.customer_carrier_id IS ' carrier_id of customer carrier';


--
-- TOC entry 23 (OID 304159026)
-- Name: COLUMN rate.validity; Type: COMMENT; Schema: public; Owner: alicia
--

COMMENT ON COLUMN rate.validity IS 'Active -A
Deleted -D';


--
-- TOC entry 24 (OID 304159026)
-- Name: COLUMN rate.effective_end_date; Type: COMMENT; Schema: public; Owner: alicia
--

COMMENT ON COLUMN rate.effective_end_date IS 'Effective date when the rate becomes Invalid';


