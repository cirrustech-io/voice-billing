#! /usr/bin/php4 -c/etc/data_copy/php.ini
<?php
	declare (ticks = 1);
	require_once ("config.inc.php");
	require_once ("db.inc.php");
	require_once ("loader_functions.inc.php");
	require_once ("handle_abort.inc.php");


$files_to_load = Array();

function cdr_load_callback ($dt) {
	global $files_to_load;
	global $new_files;
	$data = $dt;
	cdr_load_file_info ($data,1);
	if ($data['newfile']===TRUE) {
		$new_files++;
		$files_to_load[]=&$data;
		echo "{$data['filename']}";
		if ($data['filesize']==0) {
			echo " EMPTY";
		} else {
			echo " {$data['md5sum']} {$data['filesize']} bytes,   {$data['lines']} lines ";
		}
		echo "\n";
	}
	
}

	$cdr_mask = "*201602*.CDR";
	
	$cdr_mask_reg = $cdr_mask;
	$cdr_mask_reg = preg_replace ("/\\./","\\.",$cdr_mask_reg);
	$cdr_mask_reg = preg_replace ("/\*/",".*",$cdr_mask_reg);
	$cdr_mask_reg = "^$cdr_mask_reg$";
	
	echo "Mask: $cdr_mask\n";
	echo "Mask: $cdr_mask_reg (regex)\n";
	while (!$user_termination) {
		
		unset ($files_to_load);
		$files_to_load = Array();
		$new_files=0;

		echo "Locating files\n";
//		cdr_finder_main_loop ($cdr_file_paths,"/^.*\.CDR$/","cdr_load_file_info");
		
		cdr_finder_main_loop ($cdr_file_paths,"/$cdr_mask_reg/","cdr_load_callback");
		if ($new_files>50) {
			echo "Found $new_files files, not loading now, letting the background job deal with them\n";
		} elseif ($new_files>0) {
			$pl="";
			if ($new_files>1) $pl="s";
			echo "Found $new_files file$pl, loading NOW\n";
			foreach ($files_to_load as $f) {
//				echo "would load [ {$f['fileid']} \n";
//				load_cdr_file($f['fileid']);
			}
		}
		echo "Sleeping";
		for ($i=$finder_sleep_time;$i && !$user_termination;$i--) {
			sleep (1);
			echo "\rSleeping $i ";
		}
		echo "\r              \r";
	}
	
?>
