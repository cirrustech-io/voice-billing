<?php
//check if the session is still alive
include_once('DBAccessor.php');
applyIPFilter();

	session_start();
	
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	

	//read carriers and destinations
	$rows_carriers = getAllLiveCarriers();
	//$rows_destinations = getDestinations();

	if($rows_carriers->EOF){
		echo "<p id=\"errorText\">Oh-oh billing db in walker is acting funny. Developer defends code. Please contact ext. 101</p>";
	}
//session check ends here
   
	$self = $_SERVER["PHP_SELF"];
	$error = "";
	$carriers=$_POST['carrier_list'];
	
	
	if(isset($_POST['flag'])){
		
		// Where the file is going to be placed 
		$target_path = "csvs/";
		
		/* Add the original filename to the target path.  
		Result is "csvs/filename.extension" */
		$target_path = $target_path . basename( $_FILES['uploadedfile']['name']); 

		$file= $_FILES['uploadedfile']['name'];
		$fils=$_POST['fils'];
		$extention=strrchr($file,".");

		//echo $_FILES['uploadedfile']['tmp_name'];
		
		if ($extention==".csv")
		{	
			if(!move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path))
				$error = "<p id=\"errorText\">There was an error uploading the file, please try again!</p>";
	
			if ($carriers<>"")
				$carriers="carrier_list[]=" . implode("&carrier_list[]=",$_POST['carrier_list']);
			
			$prof=$_POST['prof'];
			$charges=$_POST['charges'];
			$access=$_POST['access'];
			header("Location:calculateMinutes.php?$carriers&charges=$charges&access=$access&prof=$prof"."&uploadedfile=$file");
				
		
		}
		elseif($extention=="")
			$error .= "<p id=\"errorText\">Please select a file<p/><br/>";
		else 
		{$error .= "<p id=\"errorText\">Incorrect file type, please use Csv files</p><br/>";
		unlink($_FILES['uploadedfile']['tmp_name']);}
		//should i clean the temp folder?
			
		
	}	
 ?>

<html>
<head>
<title>Minutes per Code Application</title>
</head>

<body id="pageText">
<?php include('Menu_Header.php');?>
<br/><br/>

<form enctype="multipart/form-data" action="<?php echo $self; ?>" method="POST">
<b>THIS APPLICATION IS STILL UNDER TESTING, we highly recommend cross checking your results!</b><br /><br />
<b>Choose a file to upload(CSV only), which contains a list of codes and the minutes dialed through it:</b><br /><input name="uploadedfile" type="file" /><br /><br/>
<b>Suppliers to attach:</b>
<br/>
	<select name="carrier_list[]" id="carrier_list" multiple="multiple" MULTIPLE SIZE=7>	
 				<?php
						$rows_carriers->MoveFirst();
						while(!$rows_carriers->EOF){
							$carrier_id = $rows_carriers->Fields('carrier_id');
							$carrier_name = $rows_carriers->Fields('carrier_name');
							echo "<option id=$carrier_name value=$carrier_name>";
							echo $rows_carriers->Fields('carrier_name');
							echo "</option>";
							$rows_carriers->MoveNext();
						}
					?>
	</select>

<input Name="flag" type="hidden" value="1"><br/>
<table>
	<tr>
		<td><b>Access ratio(example, 1.4):</b></td>
		<td><input type="text" name="access" value=""></td>
	</tr>
	<tr>
		<td><b>profit percentage(example, 10):</b> </td>
		<td><input type="text" name="prof" value="">%</td>
	</tr>
	<tr>
		<td><b>Extra minutes ratio(example, 1.3):</b></td>
		<td><input type="text" name="charges" value=""></td>
	</tr>
<table>
<p>If left empty, values default to the examples above</p>
<br/><br/>
<input type="submit" value="Upload File and Create Csv" />
</form>
<?php echo $error; ?>
<br/>
<?php include('Page_Footer.php');?>
</body>
</html>
