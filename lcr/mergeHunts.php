<?php
	
	include_once('DBAccessor.php');
	
	applyIPFilter();
	
	//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	
	//session check ends here

	//big tasks can timeout!
	set_time_limit(100);

	if(!array_key_exists('hunts',$_GET)){
		echo "<br><p id=\"errorText\">Security Error, stop trying to crash the application!</p>"; 
		exit(0);
	}
	
	echo "<html><head><title>Results for Hunts for codes Application</title></head>";
	echo "<body id='pageText'>";
	include('Menu_Header.php');
        
	$carriers=$_GET['carrier_list'];
	$countries=$_GET['country_list'];
	$hunts=$_GET['hunts'];
	$fils=$_GET['fils'];
	
	//check before proceeding
	if($hunts>countCarriers()){
		echo "<p id=\"errorText\">Security error in number of carriers! Please don't attempt to flood the application</p>";
		include('Page_Footer.php');
		exit(0);
	}
	$NumCarriers=count($carriers);
	if ($hunts>$NumCarriers&&($NumCarriers<>0)){
		echo "<p id=\"errorText\">You have selected $hunts hunts, but only $NumCarriers Carriers from the list.<br/>click <a href=\"Hunts_for_codes.php\">here</a> to go back</p>";
		include('Page_Footer.php');
		exit(0);
	}

	
	//get the destinations for the selected countries
        $result=getTop20($countries);
        $top20= array();
        while ($line = pg_fetch_array($result, null, PGSQL_ASSOC))
                $top20[] = $line;

	
	//get the rates we need
	$result=fetchSupplierRatesTable($carriers,$fils);
	$supprates= array();
 	while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)){	
		foreach($top20 as $t20){
			if(preg_match('/^'. $t20['code'] . '\d*/',$line['calling_code'])){
				$supprates[] = $line;
				break;
			}
		}
	}
 	
  	

	 //*** HORRIBLE fix for BT private mobile codes - 9190,9191, 9192,9193,9195,9196,9197,9198, 9199 ****//
        #Find BT rates for the main private mobile codes based on subcodes

        if(in_array('BT',$carriers)){
                //echo "<br> Entered Horrible<br>";
                $supprates_temp =  $supprates;
                $BT_India_Private_Mobile = array('9190' => 1000,
                                                '9191' => 1000,
                                                '9192' => 1000,
                                                '9193' => 1000,
                                                '9195' => 1000,
                                                '9196' => 1000,
                                                '9197' => 1000,
                                                '9198' => 1000,
                                                '9199' => 1000
                                        );
                foreach($supprates_temp as $line){

                        if(($line['supplier'] == 'BT') && preg_match("/^919[0-3|5-9]/",$line['calling_code'])) {
				
                                $pvt_mob_pfx = substr($line['calling_code'],0,4);
                                $BT_India_Private_Mobile[$pvt_mob_pfx] = ($line['rate'] <  $BT_India_Private_Mobile["{$pvt_mob_pfx}"])? $line['rate'] : $BT_India_Private_Mobile["{$pvt_mob_pfx}"];
                  
	
			}

                }
			
 		foreach($BT_India_Private_Mobile as $code => $rate){
                        if($rate <> 1000){
                                $found = false;
                                foreach($supprates_temp as $idx=>$line){
                                        if($line['supplier'] == 'BT' && $line['calling_code'] == $code){
                                                
						$supprates[$idx]['rate'] = $rate;
                                                $found = true;
                                                break;
                                        }
                                }
                                if(!$found){
                                        $insert = array();
                                        $insert['calling_code'] = $code;
                                        $insert['supplier'] = 'BT';
                                        $insert['rate'] = $rate;
                                        $supprates[] = $insert;
                                }
                        }
                }

		#Sort array by calling code desc order
                $supprates = SortArray($supprates,'calling_code','DESC');
         }

	 /*** Horrible fix ends ***/



 	//if no carriers selected , process and print for all carriers
	if($carriers==""){
		
		$carriersGet=getAllLiveCarriers();
		$carriersGet->MoveFirst();
		
		while(!$carriersGet->EOF){
			$carriers[] .= $carriersGet->Fields('carrier_name');
			$carriersGet->MoveNext();
		}
	}
	


 	
	$result = array();	
 	foreach($top20 as $idx=>$line)
	{	
		$huntsRay=array();
			
 		$result[$idx]['code']= $line['code'];
 		$result[$idx]['country']= $line['country'];
 		$result[$idx]['description'] =  $line['description'];	 	
 		for($x=0;$x<$hunts;$x++){
 			$huntsRay[$x][0]=1000000;
 			$huntsRay[$x][1]='';
 		}
 		
 		foreach($supprates as $tablecode){
 			if(preg_match('/^' . $tablecode['calling_code']   . '\d*$/',$line['code'])){
 				
				//to get out if supplier was already found earlier
 				$flag =false;
	 			foreach($huntsRay as $huntsR){
	 				if($huntsR[1] == $tablecode['supplier'])
	 					$flag = true;
	 			}
	 			if($flag)
	 				continue;
 				
	 			for($x=0;$x<$hunts;$x++){
		 			if($tablecode['rate']<$huntsRay[$x][0]){
		 				array_splice($huntsRay,$x,0,array(array($tablecode['rate'],$tablecode['supplier'])));
		 				break;
		 			}
	 			}
 			}
 		}
 		
 		$result[$idx]['hlist'] = array_slice($huntsRay,0,$hunts);		
 	}
 	
 	
 	$fils=($fils=="on")?$fils="(Fils)":"($)";
 	echo"<br/><br/><table id=\"results\" border=1><tr>";
 	echo "<th>Code</th><th>Country</th><th>Description</th>";
 	$str.="Code,Country,Description,";
 	for($x=1;$x<$hunts+1;$x++)
 	{echo "<th>Hunt #$x</th><th>Hunt #$x's Rate$fils</th>";
 	$str.="Hunt #$x,Hunt #$x's Rate$fils,";}
 	$str.="\r\n";
 	echo "</tr>";

 	$color=0;
	 	
 	foreach($result as $code){
		
		//select the appropriate color
		if ($color==0) 
			{echo "\t<tr bgcolor=\"lightgrey\" >\n";}
		else
			{echo "\t<tr bgcolor=\"white\" >\n";}
 		echo "<td>{$code['code']}</td>";
 		echo "<td>{$code['country']}</td>";
		echo "<td>{$code['description']}</td>";
 		$str.="{$code['code']},{$code['country']},{$code['description']},";
 			foreach($code['hlist'] as $hunts)
 		 		if($hunts[1]=="")
 		 		{	$str.="N/A,N/A,";
 			 		echo "<td>N/A</td><td>N/A</td>";}
 		 		else
 		 			{$str.="{$hunts[1]},{$hunts[0]},";
 		 			echo "<td>{$hunts[1]}</td><td>{$hunts[0]}</td>";}
 		$str.="\r\n";
 		echo"</tr>";
 		$color=ABS($color-1);
	}
 		
 	echo"</table>";
 	
 	$date=date("Y-m-d_H-i-s");
 	$file="Hunts_$date.csv";
 	$ofile = fopen("output/$file",'w');
	fwrite($ofile,$str);
 		 
	 echo "<br/><br/>
	<form method=\"POST\" action=\"Download_Attacher.php?file=$file\">
	<input type=\"submit\" id=\"button\" value=\"Download File\">
	</form></body>";
	include('Page_Footer.php');
	echo "</html>";	
?>

<?php

	 function SortArray($ARR,$field_name,$order){


                //prepare field that is to be sorted
                $field = array();
                foreach($ARR as $key=>$row){
                        $field[$key]=$row[$field_name];
                }

                //sort multi-dimensional array, based on field name in the desired order
                $result = ($order=='ASC')?array_multisort($field,SORT_ASC,$ARR):array_multisort($field,SORT_DESC,$ARR);

                if($result){
                        return $ARR;
                } else {
                        echo "\nCould not sort invoice\n";
                        exit(0);
                }
        }


?>
