<link rel="stylesheet" href="style.css" type="text/css" />
<script type="javascript/text" src="mootools.js"></script>
<script type="javascript/text">
/* Tips 2 */
var Tips2 = new Tips($$('.Tips2'), {
	initialize:function(){
		this.fx = new Fx.Style(this.toolTip, 'opacity', {duration: 500, wait: false}).set(0);
	},
	onShow: function(toolTip) {
		this.fx.start(1);
	},
	onHide: function(toolTip) {
		this.fx.start(0);
	}
});

</script>

<h3>Tips 2</h3>
<img src="" alt="mooCow" class="Tips2" title="Tips Title :: This is my tip content" />
<div class="tool-tip" style="position: absolute; top: 528px; left: 294px; visibility: visible;">
<div>
<div class="tool-title">
</div>
<div class="tool-text">
</div>
</div>
</div>

