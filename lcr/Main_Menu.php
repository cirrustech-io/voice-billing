<?php
//check if the session is still alive
	include_once('DBAccessor.php');
	applyIPFilter();
	//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	
	
//session check ends here ?>

<HTML>
<head><title>Web application services main menu</title></head>
<body>
<?php include('Menu_Header.php');?>
<br/>
<br/>
<table align=center id="mainMenu" border=1>
<tr><th>Application Name</th><th>Last Updated</th><th>Status</th></tr>
<?php
$result=fetchLCRUpdates();
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
	
	if ($line['lastupdate']==null)
		$update="Live";
	else
		$update=$line['lastupdate'];
		
	$exists=tableExists($line['tablename']);
	echo "<tr>";
	//echo "<td>{$line['tablename']}</td>";
	echo "<td>{$line['description']}</td>";
	echo "<td>$update</td>";
	if ($exists==1)
		echo "<td><b><font color=\"green\">Avalible</font></b></td>";
	else
		echo "<td><b><font color=\"Red\">DOWN</font></b></td>";
	echo "</tr>";
}
	echo "</table>";
?>
<br/>
<?php include('Page_Footer.php');?>
</body>
</HTML>
