<?php

session_start();

if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }

if(array_key_exists('p1',$_GET) && array_key_exists('p2',$_GET) && array_key_exists('p3',$_GET) && array_key_exists('p4',$_GET) ){

	//DBA Accessor
	require_once('DBAccessor.php');
	// Printing results in HTML
	echo "<html>\n";
	echo "<head>\n";
	echo "<title>Results</title>";
	echo "<link rel='STYLESHEET' type='text/css' href='webpage_stylesheet.css'/>\n";
	echo "</head><body id=\"pageText\" >\n";

	include ('Menu_Header.php');

	echo "Searching....<br>";

	$cat = substr($_GET['p1'],2,strlen($_GET['p1'])-4);
	$when= date('Y-m-d',strtotime(substr($_GET['p3'],2,strlen($_GET['p3'])-4)));
	$who = substr($_GET['p2'],2,strlen($_GET['p2'])-4);
	$where = substr($_GET['p4'],2,strlen($_GET['p4'])-4);


	$rows_coolness= getRatesCoolness($cat,$who,$when,$where);


	$file_str="";

	if((pg_num_rows($rows_coolness) > 0)){


		echo "Retrieved results....printing...<br><br>";
		echo "<table  id=\"results\" border=\"1\">\n";

			//header info
			echo "\t<tr class=header>\n";
				echo "\t\t<td>Calling Code</td>\n";
				echo "\t\t<td>Region Name</td>\n";
				echo "\t\t<td>Supplier</td>\n";
				echo "\t\t<td>Customer</td>\n";
				echo "\t\t<td>Rate (USD)</td>\n";
				echo "\t\t<td>Effective Date</td>\n";
			echo "\t</tr>\n";


			$file_str="Calling Code,Region Name,Supplier,Customer, Rate(USD),Effective Date\r\n";

		$prev="";
		$row_style=1;

		while ($line = pg_fetch_array($rows_coolness, null, PGSQL_ASSOC)) {
			$curr= $line['calling_code'];
			if(isRowStyle0($curr,$prev,$row_style)){
				echo "\t<tr class=d0>\n";

				$row_style=0;
			}
			else {
				echo "\t<tr class=d1>\n";

				$row_style=1;
			}

		        echo "\t\t<td>{$line['calling_code']}</td>\n";
		        echo "\t\t<td>{$line['region_name']}</td>\n";
		        echo "\t\t<td>{$line['supplier']}</td>\n";
		         echo "\t\t<td>{$line['customer']}</td>\n";
		        echo "\t\t<td>{$line['rate']}</td>\n";
		        $effective_date=prepareEffectiveDate($line['effective_date']);
		        echo "\t\t<td>{$effective_date}</td>\n";


			$file_str .= "{$line['calling_code']},{$line['region_name']},{$line['supplier']},{$line['customer']},{$line['rate']},{$effective_date}\r\n";
			

		    echo "\t</tr>\n";
		    $prev= $line['calling_code'];
		}

		echo "</table>\n";

	}
	else {
		echo "<br><p id=\"errorText\"> No results found</p>";
		$file_str = "No Results Found\r\n";
	}


		
	 $date=date("Y-m-d_H-i-s");
	 $file="LiveRates_$date.csv";
	 $ofile = fopen("output/$file",'w');
	 fwrite($ofile,$file_str);

	echo "<br/><br/>
	<form method=\"POST\" action=\"Download_Attacher.php?file=$file\">
	<input type=\"submit\" id=\"button\" value=\"Download CSV File\">
	</form>";



	include('Page_Footer.php');
	echo "</body></html>";
}

else {
 	echo "<p id=\"errorText\">Oh-oh request not received. Please go <a href=\"ViewRates.php\">back</a> and try again</p>";
} 

	function prepareEffectiveDate($effective_date){
	 $effective_date=str_replace('00:00:00','', $effective_date);
     $effective_date=str_replace('12:00:00','', $effective_date);
     return date('d-m-Y',strtotime($effective_date));

}

	function isRowStyle0($curr,$prev,$row_style){
	if($curr==$prev){
		if($row_style==0)
			return true;
		else
			return false;
	}
	else {
		if($row_style==0)
			return false;
		else
			return true;
	}
}

?>
