<?php

	include_once('DBAccessor.php');

	session_start();

	//if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }

	// build the form action
	$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

	//read carriers and destinations
	$rows_carriers = getAllLiveCarriers();
	$rows_destinations = getDestinations();


	$error_msg='';

	if($rows_carriers->EOF || $rows_destinations->EOF){
		$error_msg = "Oh-oh billing db in walker is acting funny. Developer defends code. Please contact ext. 165";
	}



	if ((isset($_POST["doodoo"])) && ($_POST["doodoo"] == "weewee")) {
		$go_ahead = true;

		$p1 = $_POST['category_list'];
		$p2 = $_POST['carrier_list'];
		$p3 = $_POST['date_field'];
		$p5 = $_POST['dest_type'];
		if($p5 == 'country'){
			$p4 = $_POST['destination_list'];
		} else {
			$p4 = $_POST['calling_code'];
			if(!preg_match('/^[1-9]{1}\d*$/',$p4))
				$go_ahead=false;
				$error_msg = "Invalid dialcode. Only numeric values allowed";

		}

		session_write_close();

		if($go_ahead)
		$goto= "Location: ViewRatesResult.php?p1='".$p1."'&p2='".$p2."'&p3='".$p3."'&p4='".$p4."'";
		header($goto);

	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>View Rates Request</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="STYLESHEET" type="text/css" href='calendar-blue2.css'/>
	<script type="text/javascript" src="calendar.js"></script>
	<script type="text/javascript" src="lang/calendar-en.js"></script>
	<script type="text/javascript" src="calendar-setup.js"></script>


	<script type="text/javascript">

		function hide(s) {
			document.getElementById(s).style.visibility = "hidden";
		}

		function show(s) {
			document.getElementById(s).style.visibility = "visible";
		}
	</script>


</head>

<body id="pageText">
	<?php include('Menu_Header.php'); ?>
	<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
	 	<table>
	 		<tr>
		 		<td>Category</td>
		 		<td>
					<select name="category_list" id="category_list">
						<option id="CC" value="CC">Customer Rates</option>
						<option id="CS" value="CS" selected>Supplier Rates</option>
					</select>
		 		</td>
		 		
	 		</tr>

	 		<tr>
		 		<td>Carrier</td>
				<td>
					<select name="carrier_list" id="carrier_list">
					<?php
						$rows_carriers->MoveFirst();
						while(!$rows_carriers->EOF){
							$carrier_id = $rows_carriers->Fields('carrier_id');
							$carrier_name = $rows_carriers->Fields('carrier_name');
							echo "<option id=$carrier_id value=$carrier_id>";
							echo $rows_carriers->Fields('carrier_name');
							echo "</option>";
							$rows_carriers->MoveNext();
						}
					?>
						<option id="ALL" value="ALL">ALL</option>
		 			</select>
		 		</td>
	  		</tr>

	  		<tr>
		 		<td>Search By</td>
		 			<td>
						<select name="dest_type" id="dest_type"
						onChange=" i=this.options[this.selectedIndex].value; if(i=='country'){ show('div_dest'); hide('div_code'); };if(i=='dial_code'){ hide('div_dest'); show('div_code') }">
							<option id=country value=country selected>Country</option>
							<option id=dial_code value=dial_code>Dial Code</option>
						</select>

		 		</td>


			</tr>
			<tr>
				<td></td>
				<td>
					<div id="div_dest">
					Country list: <select name="destination_list" id="destination_list" >
					<?php
						$rows_destinations->MoveFirst();
						while(!$rows_destinations->EOF){
							$country_code = $rows_destinations->Fields('country_code');
							$country_name = $rows_destinations->Fields('country_name');
							echo "<option id=$country_code value=$country_code>";
							echo "$country_name--$country_code";
							echo "</option>";
							$rows_destinations->MoveNext();
						}
					?>
						<option id=ALL value=ALL selected>ALL</option>
		 			</select>
		 			</div>
					<div id ="div_code" style="visibility:hidden">
					Starts with: <input name="calling_code" id="calling_code" type =text size=10>
					</div>

		 		</td>
	  		</tr>

	  		<tr>
	  			<td>Effective date</td>
	  			<td><input type="text" id="date_field" name="date_field" readonly size="10" value=<?php echo date('m/d/Y'); ?> /><button id="trigger" class="btn">...</button>
	  			</td>
	  			<td></td>
	  		</tr>
			<br>
	  		<tr>
	  			<td colspan="3">
					<p id=\"errorText\"><?php echo $error_msg ?></p>
	  				<input type="hidden" name="doodoo" value="weewee"/>
	  				<input type="submit" name="Submit" value="Get Rates"/>
	  			</td>
	  		</tr>
		</table>
	</form>

	<script type="text/javascript">
		  Calendar.setup(
		    {
		      inputField  : "date_field",   // ID of the input field
		      ifFormat    : "%m/%d/%Y",    // the date format
		      button      : "trigger"       // ID of the button
		    }
		  );
	</script>
	<?php include('Page_Footer.php'); ?>
</body>
</html>
