<?php
	include('DBAccessor.php');
	$ip_address = $_SERVER['REMOTE_ADDR'];
	applyIPFilter();
	session_start();
	
	$_SESSION['user_name'] = "";
	
	// build the form action
	$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

	$error_message = "";

	
	if ((isset($_POST["doodoo"])) && ($_POST["doodoo"] == "weewee")) {
		$username = trim($_POST['txt_username']);
		$password = trim($_POST['txt_password']);
		
		//simple flag for empty credentials
		$ok=1;
		
		if($username=="")
			{$error_message .= "Please enter your username <br/>";$ok=0;}
		if($password=="")	
			{$error_message .= "Please enter your password <br/>";$ok=0;}
		
		if($ok)
		{//checks if the username and password are correct
		$login_result=checkPassword($username,$password);

		if($login_result->EOF)
			{
				session_write_close();
				$error_message = "<p id=\"errorText\">Login failed. Please try again.</p>";
			}
		elseif($login_result==null)
				$error_message = "<p id=\"errorText\">Your credentials may not contain special characters</p>";
		else
			{
				print_r( $_POST );
				$_SESSION['user_name'] = $_POST['txt_username'];			
				session_write_close();
				header("Location: Main_Menu.php");			
				exit;
			}
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>LCR Web App Login Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
	<style>
		input.btn
		{
			background-color:#000000;
		   	border:1px solid;
		   	border-top-color:#8FC4E8;
		   	border-left-color:#8FC4E8;
		   	border-right-color:#8FC4E8;
		   	border-bottom-color:#8FC4E8;
		   	cursor:pointer;
			cursor:hand;
			font-family:calibri;
			font-size:12pt;
			color:white;
		}

		input
		{
			font-family:calibri;
			font-size:12pt;
			color:black;
		}

		body
		{
			font-family:calibri;
			font-size:12pt;
			color:black;

		}
	</style>
</head>

<body>
	<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
	<table border=0>
		<tr>
	 	<table bgcolor="Silver" cellpadding="5" cellspacing="0"">
	 		<tr>
		 		<td>User Name</td>
		 		<td>
					<input type="text" id="txt_username" name="txt_username" size="25"/>
		 		</td>
	 		</tr>
	 		<tr>
		 		<td>Password</td>
		 		<td>
					<input type="password" id="txt_password" name="txt_password" size="25"/>
		 		</td>
	 		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<br>
	  				<input type="hidden" name="doodoo" value="weewee"/>
	  				<input type="submit" name="Submit" value="Login" class="btn"/>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<font size="-1" color="Maroon"><i><?php echo $error_message ?></i></font>
	  			</td>
	  		</tr>
		</table>
		</tr>
	</table>
	</form>
<br/>
<?php
echo 'Your IP address is:<a style="color: Red;"> ' . $ip_address . '</a>';
?>
</body>
</html>
