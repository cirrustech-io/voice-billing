<?php
//check if the session is still alive
	include_once('DBAccessor.php');
	applyIPFilter();
	//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	
	
//session check ends here


	if(!array_key_exists('uploadedfile',$_GET)){
		echo "<br><p id=\"errorText\">Security Error!</p>"; exit(0);
	}	
	
	
	//CSVParser
	include_once('CSVParser.php');

	//big files can cause timeouts!
	set_time_limit(1000);
	
include('Menu_Header.php');

$carriers=array();
$carriers=$_GET['carrier_list'];
$files=$_GET['uploadedfile'];
$prof=$_GET['prof'];
$charges=$_GET['charges'];
$access=$_GET['access'];
$extention=strrchr($files,".");


//defaults
if($prof==='')
	$prof=floatval(0);
else
	$prof=floatval($prof);
if($charges==='')
	$charges=floatval(1);
else
	$charges=floatval($charges);
if($access==='')
	$access=floatval(1);
else
	$access=floatval($access);

//hardcode for now
$AccessRates=array("Batelco Rounting"=>7.8);

if ($files=="")
	{echo "<p id=\"errorText\">No file names where entered</p>";
	exit(0);}
	
echo "***Reading Supplier Rates***<br/>";
$result=fetchSupplierRatesTable($carriers,"on");

$supprates= array();
 while ($line = pg_fetch_array($result, null, PGSQL_ASSOC))	
 	$supprates[] = $line;

if($carriers=="")
	{$carriersGet=getAllLiveCarriers();
	$carriersGet->MoveFirst();
	while(!$carriersGet->EOF)
		{$carriers[] .= $carriersGet->Fields('carrier_name');
		 $carriersGet->MoveNext();}
	}


	$FilesRay[]=$files;



$parser = new CsvFileParser();


foreach($FilesRay as $tempfile){
	$current=$folder.$tempfile;
	echo "***Working on $current***<br/>";
	
	$country = $parser->ParseFromFile("csvs/".$current);
	$Odir="output/";
	
	$ofile = fopen("$Odir$tempfile",'w');
	
	$output = array();
	

	$output[].="Code,Traffic Minutes,";
	for($x=0;$x<count($carriers);$x++)
		{$y=$x+1;
		$output[]="Hunt {$y}, Cost {$y} (fils),";}
		
	$output[]="Average(Hunt#1 & #2),%Contribution,\r\n";
	
	$totMin=0;
	$totTermination=0;
	
	foreach($country as $row)
		if($row[0] != '')
			$totMin+=(double)(str_replace(",","",$row[1]));

	foreach($country as $row){
		$Min=str_replace(",","",$row[1]);
		$CarrierCost=array();
		if($row[0] == ''){
			$comma=",";
			for($x=0;$x<count($carriers);$x++)
				$comma.=",";
			$comma.="\r\n";
			$output[].=$comma;
			}
			
		else{
			//echo "searching for $row[0]\n";
			//echo "count :" . count($carriers) . "<br>"; 
			//exit(0);
			for($x=0;$x<count($carriers);$x++)
				{$CarrierCost[$x][0]='';
				$CarrierCost[$x][1]=1000000000;}
			foreach($supprates as $line){
			 	$flag =false;
	 			foreach($CarrierCost as $hray){
	 				if($hray[0] == $line['supplier'])
	 					$flag = true;
	 			}
	 			if($flag)
	 				continue;
					
				if(preg_match('/^' . $line['calling_code']   . '\d*$/',$row[0])){
					//echo "{$line['supplier']} matched {$line['calling_code']} with {$row[0]} <br/>";
						for($x=0;$x<count($carriers);$x++)
						{	
							if (($carriers[$x]==$line['supplier'])){
								$CarrierCost[$x][0]=$line['supplier'];
								$CarrierCost[$x][1]=$line['rate'];
								
								continue;
							}
						}
				}
				
			}
			
			$str = "$row[0],$Min,";
			$CarrierCost=SortArray($CarrierCost,1,"ASC");
			for($x=0;$x<count($carriers);$x++)	
				$str .=($CarrierCost[$x][1]==1000000000)?"N/A,N/A,":"{$CarrierCost[$x][0]},{$CarrierCost[$x][1]},";
			
			
			if(($CarrierCost[0][1])&&($CarrierCost[1][1]))
				$avg=(($CarrierCost[0][1]+$CarrierCost[1][1])/2);
			elseif($CarrierCost[0][1])
				$avg=$CarrierCost[0][1];
			else
				{echo "Your CSV sheet contains the invalid code '{$row[0]}', please review this code";
				 exit();} //i should be doing this earlier on in the code to save processing power, but its a mess there
			
			$str.=$avg.",";
			$PCon=(double)($Min)/$totMin;
			$weight=$PCon*$avg;
			$totTermination+=$weight;
			$PCon*=100;
			$str.=$PCon.",";
			$str .= "\r\n";
			$output[] .= $str;
		}
	}		

			$output[].="Total Minutes,Average Cost,Extra Minutes Ratio,Profit %,Access Ratio,\r\n";
			$output[].="$totMin,$totTermination,$charges,$prof,$access,\r\n\r\n";
			$output[].="From,Access Cost,Termination,Total Cost,\r\n";
			
			foreach($AccessRates as $name=>$Arate)
			{	
				$TotAccess=$Arate*$access;
				$profitRatio=(100-$prof)/100;
				$BreakEven=($totTermination+$TotAccess);
				$Retail=$BreakEven/$profitRatio;
				$RetailMin=1000/$Retail;
				$WCharges=($RetailMin*$charges);
				$IVR=1000/$WCharges;
				$output[].="{$name},{$Arate},{$totTermination},{$BreakEven},\r\n";
			}

			
	//print_r($output);
	foreach ($output as $o){
		fwrite($ofile,$o);
	}

	fclose($ofile);
	echo "***$current Done***<br/>";
	
	
}

unlink("csvs/$files");

echo"
<form method=\"POST\" action=\"Download_Attacher.php?file=$files\">
<input type=\"submit\" id=\"button\" value=\"Download File\">
</form>";

include('Page_Footer.php');

?>


<?php

	 function SortArray($ARR,$field_name,$order){


                //prepare field that is to be sorted
                $field = array();
                foreach($ARR as $key=>$row){
                        $field[$key]=$row[$field_name];
                }

                //sort multi-dimensional array, based on field name in the desired order
                $result = ($order=='ASC')?array_multisort($field,SORT_ASC,$ARR):array_multisort($field,SORT_DESC,$ARR);
				
                if($result){
                        return $ARR;
                } else {
                        echo "\nCould not sort invoice\n";
                        exit(0);
                }
        }


?>

