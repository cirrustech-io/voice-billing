<?php
//code is being cleaned, excuse it
	include_once('DBAccessor.php');

	applyIPFilter();

	//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	
	

	//page header and options
	echo "<html><head><title>LCR Results</title><body>";

	include('Menu_Header.php');

	if(!array_key_exists('type',$_GET) || !array_key_exists('item',$_GET)){
		echo "<br><p id=\"errorText\">Security Error!</p>"; exit(0);
	}	

	//get the searched item
	$item=$_GET['item'];

	//store the type
	$type=$_GET['type'];

	//store if exact option was selected
	$exact=$_GET['EXACT'];
	
	//get the fils option
	$fils=$_GET['FILS'];
	
	//get the carriers
	$carriers=$_GET['carrier_list'];

//try the query and give an error message if it fails
$result=fetchLCRTable($type,$exact,$carriers,$item,$fils);

$fils=($fils=="on")?$fils="(Fils)":"($)";
// Printing the HTML table header
echo "<br/><br/><table  id=\"results\" border=\"1\">";
echo "<tr>";
echo "<th>Calling Code</th>";
echo "<th>Region Name</th>";
echo "<th>Supplier</th>";
echo "<th>Lowest Rate$fils</th>";
echo "<th>Effective Date</th>";
echo "</tr>";

$str="Calling Code,Region Name,Supplier,Lowest Rate$fils,Effective Date";
$str.= "\r\n";

//color is used to make the table more readable by alternate coloring
$color=0;
$x=0;

//fetch query result in an array and go through it line by line
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
	$x++;
	//switch the color variable if the last line has a diffrent calling code
	if ($line['calling_code']<>$lastline)
		{$color=ABS($color-1);}
	
	//select the appropriate color
	if ($color==0) 
		{echo "\t<tr bgcolor=\"#CFCFCF\" >\n";}
	else
		{echo "\t<tr bgcolor=\"#FFFFFF\" >\n";}
	
	//print the query line in HTML table format
	echo "<td>{$line['calling_code']}</td>";
	echo "<td>{$line['region_name']}</td>";
	echo "<td>{$line['supplier']}</td>";
	echo "<td>{$line['lowest_rate']}</td>";
	echo "<td>{$line['eff_date']}</td>";
	$str.="{$line['calling_code']},{$line['region_name']},{$line['supplier']},{$line['lowest_rate']},{$line['eff_date']}"; 
	$str.= "\r\n";
	
	//save the current line's calling code
	$lastline=$line['calling_code'];

	echo "</tr>\n";
}

//close the table
echo "</table>\n";

//to count how many exist
$record=pg_num_rows($result);

// Free resultset
pg_free_result($result);
	
//print the amount of results found
if($record==0)
	{echo "<b>No results found</b>";
	$str.="No results found";}
else if($record==1)
	{echo "<b>One result found</b>";
	$str.="One result found";}
else
	{echo "<b>$record results found</b>";
	 $str.="$record results found";}
	 
	 
	 $date=date("Y-m-d_H-i-s");
 	$file="LCR_$date.csv";
 	$ofile = fopen("output/$file",'w');
	fwrite($ofile,$str);

echo "<br/><br/>
<form method=\"POST\" action=\"Download_Attacher.php?file=$file\">
<input type=\"submit\" id=\"button\" value=\"Download CSV File\">
</form>" ;
include('Page_Footer.php');
echo "</body></html>";



