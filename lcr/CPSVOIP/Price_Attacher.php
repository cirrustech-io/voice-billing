<?php
//check if the session is still alive
include_once('DBAccessor.php');
applyIPFilter();

	session_start();
	
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	

	//read carriers and destinations
	$rows_carriers = getAllLiveCarriers();
	//$rows_destinations = getDestinations();

	if($rows_carriers->EOF){
		echo "<p id=\"errorText\">Oh-oh billing db in walker is acting funny. Developer defends code. Please contact ext. 101</p>";
	}
//session check ends here
   
	$self = $_SERVER["PHP_SELF"];
	$error = "";
	$carriers=$_POST['carrier_list'];
	
	if(isset($_POST['flag'])){
		
		// Where the file is going to be placed 
		$target_path = "csvs/";

		/* Add the original filename to the target path.  
		Result is "csvs/filename.extension" */
		$target_path = $target_path . basename( $_FILES['uploadedfile']['name']); 

		$file= $_FILES['uploadedfile']['name'];
		$fils=$_POST['fils'];
		$extention=strrchr($file,".");

		//echo $_FILES['uploadedfile']['tmp_name'];
		
		if ($extention==".csv")// || $extention==".zip")
		{	
			if(!move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path))
				$error = "<p id=\"errorText\">There was an error uploading the file, please try again!</p>";
	
			if ($carriers<>"")
				$carriers="carrier_list[]=" . implode("&carrier_list[]=",$_POST['carrier_list'])."&";
				
			header("Location:mergeData.php?$carriers"."uploadedfile=$file&fils=$fils");
				
		
		}
		elseif($extention=="")
			$error .= "<p id=\"errorText\">Please select a file<p/><br/>";
		else 
		{$error .= "<p id=\"errorText\">Incorrect file type, please either use Csv or Zip files</p><br/>";
		unlink($_FILES['uploadedfile']['tmp_name']);}
		//should i clean the temp folder?
			
		
	}	
 ?>

<html>
<head>
<title>Price Attacher Application</title>
</head>

<body id="pageText">
<?php include('Menu_Header.php');?>
<br/><br/>

<form enctype="multipart/form-data" action="<?php echo $self; ?>" method="POST">
<b>Choose a file to upload(NO ZIP SUPPORT YET):</b><br /><input name="uploadedfile" type="file" /><br /><br/>
<b>Suppliers to attach:</b>
<br/>
	<select name="carrier_list[]" id="carrier_list" multiple="multiple" MULTIPLE SIZE=7>	
 				<?php
						$rows_carriers->MoveFirst();
						while(!$rows_carriers->EOF){
							$carrier_id = $rows_carriers->Fields('carrier_id');
							$carrier_name = $rows_carriers->Fields('carrier_name');
							echo "<option id=$carrier_name value=$carrier_name>";
							echo $rows_carriers->Fields('carrier_name');
							echo "</option>";
							$rows_carriers->MoveNext();
						}
					?>
	</select>

<input Name="flag" type="hidden" value="1">
<input type="checkbox" name="fils">Rate in Fils


<br/><br/>
<input type="submit" value="Upload File and Create Csv(s)" />
</form>
<?php echo $error; ?>
<p>You may upload a single csv file or a zip file with multiple csv files</p>
<br/>
<?php include('Page_Footer.php');?>
</body>
</html>
