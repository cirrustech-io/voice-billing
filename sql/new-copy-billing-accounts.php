#! /usr/bin/php4 -c/etc/data_copy/php.ini
<?php
	require_once ("DB.php");
	require_once ("db.inc.php");
	require_once ("functions.local.inc.php");
	require_once ("control.inc.php");
	
// {{{ wait_for_something()

	function wait_for_something () {
		$slept=FALSE;
		
		global $track;
		while (1) {
			$sql = "SELECT COUNT(*) as cnt FROM tracking WHERE plock is null";
			$cnt = $track->getrow ("SELECT COUNT(*) as cnt FROM tracking WHERE plock is null");
			if (handle_err ($cnt,__FILE__,__LINE__,$sql,false)) {
				echo "ERR\n";
				sleep (5);
				continue;
			}
			$cnt = (int)$cnt['cnt'];
			if (!$cnt) {
				$sleepcnt=($sleepcnt +1) %4;
				$sleeptxt = Array(0=>"/",1=>"-",2=>"\\",3=>"|");
				echo "Sleeping [".$sleeptxt[$sleepcnt]."] ";
				
				$slept=TRUE;
				sleep (1);
				continue;
			}
			if ($slept) {
				echo "                                  ";
			}
			break;
		}
	}
// }}} 
// {{{ copy_billing_entries_with_accounts
	load_control();
	function copy_billing_entries_with_accounts () {
		global $dst_max_billing_id;
		global $dst_max_account_id;
		
		global $src;
		global $dst;
		global $control;

		echo "*** Copying billing entries\n";
		
		if (!is_numeric($dst_max_billing_id)) {
			$sql = "SELECT max(billing_id) FROM billing";
			echo "[DST]\r";
			$dst_max_billing_id = $dst->getOne ($sql);
			echo "     \r";
			handle_err ($dst_max_billing_id,__FILE__,__LINE__,$sql,TRUE);
		}
//		$dst_max_billing_id = 4884103;
		set_control ('dst_max_billing_id',$dst_max_billing_id);
		echo "dst_max_billing_id = $dst_max_billing_id\n";
		
		$sql = 	"SELECT  billing_id,account_id FROM billing ".
			" WHERE billing_id > $dst_max_billing_id ".
			" ORDER BY billing_id asc ";
		
		echo "[SRC]\r";
		$bill_id = $src->query($sql);
		echo "     \r";
		handle_err ($ret,__FILE__,__LINE__,$sql,FALSE);
		$last_time = time();
		while ($bill_row = $bill_id->fetchrow())  {
			$billing_id = $bill_row ['billing_id'];
			$account_id = $bill_row ['account_id'];
			
			$sql = "SELECT max(billing_id) FROM billing";
			$src_max_billing_id = $src->getone($sql);
			set_control ('src_max_billing_id',$src_max_billing_id);
			set_control ('cur_billing_id',$billing_id);
			
			if ($account_id > $dst_max_account_id) {
				copy_missing_accounts ();	
			} else {
				$tbl_n="accounts";
				$idx_n="account_id";
				$idx_v=$account_id;
				$action="UPDATE";
				$done = copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
				if ($done) {
					echo "   Account updated\n";
				} else {
					echo "     ERROR\n";
				}
			}
			$tbl_n="billing";
			$idx_n="billing_id";
			$idx_v=$billing_id;
			$action="INSERT";
			
			$done = copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
			if ($done) {
				if ($billing_id > $dst_max_billing_id) $dst_max_billing_id = $billing_id;
				echo "   Billing entry created\n";
			} else {
				echo "     ERROR\n";
			}

			if (time() > $last_time) {
				$last_time = time();
				load_control();
				if ( ! (int)$control['enabled']) break;
			}		
	
		}
//exit();
		return $ret;
	}

// }}}
// {{{ copy_missing_accoutns
	
	function copy_missing_accounts () {
		global $src;
		global $dst;
		global $dst_max_account_id;
		
		echo "*** Copying missing accounts\n";

		if (!is_numeric($dst_max_account_id)) {
			$sql = "SELECT max(account_id) FROM accounts";
			echo "[DST]\r";
			$dst_max_account_id = $dst->getOne ($sql);
			echo "     \r";
			handle_err ($dst_max_account_id,__FILE__,__LINE__,$sql,TRUE);
		}
		
		echo "Current MAX Account Id = $dst_max_account_id\n";
		$sql = "SELECT max(account_id) FROM accounts";
		echo "[SRC]\r";
		$real_max_account_id = $src->getone ($sql);
		echo "     \r";
		handle_err ($real_max_account_id,__FILE__,__LINE__,$sql,TRUE);
		
		echo "Real    MAX Account Id = $real_max_account_id\n";
		
		if ($real_max_account_id < $dst_max_account_id) {
			echo "Error no accounts to copy !!!!\n";
			return;
		}
		if ($real_max_account_id == $dst_max_account_id) {
			return;
		}
		
		$sql =	"SELECT account_id FROM accounts ".
			" WHERE account_id > $dst_max_account_id ".
			" ORDER BY account_id ASC ";
		$acc_id = $src->query($sql);
		handle_err ($acc_id,__FILE__,__LINE__,$sql,TRUE);
		$last_time = time();
		while ($acc_row=$acc_id->fetchRow()) {
			$tbl_n="accounts";
			$idx_n="account_id";
			$idx_v=$acc_row['account_id'];
			$action="INSERT";
			
			echo "Copying NEW account (account_id = $idx_v)\n";
			
			$done = copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
			if ($done) {
				if ($idx_v > $dst_max_account_id) $dst_max_account_id = $idx_v;
			} else {
				echo "     ERROR\n";
			}
			if (time() > $last_time) {
				$last_time = time();
				load_control();
				if ( ! (int)$control['enabled']) break;
			}		

		}
	}
	
// }}}
	
	db_connect_all();
//	db_reconnect_all();
	
	$my_plock = 4;
	$in_one_go =1;
	
	//copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
//	$dst_max_billing_id = 2000000;
	$min_milling_id = NULL;

	while (1) {
		$sql = "SELECT max(billing_id) FROM billing";
		$src_max_billing_id = $src->getone($sql);
		
		set_control ('src_max_billing_id',$src_max_billing_id);
		load_control();
		
		if ((int)$control['enabled']) {
			copy_missing_accounts ();
		}
		if ((int)$control['enabled']) {
			copy_billing_entries_with_accounts ();
		}
		echo "[SLEEP]\r";
		sleep (2);
		echo "       \r";
	}
?>
