#! /usr/bin/php -c/etc/data_copy/php.ini
<?php
	require_once ("DB.php");
	require_once ("db.inc.php");
	require_once ("functions.local.inc.php");
	
// {{{ wait_for_something()

	function wait_for_something () {
		$slept=FALSE;
		
		global $track;
		while (1) {
			$sql = "SELECT COUNT(*) as cnt FROM tracking WHERE plock is null";
			$cnt = $track->getrow ("SELECT COUNT(*) as cnt FROM tracking WHERE plock is null");
			if (handle_err ($cnt,__FILE__,__LINE__,$sql,false)) {
				echo "ERR\n";
				sleep (5);
				continue;
			}
			$cnt = (int)$cnt['cnt'];
			if (!$cnt) {
				$sleepcnt=($sleepcnt +1) %4;
				$sleeptxt = Array(0=>"/",1=>"-",2=>"\\",3=>"|");
				echo "Sleeping [".$sleeptxt[$sleepcnt]."] ";
				
				$slept=TRUE;
				sleep (1);
				continue;
			}
			if ($slept) {
				echo "                                  ";
			}
			break;
		}
	}
// }}} 
	
	db_connect_all();
//	db_reconnect_all();

	$my_plock = 4;
	$in_one_go =1;
	
	//copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
	
	
	$sql = "SELECT account_id FROM accounts ".
//		"  where account_id < 500000 ".
// 		" where billing_id > 599999 AND billing_id < 700000 ".
//		" order by billing_id".
		" order by account_id desc".
//		" order by account_id asc".
	"";
	$res = $src->query ($sql);
	handle_err ($res,__FILE__,__LINE__,$sql,TRUE);
	$u=0;
	$total=0;
	echo "XXX\n";
	while (1) {
		$total++;
		if ($u--==0) $u=187;
		$row = $res->fetchrow();
		if ($row==NULL) break;
		handle_err ($res,__FILE__,__LINE__,$sql,TRUE);
		$account_id = $row['account_id'];
		$tbl_n="accounts";
		$idx_n="account_id";
		$idx_v=$account_id;
		if ($u==0) echo "[$account_id:$total]";
		$sql2 = "select count(*) from accounts where account_id = '$account_id'";
		$cnt = (int) $dst->getone ($sql2);
		if ($cnt==1) {
			$action="UPDATE";
			$done = copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
			continue;
		}
		echo "$account_id $cnt\n";
		if ($cnt>1) {
			echo "  deleting\n";
			$sql3 = "delete from accounts where account_id = '$account_id'";
			$res3 = $dst->query($sql3);
			handle_err ($res3,__FILE__,__LINE__,$sql3,FALSE);
		}
		echo "  copying\n";
		$action="INSERT";
		$done = copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
		if (!$done) echo "     ERROR\n";
	}
?>
