#! /usr/bin/php -c/root/sql/php.ini
<?php
	require_once ("DB.php");
	require_once ("db.inc.php");
	require_once ("functions.local.inc.php");
	
// {{{ wait_for_something()

	function wait_for_something () {
		$slept=FALSE;
		
		global $track;
		while (1) {
			$sql = "SELECT COUNT(*) as cnt FROM tracking WHERE plock is null";
			$cnt = $track->getrow ("SELECT COUNT(*) as cnt FROM tracking WHERE plock is null");
			if (handle_err ($cnt,__FILE__,__LINE__,$sql,false)) {
				echo "ERR\n";
				sleep (5);
				continue;
			}
			$cnt = (int)$cnt['cnt'];
			if (!$cnt) {
				$sleepcnt=($sleepcnt +1) %4;
				$sleeptxt = Array(0=>"/",1=>"-",2=>"\\",3=>"|");
				echo "Sleeping [".$sleeptxt[$sleepcnt]."] ";
				
				$slept=TRUE;
				sleep (1);
				continue;
			}
			if ($slept) {
				echo "                                  ";
			}
			break;
		}
	}
// }}} 
	 
	db_connect_all();
//	db_reconnect_all();

	$my_plock = 4;
	$in_one_go =1;

// {{{ new_copy_batch
	function new_copy_batch () {
		global $track;
		global $src;
		global $dst;
		global $my_plock;
		global $in_one_go;
		
		
		$top = (int)$track->getone ("SELECT max(num) from tracking");
		echo "new_batch $top operations\n";
		$sql = "select distinct tbl_n,idx_n from tracking";
		$tbl =& $track->getall($sql,NULL,DB_FETCHMODE_ASSOC);
		$tables = Array();

		foreach ($tbl as $n => $a) { $tables[$a['tbl_n']]=$a['idx_n']; }
		foreach ($tables as $table => $index) {
			echo "  Table $table (Index $index)\n";
			
			$sq_op = 'sq_ins';
			
			$sql = "SELECT * FROM accounts WHERE exists ( "
				." SELECT $index,t.idx_v FROM $table,martin_test.dbo.tracking t "
				." WHERE $index = t.idx_v AND t.tbl_n = '$table' AND num <= $top AND $sq_op = 1 "
			." )";
			$res = $src->query($sql);
			handle_err ($res,__FILE__,__LINE__,$sql,true);
			
			$cnt = $res->numrows();
			echo "      entries $cnt\n";
			
		}
		unset ($tbl);
	}
// }}}
// {{{ old_copy_batch

	function old_copy_batch($cnt) {
		global $track;
		global $src;
		global $dst;
		global $my_plock;
		global $in_one_go;
		
		$sql_X="SELECT date,tbl_n,idx_n,idx_v,sq_del,sq_ins,sq_upd,num FROM tracking "
		//$sql_X="SELECT * FROM tracking "
//			." WHERE plock = '$my_plock' ".
			." WHERE num <= $cnt AND isnull (plock,0)=0"
			." and datediff(s,date,getutcdate()) > 4 "
			."order by num";
		$res = $track->query ($sql_X);
		handle_err ($res,__FILE__,__LINE__,$sql_X,FALSE);
		
		$XX=0;
		$XX_max= $res->numrows();
		do {
			if ($XX==$XX_max) break;
			if ($res->numrows()==0) {
				echo "numrows==0\n";
				break;
			}
			$row = $res->fetchrow();
			if ($row == NULL) {
				if ($XX==$XX_max) break;
				echo "Unexpected fetch error (cnt=$cnt) [row=$XX rows_in_batch=$XX_max]\n";
				sleep (2);
				continue;
			}
			$tbl_n = addslashes ($row['tbl_n']);
			$idx_n = addslashes ($row['idx_n']);
			$idx_v = addslashes ($row['idx_v']);
			
			
			if ($row['sq_upd']=='1') $action = "UPDATE";
			if ($row['sq_ins']=='1') $action = "INSERT";
			if ($row['sq_del']=='1') $action = "DELETE";
			
			$done = copy_data ($src,$dst,$action,$tbl_n,$idx_n,$idx_v);
			
			if ($done) {
				$sql2 = "DELETE FROM tracking WHERE num = '{$row['num']}'";
				$res2 = $track->query($sql2);
				handle_err ($res2,__FILE__,__LINE__,$sql2,FALSE);
			} else {
				$track->query("UPDATE tracking SET plock = -1 WHERE num = '{$row['num']}'");
			}
			$XX++;
		} while (1);
	}
// }}}
	
	while (1) {
		
		wait_for_something();
		
//		$sql = "UPDATE tracking set plock = '$my_plock' WHERE id in ".
//			"(SELECT top '$in_one_go' id from tracking WHERE plock IS NULL ORDER BY date)";
//		$res = $track->query ($sql);
		
		$sql = "SELECT count(*) FROM tracking";
		$upd_cnt = (int)$track->getone ($sql);
		$sql = "SELECT max(num) FROM tracking";
		$upd_max = (int)$track->getone ($sql);
		
// 	sleep (2);
		if ($upd_cnt<10) {
			old_copy_batch($upd_max);
			continue;
		}
	//	new_copy_batch ();
		old_copy_batch ($upd_max);
	}
?>
