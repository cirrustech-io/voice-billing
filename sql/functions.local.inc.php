<?php

	$sql_expempt_timestamp = array(
		"ACTIVATION_DATE_TIME",
		"SERVICE_CHARGE_DATE",
		"CREATION_DATE_TIME",
		"DATE_OF_BIRTH",
		"SIGNUP_DATE",
	);
	function write_log ($text) {
		global $log_fd;
		if (!isset ($log_fd) || $log_fd == NULL || $log_fd == FALSE) {
			$log_fd = fopen ("/var/log/sql/replicator.log","a");
			if ($log_fd == NULL) return;
		}
		
		$date = date('%Y/%m/%d %h:%i:%s');
//		fprintf ($log_fd,"%s: %s\n",$date,$text);
		fwrite ($log_fd,sprintf("%s: %s\n",$date,$text));
	}
	function handle_err(&$d,$file,$line,$query,$fatal=TRUE) {
		if (!PEAR::isError ($d)) return FALSE;
		if (!DB::isError($d)) return FALSE;
		
		$error = $d->GetMessage();
		
		$err_aux = "DBMS_U[".($d->getUserInfo)."] DBMS_D[".($d->getDebugInfo())."]";
		$message = "$file:$line [$query] $error $err_aux";
		
		echo "               ";
		echo "*** $message\n";
		write_log ("$message");
		if ($fatal) {
			die ($message);
		} else {
			echo "*** $message\n";
		}
		return TRUE;
	}
	
	function row2sql_insert ($table,$row,$excluded = Array()) {
		global $sql_expempt_timestamp;
		$sql_1 = "INSERT INTO $table (";
		$sql_2 = ") VALUES (";
		$delim ="";
		foreach ($row as $c => $v) {
			if ($v=="" && in_array($c,$sql_expempt_timestamp)) continue;
			$sql_1 .= "$delim ".addslashes($c);
			$sql_2 .= "$delim '".addslashes($v)."'";
			$delim=",";
		}
		$sql = $sql_1.$sql_2.") ";
		return $sql;
	}
	function row2sql_update ($table,$row,$excluded = Array()) {
		global $sql_expempt_timestamp;
		$sql = "UPDATE $table SET ";
		$delim ="";
		foreach ($row as $c => $v) {
			if ($v=="" && in_array($c,$sql_expempt_timestamp)) {
				$v = "NULL";
			} else {
				$v = "'".addslashes($v)."'";
			}
			$sql .= "$delim ".addslashes($c)." = ".$v;
			$delim=",";
		}
		return $sql;
	}
	function row2sql_delete ($table,$row,$excluded = Array()) {
		$sql = "DELETE FROM $table";
		return $sql;
	}
?>
