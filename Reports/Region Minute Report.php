<?php
    ini_set("memory_limit","512M");
	include_once('DB.php');	
	$prefixes = array();
	$sumarray= array();
    //include("db-inc.php");
	require_once('includes/functions.inc.php');
    
    include_once('CSVParser.php');

    
/*    if ($_POST['download']=='yes') {
    	$fname="result.csv";
    	header ('Content-Disposition: attachment; filename='.urlencode($fname));
    	readfile("/tmp/output.csv");
    	exit;
    }*/
    
	$parser = new CsvFileParser();
	$file = "Code List.csv";
	$parsedData = $parser->ParseFromFile($file);
	
	//print_r($parsedData);
	$codes = array();
	
	foreach ($parsedData as $line)
	{

		findCode($line[0],$line[1]);
	}
	
	echo "Done \n";
	//exit("inserted the codes \n");
	
	$allPrefixes = ("SELECT * from prefixes");
	$DBprefixes = $DB->Execute($allPrefixes);
	$prefixes = array();
	$sumarray = array();
	while(!$DBprefixes->EOF){
		
		$key = $DBprefixes->fields[0];
		$code = $DBprefixes->fields[0]; 
		$region = $DBprefixes->fields[1];
		
		$prefixesLine = array ('prefix' => $code,'region_name' => $region);
		$prefixes[$key] = $prefixesLine;
		$DBprefixes->MoveNext();
	
	}
/*	echo '<pre>';
	print_r($prefixes);
	echo '</pre>';
*/	
	//querys the CDRS once	
	function loop_through_cdrs ($start_date,$end_date, $fn) {
		global $DB;
		
		$getMonthCDR = sprintf("SELECT date_part('year',start_time) As year, date_part('month',start_time) As month, clean_number, call_source_regid, call_source_uport, call_dest_regid, call_dest_uport, call_duration_int FROM new_cdr where start_time >= %s and start_time < %s and call_duration_int > 0 ",
		GetSQLValueString($start_date, "date"),
		GetSQLValueString($end_date,"date"));
		$CDRs = $DB->Execute($getMonthCDR) or die($DB->ErrorMsg());
		
		$monthCDRs = array();
		while(!$CDRs->EOF){
			
			if(strlen($CDRs->fields[2]) > 8 ){
				$year = $CDRs->fields[0];
				$month = $CDRs->fields[1];
				$number = $CDRs->fields[2];
				$source_ep = $CDRs->fields[3];
				$source_port = $CDRs->fields[4];
				$dest_ep = $CDRs->fields[5];
				$dest_port = $CDRs->fields[6];
				$duration = $CDRs->fields[7];
				$prefix = '';	
					
				$line = array (
				'year' => $year,
				'month' => $month,
				'number' => $number,
				'source_ep' => $source_ep,
				'source_port' => $source_port,
				'dest_ep' => $dest_ep,
				'dest_port' => $dest_port,
				'duration' => $duration,
				'prefix' => $prefix);
				
				$fn ($line);
			}
				$CDRs->MoveNext();
		
		}
	}
	//Querys the CDRS accessing the database for each day
	function loop_through_cdrs_by_days($start_date,$end_date, $fn) {
		$d1 = strtotime($start_date);
		$d2 = strtotime($end_date);
		$oneday = 24*60*60;
		
		while ($d1 != $d2) {
			$period = $d2 - $d1;
			if ($period > $oneday) {
				$period=$oneday;
			} 
			$period_start = strftime ('%Y-%m-%d %H:%M:%S',$d1);
			$period_end   = strftime ('%Y-%m-%d %H:%M:%S',$d1+$period);
			echo "Doing $period_start to $period_end<br>";
			
			//fflush(0);ob_flush();
			
			loop_through_cdrs ($period_start,$period_end,$fn);
			$d1 += $period;
		}
	}
	
	function process_cdr_entry ($line) {
			$line['prefix'] = longest_match($line['number']);
			if($line['prefix'] == NULL || $line['prefix'] == '')
				$line['prefix'] = '*';
	//		echo 'prefix: '.$line['prefix'].' and number is: '.$line['number']. '<br>';
			
			$yearMonth = $line['year'].$line['month'];
			createSummary($yearMonth,$line['prefix'],$line['source_ep'],$line['source_port'],$line['dest_ep'],$line['dest_port'],$line['duration']);		
	}
	// function loop_through_cdrs ($start_date,$end_date, $fn)
	// function loop_through_cdrs_by_days($start_date,$end_date, $fn)
	// function process_cdr_entry ($line)
	
	$start_date = '2014-04-01 00:00:00';
	$end_date = '2014-05-01 00:00:00';
	//loop_through_cdrs ($start_date,$end_date,"process_cdr_entry");
	loop_through_cdrs_by_days ($start_date,$end_date,"process_cdr_entry");
	echo "<pre>";
//	print_r($sumarray);
	echo "</pre>";
	
	foreach ($sumarray as $line)
	{
		insertSumTable($line);
	}
	
	//This part needs to changed based on the statistics needed, initialize variables as needed
/*	$yearMonth = '';
	$source_ep = '';
	$source_port = 0;
	$dest_ep = '';
	$dest_port = 0;
*/	
	$query = "SELECT * FROM summary WHERE year_month = '20111' AND (source_ep = 'TALKING-SIP-2' OR source_ep = 'TALKING-SIP-4') AND (source_port = 80 OR source_port = 82)";
	$summedEntries = $DB->Execute($query) or die($DB->ErrorMsg());
	
	$dialCode = '';
	$region = '';
	$seconds = 0;
	$calls = 0;
	
	$finalArray = array();
	while(!$summedEntries->EOF){
		echo "<pre>";
		print_r($summedEntries->fields);
		echo "</pre>";
		$codeRegion = sumMinutes($summedEntries->fields[1]);
		echo "<pre>";
		print_r($codeRegion);
		echo "</pre>";
		$dialCode = $codeRegion['code'];
		$region = $codeRegion['region'];
		$seconds = $summedEntries->fields[6];
		$calls = $summedEntries->fields[7];
		$key = "$dialCode-$region";
		
		if(!isset($finalArray[$key])){
			$finalArray[$key] = Array (
			"code" => $dialCode,
			"region" => $region,
			"seconds" => 0,
			"calls" => 0);
		}
		
		$finalArray[$key]["seconds"] += $seconds;
		$finalArray[$key]["calls"] += $calls;
		
		$summedEntries->MoveNext();
	}
	echo "<pre>";
	print_r($finalArray);
	echo "</pre>";
	$f = fopen ("/Results/output.csv","w");
	echo "Code,Region,Seconds,Calls\n";
	foreach ($finalArray as $x) {
		fprintf($f,"%s,%s,%s,%s\n",$x['code'],$x['region'],$x['seconds'],$x['calls']);
	}
	fclose($f);
//	echo "<form method=post><input type=hidden name=download value=yes><input type=submit value='Download file'></form>";
	
	function sumMinutes($cdrCode){
		global $parsedData;
		$m = 0;
		$found = NULL;
		$name = "";
		$ret = array();
		
		foreach ($parsedData as $line){
			
			if(strlen($line[0]) > $m && ($line[0] == substr($cdrCode,0,$line[0]))){
				$m = strlen($line[0]);
				$found = $line[0];
				$name = $line[1];
				
				
			}
		}
		$ret = array ('code' => $found, 'region' => $name);
//		echo "$found\n"; 
		if($ret['code'] == NULL || $ret['code']== '' || !isset($ret['code'])){
			$ret['code']= '*';
			$ret['region'] = 'Uknown';
		}
		return $ret;
	}

	function findError(&$result,&$DB) {
		
/*			echo "<HR><HR><HR>";
				echo print_r($result,1);
				echo "<hr><pre>";
				print_r ($DB);
				echo "</pre><hr>";
*/				$error =  $DB->ErrorMsg();
				if($error != "ERROR:  duplicate key violates unique constraint \"summary_year_month_key\""){
/*					echo "Dying<br>";
					$error1 = preg_replace('/ /','\x20',$error);
					echo "[$error1]<br>";
					echo '[ERROR: duplicate key violates unique constraint "summary_year_month_key"]<Br>';
*/					echo "<br>";
					die($DB->ErrorMsg());
				}
					
		}
	
	function insertSumTable($line){
		
		include('Connections/DB.php');
		$insertSum = sprintf("INSERT INTO summary (year_month,prefix,source_ep,source_port,dest_ep,dest_port,duration,calls) VALUES(%s,%s,%s,%s,%s,%s,0,0)",
		GetSQLValueString($line['yearmonth'],"text"),
		GetSQLValueString($line['prefix'],"text"),
		GetSQLValueString($line['source_ep'],"text"),
		GetSQLValueString($line['source_port'],"integer"),
		GetSQLValueString($line['dest_ep'],"text"),
		GetSQLValueString($line['dest_port'],"integer"));
//		echo $insertSum;
		$result = $DB->Execute($insertSum) or findError($result,$DB); 

		$updateSum= sprintf("UPDATE summary SET calls = calls+%s, duration = duration+%s WHERE year_month = %s and prefix = %s and source_ep = %s and source_port = %s and dest_ep = %s and dest_port = %s",
		GetSQLValueString($line['calls'],"integer"),
		GetSQLValueString($line['dur'],"integer"),
		GetSQLValueString($line['yearmonth'],"text"),
		GetSQLValueString($line['prefix'],"text"),
		GetSQLValueString($line['source_ep'],"text"),
		GetSQLValueString($line['source_port'],"integer"),
		GetSQLValueString($line['dest_ep'],"text"),
		GetSQLValueString($line['dest_port'],"integer"));
		$updateResult = $DB->Execute($updateSum) or die($DB->ErrorMsg());
		
		
	}

	function createSummary ($ym,$p,$oep,$op,$tep,$tp,$dur) {
		global $sumarray;
		$key = "$ym;$p;$oep;$op;$tep;$tp";
		//echo $key."\r\n";
		if (!isset($sumarray[$key])) {
					$sumarray[$key] = Array (
					"yearmonth" => $ym,
					"prefix" => $p,
					"source_ep" => $oep,
					"source_port" => $op,
					"dest_ep" => $tep,
					"dest_port" => $tp,
					"dur" => 0,
					"calls" => 0,
					);
		}
		
		$sumarray[$key]["dur"] += $dur;
		$sumarray[$key]["calls"] ++;
	}	
	
	function longest_match($num) {
	        global $prefixes;
	        $m =0;
	        $ret=NULL;
	
	       foreach ($prefixes as $p => $pfx) {
	       // 		echo $p.'<br>';
	                if (strlen($p) > $m &&  ($p == substr($num,0,strlen($p)))) {
	                        $m = strlen($p);
	                        $ret = $p;
	                }
	                
	        }
	        return $ret;
	}
	
	
	function findCode($region_name,$code)
	{
		//Connection statement
	include('Connections/DB.php');

	$checkCodeSQL = sprintf("SELECT name FROM prefixes WHERE prefix=%s",
	GetSQLValueString($code,"text"));
	//echo $checkCodeSQL."<br />";
	$region = $DB->SelectLimit($checkCodeSQL) or die($DB->ErrorMsg());
	//return $region;
	//echo "<pre>".print_r(array("region"=>$region),1)."</pre>";
	//if ($region NULL)
	if ($region->EOF)
		{
	 		$calculated = 0;
			$insertPrefixSQL = sprintf("INSERT INTO prefixes (prefix, name, calculated) VALUES (%s, %s, %s)",
			GetSQLValueString($code,"text"),
			GetSQLValueString($region_name, "text"),
			GetSQLValueString($calculated, "integer"));
			//echo $insertPrefixSQL."<br />";
			$prefixResult = $DB->Execute($insertPrefixSQL) or die($DB->ErrorMsg());
		}
	else 
		{
			//echo "No new Codes<br>";
		}	
	}
	
?>
