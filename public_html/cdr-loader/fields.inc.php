<?php

	$cdr_field_convert = Array (
		"call_dest_uport" => "skipempty",
		"call_error_dest_int" => "skipempty",
		"h323_dest_ras_error" => "skipempty",
		"h323_dest_h225_error" => "skipempty",
		//"zone" => "skipemptyhash",
		"zone" => "skipempty",
		"sip_dest_respcode" => "skipempty",
		"call_error_int" => "zeroempty",
		"call_duration_int" => "zeroempty",
		"call_duration_fractional" => "zeroempty",
	);
	$cdr_field_map_empty = Array (
		43 => 43,
		44 => 44,
		45 => 45,
		29 => 29,
		32 => 32,
		50 => 50,
	);
	$cdr_field_map = Array (
		"1" => "start_time",
		"2" => "start_time_int",
		"3" => "call_duration",
		"4" => "originator_ip",
		"6" => "terminator_ip",
		"8" => "call_source_custid",
		"9" => "called_party_on_dst",
		"10" => "called_party_on_src",
		"11" => "call_type",
		"13" => "disconnect_error_type",
		"14" => "call_error_int",
		"15" => "call_error_str",
		"18" => "ani",
		"22" => "cdr_seq_no",
		"24" => "callid",
		"25" => "call_hold_time",
		"26" => "call_source_regid",
		"27" => "call_source_uport",
		"28" => "call_dest_regid",
		"29" => "call_dest_uport",
		"30" => "isdn_cause_code",
		"31" => "call_party_after_src_calling_plan",
		"32" => "call_error_dest_int",
		"33" => "call_error_dest_str",
		"35" => "new_ani",
		"36" => "call_duration_int",
		"37" => "incoming_leg_callid",
		"38" => "protocol",
		"39" => "cdr_type",
		"40" => "hunting_attempts",
		"41" => "caller_trunk_group",
		"42" => "call_pdd",
		"43" => "h323_dest_ras_error",
		"44" => "h323_dest_h225_error",
		"45" => "sip_dest_respcode",
		"46" => "dest_trunk_group",
		"47" => "call_duration_fractional",
		"48" => "timezone",
		"49" => "msw_name",
//		"50" => "zone",
		"56" => "call_dest_custid",
		"57" => "call_zone_data"
	);
	
	
	
	
?>
