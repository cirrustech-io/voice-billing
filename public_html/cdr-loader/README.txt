


For public audience:

* file-finder.php
	Locates new CDR files in CDR directories, schedules them to load and if the number of files found is not high it loads them immediately
* file-loader.php
	Loads the CDRs that are scheduled
* reload-files.php
	Reload CDR files one by one, flush CDRs, load CDRs
	up to one file is missing at a time
* file-refinder.php
	Locates files that are loaded incorrectly, changed since loading.
	Unlike file-finder.php it compares checksums of files, thus much slower.






SELECT * from ldr_files where locked=1 and filesize>0 and filename like '%T2008%' and loaded < lines;

