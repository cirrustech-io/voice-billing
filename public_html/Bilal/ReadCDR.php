<?php

//Script for Bilal - March 11, 2008
// List cdr table data based on account code and call date

//Config File
require_once ("config.inc.php");
if (!defined("BOO")) die ("Security error - Config File missing");

// build form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["Check"])) && ($_POST["Check"] == "Go")) {
	
	if(!(isset($_POST["acc_code"])) || (strlen($_POST["acc_code"]) == 0) || !(isset($_POST["call_date"])) || (strlen($_POST["call_date"]) == 0)){
		echo "<br>Missing input<br>";
		exit();
	}
		
	$call_date = date('Y-m-d 00:00:00',trim(strtotime($_POST['call_date'])));
	$acc_code = trim($_POST['acc_code']);
		
	//db connection 
	$pg_conn_asteriskdb = pg_connect($asteriskdb_connstr);
	
	$sql = "SELECT * FROM cdr where calldate like  '".$call_date."' and accountcode = '" .$acc_code . "'";
	
	$result = pg_exec($pg_conn_asteriskdb, $sql);
	
	if($result && (pg_num_rows($result) > 0)){
		echo "<table border=1 ><br>";
		$first=true;
		while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
			
			if($first){
				$head=""; 
				$row="";
				foreach ($line as $key=>$val){
					$head .="<th>$key</th>";
					$row .="<td>$val</td>";
				}
				echo "<tr>$head</tr><br>";
				echo "<tr>$row</tr><br>";	
				$first= false;				
			}
			else {
				echo "<tr><br>";
				foreach($line as $key=>$val){										
					echo "<td>$val</td>";			
				}			
				echo "</tr><br>";	
			}					
		}
		echo "</table>";
	}
	else {
		echo "<br>No data found<br>";
	}	
}

?>


<html>
<head>
<title>Read CDR</title>
</head>

<body>
<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
	Account Code: <input type="text" name="acc_code" id="acc_code"><br>
	Call Date:  <input type="text" name="call_date" id="call_date"><br>
	
  	<input type="hidden" name="Check" value="Go" />
  	<input type="submit" name="Submit" value="Read">    
</form>
</body>
</html>























