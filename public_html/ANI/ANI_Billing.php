#! /usr/bin/php4 -c/etc/data_copy/php.ini
<?php



//function createCPSBill($cust_name,$source_regid,$source_port){
function createCPSBill($cust_name,$source_ep,$customer_id){

	include_once('DBAccessor.php');
	
	$start_date_time=date('Y-m-d 00:00:00',strtotime("01 Aug 2015"));
	$end_date_time =date('Y-m-d 23:59:59',strtotime("31 Aug 2015"));
	

	$result_rates = getCPSRates($customer_id);
	
	$RATES = array();
	
	if((pg_num_rows($result_rates) > 0)){
	
		while ($line = pg_fetch_array($result_rates, null, PGSQL_ASSOC)) {
	
			$rate = array();
			$rate['number_id'] = $line['number_id'];
			$rate['master_id'] = $line['master_id'];
		//	$rate['region_name'] = $line['region_name'];
			$rate['region_name'] = preg_replace('/-\d*/',' ',$line['region_name']);
			$rate['calling_code'] = $line['calling_code'];
			$rate['rate'] = $line['rate'];
			$rate['effective_start_date'] = $line['effective_start_date'];
			$rate['effective_end_date'] = $line['effective_end_date'];
			$RATES[] = $rate;
		}
	}
	
	
	
	$result_numbers = getNumbers();
	$NUMBERS = array();
	
	if((pg_num_rows($result_numbers) > 0)){
		while ($line = pg_fetch_array($result_numbers, null, PGSQL_ASSOC)) {
			$number = array();
			$number['number_id'] = $line['number_id'];
			$number['src'] = $line['src'];
			$number['dst'] = $line['dst'];
			$number['customer_id'] = $line['customer_id'];
			//$NUMBERS[$line['number_id']]['carrier_id'] = $line['carrier_id'];
			$number['consolidate'] = $line['consolidate'];
	
			$NUMBERS[] = $number;
	
		}
	}
	
	echo "***Starting $cust_name***\n";
	echo "Fetching CDRs...\n";
	//$result_cdrs = getCpsCDRS($start_date_time,$end_date_time,$source_regid,$source_port);
	$result_cdrs = getCpsCDRS($start_date_time,$end_date_time,$source_ep);
	if(pg_num_rows($result_cdrs) > 0){
		include_once("ANI_Bill_Magic.php");
	
		$file = fopen("Rated/$cust_name-" . date('Y-m-d-H-i-s') . ".csv", 'w') or die('Unable to open file');
	
		echo pg_num_rows($result_cdrs) . " rows found\n";
		echo "Rating calls...\n";
		
		while ($line = pg_fetch_array($result_cdrs, null, PGSQL_ASSOC)) {
			$cdr = array();
			$cdr['cdr_file_id'] = $line['cdr_file_id'];
			$cdr['cdr_line_no'] = $line['cdr_file_line'];
			$cdr['ani'] = $line['ani'];
			$cdr['clean_number'] = $line['clean_number'];
			$cdr['call_duration_int'] = $line['call_duration_int'];
			$cdr['start_time'] = $line['start_time'];
			$cdr['call_dest_regid'] = $line['call_dest_regid'];
			$cdr['call_dest_uport'] = $line['call_dest_uport'];
			$cdr['call_source_regid'] = $line['call_source_regid'];
			$cdr['call_source_uport'] = $line['call_source_uport'];
	
	
			if(floatval($cdr['call_duration_int']) > 0){
				$rated = do_ani_bill_rate($cdr,$NUMBERS,$RATES);
				if(!$rated){
					echo "Could not rate- {$cdr['ani']} - {$cdr['clean_number']}\n";
					 $ins = "{$cdr['ani']} ,{$cdr['start_time']},Unknown, {$cdr['clean_number']}, {$cdr['call_duration_int']},0,0,{$cdr['call_dest_regid']},{$cdr['call_dest_uport']},{$cdr['call_source_regid']},{$cdr['call_source_uport']}\r\n";
				} else { 

					$ins = "{$cdr['ani']} ,{$cdr['start_time']},{$rated['region_name']}, {$cdr['clean_number']}, {$cdr['call_duration_int']},{$rated['rate']},{$rated['price']},{$cdr['call_dest_regid']},{$cdr['call_dest_uport']},{$cdr['call_source_regid']},{$cdr['call_source_uport']}\r\n";
				
				}
				fwrite($file,$ins);
			}
		}
	
		fwrite($file,"done\r\n");
		fclose($file);
	} else {
		echo "No calls found\n";
	}
	echo "***$cust_name Done***\n\n";
	
}




?>

