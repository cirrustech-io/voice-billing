<?php

function do_ani_bill_magic($CDR,$NUMBERS,$RATES){
	
	require_once("DBAccessor.php");
	
	$cdr_file_id = $CDR['cdr_file_id'];
	$cdr_line_no = $CDR['cdr_line_no'];
	$ani = $CDR['ani'];
	$clean_number= $CDR['clean_number'];
	$seconds = $CDR['call_duration_int'];
	//$timeslice = $CDR['bqx'];
	$start_time = date('Y-m-d H:i:s',strtotime($CDR['start_time']));
	
	//perform magic or ignore 		
	foreach($NUMBERS as $number){
		$match_src = str_replace('%','',$number['src']);
		$match_dst = str_replace('%','',$number['dst']);
		$number_id = $number['number_id'];
		$consolidate = $number['consolidate'];
				
		//for cps customers - filter based on ani
		//for prepaid customers -filter based on ani and dialed number 
		if(preg_match('/(^' . $match_src . '$)/',$ani) 
			|| (preg_match('/(^' . $match_src . '\d$)/',$ani) && preg_match('/(^' . $match_dst . '$)/', $clean_number))){
				
			//doMagic($cdr_file_id,$cdr_line_no,$timeslice,$seconds,$start_time,$number_id,$consolidate);
			
			
			//find longest matching code and corresponding rate
			$rate_slice = array_slicer($RATES,$number_id);
			$len =0;
			$found = false;
			foreach($rate_slice as $rate){
				
				if(preg_match('/(^' . $rate['calling_code'] . '\d$)/', $clean_number) && $start_time >= date('Y-m-d H:i:s',strtotime($rate['effective_start_date'])) && $start_time < date('Y-m-d H:i:s',strtotime($rate['effective_end_date']))){
					
					if(strlen($rate['calling_code']) > $len){
						$found = true;
						$use = $rate;		
					}
				}
				
			}	
						
			if($found){
				if($seconds > 0){
					$minimum = floatval($use['rate']);
					if($seconds < 60)
						$price = $minimum;
					else 
						$price = $seconds * $minimum / 60;
				} else {
					$price =0;
				}
				$use['price'] = $price;
				return $use;
			}
			else 
				return false;	
			
		}
	}
	
	/*
	require_once("DBAccessor.php");
	
	$cdr_file_id = $CDR['cdr_file_id'];
	$cdr_line_no = $CDR['cdr_line_no'];
	$ani = $CDR['ani'];
	$clean_number= $CDR['clean_number'];
	$seconds = $CDR['call_duration_int'];
	$timeslice = $CDR['bqx'];
	$start_time = strtotime('Y-m-d H:i:s',strtotime($CDR['start_time']));
	
	//perform magic or ignore 	
	for($i=0; $i< count($NUMBERS); $i++ ){
		
		$match_src = str_replace('%','',$NUMBERS[$i]['src']);
		$match_dst = str_replace('%','',$NUMBERS[$i]['dst']);
		$number_id = $NUMBERS[$i]['number_id'];
		$consolidate = $NUMBERS[$i]['consolidate'];
				
		//for cps customers - filter based on ani
		//for prepaid customers -filter based on ani and dialed number 
		if(preg_match('^' . $match_src . '$',$ani) 
			|| (preg_match('^' . $match_src . '\d$',$ani) && preg_match('^' . $match_dst . '$', $clean_number))){
				
			doMagic($cdr_file_id,$cdr_line_no,$timeslice,$seconds,$start_time,$number_id,$consolidate);
		}	
	}		
	*/
}

function do_ani_bill_rate($CDR,$NUMBERS,$RATES){
	$cdr_file_id = $CDR['cdr_file_id'];
	$cdr_line_no = $CDR['cdr_line_no'];
	$ani = $CDR['ani'];
	$clean_number= $CDR['clean_number'];
	$seconds = $CDR['call_duration_int'];
	//$timeslice = $CDR['bqx'];
	$start_time = date('Y-m-d H:i:s',strtotime($CDR['start_time']));
	
	reset($NUMBERS);
	reset($RATES);

	//perform magic or ignore 	
	
	foreach($NUMBERS as $number){
		$match_src = str_replace('%','',$number['src']);
		$match_dst = str_replace('%','',$number['dst']);
		$number_id = $number['number_id'];
		$consolidate = $number['consolidate'];
				
		//for cps customers - filter based on ani
		//for prepaid customers -filter based on ani and dialed number 
		if(preg_match('/(^' . $match_src . '$)/',$ani) 
			|| (preg_match('/(^' . $match_src . '\d*$)/',$ani) && preg_match('/(^' . $match_dst . '$)/', $clean_number))){
	#echo "am here";		
	
			//doMagic($cdr_file_id,$cdr_line_no,$timeslice,$seconds,$start_time,$number_id,$consolidate);
			
			//find longest matching code and corresponding rate
			$rate_slice = array_slicer1($RATES,$number_id);
			$len =0;
			$found = false;
			foreach($rate_slice as $rate){

				if(preg_match("/(^{$rate['calling_code']}\d*$)/",$clean_number) && $start_time >= $rate['effective_start_date'] && $start_time < $rate['effective_end_date'] && strlen($rate['calling_code']) > $len){
						
						$found = true;
						$use = $rate;
						$len = strlen($rate['calling_code']);		
						#echo "Found match for $clean_number-{$rate['calling_code']}\n";
				}
			}
			
			if($found){
				if($seconds > 0){
					$minimum = floatval($use['rate']);
					if($seconds < 60)
						$price = $minimum;
					else 
						$price = $seconds * $minimum / 60;
				} else {
					$price =0;
				}
				$use['price'] = $price;
				return $use;
			}
			else 
				return false;				
		}
	}
}

function array_slicer1($rate_array,$number_id){
	
	$slice = array();
	foreach($rate_array as $inner_array){
		$check = array_search($number_id,$inner_array);
		if($check === 'number_id'){
			$slice[] = $inner_array;
		}
	}
	return $slice;
	
}


?> 

