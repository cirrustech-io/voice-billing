<?php

/*
function insertUpdate ($received_date,$carrierId){
//Connection statement
include('Connections/DB.php');

$serialSQL = "SELECT nextval('public.update_update_id_seq'::text)";
$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

$insertUpdateSQL = sprintf("INSERT INTO update (update_id, received_date, carrier_id) VALUES (%s,%s, %s)",
GetSQLValueString($serialResult->Fields('nextval'), "int"),
GetSQLValueString($received_date,"date"),
GetSQLValueString($carrierId,"int"));
$UpdateResult = $DB->Execute($insertUpdateSQL) or die($DB->ErrorMsg());

return $serialResult->Fields('nextval');
}

*/
function getNumbers(){

	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = "Select * from number";

	$rows =  pg_query($getRowsSQL) or die('Query failed:' . pg_last_error());
	return $rows;
}

function getCustomers(){

	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = "Select cu.customer_id,cu.name,cu.carrier_id
					from customer as cu, carrier as ca
					where ca.carrier_id = cu.carrier_id and ca.type = 'A'";

	$rows =  pg_query($getRowsSQL) or die('Query failed:' . pg_last_error());
	return $rows;
}

function getCPSRates($customer_id){
	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = "select distinct ar.number_id,ar.master_id,m.region_name,m.calling_code,
					ar.rate,ar.effective_start_date,ar.effective_end_date
					from ani_rate as ar, master_premium as m
					where m.master_id = ar.master_id
					and number_id in (select number_id from number where customer_id = $customer_id)  
					order by number_id asc,calling_code,effective_end_date asc";
#	echo "$getRowsSQL\n";
#	exit(0); 
	$rows =  pg_query($getRowsSQL) or die('Query failed:' . pg_last_error());
	return $rows;
}
/*
function getCpsCDRS($start_date_time,$end_date_time,$source_regid,$source_port){

	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = "select cdr_file_id,cdr_file_line,ani,clean_number,call_duration_int,start_time, call_source_regid,call_source_uport,call_dest_regid,call_dest_uport
from new_cdr
where start_time >= '$start_date_time' and start_time <= '$end_date_time'
and call_source_regid = '$source_regid' and call_source_uport = $source_port";

	$rows =  pg_query($getRowsSQL) or die('Query failed: ' . pg_last_error());
	return $rows;

}
*/

function getCpsCDRS($start_date_time,$end_date_time,$endpoint){
	
	//Connection statement
	include('Connections/DB.php');

	$endpoint_filter="";

	if (is_int(strpos($endpoint, ','))) { 
		$eps = explode(',',$endpoint); //multiple eps
		foreach($eps as $endpoint){
			 $ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
			 $port = substr($endpoint,strpos($endpoint,"/") + 1);
			 if($endpoint_filter == ""){
			 	$endpoint_filter = " (call_source_regid = '$ep' AND call_source_uport = $port) ";
			 } else {
			 	$endpoint_filter = $endpoint_filter . " OR (call_source_regid = '$ep' AND call_source_uport = $port) ";
			 }
		}
		$endpoint_filter = "($endpoint_filter)";

	} else {  
		$ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
		$port = substr($endpoint,strpos($endpoint,"/") + 1);
		$endpoint_filter = "((call_source_regid = '$ep' AND call_source_uport = $port))";

	}

	$getRowsSQL = "select cdr_file_id,cdr_file_line,ani,clean_number,call_duration_int,start_time, call_source_regid,call_source_uport,call_dest_regid,call_dest_uport
	from new_cdr
	where start_time >= '$start_date_time' and start_time <= '$end_date_time'
	and $endpoint_filter";
	
	
	#echo "\n$getRowsSQL\n";
	#exit(0);

	$rows =  pg_query($getRowsSQL) or die('Query failed: ' . pg_last_error());
	return $rows;


	


}


function doMagic($cdr_file_id,$cdr_line_no,$timeslice,$seconds,$start_time,$number_id,$consolidate){

	//Connection statement
	include('Connections/DB.php');

	$check_result = $DB->SelectLimit("Select * from ani_bill where timeslice = $timeslice and number_id = $number_id") or die($DB->ErrorMsg());

	if($consolidate && !$check_result->EOF){
		$ani_bill_id = $check_result->Fields('ani_bill_id');
		$ani_bill_sql = "Update ani_bil
				set seconds = seconds + $seconds,
				last_call_time = '$start_time',
				num_calls = num_calls + 1,
				where ani_bill_id = $ani_bill_id";

	}
	else{

		$serialResult = $DB->SelectLimit("SELECT nextval('public.ani_bill_ani_bill_id_seq')") or die($DB->ErrorMsg());
		$ani_bill_id = $serialResult->Fields('nextval');
		$ani_bill_sql = "Insert into ani_bill
						(ani_bill_id,number_id,seconds,timeslice,first_call_time,last_call_time,num_calls)
						values($ani_bill_id,
							   $number_id,
							   $seconds,
							   '$start_time',
							   '$start_time',
							   1)";
	}

	$ani_bill_tracker_sql = "Insert into ani_bill_tracker
						(ani_bill_id,cdr_file_id,cdr_line_no)
						values($ani_bill_id,
							   $cdr_file_id,
							  $cdr_line_no)";

	$ani_bill_result = $DB->Execute($ani_bill_sql) or die($DB->ErrorMsg());
	$ani_bill_tracker_result = $DB->Execute($ani_bill_tracker_sql) or die($DB->ErrorMsg());

}

function getCorporateRates($Corporate_NumberId){
	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = "select distinct ar.number_id,ar.master_id,m.region_name,m.calling_code,
					ar.rate,ar.effective_start_date,ar.effective_end_date
					from ani_rate as ar, master_rsm_premium as m
					where m.master_id = ar.master_id
					and number_id = $Corporate_NumberId
					order by number_id asc,calling_code,effective_end_date asc";

	$rows =  pg_query($getRowsSQL) or die('Query failed: ' . pg_last_error());
	return $rows;
}


function getCDRS($endpoint,$start_date_time,$end_date_time){

	//Connection statement
	include('Connections/DB.php');

	$endpoint_filter="";

	if (is_int(strpos($endpoint, ','))) { 
		$eps = explode(',',$endpoint); //multiple eps
		foreach($eps as $endpoint){
			 $ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
			 $port = substr($endpoint,strpos($endpoint,"/") + 1);
			 if($endpoint_filter == ""){
			 	$endpoint_filter = " (call_source_regid = '$ep' AND call_source_uport = $port) ";
			 } else {
			 	$endpoint_filter = $endpoint_filter . " OR (call_source_regid = '$ep' AND call_source_uport = $port) ";
			 }
		}
		$endpoint_filter = "($endpoint_filter)";

	} else {  
		$ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
		$port = substr($endpoint,strpos($endpoint,"/") + 1);
		$endpoint_filter = "((call_source_regid = '$ep' AND call_source_uport = $port))";

	}
		
	$getRowsSQL = "select cdr_file_id,cdr_file_line,ani,new_ani,called_party_on_src,called_party_on_dst,call_party_after_src_calling_plan,clean_number,call_duration_int,start_time, call_source_regid,call_source_uport,call_dest_regid,call_dest_uport
					from new_cdr
					where start_time >= '$start_date_time' and start_time <= '$end_date_time'
					and $endpoint_filter";
	

	#echo "\n$getRowsSQL\n";
	#exit(0);
					
	$rows =  pg_query($getRowsSQL) or die('Query failed: ' . pg_last_error());
	return $rows;
	
}










?>
