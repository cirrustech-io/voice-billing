<?php


function createCorporateBill($corporate,$numberId,$endpoint,$start_date_time,$end_date_time){

include_once('DBAccessor.php');


$bah_1 = array('region_name' => 'BAHRAIN-FIXED','calling_code' => '9731','rate' =>0.007,'unit' => 0.021);
$bah_2 = array('region_name' => 'BAHRAIN-FIXED','calling_code' => '1','rate' => 0.007,'unit' => 0.021);
$bah_3 = array('region_name' => 'BAHRAIN-MOBILE','calling_code' => '9733','rate' => 0.014,'unit' => 0.021);
$bah_4 = array('region_name' => 'BAHRAIN-MOBILE','calling_code' => '3','rate' => 0.014,'unit' => 0.021);
$bah_5 = array('region_name' => 'BAHRAIN-TOLLFREE','calling_code' => '800','rate' => 0.0,'unit' => 0.0);
$bah_6 = array('region_name' => 'BAHRAIN-DIRECTORY-ASSIST','calling_code'=>'181','rate' =>0.150,'unit'=> 0.150);
$bah_7 = array('region_name' => 'BAHRAIN-DIRECTORY-ASSIST','calling_code' => '188','rate' =>0.150,'unit' => 0.150);
$bah_8 = array('region_name' => 'BAHRAIN-EMERGENCY','calling_code' => '999','rate' => 0.0,'unit' => 0.0);
$bah_9 = array('region_name' => 'BAHRAIN-EMERGENCY','calling_code' => '199','rate' => 0.0,'unit' => 0.0);
$bah_10 = array('region_name' => 'BAHRAIN-MENAWIMAX','calling_code' => '77','rate' => 0.0159,'unit' => 0.0159);
$bah_11 = array('region_name' => 'BAHRAIN-MENA-HELPDESK','calling_code' => '07770','rate' => 0.0159,'unit' => 0.0159);
$bah_12 = array('region_name' => 'BAHRAIN-FIXED','calling_code' => '6','rate' => 0.007,'unit' => 0.021);
$bah_13 = array('region_name' => 'BAHRAIN-MOBILE','calling_code' => '663','rate' => 0.014,'unit' => 0.021);
$bah_14 = array('region_name' => 'BAHRAIN-MOBILE','calling_code' => '666','rate' => 0.014,'unit' => 0.021);



$result_rates = getCorporateRates($numberId);
$RATES = array();

if((pg_num_rows($result_rates) > 0)){

	while ($line = pg_fetch_array($result_rates, null, PGSQL_ASSOC)) {
		$rate = array();
		$rate['number_id'] = $line['number_id'];
		$rate['master_id'] = $line['master_id'];
		$rate['region_name'] = preg_replace('/-\d*/',' ',$line['region_name']);

		$line['calling_code'] = trim($line['calling_code']);
		if(preg_match('/(^00\d*$)/',$line['calling_code'])){
			$rate['calling_code'] = substr($line['calling_code'],2,(strlen($line['calling_code']) - 2));
		} else {
			$rate['calling_code'] = $line['calling_code'];
		}

		$rate['rate'] = $line['rate'];
		$rate['effective_start_date'] = $line['effective_start_date'];
		$rate['effective_end_date'] = $line['effective_end_date'];
		$RATES[] = $rate;
	}
}

echo "Fetching CDRs..\n";
$result_cdrs = getCDRS($endpoint,$start_date_time,$end_date_time);

if(pg_num_rows($result_cdrs) > 0){



	$file = fopen("Rated/". $corporate ."-" . date('Y-m-d-H-i-s') . ".csv", 'w') or die('Unable to open file');

	echo pg_num_rows($result_cdrs) . " rows found\n";

	while ($line = pg_fetch_array($result_cdrs, null, PGSQL_ASSOC)) {
		$cdr = array();
		$cdr['cdr_file_id'] = $line['cdr_file_id'];
		$cdr['cdr_line_no'] = $line['cdr_file_line'];
		$cdr['ani'] = $line['ani'];
		$cdr['new_ani'] = $line['new_ani'];

		# use 'call_party_after_src_calling_plan' field  for rating, retain 00 if present, clean other prefixes
		$cdr['dialed_number'] = preg_replace('/^.*#/','',$line['call_party_after_src_calling_plan']);
		
		$cdr['clean_number'] = $line['clean_number'];
		$cdr['call_duration_int'] = $line['call_duration_int'];
		$cdr['start_time'] = $line['start_time'];
		$cdr['call_dest_regid'] = $line['call_dest_regid'];
		$cdr['call_dest_uport'] = $line['call_dest_uport'];
		$cdr['call_source_regid'] = $line['call_source_regid'];
		$cdr['call_source_uport'] = $line['call_source_uport'];

		


		if(floatval($cdr['call_duration_int']) > 0){
			#echo "{$line['called_party_on_src']}---{$line['call_party_after_src_calling_plan']}---{$line['called_party_on_dst']}\n";
			#print_r($cdr);
			#echo "Entering DoMagic\n";
			$rated = do_magic($cdr,$RATES);
			if(!$rated){
				#echo "Did not find in DoMagic.Trying Bahrain\n";
				#echo "Trying to rate {$cdr['dialed_number']}\n";
				$bah_found =false;
				#if(strlen($cdr['clean_number']) == 8){
				if(!(preg_match('/^00\d*$/',$cdr['dialed_number']))){
					
					#strip 973	
					if(preg_match('/^973\d*$/',$cdr['dialed_number'])){
						$cdr['dialed_number']= substr($cdr['dialed_number'],3);
					}
			
					//bahrain fixed
					#if(preg_match('/(^' . $bah_2['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_2['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$duration = $cdr['call_duration_int'];
						//$price = ceil($duration / 180) * $bah_2['unit'];
						$price = ceil($duration / 180) * 0.021;

						$rated['region_name'] = $bah_2['region_name'];
						$rated['rate'] = $bah_2['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//bahrain mobile
					#if(preg_match('/(^' . $bah_4['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_4['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$duration = $cdr['call_duration_int'];
						//$price = ceil($duration / 90) * $bah_4['unit'];
						$price = ceil($duration / 90) * 0.021;

						$rated['region_name'] = $bah_4['region_name'];
						$rated['rate'] = $bah_4['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//bahrain tollfree
					#if(preg_match('/(^' . $bah_5['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_5['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$price = $bah_5['unit'];

						$rated['region_name'] = $bah_5['region_name'];
						$rated['rate'] = $bah_5['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}
				

				#if(strlen($cdr['clean_number']) == 3){
					//bahrain directory assistance -181
					#if(preg_match('/(^' . $bah_6['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_6['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$price = $bah_6['unit'];

						$rated['region_name'] = $bah_6['region_name'];
						$rated['rate'] = $bah_6['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//bahrain directory assistance -188
					#if(preg_match('/(^' . $bah_7['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_7['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$price = $bah_7['unit'];

						$rated['region_name'] = $bah_7['region_name'];
						$rated['rate'] = $bah_7['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//bahrain emergency - 999
					#if(preg_match('/(^' . $bah_8['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_8['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$price = $bah_8['unit'];

						$rated['region_name'] = $bah_8['region_name'];
						$rated['rate'] = $bah_8['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//bahrain emergency - 199
					#if(preg_match('/(^' . $bah_9['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_9['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$price = $bah_9['unit'];

						$rated['region_name'] = $bah_9['region_name'];
						$rated['rate'] = $bah_9['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//mena wimax - 77
					 if(preg_match('/(^' . $bah_10['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
                                                $duration = $cdr['call_duration_int'];
						$price = $bah_10['unit'] * ($duration/60);

                                                $rated['region_name'] = $bah_10['region_name'];
                                                $rated['rate'] = $bah_10['rate'];
                                                $rated['price'] = $price;
                                                $bah_found =true;
                                        }


					 //mena helpdesk - 07770
                                         if(preg_match('/(^' . $bah_11['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
                                                $duration = $cdr['call_duration_int'];
                                                $price = $bah_11['unit'] * ($duration/60);

                                                $rated['region_name'] = $bah_11['region_name'];
                                                $rated['rate'] = $bah_11['rate'];
                                                $rated['price'] = $price;
                                                $bah_found =true;
                                        }


                                        if(preg_match('/(^' . $bah_12['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
                                                $duration = $cdr['call_duration_int'];
                                                //$price = ceil($duration / 90) * $bah_4['unit'];
                                                $price = ceil($duration / 180) * 0.021;

                                                $rated['region_name'] = $bah_12['region_name'];
                                                $rated['rate'] = $bah_12['rate'];
                                                $rated['price'] = $price;
                                                $bah_found =true;
                                        }

                                        if(preg_match('/(^' . $bah_13['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
                                                $duration = $cdr['call_duration_int'];
                                                //$price = ceil($duration / 180) * $bah_2['unit'];
                                                $price = ceil($duration / 90) * 0.021;

                                                $rated['region_name'] = $bah_12['region_name'];
                                                $rated['rate'] = $bah_12['rate'];
                                                $rated['price'] = $price;
                                                $bah_found =true;
                                        }


                                        if(preg_match('/(^' . $bah_14['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
                                                $duration = $cdr['call_duration_int'];
                                                //$price = ceil($duration / 180) * $bah_2['unit'];
                                                $price = ceil($duration / 90) * 0.021;

                                                $rated['region_name'] = $bah_13['region_name'];
                                                $rated['rate'] = $bah_13['rate'];
                                                $rated['price'] = $price;
                                                $bah_found =true;
                                        }

				
				
				#if(strlen($cdr['clean_number']) > 8){
					
					//bahrain fixed with 973
					#if(preg_match('/(^' . $bah_1['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					/*
					if(preg_match('/(^' . $bah_1['calling_code'] . '\d*$)/',$cdr['dialed_number'])){
						$duration = $cdr['call_duration_int'];
						//$price = ceil($duration / 180) * $bah_1['unit'];
						$price = ceil($duration / 180) * 0.021;

						$rated['region_name'] = $bah_1['region_name'];
						$rated['rate'] = $bah_1['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}

					//bahrain mobile with 973
					#if(preg_match('/(^' . $bah_3['calling_code'] . '\d*$)/',$cdr['clean_number'])){
					if(preg_match('/(^' . $bah_3['calling_code'] . '\d*$)/',$cdr['dialed_number'])){	
						$duration = $cdr['call_duration_int'];
						//$price = ceil($duration / 90) * $bah_3['unit'];
						$price = ceil($duration / 90) * 0.021;

						$rated['region_name'] = $bah_3['region_name'];
						$rated['rate'] = $bah_3['rate'];
						$rated['price'] = $price;
						$bah_found =true;
					}*/
				#}

				}
				if($bah_found){

					$ins = "{$cdr['new_ani']},{$cdr['ani']} ,{$cdr['start_time']},{$rated['region_name']}, {$cdr['clean_number']}, {$cdr['call_duration_int']},{$rated['rate']},{$rated['price']},{$cdr['call_dest_regid']},{$cdr['call_dest_uport']},{$cdr['call_source_regid']},{$cdr['call_source_uport']}\r\n";

				} else {
					$ins = "{$cdr['new_ani']},{$cdr['ani']} ,{$cdr['start_time']},'Unknown', {$cdr['clean_number']}, {$cdr['call_duration_int']},0,0,{$cdr['call_dest_regid']},{$cdr['call_dest_uport']},{$cdr['call_source_regid']},{$cdr['call_source_uport']}\r\n";
				}

			} else {
				$ins = "{$cdr['new_ani']},{$cdr['ani']} ,{$cdr['start_time']},{$rated['region_name']}, {$cdr['clean_number']}, {$cdr['call_duration_int']},{$rated['rate']},{$rated['price']},{$cdr['call_dest_regid']},{$cdr['call_dest_uport']},{$cdr['call_source_regid']},{$cdr['call_source_uport']}\r\n";

			}

			fwrite($file,$ins);
		}

	}

	fwrite($file,"done\r\n");
	fclose($file);
} else {
	echo "No calls found\n";
}
}

function do_magic($CDR,$RATES){

	require_once("DBAccessor.php");

	$cdr_file_id = $CDR['cdr_file_id'];
	$cdr_line_no = $CDR['cdr_line_no'];
	$ani = $CDR['ani'];
	$clean_number= $CDR['clean_number'];
	$dialed_number = $CDR['dialed_number'];
	$seconds = $CDR['call_duration_int'];
	$start_time = date('Y-m-d H:i:s',strtotime($CDR['start_time']));

	//find longest matching code and corresponding rate

	$len =0;
	$found = false;
	//echo "dialed=$clean_number\n";

	foreach($RATES as $rate){


		#if(strlen($clean_number) > 8 &&  preg_match('/(^' . $rate['calling_code'] . '\d*$)/', $clean_number)){
		if(preg_match('/(^00' . $rate['calling_code'] . '\d*$)/', $dialed_number)){
			if(strlen($rate['calling_code']) > $len){
				$found = true;
				$use = $rate;
			}
		}

	}

	if($found){
		if($seconds > 0){
			$minimum = floatval($use['rate']);
			if($seconds < 60)
				$price = $minimum;
			else
				$price = $seconds * $minimum / 60;
		} else {
			$price =0;
		}
		$use['price'] = $price;
		return $use;
	}
	else
	return false;
}


?>
