<?php
	session_start();
	
	if (isset($_POST['submit']))
	{
		if(!$_POST['txtLogin'] | !$_POST['txtPassword'])
		{
			header('Location: login_failed.php');
		}
		else
		{
			$pg_connection = pg_connect("dbname=ivr_db_ro user=ivr_fakhro host=www.db.2connectbahrain.com password=42ced7a951");
			$str_sql = "SELECT customer_id, first_name, last_name, e_mail, login_name, login_password, company, local_phone FROM customers where login_name = '".$_POST["txtLogin"]."' and login_password = '".$_POST["txtPassword"]."'";
			$login_result = pg_exec($pg_connection, $str_sql);
			
			if(!$login_result | (pg_num_rows($login_result) < 1))
			{
				session_write_close();
				header('Location: login_failed.php');
			}
			else
			{
				$_SESSION['login_name'] = $_POST['txtLogin'];
				$_SESSION['login_password'] = $_POST['txtPassword'];
				$_SESSION['customer_id'] = pg_result($login_result, 0, 0);
				$_SESSION['customer_first_name'] = pg_result($login_result, 0, 1);
				$_SESSION['customer_last_name'] = pg_result($login_result, 0, 2);
				$_SESSION['customer_email'] = pg_result($login_result, 0, 3);
				$_SESSION['customer_login_name'] = pg_result($login_result, 0, 4);
				$_SESSION['customer_login_password'] = pg_result($login_result, 0, 5);
				$_SESSION['customer_company'] = pg_result($login_result, 0, 6);
				$_SESSION['customer_phone'] = pg_result($login_result, 0, 7);
				
				session_write_close();
				header('Location: my2connect.php');
				
				exit;
			}
		}
	}
?> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>my2connect.com - 2call</title>
    <link rel="stylesheet" href="2connect_stylesheet.css" type="text/css" />
</head>

<body dir="ltr" lang="en-us" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">

<table dir="ltr" lang="en-us" align="left" width="960" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td align="left"><img src="images/page_header.jpg" alt="" width="950" height="122" border="0"><br><br></td>
</tr>
<tr>
	<td align="left">
		<table dir="ltr" lang="en-us" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="left" valign="top" style="padding-top: 10px; padding-bottom: 10px; padding-right: 10px; padding-left: 10px">
				<br>
				<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
				<!-- modified by alicia on 27-9-2007 at 1633hrs-->
				  <p class="smallBlueFont"><strong>2Call Referral Rewards Program &nbsp;</strong></p>
				  <p class="NormalBoldFont"><strong>Download <a href="2Connect_2Call_Form.pdf">2Call Application Form</a></strong></p>
				  <p class="NormalBoldFont"><strong>Download <a href="2Connect_2call Media Kit.pdf">2Call Media Kit</a> </strong></p>
				  <p><u>Terms  &amp; Conditions</u> <br>
				    For  a registered 2Call account user to be eligible for the 2Call Referral Rewards  Program, the following items must be forwarded to our office at 12th  Floor NBB Tower, or faxed at 17500109.</p>
				  <ul type="disc">
                    <li>One 2Call registration form adequately completed and       signed &ndash; e.g. your full name and 2Call registered number(s) clearly       indicated in the <em>&quot;Referred by&quot;</em> section. (Print the       attachment) </li>
				    <li>One clear copy of the applicant&rsquo;s CPR/CR. </li>
				    <li><em>BD 5</em> in cash for registration fee (or valid credit card information). </li>
				    <li><em>BD10</em> in cash for initial minimum account credit (or valid credit card       information). </li>
			      </ul>
				  <p>Note:  We highly recommend using credit card payment, as it facilitates future call  credit refills. </p>
				  <p><strong>Rewards:</strong> <br>
                      <strong><u>FREE  BD3 Call Credit</u></strong> <br>
				    For <strong>each</strong> 2Call account referred to us you will be rewarded with:</p>
				  <ul type="disc">
                    <li><em>BD3</em> FREE       Call Credit, credited to your 2Call account as indicated in the <em>&quot;Referred       by&quot;</em> section of the application form. </li>
			      </ul>
				  <p>If  you require additional information please do not hesitate to contact us your  2Call Hotline number&nbsp; 16500129 or email us at <a href="mailto:2call@2connectbahrain.com">2call@2connectbahrain.com</a><br>
				    Regards,<br>
				    Your  2Call Team</p>
				  <p class="NormalBoldFont">&nbsp;</p>
			  </form>
			</td>
		</tr>
		</table>	
	</td>
</tr>
<tr>
	<td align="center"><br><br><font size="-1">Copyright &copy; 2Connect&nbsp;|&nbsp;<a href="../terms.php">Terms & Conditions</a>&nbsp;|&nbsp;<a href="../privacy_policy.php">Privacy Policy</a></font></td>
</tr>
</table>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-386438-3";
urchinTracker();
</script> 

</body>
</html>
