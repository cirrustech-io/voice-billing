<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>my2connect.com - privacy policy</title>
	<link rel="stylesheet" href="2call/2connect_stylesheet.css" type="text/css" />
</head>

<body dir="ltr" lang="en-us" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">

<table dir="ltr" lang="en-us" align="left" width="960" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td align="left"><img src="images/page_header.jpg" alt="" width="950" height="122" border="0"><br><br></td>
</tr>
<tr>
	<td align="left">
		<table dir="ltr" lang="en-us" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="left" valign="top" style="padding-top: 10px; padding-bottom: 10px; padding-right: 10px; padding-left: 10px">
				<table dir="ltr" lang="en-us" align="center" width="80%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td align="left" valign="top">
						<font size="-1">
							<br><strong>2Connect's Privacy Statement for "2Connect Corporate"</strong><br><br>
							This privacy statement has been created by " 2Connect Corporate " in order to demonstrate 2Connect's firm commitment to maintain privacy for the user in accordance with their declared preference. The following discloses the information gathering and dissemination practices for this 2Connect "2Connect" Website:<br><br>

							<strong>Information Automatically Logged</strong><br>
							2Connect shall use the user's IP address to help diagnose problems with its server and to administer 2Connect's "2Connect" Website. The user's IP address is also used to help identify the user, and to gather broad demographic information.<br><br>

							<strong>A. Registration Forms</strong><br>
							2Connect's "2Connect" Website registration form(s) require users to provide 2Connect with contact information (for example, name, email, and postal address), unique identifiers (for example, central population registration number), and demographic information (for example, age or income level).<br><br>

							<strong>B. Surveys</strong><br>
							Survey's conducted online by 2Connect ask visitors for contact information (for example, email addresses), financial information (for example, account or credit card numbers), and demographic information (for example, age, or income level).<br><br>

							<strong>C. Contests</strong><br>
							2Connect shall run contests on 2Connect's site in which visitors are asked for contact information (for example, email addresses), financial information (for example, account or credit card numbers), and demographic information (for example, age or income level).<br><br>
							Contact information from the registration forms and surveys is used to ship purchases and to provide information to 2Connect. The user's contact information is also used to send promotional material from some of 2Connect's partners to 2Connect's users and to contact the user when necessary.<br><br>
							Users may opt-out of receiving future mailings; see the Choice/opt-out section below.<br><br>
							Unique identifiers (such as central population registration numbers) are collected from 2Connect's "2Connect" Website visitors to verify the user's identity, and for use as account numbers in our record system.<br><br>
							Demographic and profile data is collected at the 2Connect "2Connect" Website. This information is shared with advertisers on an aggregate basis. 2Connect shall use this data to tailor the visitor's experience at 2Connect's "2Connect" Website showing the visitor content that 2Connect considers may be of interest and displaying the content according to the visitor's preferences.<br><br>
							Financial information collected is used to check the users' qualifications for registration.<br><br>

							<strong>External Links</strong><br>
							This site contains links to other sites. "2Connect" is not responsible for the privacy practices or the content of such Web sites.<br><br>

							<strong>Public Forums</strong><br>
							This site creates chat rooms, forums, message boards, and/or news groups available to its users. Any information that is disclosed in these areas becomes public information and caution should be exercised when deciding to disclose personal information.<br><br>

							<strong>Security</strong><br>
							This site has security measures in place to protect 2Connect from loss, misuse, and alteration of the information under 2Connect's control. Databases are accessible only to the Webmaster, unless a contest involving a third party that sponsors prizes, is held.<br><br>

							<strong>Choice/Opt-Out</strong><br>
							2Connect's site provides users with the option to opt-out of receiving promotional/marketing information from our partners, and ourselves.<br><br>

							<strong>Data Quality/Access</strong><br>
							This site gives users the following options for changing and modifying information previously provided.<br><br>
						</font>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><br><br><br><br><font size="-1">Copyright &copy; 2Connect&nbsp;|&nbsp;<a href="terms.php">Terms & Conditions</a>&nbsp;|&nbsp;<a href="privacy_policy.php">Privacy Policy</a></font></td>
</tr>
</table>

</body>
</html>
