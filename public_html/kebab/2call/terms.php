<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>my2connect.com - terms</title>
	<link rel="stylesheet" href="2call/2connect_stylesheet.css" type="text/css" />
</head>

<body dir="ltr" lang="en-us" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">

<table dir="ltr" lang="en-us" align="left" width="960" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td align="left"><img src="images/page_header.jpg" alt="" width="950" height="122" border="0"><br><br></td>
</tr>
<tr>
	<td align="left">
		<table dir="ltr" lang="en-us" align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="left" valign="top" style="padding-top: 10px; padding-bottom: 10px; padding-right: 10px; padding-left: 10px">
				<table dir="ltr" lang="en-us" align="center" width="80%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td align="left" valign="top">
						<font size="-1">
							<br><strong>2Connect TERMS AND CONDITIONS FOR "2Conect" WEBSITE</strong><br><br>
							The "2Connect" Website and Web pages are operated by 2Connect. The "2Connect" Website is offered to the user on the user's unconditional acceptance without modification of the terms, conditions, and notices contained herein. Subject to using the "2Connect" Website, the user confirms their unconditional agreement to all such terms, conditions, and notices.<br><br>
							Furthermore, the "2Connect" Website may contain additional terms that govern particular features or offers (for example, promotions or chat areas). In the event that any of the terms, conditions, and notices contained herein conflict with the Additional Terms or other terms and guidelines contained within any particular "2Connect" Website, then these terms shall take precedence and control.<br><br>

							<strong>MODIFICATION OF TERMS AND CONDITIONS OF USE</strong><br>
							2Connect reserves the right to change the terms, conditions, and notices under which the "2Connect" Website is offered, including but not limited to the charges associated with the use of the "2Connect" Website. The user shall be wholly responsible for regularly reviewing these terms and conditions and shall be bound by these terms and conditions at all times.<br><br>

							<strong>PERSONAL AND NON-COMMERCIAL USE LIMITATION</strong><br>
							Unless otherwise specified, the "2Connect" Website is for the user's personal and non-commercial use. The user may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, software, products or services obtained from the "2Connect" Website.<br><br>

							<strong>LINKS TO THIRD PARTY SITES</strong><br>
							The "2Connect" Website may contain links to other Web Sites (hereinafter referred to as "Linked Sites"). The Linked Sites are not under the control of 2Connect and 2Connect shall not be in any way whatsoever responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. 2Connect shall not be responsible in any way whatsoever for web casting or any other form of transmission received from any Linked Site. 2Connect shall provide these links to the user only as a convenience, and the inclusion of any link does not imply any endorsement by 2Connect of the site or any association with its operators. 2Connect shall have no liability to the user for any failure to give notice to the user of the possible contents or subject matter of any transaction received from any Linked Site.<br><br>

							<strong>NO UNLAWFUL OR PROHIBITED USE</strong><br>
							As a condition of use of the "2Connect" Website, the user warrants to 2Connect that they shall not under any circumstances whatsoever use the "2Connect" Website for any purpose that is unlawful or prohibited by these terms, conditions, and notices. The user shall not use the "2Connect" Website in any manner or form which could damage, disable, overburden, or impair the "2Connect" Website or interfere with any other party's use and enjoyment of the "2Connect" Website. The user shall not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the "2Connect" Website.<br><br>

							<strong>USE OF COMMUNICATION SERVICES</strong><br>
							The "2Connect" Website may contain bulletin board services, chat areas, news groups, forums, communities, personal web pages, calendars, and/or other message or communication facilities designed to enable the user to communicate with the public at large or with a group (hereinafter collectively referred to as, "Communication Services"), the user hereby agrees to use the Communication Services only to post, send and receive messages and material that are proper and related to the particular Communication Service. By way of example, and not as a limitation, the user agrees that when using a Communication Service, they shall not under any circumstances whatsoever:<br><br>
							Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others.<br><br>

							<strong>SOFTWARE AVAILABLE ON THE</strong> "2Connect"  WEBSITE<br>
							Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful topic, name, material or information.<br><br>
							Upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless the user owns or controls the rights thereto or has received all necessary consents.<br><br>
							Upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer.<br><br>
							Advertise or offer to sell or buy any goods or services for any business purpose, unless such Communication Service specifically allows such messages.<br><br>
							Conduct or forward surveys, contests, pyramid schemes or chain letters.<br><br>
							Download any file posted by another user of a Communication Service that the user is aware of, or should reasonably be aware of that cannot be legally distributed in such manner.<br><br>
							Falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded.<br><br>
							Restrict or inhibit any other user from using and enjoying the Communication Services.<br><br>
							Violate any code of conduct or other guidelines which may be applicable for any particular Communication Service.<br><br>
							Harvest or otherwise collect information about other users, including e-mail addresses, without the consent of the other user.<br><br>
							Do anything that is contrary to Bahrain public policy or ethics.<br><br>
							Attempt to tamper with, modify or breach the system security in any way whatsoever, or seek to gain access to the system or user accounts that are stored within the "2Connect" system.<br><br>
							2Connect has no obligation to monitor the Communication Services. However, 2Connect reserves the right to review materials posted to a Communication Service and to remove any materials at its sole discretion. 2Connect reserves the right to terminate the user's access to any or all of the Communication Services at any time without notice.<br><br>
							2Connect reserves the right at all times to disclose any information when necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, at 2Connect 's sole discretion.<br><br>
							The user should always use caution when communicating any personally identifying information about themselves or their family members in any Communication Service. 2Connect does not control or endorse the content, messages or information found in any Communication Service and, therefore, 2Connect specifically disclaims any liability with regard to the Communication Services and any actions resulting from the user's participation in any Communication Service. Managers and hosts are not authorised 2Connect spokespersons, and their views do not necessarily reflect those of 2Connect. By acceptance of these terms and conditions the user irrevocably releases 2Connect from any liability in respect of any disclosure, accidental or otherwise, of any information regarding the user or the user's family members.<br><br>
							Materials uploaded to a Communication Service may be subject to posted limitations in distribution, reproduction and/or dissemination, the user is responsible for adhering to such limitations should such materials be download.<br><br>
							MATERIALS PROVIDED TO 2CONNECT  OR POSTED AT ANY "2Connect"  WEBSITE<br><br>
							2Connect does not claim ownership of the materials provide by the user to 2Connect (including feedback and suggestions) or post, upload, input or submit to any "2Connect" Website or its associated services (hereinafter collectively referred to as "Submissions"). However, by posting, uploading, inputting, providing or submitting Submission the user is granting 2Connect , its affiliated companies, assigns and necessary sub licensees permission to use the Submission in connection with the operation of their Internet businesses including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat the Submission; and to publish the users name in connection with the Submission provided by the user.<br><br>
							No compensation will be paid with respect to the use of any Submission, as provided herein. 2Connect is under no obligation to post or use any Submission the user may provide and 2Connect may remove any Submission at any time at 2Connect 's sole discretion.<br><br>
							By posting, uploading, inputting, providing or submitting the user's Submission the user warrants and represents ownership or otherwise control of all the rights to the Submission as described in this section including, without limitation, all the rights necessary for the user to provide, post, upload, input or submit the Submissions.<br><br>

							<strong>SOFTWARE AVAILABLE ON THE "2Connect" WEBSITE</strong><br>
							Software (if any) that is made available to download from the "2Connect"  Website, excluding software that may be made available by end-users through a Communication Service, ("Software") is the copyrighted work of 2Connect  and/or its suppliers. Use of the Software by the user is governed by the terms of the end user license agreement, if any, which accompanies or is included with the Software ("License Agreement"). The user may not install or use any Software that is accompanied by or includes a License Agreement unless the user first agrees to the License Agreement terms. For any Software not accompanied by a license agreement, 2Connect hereby grants to the user, a personal, non-transferable license to use the Software for viewing and otherwise using the particular "2Connect" WEBSITE in accordance with these Terms and Conditions of Use, and for no other purpose provided that the user keeps intact all copyright and other proprietary notices. All Software remains the exclusive property of 2Connect and/or its suppliers at all times and is protected by copyright laws and international treaty provisions.<br><br>
							Any reproduction or redistribution of the Software is expressly prohibited by law, and may result in severe civil and criminal penalties. Violators will be prosecuted to the maximum extent possible. Without limiting the foregoing, copying or reproduction of the software to any other server or location for further reproduction or redistribution is expressly prohibited. The software shall be warranted only according to the terms of the applicable software license agreement. The user acknowledges that the Software, and any accompanying documentation and/or technical information, is subject to the applicable export control laws and regulations of the State of Bahrain. The user agrees not to export or re-export the Software, directly or indirectly, to any countries that are subject to Bahrain export restrictions.<br><br>

							<strong>TERMINATION/ACCESS RESTRICTION</strong><br>
							2Connect  reserves the right, at its sole discretion, to terminate the user's access to any or all "2Connect" Website and the related services or any portion thereof at any time, without notice and shall not incur any liability whatsoever to the user for such termination.<br><br>

							<strong>FORCE MAJEURE</strong><br>
							<strong>Parties are not liable for any delay or failure to perform its obligations under this agreement which is caused by :</strong><br><br>
							typhoons, floods, earthquakes, hurricanes, fire, sabotage, civil commotion, malicious acts of damage, war (declared or undeclared), rebellion, military or usurped power, revolution, general shortages of materials, industrial or labour disputes, work bans, blockades, embargoes, quarantine restrictions, occupation of site or harbor congestion delay or failure to obtain any permit, authorization, customs certificate, license, approval or acknowledgement, despite proper and timely best endeavors to obtain the same; and Provided that the effects of such event(s) could not have reasonably been foreseen, was beyond the reasonable direct or indirect control, and without the fault or negligence, of the party claiming Force Majeure and which directly resulted in that party's inability, notwithstanding all reasonable efforts, to make alternative arrangements in order to perform its obligations in whole or in part. Any such event is referred to as a "Force Majeure Event".<br><br>
							Neither party shall be considered to be in default or in breach of its obligations under the agreement to the extent that performance of such obligations is prevented by any circumstances of Force Majeure.<br><br>
							Where either party considers that any circumstances of Force Majeure have occurred which may affect the performance of its obligations it shall promptly notify the other party thereof i.e. within four (4) hours of becoming aware of the Force Majeure event. Failure to notify the other party promptly shall disqualify the claim for Force Majeure.<br><br>
							Upon the occurrence of any circumstances of Force Majeure, the parties shall endeavor to continue to perform their obligations under this agreement so far as reasonably practicable. The party claiming Force Majeure shall notify the other party of the steps it proposes to take including any reasonable alternative means for performance which is not prevented by Force Majeure. The party claiming Force Majeure shall not take any steps unless agreed to by the other party in writing.<br><br>

							<strong>CONFIDENTIALITY</strong><br>
							Parties agree that neither party shall disclose to any third party any details whatsoever of this agreement without the prior written consent of the other party except as may be provided in this agreement or except as shall be required by a court of competent jurisdiction.<br><br>

							<strong>COPYRIGHT AND TRADEMARK NOTICES</strong><br>
							All contents of the "2Connect" Website are: 2Connect Copyright � 2006. 2Connect products referenced herein are either trademarks or registered trademarks of 2Connect. The names of actual companies and products mentioned herein may be the trademarks of their respective owners.<br><br>
							The example companies, organisations, products, people and events depicted herein are fictitious. Any association with any real company, organization, product, person, or event is completely coincidental and no intended association shall be inferred.<br><br>
							Any rights not expressly granted herein are reserved.<br><br>

							<strong>HEADINGS</strong><br>
							Clause headings are inserted for convenience of reference only and shall not affect the interpretation of this agreement.<br><br>

							<strong>LANGUAGE</strong><br>
							The English language shall be the official language of this agreement and all communications under it. The parties recognize and agree that this agreement may be interpreted into other languages. The English version of this agreement shall be the official version and shall prevail if any dispute in the interpretation of this agreement between such languages arises between the parties.<br><br>
							Unless otherwise specified in a particular case, words importing the singular number shall include the plural and vice versa and words importing persons shall include bodies corporate, unincorporated associations and partnerships and vice versa.<br><br>
							Reference to one gender shall include references to all other genders.<br><br>

							<strong>GENERAL</strong><br>
							This agreement is governed by the laws of the State of Bahrain. The user hereby consents to the exclusive jurisdiction and venue of courts in Bahrain, in all disputes arising out of or relating to the use of the "2Connect" Website. Use of the "2Connect" Website is unauthorised in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph. The user agrees that no joint venture, partnership, employment, or agency relationship exists between the user and 2Connect as a result of this agreement or use of the "2Connect"  Website.2Connect 's performance of this agreement is subject to existing laws and legal processes, and nothing contained in this agreement is in derogation of 2Connect 's right to comply with governmental, court and law enforcement requests or requirements relating to the use of the "2Connect" Website by the user or information provided to or gathered by 2Connect  with respect to such use. <br><br>
							If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed as severed from these Terms and Conditions and the remainder of these Terms and Conditions shall continue in effect. Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and 2Connect with respect to the "2Connect" Website and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and 2Connect with respect to the "2Connect" Website. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.<br><br>

							<br><br>End of 2Connect 's Terms and Conditions for "2Connect"  Website.<br><br>
						</font>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><br><br><br><br><font size="-1">Copyright &copy; 2Connect&nbsp;|&nbsp;<a href="terms.php">Terms & Conditions</a>&nbsp;|&nbsp;<a href="privacy_policy.php">Privacy Policy</a></font></td>
</tr>
</table>

</body>
</html>
