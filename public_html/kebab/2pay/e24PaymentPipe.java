// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   e24PaymentPipe.java

import com.sun.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.StringTokenizer;

public final class e24PaymentPipe
{

    public e24PaymentPipe()
    {
        ssl = 0;
        webAddress = "";
        port = "";
        id = "";
        password = "";
        action = "";
        transId = "";
        amt = "";
        responseURL = "";
        trackId = "";
        udf1 = "";
        udf2 = "";
        udf3 = "";
        udf4 = "";
        udf5 = "";
        paymentPage = "";
        paymentId = "";
        result = "";
        auth = "";
        ref = "";
        avr = "";
        date = "";
        currency = "";
        errorURL = "";
        language = "";
        context = "";
        error = "";
        rawResponse = "";
        debugMsg = new StringBuffer();
    }

    public synchronized void setSSL(int i) { ssl = i; }
    public synchronized String getWebAddress() { return webAddress; }
    public synchronized void setWebAddress(String s) { webAddress = s; }
    public synchronized String getPort() { return port; }
    public synchronized void setPort(String s) { port = s; }
    public synchronized void setId(String s) { id = s; }
    public synchronized String getId() { return id; }
    public synchronized void setPassword(String s) { password = s; }
    public synchronized String getPassword() { return password; }
    public synchronized void setAction(String s) { action = s; }
    public synchronized String getAction() { return action; }
    public synchronized void setTransId(String s) { transId = s; }
    public synchronized String getTransId() { return transId; }
    public synchronized void setAmt(String s) { amt = s; }
    public synchronized String getAmt() { return amt; }
    public synchronized void setResponseURL(String s) { responseURL = s; }
    public synchronized String getResponseURL() { return responseURL; }
    public synchronized void setTrackId(String s) { trackId = s; }
    public synchronized String getTrackId() { return trackId; }
    public synchronized void setUdf1(String s) { udf1 = s; }
    public synchronized String getUdf1() { return udf1; }
    public synchronized void setUdf2(String s) { udf2 = s; }
    public synchronized String getUdf2() { return udf2; }
    public synchronized void setUdf3(String s) { udf3 = s; }
    public synchronized String getUdf3() { return udf3; }
    public synchronized void setUdf4(String s) { udf4 = s; }
    public synchronized String getUdf4() { return udf4; }
    public synchronized void setUdf5(String s) {  udf5 = s;  }
    public synchronized String getUdf5() { return udf5; }
    public synchronized String getPaymentPage() { return paymentPage; }
    public synchronized String getPaymentId() { return paymentId; }
    public synchronized void setPaymentId(String s) { paymentId = s; }
    public synchronized void setPaymentPage(String s) { paymentPage = s; }
    public synchronized String getRedirectContent() {
        return new String("<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=" + paymentPage + "?PaymentID=" + paymentId + "\">");
    }
    public synchronized String getResult() { return result; }
    public synchronized String getAuth() { return auth; }
    public synchronized String getAvr() { return avr; }
    public synchronized String getDate() { return date; }
    public synchronized String getRef() { return ref; }
    public synchronized String getCurrency() { return currency; }
    public synchronized void setCurrency(String s) { currency = s; }
    public synchronized String getLanguage() { return language; }
    public synchronized void setLanguage(String s) { language = s; }
    public synchronized String getErrorURL() { return errorURL; }
    public synchronized void setErrorURL(String s) { errorURL = s; }
    public synchronized void setContext(String s) { context = s; }
    public synchronized String getErrorMsg() { return error; }
    public synchronized String getRawResponse() { return rawResponse; }
    public synchronized String getDebugMsg() { return debugMsg.toString(); }
// {{{ initialize_payment
    public synchronized short performPaymentInitialization()
        throws NotEnoughDataException
    {
        StringBuffer stringbuffer = new StringBuffer();
// {{{ moved to constants
        if(id.length() > 0)
            stringbuffer.append("id=" + id + "&");
        if(password.length() > 0)
            stringbuffer.append("password=" + password + "&");
        if(amt.length() > 0)
            stringbuffer.append("amt=" + amt + "&");
        if(currency.length() > 0)
            stringbuffer.append("currency=" + currency + "&");
        if(action.length() > 0)
            stringbuffer.append("action=" + action + "&");
        if(language.length() > 0)
            stringbuffer.append("langid=" + language + "&");
        if(responseURL.length() > 0)
            stringbuffer.append("responseURL=" + responseURL + "&");
        if(errorURL.length() > 0)
            stringbuffer.append("errorURL=" + errorURL + "&");
        if(trackId.length() > 0)
            stringbuffer.append("trackid=" + trackId + "&");
        if(udf1.length() > 0)
            stringbuffer.append("udf1=" + udf1 + "&");
        if(udf2.length() > 0)
            stringbuffer.append("udf2=" + udf2 + "&");
        if(udf3.length() > 0)
            stringbuffer.append("udf3=" + udf3 + "&");
        if(udf4.length() > 0)
            stringbuffer.append("udf4=" + udf4 + "&");
        if(udf5.length() > 0)
            stringbuffer.append("udf5=" + udf5 + "&");
// }}}
	String s = sendMessage(stringbuffer.toString(), "PaymentInitHTTPServlet");
        if(s == null)
            return -1;
        int i = s.indexOf(":");
        if(i == -1)
        {
            error = "Payment Initialization returned an invalid response: " + s;
            return -1;
        } else
        {
            paymentId = s.substring(0, i);
            paymentPage = s.substring(i + 1);
            return 0;
        }
    }
// }}}
    public synchronized short performTransaction()
        throws NotEnoughDataException
    {
        StringBuffer stringbuffer = new StringBuffer();
// {{{ moved to constants
        if(id.length() > 0)
            stringbuffer.append("id=" + id + "&");
        if(password.length() > 0)
            stringbuffer.append("password=" + password + "&");
        if(amt.length() > 0)
            stringbuffer.append("amt=" + amt + "&");
        if(action.length() > 0)
            stringbuffer.append("action=" + action + "&");
        if(paymentId.length() > 0)
            stringbuffer.append("paymentid=" + paymentId + "&");
        if(transId.length() > 0)
            stringbuffer.append("transid=" + transId + "&");
        if(trackId.length() > 0)
            stringbuffer.append("trackid=" + trackId + "&");
        if(udf1.length() > 0)
            stringbuffer.append("udf1=" + udf1 + "&");
        if(udf2.length() > 0)
            stringbuffer.append("udf2=" + udf2 + "&");
        if(udf3.length() > 0)
            stringbuffer.append("udf3=" + udf3 + "&");
        if(udf4.length() > 0)
            stringbuffer.append("udf4=" + udf4 + "&");
        if(udf5.length() > 0)
            stringbuffer.append("udf5=" + udf5 + "&");
// }}}
        String s = sendMessage(stringbuffer.toString(), "PaymentTranHTTPServlet");
        if(s == null)
            return -1;
        ArrayList arraylist = parseResults(s);
        if(arraylist == null)
        {
            return -1;
        } else
        {
            result = (String)arraylist.get(0);
            auth = (String)arraylist.get(1);
            ref = (String)arraylist.get(2);
            avr = (String)arraylist.get(3);
            date = (String)arraylist.get(4);
            transId = (String)arraylist.get(5);
            trackId = (String)arraylist.get(6);
            udf1 = (String)arraylist.get(7);
            udf2 = (String)arraylist.get(8);
            udf3 = (String)arraylist.get(9);
            udf4 = (String)arraylist.get(10);
            udf5 = (String)arraylist.get(11);
            return 0;
        }
    }

    private synchronized String sendMessage(String s, String s1)
        throws NotEnoughDataException
    {
        StringBuffer stringbuffer = new StringBuffer();
        debugMsg.append("\n---------- " + s1 + ": " + String.valueOf(new Timestamp(System.currentTimeMillis())) + " ----------");
        if(ssl == 1)
            System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
        try
        {
            if(webAddress.length() <= 0)
            {
                error = "No URL specified.";
                return null;
            }
// {{{ done
            if(ssl == 1)
                stringbuffer.append("https://");
            else
                stringbuffer.append("http://");
            stringbuffer.append(webAddress);
            if(port.length() > 0)
            {
                stringbuffer.append(":");
                stringbuffer.append(port);
            }
            if(context.length() > 0)
            {
                if(!context.startsWith("/"))
                    stringbuffer.append("/");
                stringbuffer.append(context);
                if(!context.endsWith("/"))
                    stringbuffer.append("/");
            } else
            {
                stringbuffer.append("/");
            }
            stringbuffer.append("servlet/");
            stringbuffer.append(s1);
            debugMsg.append("\nAbout to create the URL to: " + stringbuffer.toString());
// }}}
            URL url = new URL(stringbuffer.toString());
            debugMsg.append("\nAbout to create http connection");
            Object obj;
            if(ssl == 1)
                obj = (HttpsURLConnection)url.openConnection();
            else
                obj = url.openConnection();
            debugMsg.append("\nCreated connection");
            ((URLConnection) (obj)).setDoInput(true);
            ((URLConnection) (obj)).setDoOutput(true);
            ((URLConnection) (obj)).setUseCaches(false);
            ((URLConnection) (obj)).setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            debugMsg.append("\nREQUEST: " + s);
            if(s.length() > 0)
            {
                debugMsg.append("\nabout to write DataOutputSteam");
                DataOutputStream dataoutputstream = new DataOutputStream(((URLConnection) (obj)).getOutputStream());
                debugMsg.append("\nafter DataOutputStream");
                dataoutputstream.writeBytes(s);
                dataoutputstream.flush();
                dataoutputstream.close();
                BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(((URLConnection) (obj)).getInputStream()));
                rawResponse = bufferedreader.readLine();
                debugMsg.append("\nReceived RESPONSE: " + rawResponse);
                return rawResponse;
            } else
            {
                error = "No Data To Post!";
                throw new NotEnoughDataException(error);
            }
        }
        catch(Exception exception)
        {
            clearFields();
            error = "Failed to make connection:\n" + exception;
            return null;
        }
    }

    private ArrayList parseResults(String s)
    {
        ArrayList arraylist = new ArrayList(4);
        try
        {
            if(s.startsWith("!ERROR!"))
            {
                error = s;
                return null;
            }
            StringTokenizer stringtokenizer = new StringTokenizer(s, ":\r\n", true);
            String s1 = "";
            boolean flag = false;
            while(stringtokenizer.hasMoreElements()) 
            {
                String s2 = stringtokenizer.nextToken();
                if(!s2.startsWith(":"))
                {
                    arraylist.add(s2);
                    flag = false;
                } else
                {
                    if(flag)
                        arraylist.add("");
                    flag = true;
                }
            }
            return arraylist;
        }
        catch(Exception exception)
        {
            System.out.println(exception);
        }
        error = "Internal Error!";
        return null;
    }

    public void clearFields()
    {
        error = "";
        paymentPage = "";
        paymentId = "";
    }

    public static final int SUCCESS = 0;
    public static final int FAILURE = -1;
    int ssl;
    String webAddress;
    String port;
    String id;
    String password;
    String action;
    String transId;
    String amt;
    String responseURL;
    String trackId;
    String udf1;
    String udf2;
    String udf3;
    String udf4;
    String udf5;
    String paymentPage;
    String paymentId;
    String result;
    String auth;
    String ref;
    String avr;
    String date;
    String currency;
    String errorURL;
    String language;
    String context;
    String error;
    String rawResponse;
    StringBuffer debugMsg;
}
