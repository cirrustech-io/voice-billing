<?php  /****************************************************
	*
	* e24 payment back end
	*
	*
	* Copyright (C) 2006 Martin Papik
	*
	*/

	class e24payment {

// {{{ constants
		var $payment_init = Array (
			"id",
			"password",
			"amt",
			"currency",
			"currencycode",
			"action",
			"card",
			"expYear",
			"expMonth",
			"CVV2",
			"CVC2",
			"member",
			"addr",
			"zip",
			"langid",
			"responseURL",
			"errorURL",
			"trackid",
			"udf1",
			"udf2",
			"udf3",
			"udf4",
			"udf5",
		);
		var $payment_trans = Array (
			"id",
			"password",
			"amt",
			"action",
			"card",
			"expYear",
			"expMonth",
			"CVV2",
			"CVC2",
			"member",
			"addr",
			"zip",
			"paymentid",
			"transid",
			"trackid",
			"udf1",
			"udf2",
			"udf3",
			"udf4",
			"udf5",
		);
		var $res_params=Array(
			 0 => "result",
			 1 => "auth",
			 2 => "ref",
			 3 => "avr",
			 4 => "date",
			 5 => "transid",
			 6 => "trackid",
			 7 => "udf1",
			 8 => "udf2",
			 9 => "udf3",
			10 => "udf4",
			11 => "udf1",
		);
		var $response_vars = Array(
			"paymentid",
			"result",
			"auth",
			"ref",
			"tranid",
			"postdate",
			"trackid",
			"udf1",
			"udf2",
			"udf3",
			"udf4",
			"udf5",
		);
// }}}
		var $debug = 1;
		var $_var = Array(
		);
// {{{ support functions, varget, varset, dbg
		
		function varget($v) {
			return $this->_var[$v];
		}
		function varset($v,$val) {
			$this->_var[$v]=$val;
		}
		function get($v) {
			return $this->varget($v);
		}
		function set($v,$val) {
			return $this->varset($v,$val);
		}
	
		function dbg($txt) {
			if (!$this->debug) return;
			echo "<pre style='displayx:block;background:#0000ff;color:#ffffff;overflow:scroll;'>"
				.htmlspecialchars($txt)."</pre>";
		}
// }}}
// {{{ send_message
		
		function send_message($data,$s1) {
			$rgx_norm='@^/?(.*?)/?$@';
			$webaddress = $this->varget("webaddress");
			$ssl = $this->varget("ssl");
			$port = $this->varget("port");
			$context = $this->varget("context");

			$url ="";
			$url .= ($ssl         ? "https://":"http://");
			$url .= $webaddress;
			$url .= ($port!=""    ? ":".((int)$port) : "");
			$url .= ($context!="" ? preg_replace($rgx_norm,'/$1/',$context) : "/" );
			$url .= "servlet/$s1";
			
			$this->dbg ("sendmessage: $url");

			$req = curl_init();
			curl_setopt($req, CURLOPT_URL			, $url		);
			curl_setopt($req, CURLOPT_TIMEOUT		, 20		);
			curl_setopt($req, CURLOPT_POST			, true);
			curl_setopt($req, CURLOPT_RETURNTRANSFER	, true		);
			curl_setopt($req, CURLOPT_POSTFIELDS		, $data);

			// {{{ usused options
//			curl_setopt($req, CURLOPT_HEADER		, );
//			curl_setopt($req, CURLOPT_HTTPHEADER		, );
//			curl_setopt($req, CURLOPT_NOPROGRESS		, );
//			curl_setopt($req, CURLOPT_FAILONERROR  		, );
//			curl_setopt($req, CURLOPT_PUT			, );
//			curl_setopt($req, CURLOPT_REFERER		, );
//			curl_setopt($req, CURLOPT_USERAGENT		, );
//			curl_setopt($req, CURLOPT_SSLCERT		, );
//			curl_setopt($req, );
//			curl_setopt($req, );
//			curl_setopt($req, );
//			curl_setopt($req, );
			// }}}
			
			$res = curl_exec ($req);
			if ($res === FALSE) {
				$this->dbg ("cURL error: ".curl_errno($req)." // ".curl_error($req));
				curl_close($req);
				return false;
			}
			curl_close ($req);
			
			return $res;
		}

// }}}
// {{{ prepare_url
		
		function prepare_url ($variables) {
			$ret=Array();
			$s = "";
			$delim = "";
			foreach ($variables as $v) {
				$var = $this->varget($v);
				if ($var == "" ) continue;
				$s .= "$delim$v=".urlencode($var);
				$delim = "&";
				$ret[$v]=$var;
			}
			return $s;
		}
		
// }}}
		
		function get_response() {
			$ret = Array();
			
			foreach ($this->response_vars as $v) {
				if (get_magic_quotes_gpc()) {
					$ret[$v]=stripslashes($_POST[$v]);
				} else {
					$ret[$v]=$_POST[$v];
				}
			}
			return $ret;
		}

		function initialize_payment() {
			$url = $this->prepare_url($this->payment_init);
			$s = $this->send_message($url,"PaymentInitHTTPServlet");
			// !ERROR!-GW00460-TranPortal ID required.
//			$this->
			$this->dbg("PaymentInitHTTPServlet: $url -+-$s-+-");
			if ($s===FALSE) {
				$this->dbg ("initialize_payment() error calling send_message");
				return FALSE;
			}
			if (preg_match("/^([^:]*):(.*)$/",$s,$m)) {
				$this->varset("paymentid",$m[1]);
				$this->varset("paymentpage",$m[2]);
				$this->varset("errormsg","");
				return TRUE;
			}
			if (preg_match("/^!ERROR!(.*)$/",$s,$m)) {
				$this->varset("errormsg",$m[1]);
				$this->varset("paymentid",NULL);
				$this->varset("paymentpage",NULL);
				return FALSE;
			}
			$this->dbg("Unhandled condition");
			return FALSE;
		}

		function perform_transaction() {
			$url=$this->prepare_url($this->payment_trans);
			$s = $this->send_message($url,"PaymentTranHTTPServlet");
			$this->dbg("PaymentTranHTTPServlet: $url == -+-$s-+-");
			if ($s===FALSE) {
				$this->dbg ("perform_transaction() error calling send_message");
				return FALSE;
			}
			
			$l = trim($s);
			$i=-1;
			foreach (split(":",$l) as $val) { 
				$i++;
				$idx = $this->res_params[$i];
				if ($idx=="") {
					$this->dbg("perform_transaction(): too many parameters ($i, $val)");
					continue;
				}
				$this->varset($idx,$val);
			}
			if (preg_match("/^!ERROR!(.*)$/",$s,$m)) {
				$this->varset("errormsg",$m[1]);
				$this->varset("paymentid",NULL);
				$this->varset("paymentpage",NULL);
				return FALSE;
			}

			return TRUE;
		}

	}
?>
