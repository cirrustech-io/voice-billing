<?php

//DBAccessor
require_once('DBAccessor.php');


$file1 = fopen("Results/Fastelco-Bangladesh-Jun12-Jul12.csv", 'w') or die('Unable to open file');


$start_date=date('Y-m-d 00:00:00',strtotime('12-June-2008'));
$end_date= date('Y-m-d 23:59:59',strtotime('12-July-2008'));

$cdrRows = getFastelcoBangaldeshCDR($start_date,$end_date);

while($r = $cdrRows->fetchRow()){

	//store cdr info
	//start_time,clean_number,call_duration_int,call_source_regid,call_source_uport,originator_ip
	$src_reg_id= trim($r[3]);
	$src_port =   trim($r[4]);
	$clean_number=  trim($r[1]);
	$start_time=  trim($r[0]);
	$call_duration_int=  trim($r[2]);
	$originator_ip =  trim($r[5]);

	$insert = "$start_time,$clean_number,$call_duration_int,$src_reg_id,$src_port,$originator_ip" . "\r\n" ;
	fwrite($file1,$insert);

}

fwrite($file1,"done");
fclose($file1);

?>


