<?php

//DBAccessor
require_once('DBAccessor.php');


//$uniqueness = date('d-m-Y-H-i-s');
$file1 = fopen("Results/Bharti-July2008-India.csv", 'w') or die('Unable to open file');
$file2 = fopen("Results/Bharti-July2008-RoW.csv", 'w') or die('Unable to open file');

$start_date=date('Y-m-d 21:30:00',strtotime('30-June-2008'));
$end_date= date('Y-m-d 21:29:59',strtotime('31-July-2008'));

$cdrRows = getCDR($start_date,$end_date);

while($r = $cdrRows->fetchRow()){

	//store cdr info
	//callid, call_source_regid,call_source_uport,call_dest_regid,call_dest_uport, ani, clean_number, start_time,call_duration_int,originator_ip,terminator_ip,call_error_int
	$callid= trim($r[0]);
	$src_reg_id= trim($r[1]);
	$src_port =   trim($r[2]);
	$dest_reg_id=  trim($r[3]);
	$dest_port =  trim($r[4]);
	$ani =  trim($r[5]);
	$clean_number=  trim($r[6]);
	$start_time=  trim($r[7]);
	$call_duration_int=  trim($r[8]);
	$originator_ip =  trim($r[9]);
	$terminator_ip =  trim($r[10]);
	$call_error_int =  trim($r[11]);


	$insert = "$callid,$src_reg_id,$src_port,$dest_reg_id,$dest_port,$ani,$clean_number,$start_time,$call_duration_int,$terminator_ip,$originator_ip,$call_error_int," . "\r\n" ;
	if(substr($clean_number,0,2) == '91'){
		fwrite($file1,$insert);
	} else {
		fwrite($file2,$insert);
	}
}

fwrite($file1,"done"); fwrite($file2,"done");
fclose($file1); fclose($file2);

?>


