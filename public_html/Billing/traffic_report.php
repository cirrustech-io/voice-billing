<?php
	require 'DB.php';
	
	$db = DB::Connect('pgsql://alicia:fatty@localhost/billing');
	if (DB::isError($db)){
		die ($db->getMessage());
	}

	$db->setFetchMode(DB_FETCHMODE_ASSOC);


	$today = date('Y-m-d');
	$yesterday = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));
	
	$t_obj = array();

	echo "\nFetching traffic object info\n";
	$sql = "select carrier_id,endpoint,port from endpoint where endpoint_id in (select endpoint_id from traffic_object where rate_group_id in (1,2))";
	$result = $db->query($sql);
	
	if(DB::isError($result)){
                echo "\n***ERROR***\n";
                die($result->getMessage());
        }

	while($row = $result->fetchRow()){
		$obj = array();
		$obj[0] = $row['carrier_id'];
		$obj[1] = strtoupper($row['endpoint']) . "/" . $row['port'];
		$t_obj[]=$obj;	
	}


	

	echo "\nFetching cdrs for $yesterday\n";
	$sql = "select start_time,call_source_regid, call_source_uport, call_dest_regid, call_dest_uport, call_duration_int, call_error_int
		from new_cdr
		where start_time > '$yesterday'
		and start_time < '$today'";

	
	$result = $db->query($sql);
	
	if(DB::isError($result)){
		echo "\n***ERROR***\n";
		die($result->getMessage());
		#exit(0);

	} 

	$count = $result->numRows();
	echo"\nResults returned : $count\n";
	
	$output = array();
	$current = 0;
	while($row = $result->fetchRow()){
		$src = strtoupper($row['call_source_regid']) . "/" . $row['call_source_uport'];
		$dst = strtoupper($row['call_dest_regid']) . "/" . $row['call_dest_uport'];
		$min = $row['call_duration_int']/60;
		$error_int = $row['call_error_int'];
		$date_time = date('d-m-Y H:i:s',strtotime($row['start_time']));		

                #print_r($row);
		#find out src and dst carrier ids
		$src_cid = look_in($src,$t_obj);
		$dst_cid = look_in($dst,$t_obj);
		#array_search_in_level($src,$t_obj,1,$src_result,1);  $src_cid = (count($src_result)>0)?$src_result[0]:-1;
		#array_search_in_level($dst,$t_obj,1,$dst_result,1);  $dst_cid = (count($dst_result)>0)?$dst_result[0]:-1;	

		#echo "\n$src_cid and $dst_cid\n";
		if($src_cid == -1) echo "Could not find source $src\n";
		if($dst_cid == -1) echo "Could not find destination $dst\n";
		
		if(array_key_exists($src_cid,$output) && array_key_exists($dst_cid,$output[$src_cid])){

			$output[$src_cid][$dst_cid]['calls'] += 1;
			if($error_int){ $output[$src_cid][$dst_cid]['errors'] += 1;}
			$output[$src_cid][$dst_cid]['minutes'] += $min;
			if($date_time < ($output[$src_cid][$dst_cid]['first_call'])){ $output[$src_cid][$dst_cid]['first_call'] = $date_time; }
			if($date_time > ($output[$src_cid][$dst_cid]['last_call'])){ $output[$src_cid][$dst_cid]['last_call'] = $date_time; }

		} else {
			$output[$src_cid][$dst_cid]['calls'] = 1;
			$output[$src_cid][$dst_cid]['minutes'] = $min;
			if($error_int){ $output[$src_cid][$dst_cid]['errors'] = 1;}
			$output[$src_cid][$dst_cid]['first_call'] = $date_time;
			$output[$src_cid][$dst_cid]['last_call'] = $date_time;
		}

		$current ++;
		/*
		if(($src_cid == 35) and ($dst_cid == 35)) {
			exit(0);
		}
		*/
		#if($current == 500) {print_r($output); exit(0);}
		#echo "Completed " . round(($current/$count)*100,2) . "%\r";
		 
	}
	echo  "\n***** RESULTS *****\n";
	#print_r($output);
	exit(0); 

	

	function array_search_in_level($needle, $haystack, $key, &$result, $searchlevel = 0) { 
  		while(is_array($haystack) && isset($haystack[key($haystack)])) {
    			if($searchlevel == 0 && key($haystack) == $key && $haystack[$key] == $needle) {
      				$result = $haystack;
    			} elseif($searchlevel > 0) {
      				array_search_in_level($needle, $haystack[key($haystack)], $key, $result, $searchlevel - 1);
    			}	
    			next($haystack);
  		}
	} 
	
	function look_in($obj,$t_obj){
		foreach($t_obj as $t_item){
			if($t_item[1] == $obj){ return $t_item[0];} 
		}
		return -1;
	}


?>
