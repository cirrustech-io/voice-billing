<?php

//DBAccessor
require_once('DBAccessor.php');


$uniqueness = date('H-i-s');
$file1 = fopen("Results/Bharti-Termination-15to31May-$uniqueness.csv", 'w') or die('Unable to open file');

$header = "IST,Source Reg Id,Source Port,Dest Reg Id,Dest Port,Ani,Dialled Number,Destination,Minutes,Rate,Charges" . "\r\n" ;
fwrite($file1,$header);


$start_date=date('Y-m-d 21:30:00',strtotime('15-May-2008'));
//$end_date= date('Y-m-d 9:29:59',strtotime('1-April-2008'));
$end_date= date('Y-m-d 21:29:59',strtotime('31-May-2008'));

$cdrRows = getBhartiCDR($start_date,$end_date);

while($r = $cdrRows->fetchRow()){

	//store cdr info
	//callid, call_source_regid,call_source_uport,call_dest_regid,call_dest_uport, ani, clean_number, start_time,call_duration_int,originator_ip,terminator_ip,call_error_int
	$callid= trim($r[0]);
	$src_reg_id= trim($r[1]);
	$src_port =   trim($r[2]);
	$dest_reg_id=  trim($r[3]);
	$dest_port =  trim($r[4]);
	$ani =  trim($r[5]);
	$clean_number=  trim($r[6]);
	$start_time=  trim($r[7]);
	$ist = localToIST($start_time);
	$call_duration_int=  trim($r[8]);
	$originator_ip =  trim($r[9]);
	$terminator_ip =  trim($r[10]);
	$call_error_int =  trim($r[11]);

	$minutes = $call_duration_int / 60;
	$rate=0;
	$charges=0;
	$type = '';


	if(preg_match('/^9731/', $clean_number)){
		$rate = 0.035;
		$type = 'Bahrain Fixed';
	} elseif(preg_match('/^97339/', $clean_number) || preg_match('/^973388/', $clean_number)){
		$rate = 0.056;
		$type = 'Bahrain Batelco Mobile';
	} elseif(preg_match('/^97336/', $clean_number) || preg_match('/^973377/', $clean_number)){
		$rate = 0.081;
		$type = 'Bahrain Zain Mobile';
	} else {
		$rate = 0;
		$type = 'Unknown';
	}

	$charges = $minutes * $rate;

	$insert = "$ist,$src_reg_id,$src_port,$dest_reg_id,$dest_port,$ani,$clean_number,$type,$minutes,$rate,$charges" . "\r\n" ;
	fwrite($file1,$insert);

}

fwrite($file1,"done");
fclose($file1);



function localToIST($local){

	$totalHours = date("H",strtotime($local)) + 2;
	$totalMinutes = date("i",strtotime($local)) + 30;
	$totalSeconds = date("s",strtotime($local));
	$totalMonths = date("m",strtotime($local));
	$totalDays = date("d",strtotime($local));
	$totalYears = date("Y",strtotime($local));
	$timeStamp = mktime($totalHours, $totalMinutes, $totalSeconds, $totalMonths, $totalDays, $totalYears);

	$ist = date("d-m-Y H:i:s", $timeStamp);

	return $ist;
}

?>


