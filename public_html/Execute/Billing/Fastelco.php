<?php 

//upload facility
require_once ('HTTP/Upload.php');

//CSVParser
require_once('CSVParser.php');

//DBA Accessor
//require_once('DBAcessor_Admin.php');
require_once('DBAcessor.php');


//Auxillary functions
require_once('AuxiliaryFunctions.php');


//Constants
$CARRIER_NAME = "FASTELCO";
$INPUT_FILE_STORAGE_PATH = $CARRIER_NAME . "InputCSV/";
$CSVNAME = $CARRIER_NAME.date("D _M_ Y_H_i_s");

$INCOMING_PREFIX="00";
$OUTGOING_PREFIX="00";
$RATING_STRIP="2";
$ENDPOINT_PORT= "Talking-SIP/5:Talking-SIP-2/5:Talking-SIP-4/5";

$MINIMUM_DURATION= "1";
$BILLING_INCREMENT= "1";
$EFFECTIVE_DATE = date('Y-m-d',strtotime('18-Sep-2007'));
$PLAN_TYPE= "Defaultplan";

$CODE_TYPE = 'I';


$EXCEED_THRESHOLD_DATA = array();

// build the form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM"])) && ($_POST["MM"] == "myform")) {
	
	//upload files and save to the server
	$upload = new HTTP_Upload("en");
	$files = $upload->getFiles();
	
	//initialize parser to convert raw csv to 2D array
	$parser = new CsvFileParser();

	//verify if required files have been supplied
	$flagCust=false;
	foreach ($files as $file)
	{
		if($file->isMissing() || !$file->isValid() || (strtolower(substr($file->getProp('name'),-4,4)) != ".csv"))
			continue;
			
		$file->setName($CSVNAME . '-' .$file->getProp('name'));
		$file->moveTo($INPUT_FILE_STORAGE_PATH);
		$parsedData = $parser->ParseFromFile($INPUT_FILE_STORAGE_PATH .$file->getProp("name"));
		
		if($parsedData[0][3]=='Calling Code'){ //supp file
			$parsedCustData = array_slice($parsedData,1);
			$flagCust=true;			
		}
		
	}
	
	
	//retrieve carrier id
	$carrier_idRow = getCarrierId($GLOBALS['CARRIER_NAME']);	
	if(!$carrier_idRow->EOF){
		$carrier_info['carrier_id']= $carrier_idRow->Fields('carrier_id');		
	}
	
	
	//carrier specific info
	$carrier_info['carrier_name']=$CARRIER_NAME;
	$carrier_info['incoming_prefix']=$INCOMING_PREFIX;
	$carrier_info['outgoing_prefix']=$OUTGOING_PREFIX;
	$carrier_info['rating_strip']=$RATING_STRIP;
	$carrier_info['endpoint_port']=$ENDPOINT_PORT;
	$carrier_info['minimum_duration']=$MINIMUM_DURATION;
	$carrier_info['billing_increment']=$BILLING_INCREMENT;
	$carrier_info['effective_date']=$EFFECTIVE_DATE;
	$carrier_info['plan_type']=$PLAN_TYPE;
	$carrier_info['code_type']=$CODE_TYPE;
	
	//customer file info		
	$ignore_cust = array();
	$position_cust = array('region_name' => 4,'rate' => 5, 'code' => 3, 'effective_date' => -1);
	
	//destinations to avoid - Israel,Bahrain
	$avoid_codes = array('972');
			
	if($flagCust){
				
		//insert into update table and read update_id
		$update_id = insertUpdate(date('r'), $carrier_info['carrier_id']);
				
		//create Fastelco Customer csv
		createCSVFile($carrier_info,$parsedCustData,$position_cust,$ignore_cust,$avoid_codes,'Customer','w',$update_id);
		createRegionFile();
		Print "Fastelco files processed";
	}
	else 
		print "Required files have not been supplied";
		
		
	
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Fastelco Parser</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">

<p>
	Fastelco Customer File: <input type="file" name="cust_file">
</p>
 <p>
  	<input type="hidden" name="MM" value="myform" />
  	<input type="submit" name="Submit" value="Create CSV">    
 </p>
</form>
</body>
</html>
