<?php

//DBAccessor
require_once('DBAcessor.php');

	//read bill type, carrier name, start date and end date for rating
	
	$bill_type = 'O'; //whether termination or origination
		
	$who = 'BHARTI';
	$start_date=date('Y-m-d H:i:s',strtotime('2007-04-30 18:00:00'));
	$end_date= date('Y-m-d H:i:s',strtotime('2007-05-1'));
	
	$carrier_ids=array();
	
	//retrieve info from DB based on user choice!
	if($who == "All"){ // for all carriers
		
		$carrier_idRows = getAllCarriers(); 
		while(!$carrier_idRows->EOF){
			$carrier_ids[]= $carrier_idRows->Fiegetdebuginfolds('carrier_id');
			$carrier_idRows->MoveNext();
		}		
	}
	else{ // for specific carrier
		$carrier_idRow = getCarrierId($who);
		if(!$carrier_idRow->EOF){
			$carrier_ids[]= $carrier_idRow->Fields('carrier_id');
		}
	}
		
	foreach ($carrier_ids as $carrier_id){			
		$endPointRows = getCarrierEndpoints($carrier_id); //get endpoints for each carrier
	
		if(!$endPointRows->EOF){
			print "extracting unrated cdrs for carrier id $carrier_id ...";
			//extract unrated cdrs 
			$cdrs =extractUnratedCDRs($bill_type,$endPointRows,$start_date,$end_date);

			if(count($cdrs) > 0){
				
				print count($cdrs) ." cdrs found. rating started...";
				rateCDRs($cdrs,$carrier_id);		
				print "complete!";			
						
			}
		}		
	}

	
function extractUnratedCDRs($bill_type,$endPointRows,$start_date,$end_date){
	
	$cdrs= array(); // array to hold unrated cdrs
	
			//loop through to_end points
		while(!$endPointRows->EOF){
		
			//for each end point port extract unrated CDRs from new_cdr
			$end_point = $endPointRows->Fields('endpoint');	
			$port = $endPointRows->Fields('port');
				
			$cdrRows = getCDRsForRating($bill_type,trim($end_point),trim($port),$start_date,$end_date);
		
			while(!$cdrRows->EOF){
				if (($cdrRows->Fields('clean_number') != "") || ($cdrRows->Fields('clean_number') != null)){ //if valid number
					//store cdr info
					$cdr=array();
					$cdr['callid']= trim($cdrRows->Fields('callid'));
					$cdr['source_endpoint']=trim($cdrRows->Fields('call_source_regid'));
					$cdr['source_port'] = trim($cdrRows->Fields('call_source_uport'));
					$cdr['destination_endpoint']= trim($cdrRows->Fields('call_dest_regid'));
					$cdr['destination_port'] = trim($cdrRows->Fields('call_dest_uport'));
					$cdr['dirty']= trim($cdrRows->Fields('called_party_on_dst'));
					$cdr['clean']= trim($cdrRows->Fields('clean_number'));
					$cdr['starttime']= trim($cdrRows->Fields('start_time'));
					$cdr['duration']= trim($cdrRows->Fields('call_duration'));
					$cdr['call_duration_int']= trim($cdrRows->Fields('call_duration_int'));
					
					$cdrs[]=$cdr; //add to cdrs array
				
			
			}
			$cdrRows->MoveNext();
				
		} 
		// all cdrs for particular endpoint done
		
		$endPointRows->MoveNext();	
	}
	//all endpoints for carrier done
	return $cdrs;
}

function rateCDRs($cdrs,$carrier_id){

		foreach ($cdrs as $cdr){
		
			$call_id=$cdr['callid'];
			$dialed_number = $cdr['clean'] ;
			$start_time = date('Y-m-d H:i:s',strtotime($cdr['starttime']));
			$duration = $cdr['call_duration_int'];
		
			//prepare variables for rating
			$calling_code="";
			$try_calling_code="";
			$master_id=0;
			$rate_id=0;
			$rate=0;
			
			$codeRows = getCodesForRating(substr($dialed_number,0,1),$carrier_id,$start_time);
		
			if(!$codeRows->EOF){ //Codes and Rates Found
				
				
				while(!$codeRows->EOF){
					
					//perform longest match
					$try_calling_code = trim($codeRows->Fields('calling_code'));			
					
					if(strpos($dialed_number,$try_calling_code)>-1 && strpos($dialed_number,$try_calling_code)<1  && (strlen($try_calling_code) > strlen($calling_code))){
						$calling_code = $try_calling_code;	
						$master_id = trim($codeRows->Fields('master_id'));
						$rate = trim($codeRows->Fields('rate'));
						$rate_id = trim($codeRows->Fields('rate_id'));	
					
					}
					$codeRows->MoveNext();
				}	
			}
			
			
			//code match --- rate found yipee!
			if($master_id != 0){
								
				$price= floatval($rate)/60 * floatval($duration); //pricing per second
				
				//update CDR with price
				insertCDRRate($call_id,round($price,4),$rate_id);
			}	
		}
}
?>


