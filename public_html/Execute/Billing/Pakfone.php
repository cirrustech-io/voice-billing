<?php

//DBA Accessor
require_once('DBAcessor.php');

//Auxiliary Functions
require_once('AuxiliaryFunctions.php');

//global constants
$CARRIER_NAME= "Pakfone";
$OUTPUT_FILE_STORAGE_PATH= "PakfoneOutputCSV/";
$DELIMITER = ";";
$NEWLINE= "\r\n";
$PLAN_TYPE_FIELD= "Defaultplan";
$CARRIER_FIELD = "PAKFON";


$ENDPOINT_PORT= "PAKFONE-1/8";
$INCOMING_PREFIX="00";
$OUTGOING_PREFIX="";
$RATING_STRIP="0";


/*
$ENDPOINT_PORT= "PAKFONE-1/0";
$INCOMING_PREFIX="";
$OUTGOING_PREFIX="";
$RATING_STRIP="";
*/

$MINIMUM_DURATION= "1";
$BILLING_INCREMENT= "1";
$CSVNAME = $CARRIER_NAME.date("D _M_ Y_H_i_s");
$PAKFONE_RATE= 0.0525;
$CARRIER_ID = "7";
$EFFECTIVE_DATE=date('Y-m-d',strtotime('1-May-2007'));


function GeneratePakSupplierFile(){

	//open destination CSV files
	$dest_file1 = fopen($GLOBALS['OUTPUT_FILE_STORAGE_PATH']. $GLOBALS['CSVNAME'].'_Supplier.csv', 'w') or die('Unable to open file');

	//retrieve all Pakistan destinations from master table
	$allPakDest = getAllPakfoneDestinations();

	//write to master region file
	while(!$allPakDest->EOF){

		$region_name = $allPakDest->Fields('region_name');
		$code =$allPakDest->Fields('calling_code');
		$rate =round($GLOBALS['PAKFONE_RATE'],4);

		insertRate($GLOBALS['CARRIER_ID'],$code,$rate,$GLOBALS['EFFECTIVE_DATE']);

		//Start - formulate row
		$row = "";
		$row .= $GLOBALS['CARRIER_FIELD'] . $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'] .$GLOBALS['INCOMING_PREFIX'] . $GLOBALS['DELIMITER'] . $GLOBALS['OUTGOING_PREFIX'] . $GLOBALS['DELIMITER'] . $GLOBALS['RATING_STRIP'] . $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'] ;

		$row .= $region_name . $GLOBALS['DELIMITER'] . $GLOBALS['ENDPOINT_PORT'] . $GLOBALS['DELIMITER'] . $rate . $GLOBALS['DELIMITER'] . $GLOBALS['MINIMUM_DURATION'] . $GLOBALS['DELIMITER'] . $GLOBALS['BILLING_INCREMENT'] ;
		$row .=  $GLOBALS['DELIMITER'] .  $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'];
		$row .= $GLOBALS['PLAN_TYPE_FIELD'] . $GLOBALS['NEWLINE'];
		//End - formulate row

		//for each destination write a line in Supplier CSV file
		fwrite($dest_file1, $row);

		$allPakDest->MoveNext();
	}

	//close file
	fclose($dest_file1);
}

function GeneratePakCustomerFile(){

	$dest[] = array('BAHRAIN FIXED-INT-97317',
				'BAHRAIN CELLULAR-97339','BAHRAIN CELLULAR-97336');

	$rate[]= array('','','');


}

//GeneratePakSupplierFile();
//phpinfo();

createRegionFile();
echo 'done'
?>