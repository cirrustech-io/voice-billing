<?php

	//DBAccessor
	require_once('DBAcessor.php');
	
	$file = fopen('Trial/Top2Rates-1.csv', 'w') or die('Unable to open file');
	
	
	$codes = getUniqueCodes();
	print 'starting...';
	while(!$codes->EOF){
		
		$calling_code= $codes->Fields('calling_code');
		
		$data= getTop2Rates($calling_code);
		
		$count=0;
		while(!$data->EOF){
			$count ++;
			$region_name = $data->Fields('region_name');
			$calling_code = $data->Fields('calling_code');
			$rate = $data->Fields('rate');
			$carrier_name=$data->Fields('carrier_name');
			
			$data->MoveNext();
		}
		
		if($count >0){
			$insert = $count . ',' . $calling_code . ',' . $region_name . ',' . $rate . ',' . $carrier_name . "\r\n";
			fwrite($file,$insert);
			echo $calling_code . '\n';
		}
		$codes->MoveNext();
	}
	
	fclose($file);
	echo 'done';

?>