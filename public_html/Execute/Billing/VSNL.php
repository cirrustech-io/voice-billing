<?php 

//upload facility
require_once ('HTTP/Upload.php');


//CSVParser
require_once('CSVParser.php');

//CSVParser
require_once('DBAcessor.php');

//Auxillary functions
require_once('AuxiliaryFunctions.php');


$CARRIER_NAME= "VSNL";
$INPUT_FILE_STORAGE_PATH= "VSNLInputCSV/";
$OUTPUT_FILE_STORAGE_PATH= "VSNLOutputCSV/";
$DELIMITER = ";";
$NEWLINE= "\r\n";
$PLAN_TYPE_FIELD= "Defaultplan";
$CARRIER_FIELD = "VSNL";
/*
$ENDPOINT_PORT= "VSNL-1/0:VSNL-2/0:VSNL-3/0:VSNL-4/0:VSNL-5/0:VSNL-6/0:VSNL-7/0:VSNL-8/0:VSNL-9/0:VSNL-10/0:VSNL-11/0:VSNL-12/0:VSNL-13/0:VSNL-14/0:VSNL-15/0:VSNL-16/0:VSNL-17/0:VSNL-18/0:VSNL-19/0:VSNL-20/0:VSNL-21/0:VSNL-22/0:VSNL-23/0:VSNL-24/0:VSNL-25/0:VSNL-26/0:VSNL-27/0:VSNL-28/0:VSNL-29/0";
$INCOMING_PREFIX="";
$OUTGOING_PREFIX="";
$RATING_STRIP="";
*/

$ENDPOINT_PORT= "VSNL-3/8:VSNL-7/8";
$INCOMING_PREFIX="00";
$OUTGOING_PREFIX="";
$RATING_STRIP="0";

$MINIMUM_DURATION= "1";
$BILLING_INCREMENT= "1";
$CSVNAME = $CARRIER_NAME.date("D _M_ Y_H_i_s");
$CARRIER_ID=1;
$UPDATE_ID=0;
$effective_date="";

$EXCEED_THRESHOLD_DATA = array();

// build the form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM"])) && ($_POST["MM"] == "myform")) {
$upload = new HTTP_Upload("en");
$file = $upload->getFiles("file");
$CSVName = $CARRIER_NAME.date("d _M_ Y_H_i_s");
$file->setName($CSVName.".csv");
$dest_name = $file->moveTo("VSNLinputCSV/");


$file2 = $upload->getFiles("file2");
$CSVName2 = $CARRIER_NAME.date("d _M_ Y_H_i_s");
$file2->setName($CSVName2."_customer.csv");
$dest_name2 = $file2->moveTo("vsnlInputCSV/");

$parser = new CsvFileParser();
$parsedData = $parser->ParseFromFile("VSNLInputCSV/".$file->getProp("name"));
$parsedCustData =  $parser->ParseFromFile("vsnlInputCSV/".$file2->getProp("name"));

//loop through parsed data array
$i=0;
foreach ($parsedData as $row) {
	$i++;
	//find the headers - this is where the actual data starts
	if (($row[0]== 'Country') && ($row[1] == 'Destination')){
		$startIndex = $i;
	}
	
	if (substr($row[0],0,17) == 'Please be advised') {
		$endIndex = $i;
	}
	if (substr($row[1],0,4) == 'Date') {
		$effective_date = substr($row[1],5,strlen($row[1]));
		//$effective_date = str_replace(',', '', $effective_date);
		$effective_date = date("Y-m-d H:i:s",strtotime($effective_date));
	}

}


$trimmedData = array_slice($parsedData,$startIndex,$endIndex-$startIndex-1);

//insert row in update table
$updateId = insertUpdate(date('r'), 1);
$GLOBALS['UPDATE_ID']=$updateId;


//open destination CSV file
$file = fopen('VSNLOutputCSV/' .$CSVName.'_Supplier.csv', 'w') or die('Unable to open file'); 

foreach ($trimmedData as $trimmedRow) {
	
	//insert in region table
	$region_id = insertRegion($updateId,strtoupper($trimmedRow[0]."-".$trimmedRow[1]),strtoupper($trimmedRow[0]."-".$trimmedRow[1]),$trimmedRow[4],$trimmedRow[5]);
	
	//split the area codes by the number of commas
		$codes = array(); // array to store each name 
		if (is_int(strpos($trimmedRow[3], ','))) 
		{ 
			$codes = explode(',', $trimmedRow[3]); // Multiple names 
		} else { 
			$codes[] = $trimmedRow[3]; // Just one name 
		} 
	
		foreach ($codes as $code) {
			//get region name (everytime to avoid over appending)
			$region_name = strtoupper($trimmedRow[0]."-".$trimmedRow[1]);
			
			//replace unwanted characters
			$region_name = str_replace('&', '-', $region_name);
			$region_name = str_replace('(', '', $region_name);
			$region_name = str_replace(')', '', $region_name);
			$region_name = str_replace(',', '-', $region_name);
			$region_name = str_replace('.', '', $region_name);
			
			$region_name = substr($region_name, 0,22);
			
			//add country code to the begining of each area code
			$code = $trimmedRow[2].$code;
			$code = str_replace(' ', '', $code);
			
			if(strpos($code,"x") > -1 || strpos($code,"X") > -1)
				continue;
				
			//ignore destination bahrain
			if(strpos($code,'973')> -1 && strpos($code,'973') < 1)
				continue;
			
			if(strpos($code,'972')> -1 && strpos($code,'972') < 1 ){
				print "$code inside if <br>";
				continue;
			}
			
			if(strpos($code,'181')> -1 && strpos($code,'181') < 1 ){
				print "$code inside if <br>";
				continue;
			}
			/*
			if(strpos($code,'39')> -1 && strpos($code,'39') < 1){
				print "$code inside if <br>";
				continue;
			}
			
			if(strpos($code,'36')> -1 && strpos($code,'36') <1){
				print "$code inside if <br>";
				continue;
			}
			
			if(strpos($code,'160')> -1 && strpos($code,'160') <1){
				print "$code inside if <br>";
				continue;
			}
			
			if(strpos($code,'161')> -1 && strpos($code,'161') <1){
				print "$code inside if <br>";
				continue;
			}
			
			if(strpos($code,'165')> -1 && strpos($code,'165') <1){
				print "$code inside if <br>";
				continue;
			}
				
			if(strpos($code,'17')> -1 && strpos($code,'17') <1){
				print "$code inside if <br>";
				continue;
			}
			*/
			//for each code insert in code table
			insertCode($region_id, $code);
			
			//append code to region name
			$region_name = $region_name."-".$code;
			
			//replace spaces in region name with '_'
			while(strpos($region_name," ") > -1){
				$region_name = str_replace(' ', '_', $region_name);	
			}			
			
			$region_name =strtoupper($region_name);
			
			//check the master file if it already has the code
			$masterCode = checkCode($code,"I");			
			//if the code is not present in master region table
			//add a row for it
			if (($masterCode->Fields('region_name') == "") || ($masterCode->Fields('region_name') == null)) {
				insertMasterCode ($region_name, $code,"I");
			} else {
				//use the name already in the table as a region name
				$region_name = $masterCode->Fields('region_name');
			}
			
			//insert into rate table
			insertRate(1,$code,$trimmedRow[4],$effective_date);	
				
			
			//for each region write a line in CSV file **note this is outside code for loop**
			fwrite($file, "VSNL;;" . $GLOBALS['INCOMING_PREFIX'] .";" . $GLOBALS['OUTGOING_PREFIX']. ";". $GLOBALS['RATING_STRIP'] . ";;" .$region_name . ";". "VSNL-3/8:VSNL-7/8" . ";" . round(trim($trimmedRow[4]),4) .";1;1;;;;Defaultplan"."\r\n"); 
			
			if(floatval($trimmedRow[4]) > 0.5){
				$EXCEED_THRESHOLD_DATA[] = $row;
			}		
		}

	
	
}
//close file
fclose($file); 

//createRegionFile();

//createCustomerFile($parsedCustData);
createExceedThresholdFile($EXCEED_THRESHOLD_DATA,$THRESHOLD);
print "sucessfully created region and supplier file";

}

function createCustomerFile($parsedCustData){
	
	//open destination CSV file
	$dest_file = fopen($GLOBALS['OUTPUT_FILE_STORAGE_PATH']. $GLOBALS['CSVNAME'].'_Customer.csv', 'w') or die('Unable to open file'); 
	
	
	foreach($parsedCustData as $line){
			
			//skip header and empty rows
			if($line[0]=="" || $line[0]== null)
				continue;				
				
			//skip header rows
			if(strpos($line[0],"DESTINATION") > -1)
				continue;	
		
			$rate = round(trim($line[9]),4);
			$codes = array(); // array to store each name 
			if (is_int(strpos($line[6], ','))) 
			{ 
				$codes = explode(',', $line[6]); // Multiple names 
			} else { 
				$codes[] = $line[6]; // Just one name 
			} 		
			
			foreach ($codes as $code) {
			
				//get region name (everytime to avoid over appending)
				$region_name = trim($line[0]);
				$code = "973" . $code;
				
				//remove forbidden characters - & ( ) , [ ] .
				$region_name = str_replace('&', 'and', $region_name);
				$region_name = str_replace(',', '-', $region_name);
				$region_name = str_replace('(', ' ', $region_name);
				$region_name = str_replace(')', ' ', $region_name);
				$region_name = str_replace('[', ' ', $region_name);
				$region_name = str_replace(']', ' ', $region_name);
				$region_name = str_replace('.', ' ', $region_name);
				
				//truncate region_name to 22 characters if it exceeds 22
				if(strlen($region_name) > 22)
					$region_name = substr($region_name,0,22);
					
				$code = str_replace(' ', '', $code);
					
				if(strpos($code,"x") > -1 || strpos($code,"X") > -1)
					continue;
				
				//insert into region table
				$region_id = insertRegion($GLOBALS['UPDATE_ID'],$region_name,$region_name,$rate,$GLOBALS['effective_date']);
					
				//insert into code table
				insertCode($region_id,$code);
				
				//append code to region name
				$region_name = $region_name."-".$code;
				
				
				//replace spaces in region name with '_'
				while(strpos($region_name," ") > -1){
					$region_name = str_replace(' ', '_', $region_name);	
				}			
			
				$region_name =strtoupper($region_name);
				
				
				//check the master file if it already has the code
				$masterCode = checkCode($code,"I");			
				//if the code is not present in master region table
				//add a row for it
				if (($masterCode->Fields('region_name') == "") || ($masterCode->Fields('region_name') == null)) {
					insertMasterCode ($region_name, $code,"I");
				} else {
					//use the name already in the table as a region name
					$region_name = $masterCode->Fields('region_name');
				}
			
				//insert into rate table
				insertRate(1,$code, $rate,date("Y-m-d H:i:s",strtotime($line[14])));
		
				//Start - formulate row			
				$row = "";	
								
				$row .= $GLOBALS['CARRIER_FIELD'] . $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'] .$GLOBALS['INCOMING_PREFIX'] . $GLOBALS['DELIMITER'] . $GLOBALS['OUTGOING_PREFIX'] . $GLOBALS['DELIMITER'] . $GLOBALS['RATING_STRIP'] . $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'] ;
				
				$row .= $region_name . $GLOBALS['DELIMITER'] . $GLOBALS['ENDPOINT_PORT'] . $GLOBALS['DELIMITER'] . $rate . $GLOBALS['DELIMITER'] . $GLOBALS['MINIMUM_DURATION'] . $GLOBALS['DELIMITER'] . $GLOBALS['BILLING_INCREMENT'] ;						
				$row .=  $GLOBALS['DELIMITER'] .  $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'] . $GLOBALS['DELIMITER'];
				$row .= $GLOBALS['PLAN_TYPE_FIELD'] . $GLOBALS['NEWLINE'];
				
				//End - formulate row
							
				//for each destination write a line in CSV file
				fwrite($dest_file, $row);
			}	
	}	
	//End- Conversion to desired format
	
	//close file
	fclose($dest_file);  
}


?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <p>
    VSNL Input File: <input type="file" name="file"><br>
    <hr><br>
    Files supplied by Billing team <br>
    VSNL Customer File: <input type="file" name="file2"><br>
    
</p>
  <p>
  <input type="hidden" name="MM" value="myform" />
    <input type="submit" name="Submit" value="Create CSV">
    
</p>

</form>
</body>
</html>
