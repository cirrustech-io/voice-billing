<?php

//DBAccessor
require_once('DBAcessor.php');

// build the form action
//$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

//if ((isset($_POST["MM"])) && ($_POST["MM"] == "myform")) {

	//read bill type, carrier name, start date and end date for rating

	//$bill_type = $_POST['bill_type']; //whether termination or origination
	$bill_type = 'T';

	//$who = $_POST['carrier'];
	$who = 'IDT';
	//$start_date=date('Y-m-d 00:00:00',strtotime($_POST['data1']));
	//$end_date= date('Y-m-d 23:59:59',strtotime($_POST['data2']));

	$start_date = date('Y-m-d 00:00:00', strtotime('1-Nov-2007'));
	$end_date= date('Y-m-d 23:59:59',strtotime('5-Nov-2007'));

	$carrier_ids=array();

	//retrieve info from DB based on user choice!
	if($who == "All"){ // for all carriers

		$carrier_idRows = getAllCarriers();
		while(!$carrier_idRows->EOF){
			$carrier_ids[]= $carrier_idRows->Fields('carrier_id');
			$carrier_idRows->MoveNext();
		}
	}
	else{ // for specific carrier
		$carrier_idRow = getCarrierId($who);
		if(!$carrier_idRow->EOF){
			$carrier_ids[]= $carrier_idRow->Fields('carrier_id');
		}
	}

	foreach ($carrier_ids as $carrier_id){
		$endPointRows = getCarrierEndpoints($carrier_id); //get endpoints for each carrier

		if(!$endPointRows->EOF){
			//extract unrated cdrs
			$cdrs =extractUnratedCDRs($bill_type,$endPointRows,$start_date,$end_date);

			if(count($cdrs) > 0){

				rateCDRs($cdrs,$carrier_id);
				//echo "extracted from $start_date to $end_date";

			}

		}
	}
//}

function extractUnratedCDRs($bill_type,$endPointRows,$start_date,$end_date){

	$cdrs= array(); // array to hold unrated cdrs
	$uniqueness = date('d-m-Y-H-i-s');
	$file = fopen("Trial/Unrated_IDT_CDRS-$uniqueness.csv", 'w') or die('Unable to open file');

		//loop through to_end points
		while(!$endPointRows->EOF){

			//for each end point port extract unrated CDRs from new_cdr
			$end_point = $endPointRows->Fields('endpoint');
			$port = $endPointRows->Fields('port');

			$cdrRows = getCDRsForRating1($bill_type,trim($end_point),trim($port),$start_date,$end_date);



			while(!$cdrRows->EOF){
				if (($cdrRows->Fields('clean_number') != "") || ($cdrRows->Fields('clean_number') != null)){ //if valid number
					//store cdr info
					$cdr=array();
					$cdr['callid']= trim($cdrRows->Fields('callid'));
					$cdr['source_endpoint']=trim($cdrRows->Fields('call_source_regid'));
					$cdr['source_port'] = trim($cdrRows->Fields('call_source_uport'));
					$cdr['destination_endpoint']= trim($cdrRows->Fields('call_dest_regid'));
					$cdr['destination_port'] = trim($cdrRows->Fields('call_dest_uport'));
					$cdr['source_endpoint']= trim($cdrRows->Fields('call_source_regid'));
					$cdr['source_port'] = trim($cdrRows->Fields('call_source_uport'));
					$cdr['dirty']= trim($cdrRows->Fields('called_party_on_dst'));
					$cdr['clean']= trim($cdrRows->Fields('clean_number'));
					$cdr['starttime']= trim($cdrRows->Fields('start_time'));
					$cdr['duration']= trim($cdrRows->Fields('call_duration'));
					$cdr['call_duration_int']= trim($cdrRows->Fields('call_duration_int'));
					$cdr['terminator_ip'] = trim($cdrRows->Fields('terminator_ip'));
					$cdr['originator_ip'] = trim($cdrRows->Fields('originator_ip'));
					$cdr['ani'] = trim($cdrRows->Fields('ani'));
					$cdr['error_code'] = trim($cdrRows->Fields('call_error_int'));

					$cdrs[]=$cdr; //add to cdrs array

					$insert=$cdr['callid'] .','. $cdr['destination_endpoint'] .','. $cdr['destination_port'] . ','. $cdr['ani'] .','. $cdr['clean'] . ','. $cdr['starttime'] .','. $cdr['call_duration_int'] . ','. $cdr['error_code'] . "\r\n";
					fwrite($file,$insert);

			}
			$cdrRows->MoveNext();

		}
		// all cdrs for particular endpoint done

		$endPointRows->MoveNext();
	}
	//all endpoints for carrier done

	fwrite($file,"done");
	fclose($file);
	return $cdrs;

}

function rateCDRs($cdrs,$carrier_id){

		$uniqueness = date('d-m-Y-H-i-s');
		$file = fopen('Trial/RatedIDT_CDRs-'. $uniqueness . '.csv', 'w') or die('Unable to open file');
		$logfile = fopen('Trial/Log-IDT-' . $uniqueness . '.csv', 'w') or die('Unable to open file');
		foreach ($cdrs as $cdr){

			$call_id=$cdr['callid'];
			$ani = $cdr['ani'];
			$dialed_number = $cdr['clean'] ;
			$terminator_ip= $cdr['terminator_ip'] ;
			$start_time = date('Y-m-d H:i:s',strtotime($cdr['starttime']));
			$duration = $cdr['call_duration_int'];

			//prepare variables for rating
			$calling_code="";
			$try_calling_code="";
			$master_id=0;
			$rate_id=0;
			$rate=0;

			$codeRows = getCodesForRating(substr($dialed_number,0,1),$carrier_id,$start_time);

			if(!$codeRows->EOF){ //Codes and Rates Found
				fwrite($logfile,"found codes for $dialed_number\r\n");

				while(!$codeRows->EOF){

					//perform longest match
					$try_calling_code = trim($codeRows->Fields('calling_code'));

					if(strpos($dialed_number,$try_calling_code)>-1 && strpos($dialed_number,$try_calling_code)<1  && (strlen($try_calling_code) > strlen($calling_code))){
						$calling_code = $try_calling_code;
						$master_id = trim($codeRows->Fields('master_id'));
						$rate = trim($codeRows->Fields('rate'));
						$rate_id = trim($codeRows->Fields('rate_id'));
						fwrite($logfile,"found $calling_code for $dialed_number\r\n");
					}
					$codeRows->MoveNext();
				}
			}


			//code match --- rate found yipee!
			if($master_id != 0){

				$price= round(floatval($rate)/60 * floatval($duration),5); //pricing per second

				$insertString = "$call_id,$ani,$dialed_number,$start_time,$duration,$calling_code,$rate_id,$rate,$price\r\n";
				fwrite($file,$insertString) ;


				//update CDR with price
				insertCDRRate($call_id,$price,$rate_id);
			}
		}

		fwrite($file,'done');

		fclose($file);
		fclose($logfile);
}
?>
<!--

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">@import url(calendar-win2k-1.css);</style>
<script type="text/javascript" src="calendar.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="calendar-setup.js"></script>
<link rel="STYLESHEET" type="text/css" href='calendar-blue2.css'/>

</head>
<body>
<form action="<?php // echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
<font face="verdana" size="2">
 <h3>Rate unrated CDRS</h3><br><br>
 Choose Bill Type : <select name="bill_type">
 					<option value="O">Origination</option>
 					<option  value="T">Termination</option>
 				</select><br>
 Choose carrier : <select name="carrier">
 					<option value="All">All</option>
 					<option  value="PAKFONE">Pakfone</option>
 					<option  value="IDT">IDT</option>
 					<option  value="BHARTI">Bharti</option>
 					<option  value="CITIC">Citic</option>
 					<option  value="2CONNECT">2Connect</option>
 					<option  value="VSNL">VSNL</option>
 				</select><br>
 Start date		:<input type="text" id="data1" name="data1" /><button id="trigger1">...</button><br>
 End date		:<input type="text" id="data2" name="data2" /><button id="trigger2">...</button><br><br>
 <p>
  <input type="hidden" name="MM" value="myform" />
  <input type="submit" name="Submit" value="Rate!">
</p>
</font>
</form>

<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "data1",         // ID of the input field
      ifFormat    : "%m/%d/%Y",    // the date format
      button      : "trigger1"       // ID of the button
    }
  );
  Calendar.setup(
    {
      inputField  : "data2",         // ID of the input field
      ifFormat    : "%m/%d/%Y",    // the date format
      button      : "trigger2"       // ID of the button
    }
  );
</script>
</body>

-->