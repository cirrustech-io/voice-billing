<?php

//Aditional Functions
require_once('includes/functions.inc.php');

function insertUpdate ($received_date,$carrierId){
//Connection statement
include('Connections/DB.php');

$serialSQL = "SELECT nextval('public.update_update_id_seq'::text)";
$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

$insertUpdateSQL = sprintf("INSERT INTO update (update_id, received_date, carrier_id) VALUES (%s,%s, %s)",
GetSQLValueString($serialResult->Fields('nextval'), "int"),
GetSQLValueString($received_date,"date"),
GetSQLValueString($carrierId,"int"));
$UpdateResult = $DB->Execute($insertUpdateSQL) or die($DB->ErrorMsg());

return $serialResult->Fields('nextval');
}

function insertRegion ($update_id, $region_name, $region_code, $rate, $effective_date){

//Connection statement
include('Connections/DB.php');

	$serialSQL = "SELECT nextval('public.region_region_id_seq'::text)";
$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertRegionSQL = sprintf("INSERT INTO region (region_id, update_id, region_name, region_code, rate, effective_date) VALUES (%s,%s, %s, %s, %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'), "int"),
	GetSQLValueString($update_id,"int"),
	GetSQLValueString($region_name, "text"),
	GetSQLValueString($region_code, "text"),
	GetSQLValueString($rate, "double"),
	GetSQLValueString($effective_date, "date")
	);

	$regionResult = $DB->Execute($insertRegionSQL) or die($DB->ErrorMsg());

	return $serialResult->Fields('nextval');
}

function insertCode($region_id, $calling_code) {

	//Connection statement
include('Connections/DB.php');

	$serialSQL = "SELECT nextval('public.code_code_id_seq'::text)";
	$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertCodeSQL = sprintf("INSERT INTO code (code_id, region_id, calling_code) VALUES (%s, %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'),"int"),
	GetSQLValueString($region_id, "int"),
	GetSQLValueString($calling_code, "text")
	);
	$codeResult = $DB->Execute($insertCodeSQL) or die($DB->ErrorMsg());

}

function checkCode($calling_code,$type){
	//Connection statement
include('Connections/DB.php');

	$checkCodeSQL = sprintf("SELECT * FROM master WHERE calling_code=%s and type=%s",
	GetSQLValueString($calling_code,"text"),
	GetSQLValueString($type,"text"));
	$region = $DB->SelectLimit($checkCodeSQL) or die($DB->ErrorMsg());
	return $region;
}

function insertMasterCode ($region_name, $calling_code, $type) {
		//Connection statement
include('Connections/DB.php');

	$serialSQL = "SELECT nextval('public.master_master_id_seq'::text)";
	$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertMasterSQL = sprintf("INSERT INTO master (master_id, region_name, calling_code, type) VALUES (%s, %s, %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'),"int"),
	GetSQLValueString($region_name, "text"),
	GetSQLValueString($calling_code, "text"),
	GetSQLValueString($type, "text"));

	$codeResult = $DB->Execute($insertMasterSQL) or die($DB->ErrorMsg());

	return $serialResult->Fields('nextval');

}

function getAllMasterCodes () {
			//Connection statement
include('Connections/DB.php');

	$getAllCodesSQL = "SELECT * FROM master order by region_name";
	$allMasterCodes = $DB->Execute($getAllCodesSQL) or die($DB->ErrorMsg());
	return $allMasterCodes;
}

function getCarrierId ($carrier_name) {

	//Connection statement
	include('Connections/DB.php');

	$getCarrierIdSQL = sprintf("SELECT carrier_id FROM carrier WHERE carrier_name=%s",
	GetSQLValueString($carrier_name,"text"));
	$carrier_id = $DB->SelectLimit($getCarrierIdSQL) or die($DB->ErrorMsg());

	return $carrier_id;
}

function getAllCarriers () {

	//Connection statement
	include('Connections/DB.php');

$getAllCarriersSQL = "SELECT * FROM carrier order by carrier_name";
$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

return $carrier;
}

function insertRate ($carrier_id,$code,$rate,$effective_date) {

	//Connection statement
	include('Connections/DB.php');

	$master_id=0;

	//retrieve master id for the corresponding region name from master table
	$getMasterIdSQL = sprintf("SELECT master_id FROM master WHERE calling_code=%s",
	GetSQLValueString($code,"text"));
	$master_idRow = $DB->SelectLimit($getMasterIdSQL) or die($DB->ErrorMsg());
	if(!$master_idRow->EOF)
		$master_id = $master_idRow->Fields('master_id');

	//formulate insert row for rate table
	$serialSQL = "SELECT nextval('public.master_master_id_seq'::text)";
	$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertRateSQL = sprintf("INSERT INTO rate (rate_id, carrier_id, master_id,rate,effective_date) VALUES (%s, %s, %s , %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'),"int"),
	GetSQLValueString($carrier_id, "int"),
	GetSQLValueString($master_id, "int"),
	GetSQLValueString($rate, "double"),
	GetSQLValueString($effective_date, "date")
	);
	$rateResult = $DB->Execute($insertRateSQL) or die($DB->ErrorMsg());
}

function getAllPakfoneDestinations(){
	//Connection statement
include('Connections/DB.php');

	$getAllDestSQL = "SELECT * FROM master where calling_code like '92%'";
	$allPakDest = $DB->SelectLimit($getAllDestSQL) or die($DB->ErrorMsg());
	return $allPakDest;
}

function getCarrier($carrierId){
	//Connection statement
include('Connections/DB.php');

	$getCarrierSQL = sprintf("SELECT * FROM carrier WHERE carrier_id =%s",
	GetSQLValueString($carrierId, "int")
	);
	$carrier = $DB->SelectLimit($getCarrierSQL) or die($DB->ErrorMsg());
	return $carrier;
}

function getCarrierEndpoints($carrierId){
		//Connection statement
include('Connections/DB.php');

	$getCarrierEndpointsSQL = sprintf("SELECT * FROM endpoint WHERE carrier_id=%s",
	GetSQLValueString($carrierId, "int"));

	$endpoints = $DB->SelectLimit($getCarrierEndpointsSQL) or die($DB->ErrorMsg());

	return  $endpoints;
}

function getAllEndpoints(){
//Connection statement
include('Connections/DB.php');

	$getAllEndpointsSQL = "SELECT * FROM endpoint";
	$endpoints = $DB->SelectLimit($getAllEndpointsSQL) or die($DB->ErrorMsg());

	return $endpoints;
}

function getCDRs ($originating_carrier, $termination_carrier, $startDate , $endDate) {
	//Connection statement
include('Connections/DB.php');


$getCDRsSQL = sprintf("SELECT * FROM new_cdr WHERE call_source_regid IN (%s) AND call_dest_regid IN (%s) AND start_time >= %s AND start_time <= %s ",
$originating_carrier,
$termination_carrier,
GetSQLValueString($startDate,"date"),
GetSQLValueString($endDate, "date"));

$CDRs = $DB->Execute($getCDRsSQL) or die($DB->ErrorMsg());

return $CDRs;
}

function getRate($MRF_id, $carrier_id, $effective_date){
		//Connection statement
include('Connections/DB.php');

$getRateSQL = sprintf("SELECT * FROM rate WHERE master_id=%s AND carrier_id=%s AND effective_date <=%s ORDER BY effective_date DESC LIMIT 1",

GetSQLValueString($MRF_id,"int"),
GetSQLValueString($carrier_id,"int"),
GetSQLValueString($effective_date,"date"));
$rate = $DB->SelectLimit($getRateSQL) or die($DB->ErrorMsg());
return $rate;
}

function getCDRsForRating($bill_type,$call_regid,$port,$start_date,$end_date){

	//Connection statement
include('Connections/DB.php');


	if($bill_type=="O"){
		$getCDRsForRatingSQL =sprintf("select callid, call_dest_regid,call_dest_uport, call_source_regid,call_source_uport, called_party_on_dst,ani, clean_number, start_time, call_duration, call_duration_int,rate_id,price,originator_ip,terminator_ip from new_cdr where call_source_regid = %s and
		call_source_uport = %s and start_time >= %s and start_time <= %s and call_duration_int > 0 and rate_id is null",
		GetSQLValueString($call_regid,"text"),
		GetSQLValueString($port,"int"),
		GetSQLValueString($start_date,"date"),
		GetSQLValueString($end_date,"date"));
	}
	else if($bill_type=='T'){
		$getCDRsForRatingSQL =sprintf("select callid, call_dest_regid,call_dest_uport, call_source_regid,call_source_uport, called_party_on_dst,ani, clean_number, start_time, call_duration,call_duration_int,rate_id,price,originator_ip,terminator_ip from new_cdr where call_dest_regid = %s and
		call_dest_uport = %s and  start_time >= %s and start_time <= %s and call_duration_int > 0 and rate_id is null",
		GetSQLValueString($call_regid,"text"),
		GetSQLValueString($port,"int"),
		GetSQLValueString($start_date,"date"),
		GetSQLValueString($end_date,"date"));
	}
	$CDRsForRating = $DB->SelectLimit($getCDRsForRatingSQL) or die($DB->ErrorMsg());
	return $CDRsForRating;

}

function getCDRsForRating1($bill_type,$call_regid,$port,$start_date,$end_date){

	//Connection statement
	include('Connections/DB.php');
	if($bill_type=='T'){
		$getCDRsForRatingSQL =sprintf("select callid, call_dest_regid,call_dest_uport,ani, clean_number, start_time,call_duration_int,terminator_ip,call_error_int,call_error_str
		from new_cdr
		where call_dest_regid = %s and call_dest_uport = %s
		and  start_time >= %s and start_time <= %s",
		GetSQLValueString($call_regid,"text"),
		GetSQLValueString($port,"int"),
		GetSQLValueString($start_date,"date"),
		GetSQLValueString($end_date,"date"));
	}
	$CDRsForRating = $DB->SelectLimit($getCDRsForRatingSQL) or die($DB->ErrorMsg());
	return $CDRsForRating;

}

function getCodesForRating($calling_code,$carrier_id,$start_time){

	//Connection statement
	include('Connections/DB.php');

	$check_code = $calling_code . "%";

	$getCodeSQL = "select distinct m.master_id,m.calling_code,r.rate_id,r.rate,r.effective_date from master as m,rate as r where m.master_id=r.master_id and m.calling_code like '$check_code' and r.carrier_id=$carrier_id and r.effective_date <='$start_time' ORDER BY effective_date DESC";

	$codesForRating = $DB->SelectLimit($getCodeSQL) or die($DB->ErrorMsg());
	return $codesForRating;

}

function insertCDRRate($call_id,$price,$rate_id){

	//Connection statement
	include('Connections/DB.php');

	$rateCDRSQL = sprintf("Update new_cdr set price=%s,rate_id=%s where callid=%s",
	GetSQLValueString($price,"double"),
	GetSQLValueString($rate_id,"int"),
	GetSQLValueString($call_id,"text"));

	$rateCDRResult=$DB->Execute($rateCDRSQL) or die($DB->ErrorMsg());;

}

function insertCDRRate1($call_id,$price,$rate_id){

	//Connection statement
	include('Connections/DB.php');

	$rateCDRSQL = sprintf("Update cdrs_idt_sep2007_3 set price=%s,rate_id=%s where callid=%s",
	GetSQLValueString($price,"double"),
	GetSQLValueString($rate_id,"int"),
	GetSQLValueString($call_id,"text"));

	$rateCDRResult=$DB->Execute($rateCDRSQL) or die($DB->ErrorMsg());;

}

//function getTry($call_regid,$port,$start_date,$end_date){
function getTry(){

	//Connection statement
include('Connections/DB.php');
/*
$getCDRsForRatingSQL =sprintf("select callid, call_source_regid,call_source_uport, clean_number, start_time,  call_duration_int,rate_id,price from new_cdr where call_source_regid = %s and
		call_source_uport = %s and start_time >= %s and start_time <= %s and call_duration_int > 0",
		GetSQLValueString($call_regid,"text"),
		GetSQLValueString($port,"int"),
		GetSQLValueString($start_date,"date"),
		GetSQLValueString($end_date,"date"));
		*/


	$getCDRsForRatingSQL = "select callid, ani, clean_number, start_time, call_duration_int from new_cdr where ani like '%16500156%' and clean_number like '92%' and  start_time >= '1-SEP-2007' and start_time < '15-SEP-2007'";

	$CDRsForRating = $DB->SelectLimit($getCDRsForRatingSQL) or die($DB->ErrorMsg());
	return $CDRsForRating;

}

function getUniqueCodes(){
	include('Connections/DB.php');

	$getUniqueCodesSQL = "SELECT distinct calling_code from master";
	$allCallingCodes = $DB->Execute($getUniqueCodesSQL) or die($DB->ErrorMsg());
	return $allCallingCodes;


}

function getTop2Rates($calling_code){

	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = sprintf("select distinct m.region_name, m.calling_code , r.rate, c.carrier_name from carrier as c , master as m, rate as r where m.master_id= r.master_id and c.carrier_id=r.carrier_id and calling_code = %s order by region_name asc, rate desc limit 2
",
	GetSQLValueString($calling_code,"text"));
	$rows =  $DB->Execute($getRowsSQL) or die($DB->ErrorMsg());

	return $rows;
}

function getInternationalCodes(){
	//Connection statement
	include('Connections/DB.php');

	//$getRowsSQL = "select * from master where type='I' order by calling_code";
	$getRowsSQL = "select * from master order by calling_code";
	$rows =  $DB->Execute($getRowsSQL) or die($DB->ErrorMsg());

	return $rows;
}

function getRatesPart1($effective_date){


	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = sprintf("select master_id,carrier_id,max(effective_date) as eff_date from rate where effective_date<=%s group by carrier_id,master_id order by master_id,carrier_id

",
	GetSQLValueString($effective_date,"date"));
	$rows =  $DB->Execute($getRowsSQL) or die($DB->ErrorMsg());

	return $rows;


}

function getRatesPart2($master_id,$carrier_id,$effective_date){

	//Connection statement
	include('Connections/DB.php');

	$getRowsSQL = sprintf("select rate_id,rate from rate where master_id= %s and carrier_id= %s and effective_date= %s order by rate_id desc limit 1",
	GetSQLValueString($master_id,"int"),
	GetSQLValueString($carrier_id,"int"),
	GetSQLValueString($effective_date,"date"));
	$row =$DB->Execute($getRowsSQL) or die($DB->ErrorMsg());

	return $row;
	//return $getRowsSQL;




}

function getRatesCoolness($effective_date){

	//Connection statement
	include('Connections/DB.php');

	//Nasty query which does what we want
	$getRowsSQL = sprintf("select r.rate_id,r.master_id,m.calling_code,m.region_name,r.carrier_id,c.carrier_name,r.rate,r.effective_date
							from rate as r, carrier as c, master as m
							where rate_id in
								(select rate_id from
								(select max(rate_id) as rate_id,master_id,carrier_id,effective_date
								from rate
								where (master_id,carrier_id,effective_date) in
									(select master_id,carrier_id,max(effective_date)
									from rate where effective_date<=%s
									group by carrier_id,master_id order by master_id,carrier_id)
								group by master_id,carrier_id,effective_date) as x
								)
							and m.master_id=r.master_id
							and c.carrier_id=r.carrier_id
							order by calling_code asc,carrier_name asc",
	GetSQLValueString($effective_date,"date"));

	$rows =  pg_query($getRowsSQL) or die('Query failed: ' . pg_last_error());
	return $rows;
}

function getAllLiveCarriers(){

	//Connection statement
	include('Connections/DB.php');

	$getAllCarriersSQL = "SELECT * FROM carrier where Type='C' order by carrier_name";
	$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

	return $carrier;
}

function getAllPrepaidCustomers(){

	//Connection statement
	include('Connections/DB.php');

	$getAllCarriersSQL = "SELECT * FROM carrier where Type='P' order by carrier_name";
	$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

	return $carrier;
}

function getCarrierRates($supp_id,$cust_id,$eff_date){
	
	//Connection statement
	include('Connections/DB.php');
	
	$getRowsSQL = "select r.rate_id,r.rate,r.effective_date,m.calling_code  
			from rate as r, master as m 
			where m.master_id = r.master_id
			and r.customer_carrier_id = " . GetSQLValueString($cust_id,"int") . 
			" and r.carrier_id = " . GetSQLValueString($supp_id,"int") .
			" and effective_date <= " . GetSQLValueString($eff_date,"date") .
			" order by calling_code desc, effective_date desc";
	
	$rows =  pg_query($getRowsSQL) or die('Query failed: ' . pg_last_error());
	return $rows;
			
			
}



?>