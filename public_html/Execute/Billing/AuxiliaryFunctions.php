<?php

//DBA Accessor
require_once('DBAcessor.php');

//Storage paths
$ET_FILE_PATH= "ExceedThresholdFiles/ExceedThresholdFile.csv";
$MR_FILE_PATH= "MasterRegionFiles/MR-";

function createCSVFile($carrier_info,$data,$position,$ignore,$avoid_codes,$type,$mode,$update_id){
	
	$carrier_id=$carrier_info['carrier_id'];
	$carrier_name=$carrier_info['carrier_name'];
	$incoming_prefix=$carrier_info['incoming_prefix'];
	$outgoing_prefix=$carrier_info['outgoing_prefix'];
	$rating_strip=$carrier_info['rating_strip'];
	$endpoint_port=$carrier_info['endpoint_port'];
	$minimum_duration=$carrier_info['minimum_duration'];
	$billing_increment=$carrier_info['billing_increment'];
	$plan_type=$carrier_info['plan_type'];
	$code_type=$carrier_info['code_type'];
	$effective_start_date = date('Y-m-d 00:00:00');		//off- start date for first try
	$effective_end_date = '2015-11-06 00:00:00';
	$delimiter=";";
	$newline="\r\n";
	$exceed_threshold_data= array();
	$threshold = 0.5; //50 cents
	
	if($position['effective_date'] == -1){
		$effective_date=$carrier_info['effective_date'];
		$effective_start_date= $effective_date;			//on - start date for regular scenario
	}
		
	$file_storage_path = $carrier_name . "OutputCSV/" . $carrier_name . date("D _M_ Y_H_i_s") . "-" . $type . ".csv";
	//open destination CSV file
	$dest_file = fopen($file_storage_path, $mode) or die('Unable to open file'); 
	
	foreach($data as $line){
		
		//skip header rows
		if(in_array($line[0],$ignore))
			continue;
				
		$rate = round($line[$position['rate']],4);   	//rate rounded to decimel precision of 4
		
		
		if($position['effective_date'] != -1){
			$effective_date=$line[$position['effective_date']]; //read effective date
			$effective_date=date("Y-m-d H:i:s",strtotime($effective_date)); //convert date to desired format
			$effective_start_date= $effective_date;	 	//on - start date for regular scenario
		}
			
		
		$codes=array();	
		if (is_int(strpos($line[$position['code']], ','))) { 
			$codes = explode(',', trim($line[$position['code']])); //multiple codes
		} else  
			$codes[] = trim($line[$position['code']]); //one code
					
		foreach ($codes as $code) {
			
			print $code . "<br>";
			
			//removes spaces in codes
			$code = trim($code);
			
			//skip codes present in avoid-codes array
			if(isPresent($code,$avoid_codes))
				continue;
			
			//for Batelco , skip if not GCC	
			if($carrier_name=='BATELCO' && notGCC($code))
				continue;	
			/*				
			//for NWT , skip if not India	
			if($carrier_name=='NWT' && notIndia($code))
				continue;
			*/
			
			//for NWT , skip if not Egypt	
			if($carrier_name=='NWT' && notEgypt($code))
				continue;				
					
			//skip codes that have x or X
			if(strpos($code,"x") > -1 || strpos($code,"X") > -1)
				continue;
			
			//convert region_name to uppercase
			$region_name = strtoupper(trim($line[$position['region_name']]));
						
			//truncate region_name to 22 characters if it exceeds 22
			if(strlen($region_name) > 22)
				$region_name = substr($region_name,0,22);
			
			//remove forbidden characters
			$region_name = removeForbiddenCharacters($region_name);
			
			//insert into region table
			$region_id = insertRegion($update_id,$region_name,$region_name,$rate,date('Y-m-d'));
			
			//insert into code table
			insertCode($region_id,$code);
			
			//append code to region name
			$region_name = $region_name."-".$code;					
	
			print $region_name . "<br>";
			
			//check the master file if it already has the code
			$masterCode = checkCode($code,$code_type);		
				
			//if the code is not present in master region table
			//add a row for it
			if (($masterCode->Fields('region_name') == "") || ($masterCode->Fields('region_name') == null)) {
				insertMasterCode ($region_name, $code, $code_type);
			} else {
				//use the name already in the table as a region name
				$region_name =  $masterCode->Fields('region_name');
			}
			
			print "$region_name--$code--$rate--$effective_date<br>";
			
			//insert into rate table
			insertRate($carrier_id,$code,$rate,$effective_date);	
			
			
			//create row line
			$row = "";	
			$row .= $carrier_name . $delimiter . $delimiter  .$incoming_prefix . $delimiter  . $outgoing_prefix . $delimiter  . $rating_strip . $delimiter  . $delimiter  ;	
			$row .= $region_name . $delimiter  . $endpoint_port . $delimiter  . $rate . $delimiter  . $minimum_duration . $delimiter  . $billing_increment ;						
			$row .=  $delimiter  .  $delimiter  . $effective_start_date . $delimiter  . $effective_end_date . $delimiter . $plan_type . $newline;
				
			
			//Make a note of rates above threshold - 50 cents
			if(floatval($rate) > $threshold){
				$exceed_threshold_data[] = $row;
			}	
			
			//if rate > 99 cents - dont write to csv file- temp
			if(floatval($rate)>0.99 and $carrier_name != 'FASTELCO')
				continue;			
				
		
			//for each destination write a line in CSV file
			fwrite($dest_file, $row);
			
			
		}			
	}	

	//close file
	fclose($dest_file);
	
	//write to exceed threshold file 
	if(count($exceed_threshold_data) > 0)
		createExceedThresholdFile($exceed_threshold_data,$threshold);	

}

function createRegionFile() {
		
	$timestamp = date("D _M_ Y_H_i_s");
	//open destination CSV file
	$file = fopen($GLOBALS['MR_FILE_PATH']. $timestamp .'.csv', 'w') or die('Unable to open file'); 
	
	//retrieve all region names and their codes
	$allMasterCodes = getAllMasterCodes();
	
	//write to master region file
	while(!$allMasterCodes->EOF){
		$insertString = $allMasterCodes->Fields('region_name') . ";" . $allMasterCodes->Fields('calling_code') . ";" . $allMasterCodes->Fields('region_name') . "\r\n";
		fwrite($file,$insertString);
		$allMasterCodes->MoveNext();
	}
	
	//close file
	fclose($file);
}

function createExceedThresholdFile($rows, $threshold){
	
	$file = fopen($GLOBALS['ET_FILE_PATH'], 'a') or die('Unable to open file'); 
		
	fwrite($file,"\r\nSupplier rates exceeding USD " . $threshold . "\r\n\r\n" );
	
	foreach($rows as $row){
		fwrite($file,$row);	
	}
	
	fclose($file);	
	
}

function isPresent($code,$avoid_codes){
	
	foreach($avoid_codes as $avoid_code){
		if(strpos($code,$avoid_code)> -1 && strpos($code,$avoid_code) < 1 ){
			return true;
		}
	}
	return false;
}

function removeForbiddenCharacters($region_name){
	
	$region_name = str_replace('&', '-', $region_name);
	$region_name = str_replace(',', '-', $region_name);
	$region_name = str_replace('(', '-', $region_name);
	$region_name = str_replace(')', '-', $region_name);
	$region_name = str_replace('[', '-', $region_name);
	$region_name = str_replace(']', '-', $region_name);
	$region_name = str_replace('.', '-', $region_name);
	$region_name = str_replace("'", '', $region_name);
	
	while(strpos($region_name," ") > -1){
		$region_name = str_replace(' ', '-', $region_name);	
	}	
	
	return $region_name;
}

function prepareCustData($parsedCustData,$ignore_cust){
	
	$preparedData = array();
	foreach ($parsedCustData as $row) {
		
		if(in_array($row[0],$ignore_cust) || strpos($row[0],"DESTINATION") > -1)
			continue;
			
		if($row[0]=='')
			continue;
		
		$line = array();			
		$line[0]= trim($row[0]) . ' ' . trim($row[1]);
		$line[2] = trim($row[9]);
		$line[3] = trim($row[14]);
		
		$codes=array();	
		if (is_int(strpos($row[6], ','))) { 
			$codes = explode(',', trim($row[6])); //multiple codes
		} else  
			$codes[] = trim($row[6]); //one code
		
		foreach($codes as $code){
			$line[1]= trim($row[3]) . trim($code);
			$preparedData[]=$line;
		}
			
	}
	
	
	return $preparedData;
	
}

function processCodeChanges($carrier_info,$data,$ignore,$avoid_codes,$type){
	
	$carrier_id=$carrier_info['carrier_id'];
	$carrier_name=$carrier_info['carrier_name'];
	$effective_end_date=$carrier_info['effective_end_date'];
	
}

function DinarToDollar($dinar)
	{
		$dollar = round($dinar * 2.65,4);
		return $dollar;
	}

function notIndia($code){
	if(strpos($code,'91')> -1 && strpos($code,'91') < 1 )
		return false;
	else
		return true; 
			
}
	
function notEgypt($code){
	if(strpos($code,'20')> -1 && strpos($code,'20') < 1 )
		return false;
	else
		return true; 
			
}

function notGCC($code){
	
	$gcc_codes = array('973','971','974','966','968','965','999','181','800','199','188');
	foreach($gcc_codes as $gcc_code){
		if(strpos($code,$gcc_code)> -1 && strpos($code,$gcc_code) < 1 ){
			return false;
		}
	}
	return true;
}

?>