<?php
//DB Accessor
include_once('DBAcessor.php');

$AllRegions = getAllMasterCodes();

$allCarriers = getAllCarriers();
?>
<html>
<head></head>
<body>
<table border="1">
<tbody>
<tr>
<td>Region Name</td> <td>Dialing Code</td> 
<?php while (!$allCarriers->EOF) {
	?>
	<td><?php echo $allCarriers->Fields('carrier_name'); ?></td>
	<?php
	$allCarriers->MoveNext();
}?>

<?php
while (!$AllRegions->EOF) {
	
	$allCarriers->MoveFirst();
?>	
<tr>
<td><?php echo $AllRegions->Fields('region_name') ?></td> <td><?php echo $AllRegions->Fields('calling_code') ?></td>
	<?php
	
	while (!$allCarriers->EOF) {
		$rate = getRate($AllRegions->Fields('master_id'),$allCarriers->Fields('carrier_id'), date('Y-m-d'));
		if ( $rate->Fields('rate') == null) {
			$callingCode =$AllRegions->Fields('calling_code');
			
			while (strlen($callingCode) >1 && $rate->Fields('rate') == null ) {
				$callingCode = substr($callingCode,0,strlen($callingCode)-1);
				$newMasterCode = getMasterIdForDialCode($callingCode);
				
				if($newMasterCode != null){
					$rate = getRate($newMasterCode->Fields('master_id'),$allCarriers->Fields('carrier_id'), date('Y-m-d'));
				}			
			}
			
			
		}
		?>	
<td><?php if ($rate != null ) {echo $rate->Fields('rate');} ?></td>



<?php 
		$allCarriers->MoveNext();
	} 
?>
</tr>

<?php
	
	$AllRegions->MoveNext();
}

?>
</tbody>
</table>

</body>
</html>