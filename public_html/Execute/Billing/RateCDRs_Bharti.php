<?php

//DBAccessor
require_once('DBAcessor.php');

	//read bill type, carrier name, start date and end date for rating

	//$bill_type = $_POST['bill_type']; //whether termination or origination
	$bill_type = 'T';

	//$who = $_POST['carrier'];
	$who = 'BHARTI';
	//$start_date=date('Y-m-d 00:00:00',strtotime($_POST['data1']));
	//$end_date= date('Y-m-d 23:59:59',strtotime($_POST['data2']));

	$start_date = date('Y-m-d 21:30:00', strtotime('26-Dec-2007'));
	$end_date= date('Y-m-d 21:29:59',strtotime('31-Dec-2007'));

	//$start_date = date('Y-m-d 00:00:00', strtotime('1-Jan-2008'));
	//$end_date= date('Y-m-d 23:29:59',strtotime('1-Jan-2008'));

	$carrier_ids=array();

	//retrieve info from DB based on user choice!
	if($who == "All"){ // for all carriers

		$carrier_idRows = getAllCarriers();
		while(!$carrier_idRows->EOF){
			$carrier_ids[]= $carrier_idRows->Fields('carrier_id');
			$carrier_idRows->MoveNext();
		}
	}
	else{ // for specific carrier
		$carrier_idRow = getCarrierId($who);
		if(!$carrier_idRow->EOF){
			$carrier_ids[]= $carrier_idRow->Fields('carrier_id');
		}
	}

	foreach ($carrier_ids as $carrier_id){
		$endPointRows = getCarrierEndpoints($carrier_id); //get endpoints for each carrier

		if(!$endPointRows->EOF){
			//extract unrated cdrs
			$cdrs =extractUnratedCDRs($bill_type,$endPointRows,$start_date,$end_date);

			echo "\nExtracted from $start_date to $end_date\n\n";

			if(count($cdrs) > 0){

				rateCDRs($cdrs,$carrier_id,$bill_type,$end_date);


			}

		}
	}


function extractUnratedCDRs($bill_type,$endPointRows,$start_date,$end_date){

	$cdrs= array(); // array to hold unrated cdrs
	$uniqueness = date('d-m-Y-H-i-s');
	$file = fopen("Trial/Unrated_Bharti_CDRS-$uniqueness.csv", 'w') or die('Unable to open file');

		//loop through to_end points
		while(!$endPointRows->EOF){

			//for each end point port extract unrated CDRs from new_cdr
			$end_point = $endPointRows->Fields('endpoint');
			$port = $endPointRows->Fields('port');

			$cdrRows = getCDRsForRating1($bill_type,trim($end_point),trim($port),$start_date,$end_date);

			while(!$cdrRows->EOF){
				if (($cdrRows->Fields('clean_number') != "") || ($cdrRows->Fields('clean_number') != null)){ //if valid number
					//store cdr info
					$cdr=array();
					$cdr['callid']= trim($cdrRows->Fields('callid'));
					$cdr['source_endpoint']=trim($cdrRows->Fields('call_source_regid'));
					$cdr['source_port'] = trim($cdrRows->Fields('call_source_uport'));
					$cdr['destination_endpoint']= trim($cdrRows->Fields('call_dest_regid'));
					$cdr['destination_port'] = trim($cdrRows->Fields('call_dest_uport'));
					$cdr['source_endpoint']= trim($cdrRows->Fields('call_source_regid'));
					$cdr['source_port'] = trim($cdrRows->Fields('call_source_uport'));
					$cdr['dirty']= trim($cdrRows->Fields('called_party_on_dst'));
					$cdr['clean']= trim($cdrRows->Fields('clean_number'));
					$cdr['starttime']= trim($cdrRows->Fields('start_time'));
					$cdr['duration']= trim($cdrRows->Fields('call_duration'));
					$cdr['call_duration_int']= trim($cdrRows->Fields('call_duration_int'));
					$cdr['terminator_ip'] = trim($cdrRows->Fields('terminator_ip'));
					$cdr['originator_ip'] = trim($cdrRows->Fields('originator_ip'));
					$cdr['ani'] = trim($cdrRows->Fields('ani'));
					$cdr['error_code'] = trim($cdrRows->Fields('call_error_int'));

					if(floatval($cdr['error_code']) == 0){
						$cdrs[]=$cdr; //add to cdrs array
					}

					$insert=$cdr['callid'] .','. $cdr['destination_endpoint'] .','. $cdr['destination_port'] . ','. $cdr['ani'] .','. $cdr['clean'] . ','. $cdr['starttime'] .','. $cdr['call_duration_int'] . ','. $cdr['error_code'] . "\r\n";
					fwrite($file,$insert);

			}
			$cdrRows->MoveNext();

		}
		// all cdrs for particular endpoint done

		$endPointRows->MoveNext();
	}
	//all endpoints for carrier done

	fwrite($file,"done");
	fclose($file);
	return $cdrs;

}

function rateCDRs($cdrs,$carrier_id,$bill_type,$start_date){

		switch($bill_type){
			case 'T':
				$rateRows = getCarrierRates($carrier_id,6,$start_date);
				break;
			case 'O':
				$rateRows = getCarrierRates(6,$carrier_id,$start_date);
				break;
		}

		if(pg_num_rows($rateRows) > 0){

			$RATES = array();
			while($line = pg_fetch_array($rateRows,null,PGSQL_ASSOC)){

				$calling_code = $line['calling_code'];

				$rate = array();
				$rate['rate_id'] = $line['rate_id'];
				$rate['rate'] = $line['rate'];
				$rate['effective_date'] = $line['effective_date'];

				if(!rowFinder($RATES,$calling_code,$rate)){
					$RATES[$calling_code][] = $rate;
				}

			}
		}

		echo "\n\nExtracted=" . count($cdrs) . " cdrs\n\n";

		$RATED = array();

		foreach ($cdrs as $cdr){

			$call_id=$cdr['callid'];
			$ani = $cdr['ani'];
			$dialed_number = $cdr['clean'] ;
			$terminator_ip= $cdr['terminator_ip'] ;
			$start_time = date('Y-m-d H:i:s',strtotime($cdr['starttime']));
			$duration = $cdr['call_duration_int'];

			//search for longest match
			foreach($RATES as $key=>$rate_slice){
				if(preg_match("/(^{$key}\d*$)/",$dialed_number)){

					$go_through = $rate_slice;
					$found = false;
					foreach($go_through as $rate_row){
						if(date('Y-m-d H:i:s',strtotime($rate_row['effective_date'])) <= $start_time){


							$calling_code = $key;
							$rate = $rate_row['rate'];
							$rate_id = $rate_row['rate_id'];

							//echo "Found $calling_code as longest match for $dialed_number\n";

							$price= round(floatval($rate)/60 * floatval($duration),5); //pricing per second
							$cdr['longest_match']= $calling_code;
							$cdr['rate'] = $rate;
							$cdr['rate_id'] = $rate_id;
							$cdr['price'] = $price;

							$RATED[] = $cdr;

							echo round(count($RATED)/count($cdrs) * 100,3) . " % done\r";
							$found=true;
							break;
						}
					}

					if($found){
						break;
					}
				}
			}
		}

		echo "\n\nRated=" . count($RATED) . " cdrs\n\n";
		//echo "Updating database.. Writing to file...\n";
		echo "Writing to file...\n";
		$uniqueness = date('d-m-Y-H-i-s');
		$file = fopen('Trial/RatedBharti_CDRs-'. $uniqueness . '.csv', 'w') or die('Unable to open file');

		foreach($RATED as $cdr_r){

			$call_id=$cdr_r['callid'];
			$ani = $cdr_r['ani'];
			$dialed_number = $cdr_r['clean'] ;
			$start_time = date('Y-m-d H:i:s',strtotime($cdr_r['starttime']));
			$duration = $cdr_r['call_duration_int'];
			$calling_code = $cdr_r['longest_match'];
			$rate= $cdr_r['rate'];
			$rate_id = $cdr_r['rate_id'];
			$price = $cdr_r['price'];

			$insertString = "$call_id,$ani,$dialed_number,$start_time,$duration,$calling_code,$rate_id,$rate,$price\r\n";
			fwrite($file,$insertString) ;

			//update CDR with price
			//insertCDRRate($call_id,$price,$rate_id);

		}

		fwrite($file,'done');
		fclose($file);
		echo "\n---woohoo---\n";

}

function rowFinder($RATES,$calling_code,$rate){

	if(array_key_exists($calling_code,$RATES)){
		$rate_slice = $RATES[$calling_code];
		foreach($rate_slice as $row){
			if($row['effective_date'] == $rate['effective_date'])
				return true;
		}
		return false;
	}
	else
		return false;
}

?>
