<?php

//CSVParser
require_once('CSVParser.php');

$parser = new CsvFileParser();
$parsedDataCustomer = $parser->ParseFromFile("customer-codes.csv");
$parsedDataSupplier = $parser->ParseFromFile("supplier-rates.csv");

$file = fopen('Output.csv','w');
fwrite($file,"Code,VSNL,CITIC,NWT,BHARTI,KPN\r\n");

foreach($parsedDataCustomer as $crow){


	$c_code = $crow[0];
	$bha_rate = '-'; $bha_len = 0;
	$cit_rate = '-'; $cit_len = 0;
	$kpn_rate = '-'; $kpn_len = 0;
	$nwt_rate = '-'; $nwt_len = 0;
	$vsnl_rate = '-'; $vsnl_len = 0;

	if($c_code == ""){
		$insert = "\r\n";
	} else {
		foreach($parsedDataSupplier as $srow){

					$s_code = $srow[0];
					$supp = $srow[1];
					$supp_rate = round($srow[2] * 377.36,2);


					switch($supp){
						case 'VSNL':
											if(preg_match('/^' . $s_code . '/',$c_code) && strlen($s_code) <= strlen($c_code) && strlen($s_code) > $vsnl_len){

													$vsnl_rate = $supp_rate;
													$vsnl_len = strlen($s_code);

											}
							break;
						case 'CITIC':
											if(preg_match('/^' . $s_code . '/',$c_code) && strlen($s_code) > $cit_len && strlen($s_code) <= strlen($c_code) ){

													$cit_rate = $supp_rate;
													$cit_len = strlen($s_code);

											}
							break;

						case 'BHARTI':
							if(preg_match('/^' . $s_code . '/',$c_code) && strlen($s_code) > $bha_len && strlen($s_code) <= strlen($c_code)){


									$bha_rate = $supp_rate;
									$bha_len = strlen($s_code);

							}
							break;


						case 'KPN':
							if(preg_match('/^' . $s_code . '/',$c_code) && strlen($s_code) > $kpn_len && strlen($s_code) <= strlen($c_code) ){

									$kpn_rate = $supp_rate;
									$kpn_len = strlen($s_code);

							}
							break;
						case 'NWT':
							if(preg_match('/^' . $s_code . '/',$c_code) && strlen($s_code) > $nwt_len && strlen($s_code) <= strlen($c_code)){

									$nwt_rate = $supp_rate;
									$nwt_len = strlen($s_code);


							}
							break;


					}

				}

		$insert = "$c_code,$vsnl_rate,$cit_rate,$nwt_rate,$bha_rate,$kpn_rate\r\n";

	}

	fwrite($file,$insert);

}

fclose($file);
echo "done";


?>