<?php
require_once("DBAcessor.php");
$carriers =getAllCarriers();

// build the form action
$formAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "myform")) {

if ($_POST['select'] == 1) {
$page = "VSNL.php";
} else if ($_POST['select'] == 2) {
$page = "CITIC.php";
} else if ($_POST['select'] == 3) {
$page = "Bharti.php";
} else if ($_POST['select'] == 4) {
$page = "IDT.php";
} else if ($_POST['select'] == 5) {
$page = "Batelco.php";
} else if ($_POST['select'] == 6) {
$page = "2Connect.php";
} else if ($_POST['select'] == 7) {
$page = "Pakfone.php";
}
header('Location:'.$page) ;
exit(0);

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Billing CSV Parsers</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="../Distributor/taskStyle.css" rel="stylesheet" type="text/css" />

</head>

<body>
<table id="Table_01" width="832" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
    <td width="832" height="127" valign="top"><? include ("top.php")?>    </td>
  </tr>
  <tr>

    <td height="134" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
      <tr>
        <td width="130" rowspan="2" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
        <td height="31" valign="top" class="BlueHeadingFont">Choose Carrier </td>
        <td width="60" rowspan="2" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
      </tr>
      <tr>
        <td height="156" valign="top"><p class="NormalBoldFont">&nbsp;</p>
          <form id="form1" name="form1" method="post" action="<?php echo $formAction ?>">
            <label>
              <span class="NormalBoldFont">Carrier </span>
              <select name="select">
			  <?php while (!$carriers->EOF) { ?>
                <option value="<?php echo $carriers->Fields('carrier_id')?>"><?php echo $carriers->Fields('carrier_name')?></option>

                <?php
				$carriers->MoveNext();
			  } ?>
              </select>
              </label>
            <label>
			<input type="hidden" name="MM_insert" value="myform" />
            <input type="submit" name="Submit" value="Submit">

            </label>
          </form>
          </td>
      </tr>
    </table>
</td>

  <tr>
    <td height="21"></td>
  </tr>

  <tr>
    <td height="41" valign="top"><? include ("bottom.php") ?></td>
  </tr>

</table>
</body>
</html>