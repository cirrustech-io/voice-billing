<?php

//CSVParser
require_once('CSVParser.php');

$parser = new CsvFileParser();
$parsedDataCustomer = $parser->ParseFromFile("Customer-Rates.csv");
$parsedDataSupplier = $parser->ParseFromFile("Supplier-Rates.csv");


$file = fopen('Output.csv','w');
fwrite($file,"Customer,Destination,Code,Source,Cust_Rate,Batelco,Bharti,Citic,Computer-Tel,IDT,KPN,NWT,VSNL\r\n");

foreach($parsedDataCustomer as $crow){

	$customer = $crow[0];
	$destination= $crow[1];
	$c_code = $crow[2];
	$source = $crow[4];
	$cust_rate = $crow[3];

	$bat_rate = '-'; $bat_len = 0;
	$bha_rate = '-'; $bha_len = 0;
	$cit_rate = '-'; $cit_len = 0;
	$ctel_rate = '-'; $ctel_len = 0;
	$idt_rate = '-'; $idt_len = 0;
	$kpn_rate = '-'; $kpn_len = 0;
	$nwt_rate = '-'; $nwt_len = 0;
	$vsnl_rate = '-'; $vsnl_len = 0;


	foreach($parsedDataSupplier as $srow){

		$s_code = $srow[0];
		$supp = $srow[2];
		$supp_rate = round($srow[3] * 377.36,2);


		switch($supp){
			case 'BATELCO':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $bat_len){

				
					if($bat_len == 0 || strlen($s_code) <= strlen($c_code))
					{
							$bat_rate = $supp_rate;
							$bat_len = strlen($s_code);
					} 
					

				}
				break;
			case 'BHARTI':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $bha_len ){
					
					if($bha_len == 0 || strlen($s_code) <= strlen($c_code))
					{
						$bha_rate = $supp_rate;
						$bha_len = strlen($s_code);
						
					}

					

				}
				break;
			case 'CITIC':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $cit_len ){
					if($cit_len == 0 || strlen($s_code) <= strlen($c_code))
					{
						$cit_rate = $supp_rate;
						$cit_len = strlen($s_code);
					}	

				}
				break;
			case 'COMPUTER-TEL':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $ctel_len){
					
					if($ctel_len == 0 || strlen($s_code) <= strlen($c_code))
					{
						$ctel_rate = $supp_rate;
						$ctel_len = strlen($s_code);
						
					}

				}
				break;
			case 'IDT':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $idt_len ){
					if($idt_len == 0 || strlen($s_code) <= strlen($c_code))
					{

						$idt_rate = $supp_rate;
						$idt_len = strlen($s_code);
					}

				}
				break;
			case 'KPN':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $kpn_len ){
					if($kpn_len == 0 || strlen($s_code) <= strlen($c_code))
					{
						$kpn_rate = $supp_rate;
						$kpn_len = strlen($s_code);

					}

					

				}
				break;
			case 'NWT':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $nwt_len){
					if($nwt_len == 0 || strlen($s_code) <= strlen($c_code))
					{
						$nwt_rate = $supp_rate;
						$nwt_len = strlen($s_code);
						
					}

					

				}
				break;
			case 'VSNL':
				if(preg_match('/^' . $c_code . '/',$s_code) && strlen($s_code) > $vsnl_len){
					
					if($vsnl_len == 0 || strlen($s_code) <= strlen($c_code))
					{
						$vsnl_rate = $supp_rate;
						$vsnl_len = strlen($s_code);
						
					}

					

				}
				break;

		}

	}



	$insert = "$customer,$destination,$c_code,$source,$cust_rate,$bat_rate,$bha_rate,$cit_rate,$ctel_rate,$idt_rate,$kpn_rate,$nwt_rate,$vsnl_rate\r\n";

	echo ".<br>";
	fwrite($file,$insert);

}

fclose($file);
echo "done";


?>