<?php

//Aditional Functions
require_once('includes/functions.inc.php');

function insertUpdate ($received_date,$carrierId){
//Connection statement
include('Connections/DB.php');

$serialSQL = "SELECT nextval('public.update_update_id_seq'::text)";
$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

$insertUpdateSQL = sprintf("INSERT INTO update (update_id, received_date, carrier_id) VALUES (%s,%s, %s)",
GetSQLValueString($serialResult->Fields('nextval'), "int"),
GetSQLValueString($received_date,"date"),
GetSQLValueString($carrierId,"int"));
$UpdateResult = $DB->Execute($insertUpdateSQL) or die($DB->ErrorMsg());

return $serialResult->Fields('nextval');
}

function insertRegion ($update_id, $region_name, $region_code, $rate, $effective_date){

//Connection statement
include('Connections/DB.php');

	$serialSQL = "SELECT nextval('public.region_region_id_seq'::text)";
$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertRegionSQL = sprintf("INSERT INTO region (region_id, update_id, region_name, region_code, rate, effective_date) VALUES (%s,%s, %s, %s, %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'), "int"),
	GetSQLValueString($update_id,"int"),
	GetSQLValueString($region_name, "text"),
	GetSQLValueString($region_code, "text"),
	GetSQLValueString($rate, "double"),
	GetSQLValueString($effective_date, "date")
	);

	$regionResult = $DB->Execute($insertRegionSQL) or die($DB->ErrorMsg());

	return $serialResult->Fields('nextval');
}

function insertCode($region_id, $calling_code) {

	//Connection statement
include('Connections/DB.php');

	$serialSQL = "SELECT nextval('public.code_code_id_seq'::text)";
	$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertCodeSQL = sprintf("INSERT INTO code (code_id, region_id, calling_code) VALUES (%s, %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'),"int"),
	GetSQLValueString($region_id, "int"),
	GetSQLValueString($calling_code, "text")
	);
	$codeResult = $DB->Execute($insertCodeSQL) or die($DB->ErrorMsg());

}

function checkCode($calling_code){
	//Connection statement
include('Connections/DB.php');

	$checkCodeSQL = sprintf("SELECT * FROM master WHERE calling_code=%s",
	GetSQLValueString($calling_code,"text"));
	$region = $DB->SelectLimit($checkCodeSQL) or die($DB->ErrorMsg());
	return $region;
}

function insertMasterCode ($region_name, $calling_code) {
		//Connection statement
include('Connections/DB.php');

	$serialSQL = "SELECT nextval('public.master_master_id_seq'::text)";
	$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertMasterSQL = sprintf("INSERT INTO master (master_id, region_name, calling_code) VALUES (%s, %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'),"int"),
	GetSQLValueString($region_name, "text"),
	GetSQLValueString($calling_code, "text"));

	$codeResult = $DB->Execute($insertMasterSQL) or die($DB->ErrorMsg());

	return $serialResult->Fields('nextval');

}

function getAllMasterCodes () {
			//Connection statement
include('Connections/DB.php');

	$getAllCodesSQL = "SELECT * FROM master order by region_name";
	$allMasterCodes = $DB->Execute($getAllCodesSQL) or die($DB->ErrorMsg());
	return $allMasterCodes;
}

function getCarrierId ($carrier_name) {

	//Connection statement
	include('Connections/DB.php');

	$getCarrierIdSQL = sprintf("SELECT carrier_id FROM carrier WHERE carrier_name=%s",
	GetSQLValueString($carrier_name,"text"));
	$carrier_id = $DB->SelectLimit($getCarrierIdSQL) or die($DB->ErrorMsg());

	return $carrier_id;
}

function getAllCarriers () {

	//Connection statement
	include('Connections/DB.php');

$getAllCarriersSQL = "SELECT * FROM carrier";
$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

return $carrier;
}

function insertRate ($carrier_id,$code,$rate,$effective_date) {

	//Connection statement
	include('Connections/DB.php');

	//retrieve master id for the corresponding region name from master table
	$getMasterIdSQL = sprintf("SELECT master_id FROM master WHERE calling_code=%s",
	GetSQLValueString($code,"text"));
	$master_idRow = $DB->SelectLimit($getMasterIdSQL) or die($DB->ErrorMsg());
	if(!$master_idRow->EOF)
		$master_id = $master_idRow->Fields('master_id');

	//formulate insert row for rate table
	$serialSQL = "SELECT nextval('public.master_master_id_seq'::text)";
	$serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

	$insertRateSQL = sprintf("INSERT INTO rate (rate_id, carrier_id, master_id,rate,effective_date) VALUES (%s, %s, %s , %s, %s)",
	GetSQLValueString($serialResult->Fields('nextval'),"int"),
	GetSQLValueString($carrier_id, "int"),
	GetSQLValueString($master_id, "int"),
	GetSQLValueString($rate, "double"),
	GetSQLValueString($effective_date, "date")
	);
	$rateResult = $DB->Execute($insertRateSQL) or die($DB->ErrorMsg());
}

function getAllPakfoneDestinations(){
	//Connection statement
include('Connections/DB.php');

	$getAllDestSQL = "SELECT * FROM master where calling_code like '92%'";
	$allPakDest = $DB->SelectLimit($getAllDestSQL) or die($DB->ErrorMsg());
	return $allPakDest;
}

function getCarrier($carrierId){
	//Connection statement
include('Connections/DB.php');

	$getCarrierSQL = sprintf("SELECT * FROM carrier WHERE carrier_id =%s",
	GetSQLValueString($carrierId, "int")
	);
	$carrier = $DB->SelectLimit($getCarrierSQL) or die($DB->ErrorMsg());
	return $carrier;
}

function getCarrierEndpoints($carrierId){
		//Connection statement
include('Connections/DB.php');

	$getCarrierEndpointsSQL = sprintf("SELECT * FROM endpoint WHERE carrier_id=%s",
	GetSQLValueString($carrierId, "int"));

	$endpoints = $DB->SelectLimit($getCarrierEndpointsSQL) or die($DB->ErrorMsg());

	return  $endpoints;
}

function getAllEndpoints(){
//Connection statement
include('Connections/DB.php');

	$getAllEndpointsSQL = "SELECT * FROM endpoint";
	$endpoints = $DB->SelectLimit($getAllEndpointsSQL) or die($DB->ErrorMsg());

	return $endpoints;
}

function getCDRs ($originating_carrier, $termination_carrier, $startDate , $endDate) {
	//Connection statement
include('Connections/DB.php');


$getCDRsSQL = sprintf("SELECT * FROM new_cdr WHERE call_source_regid IN (%s) AND call_dest_regid IN (%s) AND start_time >= %s AND start_time <= %s ",
$originating_carrier,
$termination_carrier,
GetSQLValueString($startDate,"date"),
GetSQLValueString($endDate, "date"));

$CDRs = $DB->Execute($getCDRsSQL) or die($DB->ErrorMsg());

return $CDRs;
}

function getRate($MRF_id, $carrier_id, $effective_date){
		//Connection statement
include('Connections/DB.php');

$getRateSQL = sprintf("SELECT * FROM rate WHERE master_id=%s AND carrier_id=%s AND effective_date <=%s ORDER BY effective_date DESC LIMIT 1",

GetSQLValueString($MRF_id,"int"),
GetSQLValueString($carrier_id,"int"),
GetSQLValueString($effective_date,"date"));
$rate = $DB->SelectLimit($getRateSQL) or die($DB->ErrorMsg());
return $rate;
}

function getDoIt(){
//Connection statement
include('Connections/DB.php');

	$getDoItSQL = "select callid, called_party_on_dst, clean_number from new_cdr where clean_number is not null";
	$DoIt = $DB->SelectLimit($getDoItSQL) or die($DB->ErrorMsg());
	return $DoIt;
}

function getCDRsForRating($call_dest_regid,$start_date,$end_date){
	//Connection statement
	include('Connections/DB.php');

	$getCDRsForRatingSQL = "select callid, clean_number, call_duration from new_cdr";
	$getCDRsForRatingSQL .= " where (clean_number is not null)";
	$getCDRsForRatingSQL .= " AND (price is null)";
	$getCDRsForRatingSQL .= " AND (call_dest_regid= '";
	$getCDRsForRatingSQL .= $call_dest_regid;
	$getCDRsForRatingSQL .= "') AND (start_time >= ";
	$getCDRsForRatingSQL .= $start_date;
	$getCDRsForRatingSQL .= ") AND (start_time <= ";
	$getCDRsForRatingSQL .= $end_date;
	$getCDRsForRatingSQL .= ")";

	
	$CDRsForRating = $DB->SelectLimit($getCDRsForRatingSQL) or die($DB->ErrorMsg());
	return $CDRsForRating;
}

function getMasterIdForDialCode($dialing_code){
		//Connection statement
include('Connections/DB.php');

	$getAllCodesSQL = sprintf("SELECT * FROM master WHERE calling_code=%s ORDER BY region_name",
	GetSQLValueString($dialing_code,"text"));
	$masterCode = $DB->Execute($getAllCodesSQL) or die($DB->ErrorMsg());
	return $masterCode;
}


?>