<?php
$dir="output/";

    $mydir = opendir($dir);

    while(false !== ($file = readdir($mydir))) {
		$extention=strrchr($file,".");
        if($file != "." && $file != ".." && $extention==".csv") {
            chmod($dir.$file, 0777);

            if(is_dir($dir.$file)) {

                chdir('.');

                destroy($dir.$file.'/');
				
                rmdir($dir.$file) or DIE("couldn't delete $dir$file<br/>");

            }

            else

                unlink($dir.$file) or DIE("couldn't delete $dir$file<br/>");

        }

    }

    closedir($mydir);

?>