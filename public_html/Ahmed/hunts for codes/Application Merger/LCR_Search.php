<?php
//check if the session is still alive
include_once('DBAccessor.php');
applyIPFilter();

	session_start();
	
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	

	//read carriers and destinations
	$rows_carriers = getAllLiveCarriers();
	//$rows_destinations = getDestinations();

	if($rows_carriers->EOF){
		echo "<p id=\"errorText\">Oh-oh billing db in walker is acting funny. Developer defends code. Please contact ext. 101</p>";
	}
//session check ends here
 ?>

<html>
<head>
<title> LCR Search </title>
</head>

<body>
<?php include('Menu_Header.php');?>
<br/><br/>

<form method="get" action="LCR_Result.php">
<b>Search For:</b> <input type="text" name="item" value="">
<input type="checkbox" name="EXACT">Exact match
<input type="checkbox" name="FILS">Rate in Fils
<br/>
<input type="radio" name="type" value="Code" checked="yes">Calling Code
<input type="radio" name="type" value="Region">Region Name

<br/><br/>
<b>Supplier filter:</b>
<br/>



<select name="carrier_list[]" id="carrier_list" multiple="multiple" MULTIPLE SIZE=7>	
 				<?php
						$rows_carriers->MoveFirst();
						while(!$rows_carriers->EOF){
							$carrier_id = $rows_carriers->Fields('carrier_id');
							$carrier_name = $rows_carriers->Fields('carrier_name');
							echo "<option id=$carrier_name value=$carrier_name>";
							echo $rows_carriers->Fields('carrier_name');
							echo "</option>";
							$rows_carriers->MoveNext();
						}
					?>
		 			</select>
		 			
		 			
<br/><br/>
<input type="submit" value="Find">
</form>
<p>Leave the input field empty to view all destinations and similarly with the supplier filter</p>
<br/>
<?php include('Page_Footer.php');?>
</html>
