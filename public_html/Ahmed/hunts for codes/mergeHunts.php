<?php

//CSVParser
include_once('CSVParser.php');

$parser = new CsvFileParser();

echo "***Reading Supplier Rates***\n";
$supprates = $parser->ParseFromFile('csvs/lcr-supp-rates.csv');

echo "***Reading Code List**\n";
$file = "leastcosthunts.csv";
$codelist = $parser->ParseFromFile("csvs/$file");

echo "***Working on $file***\n";

$ofile = fopen("output/$file",'w');
$output = array();

foreach($codelist as $row){
	if($row[0] == '' || strtoupper(trim($row[0]))== 'CODE'){
		$output[] = "$row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7]\r\n";
	}
	else{
		echo "searching for $row[0]\n";
		$fhc="-";
		$fhr=1000;
		$shc="-";
		$shr=1000;
		$thc="-";
		$thr=1000;

		//find longest match
		foreach($supprates as $line){

			
			//compare dialcode
			if(preg_match('/^' . $line[0]   . '\d*$/',$row[0])){

				$supp = $line[1];
				$rate = floatval($line[2]);
				
					

				//check for 1 hunt
				if($rate <= $fhr && $fhc != $supp){

					//shift 2 & 3 , assign 1
					$thc=$shc; $thr=$shr;
					$shc=$fhc; $shr=$fhr;
					$fhc=$supp; $fhr= $rate;
					//echo "assigning $supp as first hunt with rate $rate\n";

				}

				//check for 2 hunt
				elseif($rate <= $shr && $fhc != $supp && $shc != $supp){

					//shift 3 , assign 2
					$thc=$shc; $thr=$shr;
					$shc=$supp; $shr= $rate;
					//echo "assigning $supp as second hunt with rate $rate\n";
				}
				
				//check for 3 hunt
				elseif($rate <= $thr && $fhc != $supp && $shc != $supp && $thc != $supp){
					//assign 3
					$thc=$supp; $thr= $rate;
					//echo "assigning $supp as third hunt with rate $rate\n";
				}


				//echo "Match {$row[0]} with {$line[0]}>>>{$fhc}>{$fhr}>{$shc}>{$shr}>{$thc}>{$thr}\n\n";
			}
		}
		
		$fhr=($fhr==1000)?0:$fhr;
		$shr=($shr==1000)?0:$shr;
		$thr=($thr==1000)?0:$thr;

		$str = "{$row[0]},{$row[1]},$fhc,$fhr,$shc,$shr,$thc,$thr\r\n";
		$output[] = $str;
	}
}

 foreach ($output as $o){
 	fwrite($ofile,$o);
}

fclose($ofile);
echo "***$file Done***\n";














?>
