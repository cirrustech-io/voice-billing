<?php
function startDB(){

# PHP ADODB document - made with PHAkt
	# FileName="Connection_php_adodb.htm"
	# Type="ADODB"
	# HTTP="true"
	# DBTYPE="postgres7"
	
	$MM_DB_HOSTNAME = 'walker.2connectbahrain.com';
	$MM_DB_DATABASE = 'postgres7:billing';
	$MM_DB_DBTYPE   = preg_replace('/:.*$/', '', $MM_DB_DATABASE);
	$MM_DB_DATABASE = preg_replace('/^[^:]*?:/', '', $MM_DB_DATABASE);
	$MM_DB_USERNAME = 'ahmedyounis';
	$MM_DB_PASSWORD = 'kl56kl';
	$MM_DB_LOCALE = 'En';
	$MM_DB_MSGLOCALE = 'En';
	$MM_DB_CTYPE = 'P';
	$KT_locale = $MM_DB_MSGLOCALE;
	$KT_dlocale = $MM_DB_LOCALE;
	$KT_serverFormat = '%Y-%m-%d %H:%M:%S';
	$QUB_Caching = 'false';

	$KT_localFormat = $KT_serverFormat;
	
	if (!defined('CONN_DIR')) define('CONN_DIR',dirname(__FILE__));
	//require_once(CONN_DIR.'/../adodb/adodb.inc.php');
	require_once(CONN_DIR.'/adodb/adodb.inc.php');
	$DB=&KTNewConnection($MM_DB_DBTYPE);

	if($MM_DB_DBTYPE == 'access' || $MM_DB_DBTYPE == 'odbc'){
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_DATABASE, $MM_DB_USERNAME,$MM_DB_PASSWORD);
		} else $DB->Connect($MM_DB_DATABASE, $MM_DB_USERNAME,$MM_DB_PASSWORD);
	} else if (($MM_DB_DBTYPE == 'ibase') or ($MM_DB_DBTYPE == 'firebird')) {
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_HOSTNAME.':'.$MM_DB_DATABASE,$MM_DB_USERNAME,$MM_DB_PASSWORD);
		} else $DB->Connect($MM_DB_HOSTNAME.':'.$MM_DB_DATABASE,$MM_DB_USERNAME,$MM_DB_PASSWORD);
	}else {
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_HOSTNAME,$MM_DB_USERNAME,$MM_DB_PASSWORD, $MM_DB_DATABASE);
		} else $DB->Connect($MM_DB_HOSTNAME,$MM_DB_USERNAME,$MM_DB_PASSWORD, $MM_DB_DATABASE);
   }

	if (!function_exists('updateMagicQuotes')) {
		function updateMagicQuotes($HTTP_VARS){
			if (is_array($HTTP_VARS)) {
				foreach ($HTTP_VARS as $name=>$value) {
					if (!is_array($value)) {
						$HTTP_VARS[$name] = addslashes($value);
					} else {
						foreach ($value as $name1=>$value1) {
							if (!is_array($value1)) {
								$HTTP_VARS[$name1][$value1] = addslashes($value1);
							}
						}
					}
				}
			}
			return $HTTP_VARS;
		}
		
		if (!get_magic_quotes_gpc()) {
			$_GET = updateMagicQuotes($_GET);
			$_POST = updateMagicQuotes($_POST);
			$_COOKIE = updateMagicQuotes($_COOKIE);
		}
	
	}
	if (!isset($_SERVER['REQUEST_URI']) && isset($_ENV['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_ENV['REQUEST_URI'];
	}
	if (!isset($_SERVER['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_SERVER['PHP_SELF'].(isset($_SERVER['QUERY_STRING'])?"?".$_SERVER['QUERY_STRING']:"");
	}
	
	return($DB);
}

function performSupplierRatesTableRefresh($today){
	
	//Connection statement
	$DB=startDB();
	
	/*
	$ip_address = $_SERVER['REMOTE_ADDR'];
	
	echo $ip_address;
	
	if($ip_address!="")
	exit(0);*/
	
	$tableExists = pg_query("SELECT tablename FROM pg_tables WHERE schemaname='public' AND tablename='supplier_rates'") or die('Query failed: ' . pg_last_error());
	if(pg_num_rows($tableExists)==1)
	{$delete_query="DROP TABLE supplier_rates";
	pg_query($delete_query) or die($logdata.='Delete Query failed: ' . pg_last_error() ."\n");}

$query = "
select m.calling_code,c.carrier_name as supplier,r.rate into supplier_rates
from rate as r, carrier as c,master as m
where rate_id in
		(select rate_id from
                	(select max(rate_id) as rate_id,master_id,carrier_id,effective_date
                         from rate
                         where (master_id,carrier_id,effective_date) in
                         	(select master_id,carrier_id,max(effective_date)
                                 from rate where effective_date<='5-Nov-2008'
				 and customer_carrier_id=6
				-- and carrier_id in (1,2,3,11,25)
				 group by carrier_id,master_id 
				 order by master_id,carrier_id)
                         group by master_id,carrier_id,customer_carrier_id,effective_date) 
		as x)
and m.master_id=r.master_id
and c.carrier_id=r.carrier_id
order by calling_code desc,supplier asc
";

	$result = pg_query($query) or die($logdata.='Create supplier_rates query failed: ' . pg_last_error() ."\n");

	// Free resultset
	pg_free_result($result);

	return($logdata);
}

function getAllLiveCarriers(){
	
	//Connection statement
	$DB=startDB();
	
	
	$getAllCarriersSQL = "SELECT * FROM carrier where Type in ('C') AND carrier_name NOT LIKE '2CONNECT' order by carrier_name";
	$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

	return $carrier;
}

function fetchSupplierRatesTable($query)
{	
	//Connection statement
	$DB=startDB();
	
	//echo $query ;
	//exit(0);
	$result = pg_query($query) or die('Query failed: ' . pg_last_error());
	return ($result);	
}


?>
