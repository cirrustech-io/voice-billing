<?php

include('DBAccessor.php');

$today = date('d-M-y');
$log = "logs//$today.log";

$logdata="fetching latest data, dated $today\n";

//refresh the lcr table rates
$logdata.=performSupplierRatesTableRefresh($today);


$logdata.="Done\n\n\n";

//write the log
$fh = fopen($log, 'a') or die("could not write log file!?\n");
fwrite($fh, $logdata);
fclose($fh);

//echo "Done";

?>
