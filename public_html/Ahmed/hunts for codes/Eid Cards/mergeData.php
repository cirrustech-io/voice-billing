<?php

//CSVParser
include_once('CSVParser.php');
/*
$files = array('afghanistan.csv','austrailia.csv','bangladesh.csv','ethiopia.csv','egypt.csv','india.csv','indonesia.csv',
		'iran.csv','ireland.csv','japan.csv','kuwait.csv','lebanon.csv','malaysia.csv','nepal.csv','oman.csv','pakistan.csv',
		'philippines.csv','qatar.csv','saudi arabia.csv','singapore.csv','sri-lanka.csv','sudan.csv','syria.csv',
		'thailand.csv','turkey.csv','uae.csv','uk.csv','usa.csv','yemen.csv');

*/
$files = array('jordanchina.csv');
$parser = new CsvFileParser();

echo "***Reading Supplier Rates***\n";
$supprates = $parser->ParseFromFile('csvs/supp.csv');

foreach($files as $file){
	echo "***Working on $file***\n";
	$country = $parser->ParseFromFile("csvs/$file");
	$ofile = fopen("output/$file",'w');
	$output = array();
	foreach($country as $row){
		if($row[0] == '' || strtoupper(trim($row[0]))== 'COUNTRY'){
			$output[] = "$row[0],$row[1],$row[2],$row[3],$row[4],$row[5]\r\n";
		}
		else{
			//echo "searching for $row[0]\n";
			$vsnl = 0;
			$citic = 0;
			$nwt = 0;
			$bharti =0;
			$kpn =0;
			foreach($supprates as $line){
				//echo "comparing with $line[0]..\n";
				if(preg_match('/^' . $line[0]   . '\d*$/',$row[0])){
					//echo "found $line[0] for $line[1] \n";
					switch($line[1]){
						case 'VSNL':
							$vsnl = $line[2];
							break;
						case 'CITIC':
							$citic = $line[2];
							break;
						case 'NWT':
							$nwt = $line[2];
							break;
						case 'BHARTI':
							$bharti = $line[2];
							break;
						case 'KPN':
							$kpn = $line[2];
							break;
					}
				}
			}
			$str = "$row[0],";
			$str .= ($vsnl==0)?",":"$vsnl,";
			$str .= ($citic==0)?",":"$citic,";
			$str .= ($nwt==0)?",":"$nwt,";
			$str .= ($bharti==0)?",":"$bharti,";
			$str .= ($kpn==0)?",":"$kpn,";
			$str .= "\r\n";
			$output[] = $str;
		}
	}
	foreach ($output as $o){
		fwrite($ofile,$o);
	}
	fclose($ofile);
	echo "***$file Done***\n";
}













?>
