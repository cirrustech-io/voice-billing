<?php


include('includes/functions.inc.php');



function startDB(){

# PHP ADODB document - made with PHAkt
	# FileName="Connection_php_adodb.htm"
	# Type="ADODB"
	# HTTP="true"
	# DBTYPE="postgres7"

	$MM_DB_HOSTNAME = '192.168.2.190';
	$MM_DB_DATABASE = 'postgres8:billing';
	$MM_DB_DBTYPE   = preg_replace('/:.*$/', '', $MM_DB_DATABASE);
	$MM_DB_DATABASE = preg_replace('/^[^:]*?:/', '', $MM_DB_DATABASE);
	$MM_DB_USERNAME = 'alicia';
	$MM_DB_PASSWORD = 'fatty';
	$MM_DB_LOCALE = 'En';
	$MM_DB_MSGLOCALE = 'En';
	$MM_DB_CTYPE = 'P';
	$KT_locale = $MM_DB_MSGLOCALE;
	$KT_dlocale = $MM_DB_LOCALE;
	$KT_serverFormat = '%Y-%m-%d %H:%M:%S';
	$QUB_Caching = 'false';

	$KT_localFormat = $KT_serverFormat;

	if (!defined('CONN_DIR')) define('CONN_DIR',dirname(__FILE__));
	//require_once(CONN_DIR.'/../adodb/adodb.inc.php');
	require_once(CONN_DIR.'/adodb/adodb.inc.php');
	$DB=&KTNewConnection($MM_DB_DBTYPE);

	if($MM_DB_DBTYPE == 'access' || $MM_DB_DBTYPE == 'odbc'){
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_DATABASE, $MM_DB_USERNAME,$MM_DB_PASSWORD);
		} else $DB->Connect($MM_DB_DATABASE, $MM_DB_USERNAME,$MM_DB_PASSWORD);
	} else if (($MM_DB_DBTYPE == 'ibase') or ($MM_DB_DBTYPE == 'firebird')) {
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_HOSTNAME.':'.$MM_DB_DATABASE,$MM_DB_USERNAME,$MM_DB_PASSWORD);
		} else $DB->Connect($MM_DB_HOSTNAME.':'.$MM_DB_DATABASE,$MM_DB_USERNAME,$MM_DB_PASSWORD);
	}else {
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_HOSTNAME,$MM_DB_USERNAME,$MM_DB_PASSWORD, $MM_DB_DATABASE);
		} else $DB->Connect($MM_DB_HOSTNAME,$MM_DB_USERNAME,$MM_DB_PASSWORD, $MM_DB_DATABASE);
   }

	if (!function_exists('updateMagicQuotes')) {
		function updateMagicQuotes($HTTP_VARS){
			if (is_array($HTTP_VARS)) {
				foreach ($HTTP_VARS as $name=>$value) {
					if (!is_array($value)) {
						$HTTP_VARS[$name] = addslashes($value);
					} else {
						foreach ($value as $name1=>$value1) {
							if (!is_array($value1)) {
								$HTTP_VARS[$name1][$value1] = addslashes($value1);
							}
						}
					}
				}
			}
			return $HTTP_VARS;
		}

		if (!get_magic_quotes_gpc()) {
			$_GET = updateMagicQuotes($_GET);
			$_POST = updateMagicQuotes($_POST);
			$_COOKIE = updateMagicQuotes($_COOKIE);
		}

	}
	if (!isset($_SERVER['REQUEST_URI']) && isset($_ENV['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_ENV['REQUEST_URI'];
	}
	if (!isset($_SERVER['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_SERVER['PHP_SELF'].(isset($_SERVER['QUERY_STRING'])?"?".$_SERVER['QUERY_STRING']:"");
	}

	return($DB);
}

function performLCRTableRefresh($today){

	//Connection statement
	$DB=startDB();

	$tableExists = pg_query("SELECT tablename FROM pg_tables WHERE schemaname='public' AND tablename='lcr_rates'") or die('Query failed: ' . pg_last_error());
	if(pg_num_rows($tableExists)==1)
	{$delete_query="DROP TABLE lcr_rates";
	pg_query($delete_query) or die($logdata.='Delete Query failed: ' . pg_last_error() ."\n");}

	$query = "
	select m.calling_code,m.region_name, c.carrier_name as supplier ,r.rate as lowest_rate, max(r.effective_date) as eff_date
	into lcr_rates
	from rate r, master m, carrier c
	where (r.master_id,r.rate) in
				(
				select master_id,min(rate)
				from rate
				where rate_id in (
						select rate_id from (
									select max(rate_id) as rate_id,master_id,carrier_id
									from rate
									where (master_id,carrier_id,effective_date) in
															(select master_id,carrier_id,max(effective_date)
															from rate where effective_date<='$today'
															and customer_carrier_id=6
															and carrier_id in (select carrier_id from carrier where type='C')
															group by master_id,carrier_id
															)
									group by master_id,carrier_id) as X
							)
				group by master_id
				)
	and m.master_id = r.master_id
	and c.carrier_id = r.carrier_id
	and NOT c.carrier_id=6
	and c.type = 'C'
	group by m.calling_code,m.region_name,c.carrier_name,r.rate
	order by calling_code
	";

	$result = pg_query($query) or die($logdata.='Create LCR_Rates query failed: ' . pg_last_error() ."\n");

	// Free resultset
	pg_free_result($result);

	updateDates("lcr_rates");

	return($logdata);
}

function performSupplierRatesTableRefresh($today){
	//Connection statement
	$DB=startDB();

	$tableExists = pg_query("SELECT tablename FROM pg_tables WHERE schemaname='public' AND tablename='supplier_rates_table'") or die('Query failed: ' . pg_last_error());
	if(pg_num_rows($tableExists)==1)
	{$delete_query="DROP TABLE supplier_rates_table";
	pg_query($delete_query) or die($logdata.='Delete Query failed: ' . pg_last_error() ."\n");}

$query = "
select m.calling_code,c.carrier_name as supplier,r.rate into supplier_rates_table
from rate as r, carrier as c,master as m
where rate_id in
		(select rate_id from
                	(select max(rate_id) as rate_id,master_id,carrier_id,effective_date
                         from rate
                         where (master_id,carrier_id,effective_date) in
                         	(select master_id,carrier_id,max(effective_date)
                                 from rate where effective_date<='$today'
				 and customer_carrier_id=6
				 and carrier_id in (select carrier_id from carrier where type = 'C')
				 group by carrier_id,master_id
				 order by master_id,carrier_id)
                         group by master_id,carrier_id,customer_carrier_id,effective_date)
		as x)
and m.master_id=r.master_id
and c.carrier_id=r.carrier_id
and c.type = 'C'
order by calling_code desc,supplier asc
";

	$result = pg_query($query) or die($logdata.='Create supplier_rates_table query failed: ' . pg_last_error() ."\n");

	// Free resultset
	pg_free_result($result);

	updateDates("supplier_rates_table");

	return($logdata);
}

function CheckIfRealCarriers($selected)
{
	if ($selected){
	 	foreach ($selected as $s)
	 	{
			//boolean to flag if the selected carrier is real
	 		$real_carrier=0;
	 		//get all carriers
	 		$rows_carriers=getAllLiveCarriers();

	 		$rows_carriers->MoveFirst();
			while(!$rows_carriers->EOF){

			if($rows_carriers->Fields('carrier_name')==$s)
				{$real_carrier=1;
				break;}

			$rows_carriers->MoveNext();
			}
			if($real_carrier==0)
				break;
	 	}
	}
	else
		{if($real_carrier==0)
		{echo "<br>Security Error in carriers!";
		exit(0); }}

}

function maxPerCode($code,$selected,$fils,$exceptions)
{	applyIPFilter();
	//Connection statement
$DB=startDB();
$code=mssql_real_escape_ray($code);
$exceptions=mssql_real_escape_ray($exceptions);

$fils=($fils=="on")?$fils="* 377.36 as rate":"";

$code_filter = "";


$query="select calling_code, supplier, rate $fils from supplier_rates_table where rate in(
select max(rate)
from supplier_rates_table
where ";


$code_filter = " calling_code like '$code%'";

	if($exceptions != ""){
		$exs = explode(",",$exceptions);
		foreach ($exs as $ex){
			 $code_filter .= " and calling_code not like '$ex%' ";
		}
	}

$query .= "$code_filter and supplier in($selected)) and ($code_filter)";

	#echo $query;
	#exit(0);

	$exists=tableExists("supplier_rates_table");

	if($exists==1)
	$result = pg_query($query) or die('Query failed: ' . pg_last_error());
	else
	{echo "<p =\"errorText\">Critical error: supplier rates table is down! please contact the developer immediately.</p>";
	exit(0);}

return ($result);
}

function getAllLiveCarriers(){
	applyIPFilter();
	//Connection statement
	$DB=startDB();


	$getAllCarriersSQL = "SELECT * FROM carrier where Type in ('C') AND carrier_name NOT LIKE '2CONNECT' order by carrier_name";
	$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

	return $carrier;
}

function getAllLiveCounties()
{
	applyIPFilter();
	//Connection statement
	$DB=startDB();


	$getAllCountriesSQL = "SELECT DISTINCT country FROM top_20_master ORDER by country";
	$country = $DB->SelectLimit($getAllCountriesSQL) or die($DB->ErrorMsg());

	return $country;
}


function fetchSupplierRatesTable($carriers,$fils)
{	applyIPFilter();
	//Connection statement
	$DB=startDB();
	$carriers=mssql_real_escape_string($carriers);
	//print_r($carriers);
	if($carriers<>"")
		{
		$real_carrier=CheckIfRealCarriers($carriers);
		$selected="'".implode("','",$carriers)."'";
		$condition="WHERE supplier IN ($selected)";
		}
	else
		$condition="";


	$fils=($fils=="on")?$fils="* 377.36 as rate":"";

	$query="SELECT calling_code, supplier, rate $fils
			FROM supplier_rates_table
			$condition
			ORDER BY calling_code desc";

	//echo $query;

	$result = pg_query($query) or die('Query failed: ' . pg_last_error());


	$exists=tableExists("supplier_rates_table");

	if($exists==1)
	$result = pg_query($query) or die('Query failed: ' . pg_last_error());
	else
	{echo "<p id=\"errorText\">Critical error: supplier rates table is down! please contact the developer immediately.</p>";
	exit(0);}


	return ($result);
}


function getRatesCoolness($cat,$who,$when,$where){

	applyIPFilter();

	//Connection statement
	$DB=startDB();


	$sql = "Select carrier_id from carrier where carrier_name='2CONNECT'";
	$twoconn = $DB->SelectLimit($sql) or die($DB->ErrorMsg());

	switch($cat){
		case "CS":
			$supp = $who;
			$cust = $twoconn->Fields('carrier_id');
			break;
		case "CC":
			$supp = $twoconn->Fields('carrier_id');
			$cust = $who;
			break;
	}

	$when = GetSQLValueString($when,"date");
	$cust =	GetSQLValueString($cust,"int");
	$supp = GetSQLValueString($supp,"int");


	$sql = "select r.rate_id,r.master_id,m.calling_code,m.region_name,r.carrier_id,c1.carrier_name as supplier,c2.carrier_name as customer,r.rate,r.effective_date
			from rate as r, carrier as c1, carrier as c2, master as m
			where rate_id in
				(select rate_id from
				(select max(rate_id) as rate_id,master_id,carrier_id,customer_carrier_id,effective_date
				from rate
				where (master_id,carrier_id,customer_carrier_id,effective_date) in
					(select master_id,carrier_id,customer_carrier_id,max(effective_date)
					from rate where effective_date<=$when";

	switch($who){
		case 'ALL':
				if($cat == "CS"){
					$sql .= " and customer_carrier_id=$cust";
				}
				else {
					$sql .= " and carrier_id=$supp";
				}
				break;
		default:
				$sql .= " and carrier_id=$supp and customer_carrier_id=$cust";
	}

	switch($where){
		case 'ALL':
			break;
		default:
			$sql .= " and master_id in (select master_id from master where calling_code like '$where%')";
	}



	$sql .= " group by carrier_id,customer_carrier_id,master_id order by master_id,carrier_id,customer_carrier_id)
				group by master_id,carrier_id,customer_carrier_id,effective_date) as x)
					and m.master_id=r.master_id
					and c1.carrier_id=r.carrier_id
					and c2.carrier_id=r.customer_carrier_id
					order by calling_code asc,supplier asc";

	$rows =  pg_query($sql) or die('Query failed: ' . pg_last_error());
	return $rows;

}

function getDestinations(){
	applyIPFilter();

	//Connection statement
	$DB=startDB();


	$sql = "SELECT * FROM country_real order by country_name";
	$result = $DB->SelectLimit($sql) or die($DB->ErrorMsg());

	return $result;
}

function fetchLCRTable($type,$exact,$carriers,$item,$fils){
	applyIPFilter();
	//Connection statement
	$DB=startDB();
	$fils=($fils=="on")?$fils="* 377.36 as lowest_rate":"";


	if($type == 'Code'){
	$item=mssql_real_escape_num($item);
		if ($exact=='on')
			{$query = "SELECT calling_code,region_name,supplier,lowest_rate $fils,eff_date FROM lcr_rates WHERE calling_code LIKE '$item'";}
		else
			{$query = "SELECT calling_code,region_name,supplier,lowest_rate $fils,eff_date FROM lcr_rates WHERE calling_code LIKE '$item%'";}
	}
	else if($type=='Region'){
	$item=mssql_real_escape_string($item);
		$item=mb_strtoupper($item);

		if ($exact=='on')
			{$query = "SELECT calling_code,region_name,supplier,lowest_rate $fils,eff_date FROM lcr_rates WHERE region_name LIKE '$item'";}
		else
			{$query = "SELECT calling_code,region_name,supplier,lowest_rate $fils,eff_date FROM lcr_rates WHERE region_name LIKE '%$item%'";}
	}
	else{
		echo "<p id=\"errorText\">Security Error in type!<p>";
		exit(0);
	}

	if ($carriers){
		CheckIfRealCarriers($carriers);
		$selected="'".implode("','",$carriers)."'";
		$condition="AND supplier IN ($selected)";
	}

	$query.=" $condition";

	//echo $query;


	//$tableExists = pg_query("SELECT tablename FROM pg_tables WHERE schemaname='public' AND tablename='lcr_rates'") or die('Query failed: ' . pg_last_error());

	$exists=tableExists("lcr_rates");

	if($exists==1)
	$result = pg_query($query) or die('Query failed: ' . pg_last_error());
	else
	{echo "<p id=\"errorText\">Critical error: LCR table is down! please contact the developer immediately.</p>";
	exit(0);}

	return ($result);
}

function fetchLCRUpdates()
{
	applyIPFilter();
	//Connection statement
	$DB=startDB();

	$query = "SELECT * FROM lcr_update";

	$result  = pg_query($query) or die('Query failed: ' . pg_last_error());
	return ($result);

}

function updateDates($tablename){

	//Connection statement
	$DB=startDB();
	$date=date("Y-m-d H:i:s");
	

	$query = "UPDATE lcr_update
						SET lastupdate='$date'
						WHERE tablename='$tablename'";

	$exists=tableExists("lcr_update");

	if($exists==1)
	$result = pg_query($query) or die('Query failed: ' . pg_last_error());
	else
	{echo "<p id=\"errorText\">Critical error: lcr_update table doesn't exist, please contact the developer immediately.</p>";
	exit(0);}

	return ($result);
}


function countCarriers()
{	applyIPFilter();

	$x=0;

	$carriersGet=getAllLiveCarriers();
	$carriersGet->MoveFirst();
	while(!$carriersGet->EOF)
	{$x++;
	$carriersGet->MoveNext();}

	return($x);
}

function tableExists($tablename)
{
	//Connection statement
	$DB=startDB();

	$tableExists = pg_query("SELECT tablename FROM pg_tables WHERE schemaname='public' AND tablename='$tablename'") or die('Query failed: ' . pg_last_error());

	if(pg_num_rows($tableExists)==1)
		$result = 1;
	else
		$result = 0;

	return ($result);
}

function checkPassword($username,$password){
	applyIPFilter();
	$username=mssql_real_escape_password($username);
	$password=mssql_real_escape_password($password);

	if($username&&$password)
		{//Connection statement
		$DB=startDB();
		$query = "SELECT * FROM liverate_login where username='$username' and password='$password'";
		$login_result  = $DB->SelectLimit($query) or die($DB->ErrorMsg());
		return ($login_result);}
	else
		return 0;
}

function getTop20($countries)
{
	applyIPFilter();
	//Connection statement
	$DB=startDB();
	$countries=mssql_real_escape_string($countries);
	if(!$countries=="")
	{$selected="'".implode("','",$countries)."'";
	$selected=str_replace("_"," ",$selected);
	$condition="WHERE country IN ($selected)";}

	$query = "SELECT * FROM top_20_master
			  $condition
			  ORDER BY code";


	$result = pg_query($query) or die('Query failed: ' . pg_last_error());
	return ($result);

}

function insertTop20($code, $description, $country) {

	//Connection statement
	$DB=startDB();


    $serialSQL = "SELECT nextval('public.top_20_master_id_seq'::text)";
    $serialResult = $DB->SelectLimit($serialSQL) or die($DB->ErrorMsg());

    $insertMasterSQL = "INSERT INTO top_20_master (id, code, description, country) VALUES (" . $serialResult->Fields('nextval') .
    ",'$code','$description','$country');";

    echo $insertMasterSQL . "<br>";
 	$codeResult = $DB->Execute($insertMasterSQL) or die($DB->ErrorMsg());
}


function cleanOutput(){
$dir="/var/www/html/lcr/www/output/";

    $mydir = opendir($dir);

    while(false !== ($file = readdir($mydir))) {
		$extention=strrchr($file,".");
        if($file != "." && $file != ".." && $extention==".csv") {
            //chmod($dir.$file, 0777);

            if(is_dir($dir.$file)) {

                chdir('.');

                destroy($dir.$file.'/');
                $cleanResult.=$dir.$file."________________________________________________\n";
                $cleanResult.=$dir.$file." File Cleaning Started\n";
                $cleanResult.=$dir.$file." was cleaned\n";

                rmdir($dir.$file) or DIE($cleanResult.="couldn't delete $dir$file\n");

            }

            else

                unlink($dir.$file) or DIE($cleanResult.="couldn't delete $dir$file\n");

        }

    }

    closedir($mydir);

        $cleanResult.=$dir.$file." File Cleaning Done\n";
		$cleanResult.=$dir.$file."________________________________________________\n";

    return($cleanResult);
}


function applyIPFilter(){
//first check if the IP is local, otherwise kick the intruder out
$allowed_IP1="80.88.240";
$allowed_IP2="80.88.242";
$allowed_IP3="80.88.248.30";
$allowed_IP4="80.88.248.141";
$allowed_IP5="80.88.248.142";

$ip_address = $_SERVER['REMOTE_ADDR'];

if(		(substr($ip_address,0,9)<>$allowed_IP1)
	&&	(substr($ip_address,0,9)<>$allowed_IP2)
	&&	($ip_address<>$allowed_IP3)
	&&	($ip_address<>$allowed_IP4)
	&&	($ip_address<>$allowed_IP5)
	){
	echo "<p id=\"errorText\">Your computer IP is not allowed to access this service</p>";
	exit(0);}
//IP checking ends here, you may add/remove IPs if any changes happen in the companies network
}

function mssql_real_escape_num($num_to_escape) {
//echo "b: ".$num_to_escape;
$replaced_num=preg_replace('/[^0-9]/', "_", $num_to_escape);
//echo "a: ".$replaced_num;

return $replaced_num;
} 

function mssql_real_escape_string($string_to_escape) {
//echo "b: ".$string_to_escape;
$replaced_string =preg_replace('/[^a-zA-Z0-9\.]/', "_", $string_to_escape);
//echo "a: ".$replaced_string;

return $replaced_string;
} 

function mssql_real_escape_ray($string_to_escape) {
//echo "b: ".$string_to_escape;
$replaced_string =preg_replace('/[^a-zA-Z0-9\,]/', ",", $string_to_escape);
//echo "a: ".$replaced_string;

return $replaced_string;
}

function mssql_real_escape_password($string_to_escape) {

if(preg_match('/[^a-zA-Z0-9\.]/', $string_to_escape))
	return null;
else
	return $string_to_escape;
}

function record_sort($records, $field, $reverse=false)
{
    $hash = array();
   
    foreach($records as $record)
    {
        $hash[$record[$field]] = $record;
    }
   
    ($reverse)? krsort($hash) : ksort($hash);
   
    $records = array();
   
    foreach($hash as $record)
    {
        $records []= $record;
    }
   
    return $records;
}
?>
