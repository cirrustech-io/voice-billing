var tomorrow = new Date();
tomorrow.setTime(tomorrow.getTime() + (1000*3600*24));
var cal = new CalendarPopup("CalendarDiv");
cal.addDisabledDates(formatDate(tomorrow,"yyyy-MM-dd"),null);
cal.addDisabledDates(null,"Jan 1, 2005");
cal.setCssPrefix("Calendar");

var cal2 = new CalendarPopup("CalendarDiv");
cal2.addDisabledDates(formatDate(tomorrow,"yyyy-MM-dd"),null);
cal2.addDisabledDates(null,"Jan 1, 2005");
cal2.setCssPrefix("Calendar");

function ValidateForm(){
	var dt=document.submitForm.FromDate.value;
	var dt2=document.submitForm.ToDate.value;

	if (isDate(dt,"MM/dd/yyyy")==false || isDate(dt2,"MM/dd/yyyy")==false)
		{alert("Date fomat has to be mm/dd/yyyy");
		return false}

	if(compareDates(dt,"MM/dd/yyyy",dt2,"MM/dd/yyyy")==1)
		{alert("From Date can't be after To Date");
		return false}
	
	today=new Date();
	today=dateFormat(today,"mm/dd/yyyy");

	if((compareDates(dt,"MM/dd/yyyy","12/31/2004","MM/dd/yyyy")==0)||(compareDates(dt2,"MM/dd/yyyy",today,"MM/dd/yyyy")==1))
		{alert("Dates have to be between 01/01/2005 and "+today);
		return false}
		
		
    return true
}