<?php

	include('DBAccessor.php');
	
	session_start();
	$_SESSION['user_name'] = "";
	
	// build the form action
	$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");
	$error_message = "";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>2Call Login</title>

</head>

<body>
<noscript>
    <style type="text/css">
        .JS { display:none; }
    </style>
	<link rel="stylesheet" href="style.css" type="text/css" />
	<div style="position: fixed; background-color:white; top: 0pt; left: 0pt;opacity:1;">
	<img rowspan=2 src="imgs/slogan.gif"/>
	<img src="imgs/bar.gif" style="width:100%;"/>
	<br/></div><br/><br/><br/><br/><br/>
	<B><U>Your Javascript is disabled, please enable it or simply use another browser(we recommend FireFox)!</U></B>
</noscript>
<div class="JS">
<link rel="stylesheet" href="style.css" type="text/css" />
<div style="position: fixed; background-color:white; top: 0pt; left: 0pt;opacity:1;">
<img rowspan=2 src="imgs/slogan.gif"/>
<img src="imgs/bar.gif" style="width:100%;"/>


<br/>
</div>
	<br/><br/><br/><br/>
	<SCRIPT LANGUAGE="JavaScript">if(navigator.appName=="Microsoft Internet Explorer")document.write("<br/>");</SCRIPT>
	<form action="<?php echo $editFormAction ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">	
	
	<?php
	if ((isset($_POST["doodoo"])) && ($_POST["doodoo"] == "weewee")) {
		$username = $_POST['txt_username'];
		$password = $_POST['txt_password'];
		$CT = $_POST['User_Type'];

		//checks user and pass for customer service
	
		$error="";
		if($username=="")
			$error .= "Please enter your username <br/>";
		if($password=="")	
			$error .= "Please enter your password <br/>";
		if($error!="")
			$error_message= "<p id=\"errorText\">$error</p>";
		else
		{if($CT=="CS")
			{applyIPFilter();
			$myFile = "../CS.txt";
			$fh = fopen($myFile, 'r');
			$theData = fread($fh, filesize($myFile));
			fclose($fh);
			$ver=$username."*".$password;
		if($ver==$theData)
			{$_SESSION['user_name'] = $_POST['txt_username'];
			$_SESSION['CT']="CS";
			session_write_close();
			$result=cleanOutput();
			header("Location: Customer_Service.php");
			exit;
			}
		else
			$error_message = "<p id=\"errorText\">Login failed. Please check your credentials.</p>";
		}
		else{
		//checks if the username and password are correct
			$ID=checkPassword($username,$password);
			 if($ID==NULL)
				{
					session_write_close();
					$error_message = "<p id=\"errorText\">Login failed. Please check your credentials.</p>";
				}
			else
				{	$_SESSION['user_name'] = $_POST['txt_username'];
					$_SESSION['ID']=$ID;
					$_SESSION['CT']="C";
					session_write_close();
					header("Location: Main_Menu.php");
				}
			}
		}
	}
	
	?>
	<table border=0>
		<tr>
	 	<table bgcolor="Silver" cellpadding="5" cellspacing="0">
	 		<tr>
		 		<td>User Name</td>
		 		<td>
					<input type="text" id="txt_username" name="txt_username" size="25"/>
		 		</td>
	 		</tr>
	 		<tr>
		 		<td>Password</td>
		 		<td>
					<input type="password" id="txt_password" name="txt_password" size="25"/>
		 		</td>
	 		</tr>
			<tr>
				<td>User Type:</td>
				<td>
					<select name="User_Type">
						<option value="Customer">Customer</option>
						<option value="CS">Customer Service</option>
					</select>
				</td>
			</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<br>
	  				<input type="hidden" name="doodoo" value="weewee"/>
	  				<input type="submit" name="Submit" value="Login" class="btn"/>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<font size="-1" color="Maroon"><i><?php echo $error_message ?></i></font>
	  			</td>
	  		</tr>
		</table>
		</tr>
	</table>
	</form>
<?php include('Page_Footer.php'); ?>
</body>
</html>
