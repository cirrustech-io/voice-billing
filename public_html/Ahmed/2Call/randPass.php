<?php
// {{{ rbyte
$rbyte_seed=NULL;
$rbyte_urand="";
$rbyte_bytes=Array();
$charSet = "23456789ABCDEFGHJKLMNPRSTUVWXYZabcdefghijkmnopqrstuvwxyz";


function rbyte () {
        global $rbyte_seed;
        global $rbyte_urand;
        global $rbyte_bytes;
        if ($rbyte_seed==NULL) {
                $s="";
                for ($i=0;$i<100;$i++) {
                        $s.=mt_rand(0,0x100000).sha1($s);
                }
                $rbyte_seed = sha1($s);
                $f=fopen("/dev/urandom","r");
                if ($f) {
                        $rbyte_urand=fread($f,29);
                        fclose ($f);
                } else {
                        $rbyte_urand = sha1(microtime().mt_rand());
                }
        }
        if (count($rbyte_bytes)==0) {
                $rbyte_seed = sha1($rbyte_seed.sha1($urand)).$rbyte_seed;
                $rand_str = substr($rand_str,0,137);
                $r = sha1($rbyte_seed.$rbyte_urand.mt_rand(0,1000000),1);
                $rbyte_bytes = str_split ($r,1);
        }
        return ord(array_pop ($rbyte_bytes));
}
// }}}
// {{{ rnum
function rnum($max) {
        do {
                $b = rbyte();
                $m = 1;
                while ($m<=$max) {
                        $m*=2;
                }
                $m-=1;
                $b &= $m;
        } while ($b>$max);
        return $b;
}
// }}}
// {{{ rpass
function rpass($size,$charset) {
        $p="";
        $setsize=strlen($charset)-1;
		for ($i=0;$i<$size;$i++) {
                $p .= $charset[rnum($setsize)];
        }
        return $p;
}
// }}}
echo "<pre>";

$editFormAction = $_SERVER['PHP_SELF'];

$len=$_POST['len'];

if ($len==NULL)
	$len=7;

if($len>24)
	echo ("length is too long, max is 24");
elseif($len<4)
	echo ("length is too short, min is 4");
else
	echo "random pass of length $len: ".rpass($len,$charSet)."\n";
	
echo "</pre>";
?>

<html>
<head></head>
<body>
<form method='POST' action="<?php echo $editFormAction ?>">
Password length: <input type='text' name='len' value="<?php echo $len; ?>"/>
<input type='submit' name='but' type='submit' value='Generate!'/>
</body>
</form>