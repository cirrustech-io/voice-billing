<?php
	include_once('DBAccessor.php');
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }
//session check ends here
?>

<HTML>
<head>
	<title>2Call services - Main Menu</title>
	<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>
	<script LANGUAGE="JavaScript" SRC="Dates.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="LoadandValidate.js"></SCRIPT>
</head>


<body>
<?php
include('Menu_Header.php');

if (isset($_SESSION['ID']))
	$ID = $_SESSION['ID'];
if (isset($_POST["AccountID"]))
	{$AccountID = $_POST["AccountID"];
	 $BillingType=fetchBillingType($AccountID);}
elseif(isset($_SESSION["AccountID"]))
	{$AccountID = $_SESSION["AccountID"];
	 $BillingType=fetchBillingType($AccountID);
	 $_SESSION["AccountID"]="";
	 }
if (isset($_POST['type']))
		$type = $_POST['type'];
else
		$type="All";
		
if (isset($_POST['sort']))		
	$Sort=$_POST['sort'];
else
	$Sort= "time";
	
if (isset($_POST['way']))
	$way=$_POST['way'];	
else
	$way="Ascen";
	
if(($type=="Refills")||($type=="All"))
	if(($Sort!= "amt")&&($Sort!= "time"))
		$Sort= "time";
		

if (isset($_POST['ToDate']))
		$ToDate = $_POST['ToDate'];
else
		$ToDate=date("m/d/Y");
if (isset($_POST["FromDate"]))
		$FromDate= $_POST["FromDate"];
else
		$FromDate=date("m/d/Y",strtotime($ToDate) - (7*86400));	

$accounts=fetchAccounts($ID);


//make the bill!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
if($_SESSION['CT']=="CS")
{
$Bacc=fetchNumber($AccountID);
$details=customerDetails($AccountID);
$line = mssql_fetch_array($details, MSSQL_ASSOC);
$str.="Account:,{$Bacc}\r\n";
$str.="Contact:,\"{$line['customer']}\"\r\n";
$str.="Company:,\"{$line['company']}\"\r\n";
$str.="Address:,\"{$line['address1']}\"\r\n";
$str.="Billing activity\r\n";
$str.="Date,Time,Description,Detail,Quantity,Amount\r\n";

$bill=generateBill($AccountID,$FromDate,$ToDate);
while ($line = mssql_fetch_array($bill, MSSQL_ASSOC))
	{$amt=round($line['amount']/1000,3);
	$str.="{$line['DATE1']},{$line['TIME1']},{$line['description']},{$line['detail']},{$line['quantity']},{$amt}\r\n";}
	
if($str=="")
	$str="No data found";
	
	$date=date("Y-m-d_H-i-s");
 	$file="Bill_$date.csv";
 	$ofile = fopen("../output/$file",'w');
	fwrite($ofile,$str);
	
}

print("<form name='submitForm' onSubmit=\"return ValidateForm()\" action='Main_Menu.php' method='POST'>");
print("<TABLE><TR>");
print("<TD>Account:</TD>");
print("<TD><select name='AccountID' id=0>" );
while ($line = mssql_fetch_array($accounts, MSSQL_ASSOC))
	{
	$Account=$line['account'];
	$acc=$line['account_id'];
	print("<option value='$acc'>$Account</option>");
	}
print("</select>");
print("</TD></TR>");

print("<TR>");
print("<TD>Record type:</TD>");
print("<TD><select name='type' id=1>");
print("<option value='All'>All</option>");
if($BillingType=='1')
	print("<option value='Refills'>Refills</option>");
else
	print("<option value='Refills'>Payments</option>");
print("<option value='Calls'>Calls made</option>");
print("</select></TD>");
print("</TR>");


if(!isset($AccountID))
	$AccountID=$acc;

//if(!$Account)
	$Account=fetchNumber($AccountID);

	
print("<TR>");
print("<TD>From:</TD>");
print("<TD><INPUT TYPE='text' NAME='FromDate' value='{$FromDate}'SIZE=25>");
print "<A HREF=\"#\"\n";
print "onClick=\"cal.select(document.forms['submitForm'].FromDate,'anchor1','MM/dd/yyyy'); return false;\"\n";
print "NAME=\"anchor1\" ID=\"anchor1\">select</A></TD>";
print("</TR></TD>");
print("<TR>");
print ("<TD>To:</TD>");
print ("<TD><INPUT TYPE='text' NAME='ToDate' value='{$ToDate}' SIZE=25>");
print "<A HREF=\"#\"\n"; 
print "onClick=\"cal2.select(document.forms['submitForm'].ToDate,'anchor2','MM/dd/yyyy'); return false;\"\n"; 
print "NAME=\"anchor2\" ID=\"anchor2\">select</A></TD>"; 
print("</TR>");
print("<TR>");
print ("<TD>Sort By:</TD>");
print ("<TD><SELECT name=\"sort\" id=2>");
print ("<option value=\"time\"/>Time");
print ("<option value=\"amt\">Total Charge");
if($type=="Calls")
	{print ("<option value=\"des\"/>Destination");
	print ("<option value=\"num\"/>Number");
	print ("<option value=\"quan\"/>Call duration");
	print ("<option value=\"charge\"/>Minute Charge</option></TD>");}
print("</TR>");
print("<TR>");
print ("<TD>Order method:</TD>");
print ("<TD><SELECT name=\"way\" id=3><option value=\"Ascen\"/>Ascending");
print ("<option value=\"Desce\"/>Descending</TD>");
print("</TR>");
print("<TR>");
print("<TD colspan=2><input type='submit' value='Display details'>");
print("</TD>");
print("<input type='hidden' value='$ID' name='ID'>");
print("</TR>");
print("</form>");
if($_SESSION['CT']=="CS")
	print("<TR><TD><FORM name='billForm' method='POST' action='Download_Attacher.php'><input type='hidden' name='file' value=$file><input type='Submit' value='Generate Bill'></FORM></TR></TD>");
print "<DIV ID=\"CalendarDiv\" STYLE=\"position:absolute;visibility:hidden;background-color:white;layer-background-color:white;\"></DIV>\n"; 
print("</TABLE>");

$Name=fetchName($AccountID);

print("<br/>Customer Name: $Name");
print("<br/>Details for the account: $Account");
$balance=fetchBalance($AccountID);
print("<br/><font size='5'");
if($balance>=0)
	printf(" color='green'><b>Credit remaining: BD%.3f",$balance);
else
	{$balance=abs($balance);
	printf(" color='red'><b>Amount Due: BD%.3f",$balance);}
print("</b></font>");
	


print("<script>document.getElementById('2').value = '{$Sort}';
document.getElementById('3').value = '{$way}';
document.getElementById('0').value = '{$AccountID}';
document.getElementById('1').value = '{$type}';</script>");

$bill=fetchBilling($AccountID,$type,$FromDate,$ToDate,$Sort,$way);

$totCalls=0;
$totRefills=0;
if($type=="All" ||$type=="Calls")
{
	// Printing the HTML table header
	print "<br/><br/><table  id=\"results\" border=\"1\">";
	print "<tr>";
	print "<th colspan=4></th>";
	print "<th>Call duration</th>";
	print "<th>Minute charge</th>";
	print "<th>Total charge</th>";
	print "</tr>";
	print "<tr>";
	print "<th>Destination</th>";
	print "<th>Number</th>";
	print "<th>Date</th>";
	print "<th>Time</th>";
	print "<th>Minutes</th>";
	print "<th>Fils</th>";
	print "<th>BD</th>";
	print "</tr>";

	//color is used to make the table more readable by alternate coloring
	$color=0;
	$x=0;
	$flag=false;
	if($BillingType=='1')
		$printType="Refill";
	else			
		$printType="Payment";
			
	//fetch query result in an array and go through it line by line
	while ($line = mssql_fetch_array($bill, MSSQL_ASSOC)) {
		$x++;
		$flag=true;
		//switch the color variable if the last line has a diffrent calling code
		$color=ABS($color-1);
		
		//select the appropriate color
		if ($line['amount']<0)
			{print "\t<tr bgcolor=\"#CDEDFF\" >\n";
			 $color=ABS($color-1);}
		else if ($color==0) 
			print "\t<tr bgcolor=\"#CFCFCF\" >\n";
		else
			print "\t<tr bgcolor=\"#FFFFFF\" >\n";

				
		$amount=abs($line['amount'])/1000;
		if($line['amount']>=0)
		   {print "<td>{$line['description']}</td>";
		    print "<td>{$line['detail']}</td>";			
			print "<td>{$line['start_date_time1']}</td>";
			print "<td>{$line['start_time']}</td>";
			//print "<td>{$line['disconnect_date_time']}{$line['disconnect_time']}</td>";
			print "<td>{$line['quantity']}</td>";
			print "<td>{$line['per_minute_charge']}</td>";
			printf("<td>%.3f</td>",$amount);
			$totCalls+=$amount;}
		else
			{				
			print "<td colspan=2>$printType</td>";
			print "<td>{$line['start_date_time1']}</td>";
			print "<td>{$line['start_time']}</td>";
			print "<td colspan=2></td>";
			printf("<td>%.3f</td>",$amount);
			$totRefills+=$amount;}
			
		print "</tr>";
	}
	if($flag==false)
		print "<TD colspan=7 bgcolor=\"#FCB8B8\" >No records available from {$FromDate} to {$ToDate}.</TD></TR>";
	else
	{if(($type=="All") || ($type=="Calls"))
		{print "<TR><TD bgcolor=\"#AAFFAA\">Total calls</TD><TD colspan=5 rowspan=2 bgcolor=\"#AAFFAA\" >From {$FromDate} to {$ToDate}</TD>";
		 printf ("<TD  bgcolor=\"#AAFFAA\" >%.3f</TD></TR>",$totCalls);}
	 if(($type=="All"))
		{print "<TR><TD bgcolor=\"#AAFFAA\">Total {$printType}s</TD>";
		 printf ("<TD  bgcolor=\"#AAFFAA\" >%.3f</TD></TR>",$totRefills);}
	}
	print"</table>";
}

if($type=="Refills")
{
// Printing the HTML table header
print "<br/><br/><table  id=\"results\" border=\"1\">";
print "<tr>";
if($BillingType=='1')
	{print "<th>Refill date</th>";
	print "<th>Refill time</th>";}
else
	{print "<th>Payment date</th>";
	print "<th>Payment time</th>";}
print "<th>Amount(BD)</th>";
print "</tr>";

//color is used to make the table more readable by alternate coloring
$color=0;
$x=0;
$flag=false;
//fetch query result in an array and go through it line by line
while ($line = mssql_fetch_array($bill, MSSQL_ASSOC)) {
	$x++;
	$flag=true;
	//switch the color variable if the last line has a diffrent calling code
	$color=ABS($color-1);
	
	//select the appropriate color
	if ($color==0) 
		{print "\t<tr bgcolor=\"#CFCFCF\" >\n";}
	else
		{print "\t<tr bgcolor=\"#FFFFFF\" >\n";}
	
	//print the query line in HTML table format
	print "<td>{$line['start_date_time1']}</td>";
	print "<td>{$line['start_time']}</td>";
	$amount=(float)($line['amount']);
	$amount=abs(round(($amount/1000),3));
	printf("<td>%.3f</td>",$amount);
	print "</tr>";	
	$totRefills+=$amount;
}
if($flag==false)
	print "<TD colspan=5 bgcolor=\"#FCB8B8\" >No records available from {$FromDate} to {$ToDate}.</TD></TR>";
else
	{print "<TR><TD colspan=2 bgcolor=\"#AAFFAA\">Total refills</TD>";
	printf ("<TD  bgcolor=\"#AAFFAA\" >%.3f</TD></TR>",$totRefills);}
print"</table>";
}
include('Page_Footer.php'); ?>

<SCRIPT LANGUAGE="JavaScript">
if(navigator.appName!="Microsoft Internet Explorer")
	document.write("<BR/><BR/>");
</SCRIPT>

</body>
</HTML>
