<?php
	include_once('DBAccessor.php');
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }
//session check ends here
?>

<HTML>
<head>
	<title>2Call services - Change Password</title>
</head>


<body>
<script type="text/javascript">
function Val()
{	
	var data=document.changePass.txt_password.value;
	//data=data.replace(" ","!");
	var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?~_ ";
	for (var i = 0; i < data.length; i++) {
		if (iChars.indexOf(data.charAt(i)) != -1) {
			alert ("Your new password has special characters, you may only use a-z, A-Z and 0-9");
			return false;
		}
	}
	return true;
}
</script>
<?php
include('Menu_Header.php');

if (isset($_SESSION['ID']))
	$ID = $_SESSION['ID'];
else
	header("location: logout.php");

$sub=$_POST['submitted'];
$old=$_POST['txt_old_password'];
$new1=$_POST['txt_password'];
$new2=$_POST['txt_password_again'];

if($sub)
{if(($old)&&($new1)&&($new2))
{
	if($new1===$new2)
	{	if((strlen($new1)>=6)&&(strlen($new1)<=18))
		{	
			$result=ChangePassword($ID,$old,$new1);
			if($result==0)
					{
						$error_message = "<p id=\"errorText\">Your old password is incorrect, please try again.</p>";
					}
			elseif($result==1)
					{
						$error_message = "<p id=\"msgText\"><a href=\"Logout.php\">Your Password has been succesfully changed! Click here to logout</a></p>";
					}
			elseif($result==-1)
					{
						$error_message = "<p id=\"errorText\">An error has occured please contact us <a href=\"mailto:ahmed.younis@2connectbahrain.com?subject=2Call Portal\">here</a> with the details!</p>";
					}
			elseif($result==-2)
					{
						$error_message = "<p id=\"errorText\">Your Javascript is disabled, please enable it or simply use another browser with Javascript enabled(we recommend FireFox)!</p>";
					}
		}
		else
			$error_message = "<p id=\"errorText\">Password length has to be between 6-18 characters</p>";
	}
	else
		$error_message="<p id=\"errorText\">The new passwords don't match, please try again</p>";
}

else
	$error_message="<p id=\"errorText\">Please fill in all the required details</p>";
}
else
	$error_message = "<p id=\"msgText\">Password length has to be 6-18 characters consisting of only 0-9,a-z and A-Z.</p>";

?>
<form name="changePass" action="Change_Pass.php" method="POST" enctype="multipart/form-data">	
<table bgcolor="Silver" cellpadding="5" cellspacing="0">
	 		<tr>
		 		<td>Old Password</td>
		 		<td>
					<input type="password" id="txt_old_password" name="txt_old_password" size="25"/>
		 		</td>
	 		</tr>
	 		<tr>
		 		<td>New Password</td>
		 		<td>
					<input type="password" id="txt_password" name="txt_password" size="25"/>
		 		</td>
	 		</tr>	 		
			<tr>
		 		<td>New Password again</td>
		 		<td>
					<input type="password" id="txt_password_again" name="txt_password_again" size="25"/>
		 		</td>
	 		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<br/>
	  				<input type="hidden" name="submitted" value="yes" class="btn"/>
					<input type="submit" name="Submit" value="Change Password" onclick="return Val();" class="btn"/>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<font size="-1"><i><?php echo $error_message ?></i></font>
	  			</td>
	  		</tr>
		</table>
		</tr>
	</table>
</form>
</body>
</HTML>
<?php include('Page_Footer.php'); ?>

<SCRIPT LANGUAGE="JavaScript">
if(navigator.appName!="Microsoft Internet Explorer")
	document.write("<BR/><BR/>");
</SCRIPT>