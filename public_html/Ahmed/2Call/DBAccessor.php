<?php
#include('includes/functions.inc.php');

function startDB(){
	#$DB = mssql_connect('192.168.2.45', 'ahmed.younis', 'wy65b2sm'); //clone
	$DB = mssql_connect('80.88.247.20', 'ahmed.younis', 'K3takXJr'); //original
	if (!$DB) {
		die('Could not connect: ' . mssql_error());}
	else
		mssql_select_db ('TEL_DATA'); 

	return($DB);
	
}

function applyIPFilter(){
//first check if the IP is local, otherwise kick the intruder out
$allowed_IP1="80.88.240";
$allowed_IP2="80.88.242";
$allowed_IP3="80.88.248.30";
$allowed_IP4="80.88.248.141";
$allowed_IP5="80.88.248.142";
$allowed_IP6="192.168.2";//perhaps might need to remove it later
$allowed_IP7="192.168.5"; //added by haifa to include her IP

$ip_address = $_SERVER['REMOTE_ADDR'];
 
if(		(substr($ip_address,0,9)<>$allowed_IP1)
	&&	(substr($ip_address,0,9)<>$allowed_IP2)
	&&	($ip_address<>$allowed_IP3)
	&&	($ip_address<>$allowed_IP4)
	&&	($ip_address<>$allowed_IP5)
	&&	(substr($ip_address,0,9)<>$allowed_IP6)
	&&      (substr($ip_address,0,9)<>$allowed_IP7)
	){
	echo "<p id=\"errorText\">$ip_address<br/>Your computer IP is not allowed to access this service</p>";
	exit(0);}
//IP checking ends here, you may add/remove IPs if any changes happen in the company's network
}

function fetchAccounts($ID){
	$ID=mssql_real_escape_num($ID);
	//Connection statement
	$DB=startDB();
	$query = "SELECT account_id,account FROM accounts where customer_id=$ID";
	$accounts = mssql_query($query);	
	

	return $accounts;
}

function fetchNumber($account){
	$account=mssql_real_escape_num($account);
	//Connection statement
	$DB=startDB();
	
	$query = "SELECT account from accounts where account_id=$account";
	$result = mssql_query($query);	
	$line = mssql_fetch_array($result ,MSSQL_BOTH);
	$Number=$line['account'];
	
	
	return $Number;
}
function fetchAccountID($Number){
	$Number=mssql_real_escape_num($Number);
	//Connection statement
	$DB=startDB();
	
	$query = "SELECT account_id from accounts where account='$Number'";
	$result = mssql_query($query);	
	$line = mssql_fetch_array($result ,MSSQL_BOTH);
	$Account_ID=$line['account_id'];
	
	// was "" and not null, checking if this will work!
	if($Account_ID==NULL)
		return -1;
	
	
	return $Account_ID;
}

function getCustomerID($Account_ID){
	$Account_ID=mssql_real_escape_num($Account_ID);
	//Connection statement
	$DB=startDB();
	
	$query = "SELECT customer_id from accounts where account_id=$Account_ID";
	$result = mssql_query($query);	
	$line = mssql_fetch_array($result ,MSSQL_BOTH);
	$Customer_ID=$line['customer_id'];
	
	if($Customer_ID==0)
		$Customer_ID=-1;
	elseif($Customer_ID==NULL)
		$Customer_ID=0;
	
	
	return $Customer_ID;
}

function fetchName($account_ID){
	$account_ID=mssql_real_escape_num($account_ID);
	//Connection statement
	$DB=startDB();
	
	$query = "select customer from customers
			  where customer_ID=(select customer_ID from accounts where account_id=$account_ID)";
	$result = mssql_query($query);	
	$line = mssql_fetch_array($result ,MSSQL_BOTH);
	$Name=$line['customer'];
	
	
	return $Name;
}

function fetchBillingType($account){
	$account=mssql_real_escape_num($account);
	//Connection statement
	$DB=startDB();
	
	$query = "SELECT billing_type from accounts where account_id=$account";
	$result = mssql_query($query);	
	$line = mssql_fetch_array($result ,MSSQL_BOTH);
	$type=$line['billing_type'];
	
	
	return $type;
}

function fetchBalance($account){
	$account=mssql_real_escape_num($account);
	//Connection statement
	$DB=startDB();
	
	$query = "SELECT balance from accounts where account_id=$account";
	$result  = mssql_query($query);	
	$line = mssql_fetch_array($result ,MSSQL_BOTH);
	$balance=(float)$line['balance'];
	
	//php has -0! so adding 0 will make the -0 a 0!!!!!
	$balance=round(($balance/1000),4)+0;
	
	
	return $balance;
}

function fetchBilling($account,$type,$FromDate,$ToDate,$Sort,$way){
$error="<br/><br/><B><U>Your Javascript is disabled, please enable it or simply use another browser with Javascript enabled(we recommend FireFox)!</U></B>";
	$account=mssql_real_escape_num($account);
		
	$ToDate=trim($ToDate);
	$FromDate=trim($FromDate);

	if((strlen($ToDate)>10)||(strlen($FromDate)>10))
		exit();
	
	$Stamp = strtotime( $ToDate );
	$Month = date( 'm', $ToDate );
	$Day   = date( 'd', $ToDate );
	$Year  = date( 'Y', $ToDate );
	if(checkdate( $Month, $Day, $Year )==false)
		{echo $error;
		include('Page_Footer.php'); 
		exit();}

	$Stamp = strtotime( $FromDate );
	$Month = date( 'm', $FromDate );
	$Day   = date( 'd', $FromDate );
	$Year  = date( 'Y', $FromDate );
	if(checkdate( $Month, $Day, $Year )==false)
		{echo $error;
		include('Page_Footer.php'); 
		exit();}
		
	if($ToDate=='')
		$ToDate=date("m/d/y");
	elseif(strtotime($ToDate)>strtotime(date("m/d/y")))
		{echo $error;
		include('Page_Footer.php'); 
		exit();}
		
	
	if($FromDate=='')
		$FromDate=date("m/d/y",strtotime($ToDate) - (7*86400));
	elseif(strtotime($FromDate)<strtotime('01/01/2005'))
		{echo $error;
		include('Page_Footer.php'); 
		exit();}
		

	$ToDate = date ("m/d/y",strtotime($ToDate) + 86400);
	
	//added more validation on server side
	if(strtotime($FromDate)>strtotime($ToDate))
		{echo $error;
		include('Page_Footer.php'); 
		exit();}

	if($way=="Desce")
		$way="Desc";
	else
		$way="";
	
		switch($Sort)
		{case "time":
			$Sort="start_date_time";
			break;
		case "amt":
			$Sort="abs(amount)";
			break;
		case "des":
			$Sort="description,detail";
			break;
		case "num":
			$Sort="detail";
			break;
		case "quan":
			$Sort="quantity";
			break;	
		case "charge":
			$Sort="per_minute_charge";
			break;
		default:
			$Sort="start_date_time";
			break;}
			
	//Connection statement
	$DB=startDB();	
	
	if($type=="Calls")
		$query = "SELECT convert(nvarchar(10),dateadd(hour,3,start_date_time),108) as start_time,convert(nvarchar(10),start_date_time,1) as start_date_time1
		,detail,description,per_minute_charge,quantity,amount FROM billing where account_id=$account and amount>=0 and start_date_time>='$FromDate'
		and start_date_time<='$ToDate' and node_type in(1,3,100) order by $Sort $way";
	else if($type=="Refills")
		$query = "SELECT convert(nvarchar(10),dateadd(hour,3,start_date_time),108) as start_time,convert(nvarchar(10),start_date_time,1) as start_date_time1,amount FROM billing where account_id=$account and amount<0 and start_date_time>='$FromDate'
		and start_date_time<='$ToDate' and node_type in(1,3,100) order by $Sort $way";
	else if($type=="All")
		$query = "SELECT convert(nvarchar(10),dateadd(hour,3,start_date_time),108) as start_time,convert(nvarchar(10),start_date_time,1) as start_date_time1,detail,description,per_minute_charge,quantity,amount FROM billing where account_id=$account and start_date_time>='$FromDate'
		and start_date_time<='$ToDate' and node_type in(1,3,100) order by $Sort $way";
	
	//echo $query;
	
	$bill = mssql_query($query);	
	
	
	return $bill;
}

function generateBill($account,$FromDate,$ToDate)
{	$error="<br/><br/><B><U>Your Javascript is disabled, please enable it or simply use another browser with Javascript enabled(we recommend FireFox)!</U></B>";
	$account=mssql_real_escape_num($account);
		
	if($ToDate=='')
		$ToDate=date("m/d/y");
	elseif(strtotime($ToDate)>strtotime(date("m/d/y")))
		{echo $error;
		include('Page_Footer.php');
		exit();}
		
	if($FromDate=='')
		$FromDate=date("m/d/y",strtotime($ToDate) - (7*86400));
	elseif(strtotime($FromDate)<strtotime('01/01/2005'))
		{echo $error;
		include('Page_Footer.php'); 
		exit();}
		
	//added more validation on server side
	if(strtotime($FromDate)>strtotime($ToDate))
		{echo $error;
		include('Page_Footer.php'); 
		exit();}
		
	

	
	$ToDate = date ("m/d/y",strtotime($ToDate) + 86400);	
	
	//0 amounts are there for finance/billing to detect any mistakes in billing! node type is set all
	
	$query = "SELECT convert(nvarchar(10),dateadd(hour,3,start_date_time),108) as TIME1,convert(nvarchar(10),start_date_time,101)as DATE1
	,detail,description,per_minute_charge,quantity,amount FROM billing where account_id='$account' and amount>=0 and start_date_time>='$FromDate'
	and start_date_time<='$ToDate' order by start_date_time";
		
	$bill = mssql_query($query);	

	//echo $query;
	
	
	return $bill;
}
function customerDetails($account)
{	$account=mssql_real_escape_num($account);
	$query="select customer,company,address1 from customers where customer_id =(select customer_id from accounts where account_id='$account')";
	
	$details = mssql_query($query);	
	
	//echo $query;
	
	
	return $details;
}

function checkPassword($username,$password){
	$DB=startDB();
	//' or customer like '%kooheji%  
	//' or customer_id=123 and '1'='1
	//!!!! must use the following to disable such things!
	//CHECK IF EMPTY!!!!!!
	
	$username=mssql_real_escape_string($username);
	$password=mssql_real_escape_string($password);
	
	
	if($username==NULL || $username=="" || $password==NULL || $password==""|| $username==false || $password==false)//just to make sure
		$ID=NULL;
	else
		{
		$query = "SELECT customer_id FROM Customers where LOGIN_NAME='$username' and LOGIN_PASSWORD='$password'";
		$login_result  = mssql_query($query);
		$line = mssql_fetch_array($login_result,MSSQL_BOTH);
		$ID=$line['customer_id'];
		}
	
	
	
	return $ID;
}

function getUsername($CID)
{		
		$DB=startDB();
		$CID=mssql_real_escape_string($CID);
		$query = "SELECT LOGIN_NAME FROM CUSTOMERS where CUSTOMER_ID='$CID'";
		$login_result  = mssql_query($query);
		$line = mssql_fetch_array($login_result,MSSQL_BOTH);
		$user=$line['LOGIN_NAME'];
		
		
		return $user;
}

function ChangePassword($CID,$old,$new)
{	$DB=startDB();
	$CID=mssql_real_escape_string($CID);
	$old=mssql_real_escape_string($old);
	//$new=mssql_real_escape_string($new);
	

	if(preg_match("/[^a-zA-Z0-9]/", $new))
		return(-2);
	
	$username=getUsername($CID);
	$CID2=checkPassword($username,$old);
	if($CID2)
	{if($CID==$CID2)
		{
		$result=-1;
		$query = "UPDATE Customers SET LOGIN_PASSWORD='$new' where LOGIN_NAME='$username' and LOGIN_PASSWORD='$old' and CUSTOMER_ID='$CID'";
		$login_result  = mssql_query($query);
		if($login_result)
			$result=1;
		return($result);
		}
	else
		return(-1);}
	else
		return(0);

}



function mssql_real_escape_string($string_to_escape) {
//echo "b: ".$string_to_escape;
$replaced_string =preg_replace('/[^a-zA-Z0-9]/', "_", $string_to_escape);
//echo "a: ".$replaced_string;

return $replaced_string;
} 

function mssql_real_escape_num($num_to_escape) {
//echo "b: ".$num_to_escape;
$replaced_num=preg_replace('/[^0-9]/', "_", $num_to_escape);
//echo "a: ".$replaced_num;

return $replaced_num;
} 

function cleanOutput(){
$dir="../output/";

    $mydir = opendir($dir);

    while(false !== ($file = readdir($mydir))) {
		$extention=strrchr($file,".");
        if($file != "." && $file != ".." && $extention==".csv") {

            if(is_dir($dir.$file)) {

                chdir('.');

                destroy($dir.$file.'/');
                $cleanResult.=$dir.$file."________________________________________________\n";
                $cleanResult.=$dir.$file." File Cleaning Started\n";
                $cleanResult.=$dir.$file." was cleaned\n";

                rmdir($dir.$file) or DIE($cleanResult.="couldn't delete $dir$file\n");

            }

            else

                unlink($dir.$file) or DIE($cleanResult.="couldn't delete $dir$file\n");

        }

    }

    closedir($mydir);

        $cleanResult.=$dir.$file." File Cleaning Done\n";
		$cleanResult.=$dir.$file."________________________________________________\n";
				
    return($cleanResult);
}

?>
