<?php
//check if the session is still alive
	include_once('DBAccessor.php');
	//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }
	applyIPFilter();
//session check ends here
?>

<HTML>
<head>
	<title>2Call services - Customer Service</title>
</head>
<body>
<?php include('Menu_Header.php');
if($_SESSION['CT']=="CS")
	{if(isset($_POST['Account']))
		{$AccountID=fetchAccountID($_POST['Account']);
		if($AccountID==-1)
			echo "<font color=red>Account Number not found!</font>";
		else
			{$ID=getCustomerID($AccountID);
		
			if($ID==-1)
				{echo "<p id=\"errorText\">This customer is facing some issues, please click <a href=\"mailto:ahmed.younis@2connectbahrain.com?subject=2Call Portal Error&body=The Account # '{$_POST['Account']}' has faced a problem in logging in due to there ID\">here</a> to report this problem!</p>";
				break;}
			
			$_SESSION['AccountID']=$AccountID;
			$_SESSION['ID']=$ID;
			//$_SESSION['USER']="CS";
			session_write_close();
			header("Location: Main_Menu.php");
			exit();}
		}
	}
else
	header("Location: Logout.php");

?>

<form name='submitForm' action='Customer_Service.php' method='POST'>
Account #: <INPUT TYPE='text' NAME='Account'></INPUT>
<input type="submit" name="Submit" value="Find Customer" class="btn"/>
</form>
<?php include('Page_Footer.php'); ?>
</body>
</HTML>
