<?php
//CSV File location
$File = "output//data.csv";

include_once('DBAccessor.php');
//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	
//session check ends here

//page header and options
echo "<html><head><title>LCR Results</title><body>";
echo "<b>Application in testing stage, Please report any errors/bugs to <a href=\"mailto:ahmed.younis@2connectbahrain.com?subject=LCR Web Application Error/Bug\">ahmed.younis@2connectbahrain.com</b><br/>";
echo "<a href=\"LCR_Search.php\"><b>Back</b></a>&nbsp&nbsp&nbsp&nbsp&nbsp<a href=\"Logout.php\"><b>LogOut</b></a><br/><br/>";


if(!array_key_exists('type',$_GET) || !array_key_exists('item',$_GET)){
	echo "<br>Security Error!";
	exit(0);
}


//Get the searched item
$item=$_GET['item'];

//store the type
$type=$_GET['type'];

//store if exact option was selected
$exact=$_GET['EXACT'];

//Check the type, code or name and if the exact option was selected, and finaly write the initial query
if($type == 'Code')
	{if ($exact=='on')
		{$query = "SELECT * FROM lcr_rates WHERE calling_code LIKE '$item'";}
	else
		{$query = "SELECT * FROM lcr_rates WHERE calling_code LIKE '$item%'";}}
else if($type=='Region')
	{$item=mb_strtoupper($item);
	if ($exact=='on')
		{$query = "SELECT * FROM lcr_rates WHERE region_name LIKE '$item'";}
	else
		{$query = "SELECT * FROM lcr_rates WHERE region_name LIKE '%$item%'";}}
else
{	echo "<br>Security Error!";
	exit(0); }

$selected=$_GET['carrier_list'];
//echo $_GET['carrier_list'];

if ($selected)
{	 
	 foreach ($selected as $s)
	 {	//boolean to check if the selected carrier is real
	 	$real_carrier=0;
	 	//get all carriers
	 	$rows_carriers=getAllLiveCarriers();
	 	
	 	$rows_carriers->MoveFirst();
		while(!$rows_carriers->EOF){
		
			if($rows_carriers->Fields('carrier_name')==$s)
				{$real_carrier=1;
				break;}
			
			$rows_carriers->MoveNext();
		}
		
		if($real_carrier==0)
		{echo "<br>Security Error!";
		exit(0); }
		
	 	{
	 	if($carriers == "")
	 	{$carriers = " AND (supplier='" . $s . "'";}
	 	else
	 	{$carriers .=  " OR supplier=" . "'" . $s. "'";}
	 	}
	 }
	 
	 if ($carriers<>"")
	 $query .= $carriers.")";
}


//the following commented code is usefull to view the final query that can be copy pasted in pgSQL
//echo "<b>jibbrish here is the developer testing the application</b><br/>";
//echo $query;

//try the query and give an error message if it fails
$result=fetchLCRTable($query);


// Printing the HTML table header
echo "<table border='2'>\n";
echo "<tr bgcolor=\"#99CCFF\">";
echo "<td>Calling Code</td>";
echo "<td>Region Name</td>";
echo "<td>Supplier</td>";
echo "<td>Lowest Rate</td>";
echo "<td>Effective Date</td>";
echo "</tr>";

//color is used to make the table more readable by alternate coloring
$color=0;
//to count how many exist
$record=0;

//csv file header
$csv_content .= "Calling Code" . ',' . "Region Name" . ',' . "Supplier" . ',' . "Lowest Rate" . ',' ."Effective Date" . "\n"; 

//fetch query result in an array and go through it line by line
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
	
	//switch the color variable if the last line has a diffrent calling code
	if ($line['calling_code']<>$lastline)
		{$color=ABS($color-1);}
	
	//select the appropriate color
	if ($color==0) 
		{echo "\t<tr bgcolor=\"#CFCFCF\" >\n";}
	else
		{echo "\t<tr bgcolor=\"#FFFFFF\" >\n";}
	
	//print the query line in HTML table format
	echo "<td>{$line['calling_code']}</td>";
	echo "<td>{$line['region_name']}</td>";
	echo "<td>{$line['supplier']}</td>";
	echo "<td>{$line['lowest_rate']}</td>";
	echo "<td>{$line['eff_date']}</td>";
	
	//create CSV data
	$csv_content .= $line['calling_code'] . ',' . $line['region_name'] . ',' . $line['supplier'] . ',' . $line['lowest_rate'].','.$line['eff_date'] . "\n"; 
	
	//save the current line's calling code
	$lastline=$line['calling_code'];

	echo "</tr>\n";

	$record=$record+1;
}

//close the table
echo "</table>\n";
	
// Free resultset
pg_free_result($result);
	
//print the amount of results found
if($record==0)
	{echo "<b>No results found</b>";}
else if($record==1)
	{echo "<b>One result found</b>";}
else
	{echo "<b>$record results found</b>";}

//add the amount of results to the end of the csv data
$csv_content .= $record . " results found";
	

?>
<br/><br/>
<form method="post" action="
<?$fh = fopen($File, 'w') or die("can't open csv file for writing");
fwrite($fh, $csv_content);
fclose($fh);
?>Download.php">
<input type="submit" id="button" value="Download CSV File">
</form>
</body>
</html>
