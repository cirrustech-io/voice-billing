<?php
include('DB.php');
include('DBAccessor.php');
$today = date('d-M-y');
$File = "output//data.csv";
$log = "logs//$today.log";

$logdata="fetching latest data, dated $today\n";

//refresh the lcr table rates
$logdata.=performLCRTableRefresh($today);

//remove csv file content
$fh = fopen($File, 'w') or die($logdata.="could't clean CSV\n");
fwrite($fh, "");
fclose($fh);

$logdata.="Done\n";

//write the log
$fh = fopen($log, 'w') or die("could not write log file!?\n");
fwrite($fh, $logdata);
fclose($fh);

echo "Done";

?>
