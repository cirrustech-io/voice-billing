<?php


include_once('CSVParser.php');
$inputfilepath = 'BATELCO RATES - NAT and INTL-New.csv';


$parser = new CsvFileParser();

$parsedData = $parser->ParseFromFile($inputfilepath);

$batelco_rates = array();

foreach($parsedData as $line){

	 $rateline = array();
	 $rateline['rate'] = trim($line[2]);
	 $rateline['len']= $line[3];
        		

	if(is_int(strpos($line[1],','))){
		$codes =  explode(',', trim($line[1]));
		foreach($codes as $code){
			 $rateline['region_name'] = $line[0] . '-' . $code;
			 $rateline['code'] = $code;
			 $batelco_rates[] = $rateline;
		}

	} else {
		$rateline['region_name'] = $line[0] . '-' .  $line[1];
		$rateline['code'] = trim($line[1]);
		$batelco_rates[] = $rateline;
	}
}

//prepare field that is to be sorted - dialcode desc, then len desc
$field1 = array();
$field2 = array();

//sorting based on dialcode - desc
foreach($batelco_rates as $key=>$row){
	$field1[] = $row['code'];
	$field2[] = $row['len'];
}

array_multisort($field1,SORT_DESC,$field2,SORT_ASC,$batelco_rates);


/*
foreach($batelco_rates as $line){
	print_r($line);
}
*/


?>
