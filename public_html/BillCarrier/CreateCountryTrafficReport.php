<?php
        ini_set("memory_limit","512M");
	include_once('DBAccessor.php');	

//	$destination = array('91','94','92','880'); #India, Sri Lanka, Pakistan, Bangladesh
	$destination = array('91','94','92','880','1','66','63','86','977'); #India, Sri Lanka, Pakistan, Bangladesh, USA, Thailand, Philipines, China, Nepal

	$bucket_length = 4;

	$carrier_name = 'VSNL';
	$bill_type = 'ORIGINATION';
	$endpoint = array();

#	$filter_type = 'EXACT';
	$filter_type = 'LIKE';

#VSNL
	$endpoint[0] = array('VSNL',-1);

#KPN
/*
	$endpoint[0] = array('UK_AS5400A',11);
	$endpoint[1] = array('UK_AS5350B',11);
 	$endpoint[2] = array('UK_AS5350B',81);
     	$endpoint[3] = array('UK_AS5400A',81);
*/

#Old-Bharti-Unused
/*	
	$endpoint[0] = array('UK_AS5400A',0);	
	$endpoint[1] = array('UK_AS5350B',0);
	$endpoint[2] = array('UK_AS5400A',4);
	$endpoint[3] = array('UK_AS5350B',4);

*/
#Bharti
/*
	$endpoint[0] = array('UK_AS5400A',10);
	$endpoint[1] = array('UK_AS5350B',10);
	$endpoint[2] = array('UK_AS5400A',80);
	$endpoint[3] = array('UK_AS5350B',80);
	$endpoint[4] = array('BHARTI_VoIP',80);
	$endpoint[5] = array('BHARTI_VoIP',10);
*/
#BT
/*
	$endpoint[0] = array('UK_AS5400A',90);
        $endpoint[1] = array('UK_AS5350B',90);
	$endpoint[2] = array('UK_AS5400A',91);
        $endpoint[3] = array('UK_AS5350B',91);
*/

	$start_date = date('Y-m-d 00:00:00',strtotime('1-Apr-2010'));
	$end_date = date('Y-m-d 23:59:59',strtotime('31-May-2010'));

	$rated_filename = "Results/$carrier_name-$bill_type-RATED-1Apr10.csv";
	$bill_filename = "Results/$carrier_name-$bill_type-REPORT-31May10.csv";

	 //1. Fetch Rates for carrier
	 //2. Create Rate Buckets
	 //3. Fetch CDRs
	 //4. Rate CDRs and Create Bill Summary

	echo "Fetching Rates for $carrier_name-$bill_type\n";
	$R_Result = ExtractRates($carrier_name,$bill_type,$end_date);
	echo "Done\n";
	if(pg_num_rows($R_Result) <= 0){
		echo "\nNo Rates Found for $carrier_name $bill_type\n";
		exit(0);
	} else {

		$RATES = array();
		while($line=pg_fetch_array($R_Result, null, PGSQL_ASSOC)){
		$RATES[] = $line;
		}	

		echo "\nCreating Rate Buckets\n";
		
		$B_RATES = CreateRateBuckets($bucket_length,$RATES);

	}

//	print_r($B_RATES);
//	exit(0);

	#echo "\nFetching CDRs for $carrier_name-$bill_type\n";
	echo "\nFetching CDRs for Traffic Report-$bill_type\n";


	#$C_Result = ExtractCDRs($bill_type,$endpoint,$start_date,$end_date,$filter_type);


	$C_Result = ExtractReportCDRs($bill_type,$start_date,$end_date);	
	

	if(pg_num_rows($C_Result) <= 0){
		echo "\nNo CDRs Found for $carrier_name $bill_type\n";
		exit(0);
	} else {

		

		$BILL = array();
		//Output Files
		$f1= fopen($rated_filename,'w');
		$f2 = fopen($bill_filename, 'w');
		
		$target = (pg_num_rows($C_Result));
		$current = 0;

		echo "\nRating CDRs for Report-$bill_type($target)\n";

		//loop through CDRs
		 while($line=pg_fetch_array($C_Result, null, PGSQL_ASSOC)){
			 
			//increment cdr counter
			$current += 1;

			$cdr= RateCDR($line,$B_RATES,$bucket_length,$destination);
			
			
			
			//print progress
			echo "\r  Completed: ($current)". (int)($current*100/$target) . "%";

			if(!$cdr){
				continue;
			}
			
			//write rated cdr
			$str = "";
			foreach($cdr as $val){
				$str .= "$val,";
			}
			$str .= "\r\n";
			fwrite($f1,$str);
			



			//echo "{$cdr['rate']}    &&&    {$cdr['calling_code']}\n";
			//continue;
			
			//prepare bill summary			
			 if(array_key_exists($cdr['date'],$BILL) && array_key_exists("{$cdr['calling_code']}",$BILL[$cdr['date']])){
				 $BILL[$cdr['date']]["{$cdr['calling_code']}"]['calls'] += 1;
				 $BILL[$cdr['date']]["{$cdr['calling_code']}"]['charges'] += $cdr['charges'];
				 $BILL[$cdr['date']]["{$cdr['calling_code']}"]['minutes'] += $cdr['minutes'];
				
				/*	 				 
				 if($cdr['start_time'] > ($BILL[$cdr['date']]["{$cdr['calling_code']}"]['last_call_time'])){
				 	$BILL[$cdr['date']]["{$cdr['calling_code']}"]['last_call_time'] = $cdr['start_time'];
				 } 

				 if($cdr['start_time'] < ($BILL[$cdr['date']]["{$cdr['calling_code']}"]['start_call_time'])){
				 	$BILL[$cdr['date']]["{$cdr['calling_code']}"]['first_call_time'] = $cdr['start_time'];
				 }
				*/

			} else {
				$insert = array();
				$insert['date'] = $cdr['date']; 
				$insert['region_name'] = $cdr['region_name'];
				$insert['rate'] = $cdr['rate'];
				$insert['calls'] = 1;
				$insert['minutes'] = $cdr['minutes'];
                                $insert['charges'] = $cdr['charges'];
				#$insert['first_call_time'] = $cdr['start_time'];
				#$insert['last_call_time'] = $cdr['start_time'];
                                $BILL[$cdr['date']]["{$cdr['calling_code']}"]= $insert;	
			}
		 }


		echo "\nCreating Report  Summary\n";

		//sort report based on date - ASC order
		$BILL = SortBill($BILL,'date','ASC');
	
		
		//print_r($BILL);
		//exit(0);


		//Write Summarized Bill
		fwrite($f2,"DATE,DESTINATION,RATE,CALLS,MINUTES,CHARGES\r\n");
		foreach($BILL as $r){

			foreach($r as $c){
				$str = '';
				foreach($c as $val){
					$str .= "$val,";
				}
				$str .= "\r\n";
				fwrite($f2,$str);
			}
		}

		fclose($f1);
		fclose($f2);

		echo "\n************Done************\n";
	}

	function RateCDR($cdr,$B_RATES,$bucket_length,$destination){
			
			$number = trim($cdr['clean_number']);
		
			#Filter calls based on required destinations for the report
			$skip = true;
			foreach($destination as $dest){
				if(preg_match("/^{$dest}\d*/",$number)){
					$skip = false;
					break;
				}
			}
			if($skip){
				return false;	
			}


			$start_time = trim(date('Y-m-d H:i:s',strtotime($cdr['start_time'])));
			$cdr['minutes'] = $minutes = round(($cdr['call_duration_int']/60),2);	

			
			$found = false;

			//slice RATES array based on buckets
			$number_slice = substr($number,0,$bucket_length);

			if($number_slice != "" && array_key_exists($number_slice,$B_RATES)){
				$BUCKET = $B_RATES[$number_slice];
				
				//search in bucket 
				//look for longest matching dial code with valid effective date
				foreach($BUCKET as $r){
	                                $calling_code = trim($r['calling_code']);
	                                $rate = floatval($r['rate']);
	                                $eff_date = date('Y-m-d H:i:s',strtotime($r['eff_date']));
	                                $region_name = $r['region_name'];


				//	echo "Matching $calling_code with $number AND $eff_date with $start_time\n";
	                                //check code and effective date
	                                if((preg_match("/^{$calling_code}\d*$/",$number)) && $start_time >= $eff_date){

						//found longest match
						$cdr['rate']    = $rate;
						$cdr['charges'] = round($minutes * $rate,3);
						$cdr['region_name'] = $region_name;
						$cdr['calling_code'] = $calling_code;
						$cdr['date'] = date('Y-m-d',strtotime($start_time));
						//break since longest match found
						$found = true;
						break;
																					                                }																                    }
			} 

			//if matching dialcode is not found, mark as Unknown
			if(!$found){
					echo "\nDid not find matching dialcode for $number\n"; 
					$cdr['rate']	= 0;
					$cdr['charges'] = 0;
					$cdr['region_name'] = 'Unknown';	
			}
			
			return $cdr;	            
	}

	function CreateRateBuckets($bucket_length,$RATES){

		//output array
		$B_RATES = array();

		//loop through rates and create bucket
		foreach($RATES as $r){
			
/*
			if($r['calling_code'] == '91'){
				print_r($r);
			} else {
				continue;
			}
*/
			//extract bucket length number of digits from the dialcode
			$trimmed = substr($r['calling_code'],0,$bucket_length);
                        
			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){
                               
				$diff = $bucket_length - strlen($trimmed); 
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;

				
				for($i = $start; $i <= $end ; $i++){
					$B_RATES[$i][] = $r; 	
				}
			} else {
				$B_RATES[$trimmed][] = $r; 
			}		
		}	
		
		return $B_RATES;
	}

	function SortBill($BILL,$field_name,$order){
		
		//prepare field that is to be sorted
		$field = array();
		foreach($BILL as $code=>$row){
			foreach($row as $key=>$val){
				$field[$code] = $val[$field_name];
			}
		}

		//sort multi-dimensional array, based on field name in the desired order
		$result = ($order=='ASC')?array_multisort($field,SORT_ASC,$BILL):array_multisort($field,SORT_DESC,$BILL);

		if($result){
			return $BILL;
		} else {
			echo "\nCould not sort invoice\n";
			exit(0);
		}
	}




?>
