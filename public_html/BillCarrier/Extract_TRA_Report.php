<?

include_once('DB.php');
include_once('CSVParser.php');

echo "\nExtracting Countries and Zones..\n";
$parser = new CsvFileParser();

$COUNTRIES = $parser->ParseFromFile('CountryInfo.csv');

if(count($COUNTRIES) <= 0){
	echo "\nNo Countries found\n";
	exit(0);
} else {

	echo "\nCreating Country Buckets\n";
	$B_COUNTRIES = CreateCountryBuckets(4,$COUNTRIES);
}


//print_r($B_COUNTRIES);
//exit(0);



include_once('CORP_EP_inc.php');
#include_once('CPS_EP_inc.php');

$DB = startDB();

echo "\nExtracting CDRs for Mar 10..\n";


$sql = "SELECT ani,call_party_after_src_calling_plan,call_duration_int,call_source_regid,call_source_uport,call_dest_regid, call_dest_uport,call_error_int
	from new_cdr
	where start_time >= '1-Jan-2010 00:00:00' and start_time <= '31-Mar-2010 23:59:59'";
	
$result = pg_query($sql);

if(pg_num_rows($result) <= 0){
	echo "\nNo CDRs found\n";
	exit(0);
}


$total = pg_num_rows($result);
$current = 0;
$genius = fopen('Results/TopTraffic-Q12010-Output.csv','w');



/*
//CPS
$cps_f2f_count = 0;
$cps_f2m_count = 0;
$cps_f2g_count = 0;
$cps_f2z2_count = 0;
$cps_f2z3_count = 0;
$cps_f2z4_count = 0;
$cps_f2u_count = 0;
$cps_int = array(); 

//PPCC
$cc_f2f_count = 0;
$cc_f2m_count = 0;
$cc_f2g_count = 0;
$cc_f2z2_count = 0;
$cc_f2z3_count = 0;
$cc_f2z4_count = 0;
$cc_f2u_count = 0;
$cc_m2f_count = 0;
$cc_m2m_count = 0;
$cc_m2g_count = 0;
$cc_m2z2_count = 0;
$cc_m2z3_count = 0;
$cc_m2z4_count = 0;
$cc_m2u_count = 0;
$cc_int = array(); 

//Corp PBX
$corp_f2f_count = 0;
$corp_f2m_count = 0;
$corp_f2g_count = 0;
$corp_f2z2_count = 0;
$corp_f2z3_count = 0;
$corp_f2z4_count = 0;
$corp_f2u_count = 0;
$corp_int = array(); 
*/

//International calls - circuit & packet switched count
#$i_c_switch = 0;
#$i_p_switch = 0;


//National calls - circuit & packet switched count
#$n_c_switch = 0;
#$n_p_switch = 0;

$dest_int = array();

echo "\nMessing with CDRs...\n";

while($line = pg_fetch_array($result,null,PGSQL_ASSOC)){

	$current ++;
	echo "Progress ({$current}/$total) " . round((($current*100)/$total),2) . "% \r";
	
	$ani = (preg_match('/^973/',$line['ani']))?substr($line['ani'],3):$line['ani'];

	$bill_use_number = preg_replace('/^.*#/','',trim($line['call_party_after_src_calling_plan']));
	$call_duration_int = $line['call_duration_int'];
	$call_source_regid = strtoupper($line['call_source_regid']);
	$call_source_uport = $line['call_source_uport'];
	$call_dest_regid = strtoupper($line['call_dest_regid']);
	$call_dest_uport = $line['call_dest_uport'];
	$call_error_int = $line['call_error_int'];

	$source_ep = $call_source_regid . "/" . $call_source_uport;
	$dest_ep = $call_dest_regid . "/" . $call_dest_uport;
	$min =  (float)($call_duration_int/60);
	


	//skip error calls, invalid anis, invalid dialed numbers, wholesale calls to Bahrain
	if($call_error_int || ($ani == '') || ($bill_use_number == '') || (strlen($bill_use_number) < 4) || (preg_match('/^00973/',$bill_use_number)))
		continue;
	

	//Local Calls
	if( !(preg_match('/^00/',$bill_use_number))){
			
		#strip 973 if present
		if(preg_match('/^973/',$bill_use_number)){ 
			$bill_use_number = substr($bill_use_number,3);
		}

		#skip tollfree dialed numbers
		if((substr($bill_use_number,0,3) == '800')){
			continue;
		}

		 $country = 'BAHRAIN'; #$zone = 'B';
		
		 //national switch count
                if(preg_match('/^2CONNECT_AS/',$call_dest_regid) || preg_match('/^UK_AS/',$call_dest_regid) ){ $n_c_switch++; }
                else{  $n_p_switch++; }

	} else {
		
		

		 //isolate country and zone - First strip 00, then take first 4 digits
                $check = (int)substr(substr($bill_use_number,2),0,4);
                if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]['country']; $zone =  $B_COUNTRIES[$check]['zone'];  }
                else {  $country = 'Unknown'; #$zone = 'U'; }

		 //international switch count
                #if(preg_match('/^2CONNECT_AS/',$call_dest_regid) || preg_match('/^UK_AS/',$call_dest_regid) ){ $i_c_switch++; }
                #else{  $i_p_switch++; }
		#if($country == 'HONG KONG'){
		#	print_r($line);
		#}

	}

	if($country == 'BAHRAIN'){
		continue;
	}
	


		
	/*	

	//Calls from CPS - all originating from Fixed line and hopping into premium partition
	if($dest_ep == "HOP2_PREMIUM_PEERS/200") {
	
		

		//f 2 int zones
		switch($zone){
			case 'G': 
				//echo "Enter 1\n";
				$cps_f2g_count += $min;
				break;
			case 'Z2': 
				// echo "Enter 2\n";
				$cps_f2z2_count += $min;
				//echo $cps_f2z3_count;
				break;

			case 'Z3': 
				 //echo "Enter 3\n";
				$cps_f2z3_count += $min;
				//echo $cps_f2z3_count;
				break;
			case 'Z4': 
				 //echo "Enter 4\n";
				$cps_f2z4_count += $min;
				//$cps_f2z4_count;
				break;
			case 'U': 
				// echo "Enter 5\n";
				$cps_f2u_count += $min;
				break;
			case 'B': 
				break;
			default: echo "Entered into CPS switch\n";
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$cps_int)){
			$cps_int[$country] += $min ;
		} else {
			$cps_int[$country] = $min;
		}
		#if($country == 'HONG KONG') {
		#fwrite($numbers,"$start_time,$ani,$bill_use_number,$min,$country,$zone\r\n");
		#}
	}


	//Fixed Calls from PPCC - ports 80 and 82
	if(((substr($call_source_regid,0,4) == 'TALK') && (($call_source_uport ==  80) || ($call_source_uport == 82) )) && preg_match("/^1/",$ani)){
		
		#local calls
		if(!preg_match('/^00/',$bill_use_number)){
			//f2f
			if(preg_match('/^1\d{7,}$/',$bill_use_number)){
				$cc_f2f_count += $min;
				continue;
			}

			//f2m
			if(preg_match('/^3\d{7,}$/',$bill_use_number)){
				$cc_f2m_count += $min;
				continue;			
			} 

			#loop for all other local calls like 999,181 etc
			continue; 
		}


		

		//f 2 int zones
		switch($zone){
			case 'G': 
				$cc_f2g_count += $min;
				break;
			case 'Z2': 
				$cc_f2z2_count += $min;
				break;	

			case 'Z3': 
				$cc_f2z3_count += $min;
				break;
			case 'Z4': 
				$cc_f2z4_count += $min;
				break;	
			case 'U': 
				$cc_f2u_count += $min;
				break;
			case 'B': 
				break;	
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$cc_int)){
			$cc_int[$country] += $min ;
		} else {
			$cc_int[$country] = $min;
		}

	}


	//Mobile Calls from PPCC
	if(((substr($call_source_regid,0,4) == 'TALK') && (($call_source_uport ==  80) || ($call_source_uport == 82) ) ) && preg_match("/^3/",$ani)){

		
		 #local calls
                if(!preg_match('/^00/',$bill_use_number)){

			//m2f
			if(preg_match('/^1\d{7,}$/',$bill_use_number)){
				$cc_m2f_count += $min;
				continue;
			}

			//m2m
			if(preg_match('/^3\d{7,}$/',$bill_use_number)){
				$cc_m2m_count += $min;
				continue;			
			}
			
			#escape any other type of local calls
			continue;
			 
		}

		

		//m 2 int zones
		switch($zone){
			case 'G': 
				$cc_m2g_count += $min;
				break;
			case 'Z2': 
				$cc_m2z2_count += $min;
				break;	

			case 'Z3': 
				$cc_m2z3_count += $min;
				break;
			case 'Z4': 
				$cc_m2z4_count += $min;
				break;	
			case 'U': 
				$cc_m2u_count += $min;
				break;
			case 'B':
				break;	
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$cc_int)){
			$cc_int[$country] += $min ;
		} else {
			$cc_int[$country] = $min;
		}

	}


	//Calls from Corp
	if(in_array($source_ep,$arr_corp)){

		#local calls
                if(!preg_match('/^00/',$bill_use_number)){


			//f2f 
			if(preg_match('/^1\d{7,}$/',$bill_use_number)){
				$corp_f2f_count += $min;
				continue;
			}

			//f2m
			if(preg_match('/^3\d{7,}$/',$bill_use_number)){
				$corp_f2m_count += $min;
				continue;			
			}
			
			#escape any other type of local calls
                        continue;
 
		}
	
		//f 2 int zones
		switch($zone){
			case 'G': 
				$corp_f2g_count += $min;
				break;
			case 'Z2': 
				$corp_f2z2_count += $min;
				break;	

			case 'Z3': 
				$corp_f2z3_count += $min;
				break;
			case 'Z4': 
				$corp_f2z4_count += $min;
				break;	
			case 'U': 
				$corp_f2u_count += $min;
				break;	
			case 'B':
				break;
				  
		}
		

		//traffic volume for each country		
		if(array_key_exists($country,$corp_int)){
			$corp_int[$country] += $min ;
		} else {
			$corp_int[$country] = $min;
		}
	}
*/
	//traffic volume for each country
                if(array_key_exists($country,$dest_int)){
                        $dest_int[$country] += $min ;
                } else {
                        $dest_int[$country] = $min;
                }


}

/*
//sort and truncate  international traffic arrays
arsort($cps_int); #$cps_int = array_slice($cps_int,0,10,TRUE);
arsort($cc_int); #$cc_int = array_slice($cc_int,0,10,TRUE);
arsort($corp_int); #$corp_int = array_slice($corp_int,0,10,TRUE);
*/
arsort($dest_int);



echo "\nWriting Output...\n";

//Write Output
/*
fwrite($genius,"CPS\r\n");
fwrite($genius,"Fixed to Fixed,$cps_f2f_count\r\n");
fwrite($genius,"Fixed to Mobile,$cps_f2m_count\r\n");
fwrite($genius,"Fixed to GCC,$cps_f2g_count\r\n");
fwrite($genius,"Fixed to Zone2,$cps_f2z2_count\r\n");
fwrite($genius,"Fixed to Zone3,$cps_f2z3_count\r\n");
fwrite($genius,"Fixed to Zone4,$cps_f2z4_count\r\n");
fwrite($genius,"Fixed to Unknown,$cps_f2u_count\r\n");

fwrite($genius,"CPS International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($cps_int as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");

}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");


fwrite($genius,"PPCC\r\n");
fwrite($genius,"Fixed to Fixed,$cc_f2f_count\r\n");
fwrite($genius,"Fixed to Mobile,$cc_f2m_count\r\n");
fwrite($genius,"Fixed to GCC,$cc_f2g_count\r\n");
fwrite($genius,"Fixed to Zone2,$cc_f2z2_count\r\n");
fwrite($genius,"Fixed to Zone3,$cc_f2z3_count\r\n");
fwrite($genius,"Fixed to Zone4,$cc_f2z4_count\r\n");
fwrite($genius,"Fixed to Unknown,$cc_f2u_count\r\n");
fwrite($genius,"Mobile to Fixed ,$cc_m2f_count\r\n");
fwrite($genius,"Mobile to Mobile,$cc_m2m_count\r\n");
fwrite($genius,"Mobile to GCC,$cc_m2g_count\r\n");
fwrite($genius,"Mobile to Zone2,$cc_m2z2_count\r\n");
fwrite($genius,"Mobile to Zone3,$cc_m2z3_count\r\n");
fwrite($genius,"Mobile to Zone4,$cc_m2z4_count\r\n");
fwrite($genius,"Mobile to Unknown,$cc_m2u_count\r\n");


fwrite($genius,"PPCC International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($cc_int as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");

}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"CORP\r\n");
fwrite($genius,"Fixed to Fixed,$corp_f2f_count\r\n");
fwrite($genius,"Fixed to Mobile,$corp_f2m_count\r\n");
fwrite($genius,"Fixed to GCC,$corp_f2g_count\r\n");
fwrite($genius,"Fixed to Zone2,$corp_f2z2_count\r\n");
fwrite($genius,"Fixed to Zone3,$corp_f2z3_count\r\n");
fwrite($genius,"Fixed to Zone4,$corp_f2z4_count\r\n");
fwrite($genius,"Fixed to Unknown,$corp_f2u_count\r\n");

fwrite($genius,"CORP International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($corp_int as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");

}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"International Switching Info\r\n");
fwrite($genius,"Packet switch,$i_p_switch\r\n");
fwrite($genius,"Circuit switch,$i_c_switch\r\n");

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"National Switching Info\r\n");
fwrite($genius,"Packet switch,$n_p_switch\r\n");
fwrite($genius,"Circuit switch,$n_c_switch\r\n");

*/

fwrite($genius,"Top International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($dest_int as $key=>$val){
        fwrite($genius,"$key,$val,\r\n");

}



fclose($genius);
#fclose($numbers);
echo "\nDone\n";
exit(0);


function CreateCountryBuckets($bucket_length,$COUNTRIES){

		//output array
		$B_COUNTRIES = array();

		//loop through rates and create bucket
		foreach($COUNTRIES as $c){

			//extract bucket length number of digits from the dialcode
			$trimmed = substr($c[0],0,$bucket_length);

			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){

				$diff = $bucket_length - strlen($trimmed);
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;


				for($i = $start; $i <= $end ; $i++){
					$B_COUNTRIES[$i]['country'] = $c[1];
					$B_COUNTRIES[$i]['zone'] = $c[2];
				}
			} else {
				$B_COUNTRIES[$trimmed]['country']= $c[1];
				$B_COUNTRIES[$trimmed]['zone']= $c[2];
			}
		}

		return $B_COUNTRIES;
}


?>
