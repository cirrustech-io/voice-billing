<?php
        ini_set("memory_limit","6100M");
	include_once('DBAccessor.php');	


	$bucket_length = 4;

	$carrier_name = 'TATACOMMS';
	$bill_type = 'ORIGINATION';
#	$bill_type = 'TERMINATION';
	$endpoint = array();

#	$filter_type = 'EXACT';
	$filter_type = 'LIKE';

#VSNL
	$endpoint[0] = array('VSNL',-1);
	$endpoint[1] = array('TATACOMMS',-1);

#KPN
/*
	$endpoint[0] = array('UK_AS5400A',11);
	$endpoint[1] = array('UK_AS5350B',11);
 	$endpoint[2] = array('UK_AS5350B',81);
     	$endpoint[3] = array('UK_AS5400A',81);
*/

#Old-Bharti-Unused
/*	
	$endpoint[0] = array('UK_AS5400A',0);	
	$endpoint[1] = array('UK_AS5350B',0);
	$endpoint[2] = array('UK_AS5400A',4);
	$endpoint[3] = array('UK_AS5350B',4);

*/
#Bharti
/*	$endpoint[0] = array('UK_AS5400A',10);
	$endpoint[1] = array('UK_AS5350B',10);
	$endpoint[2] = array('UK_AS5400A',80);
	$endpoint[3] = array('UK_AS5350B',80);
	$endpoint[4] = array('BHARTI_VoIP',80);
	$endpoint[5] = array('BHARTI_VoIP',10);
*/

#BT

/*	$endpoint[0] = array('UK_AS5400A',90);
        $endpoint[1] = array('UK_AS5350B',90);
	$endpoint[2] = array('UK_AS5400A',91);
        $endpoint[3] = array('UK_AS5350B',91);
	$endpoint[4] = array('UK_AS5400A',31);
        $endpoint[5] = array('UK_AS5350B',31);
*/


	$start_date = date('Y-m-d 00:00:00',strtotime('1-Jul-2013'));
	$end_date = date('Y-m-d 23:59:59',strtotime('31-Aug-2013'));

	$rated_filename = "Results/$carrier_name-$bill_type-RATED-1to31JultoAug13.csv";
	$bill_filename = "Results/$carrier_name-$bill_type-BILL-1to31JultoAug13.csv";

	 //1. Fetch Rates for carrier
	 //2. Create Rate Buckets
	 //3. Fetch CDRs
	 //4. Rate CDRs and Create Bill Summary

	echo "Fetching Rates for $carrier_name-$bill_type\n";
	$R_Result = ExtractRates($carrier_name,$bill_type,$end_date);
	echo "Done\n";
	if(pg_num_rows($R_Result) <= 0){
		echo "\nNo Rates Found for $carrier_name $bill_type\n";
		exit(0);
	} else {

		$RATES = array();
		while($line=pg_fetch_array($R_Result, null, PGSQL_ASSOC)){
		$RATES[] = $line;
		}	

		echo "\nCreating Rate Buckets\n";
		
		$B_RATES = CreateRateBuckets($bucket_length,$RATES);

	}

//	print_r($B_RATES);
//	exit(0);

	echo "\nFetching CDRs for $carrier_name-$bill_type\n";
	$C_Result = ExtractCDRs($bill_type,$endpoint,$start_date,$end_date,$filter_type);	
	if(pg_num_rows($C_Result) <= 0){
		echo "\nNo CDRs Found for $carrier_name $bill_type\n";
		exit(0);
	} else {

		

		$BILL = array();
		//Output Files
		$f1= fopen($rated_filename,'w');
		$f2 = fopen($bill_filename, 'w');
		
		$target = (pg_num_rows($C_Result));
		$current = 0;

		echo "\nRating CDRs for $carrier_name-$bill_type($target)\n";

		//loop through CDRs
		 while($line=pg_fetch_array($C_Result, null, PGSQL_ASSOC)){
			 
			//increment cdr counter
			$current += 1;

			$cdr= RateCDR($line,$B_RATES,$bucket_length);
			
			//print progress
			echo "\r  Completed: ($current)". (int)($current*100/$target) . "%";


			//write rated cdr
			$str = "";
			foreach($cdr as $val){
				$str .= "$val,";
			}
			$str .= "\r\n";

			fwrite($f1,$str);



			//echo "{$cdr['rate']}    &&&    {$cdr['calling_code']}\n";
			//continue;
			
			//prepare bill summary			
			if(array_key_exists($cdr['calling_code'],$BILL) && array_key_exists("{$cdr['rate']}",$BILL[$cdr['calling_code']])){
				 $BILL[$cdr['calling_code']]["{$cdr['rate']}"]['calls'] += 1;
				 $BILL[$cdr['calling_code']]["{$cdr['rate']}"]['charges'] += $cdr['charges'];
				 $BILL[$cdr['calling_code']]["{$cdr['rate']}"]['minutes'] += $cdr['minutes'];
				 				 
				 if($cdr['start_time'] > ($BILL[$cdr['calling_code']]["{$cdr['rate']}"]['last_call_time'])){
				 	$BILL[$cdr['calling_code']]["{$cdr['rate']}"]['last_call_time'] = $cdr['start_time'];
				 } 

				 if($cdr['start_time'] < ($BILL[$cdr['calling_code']]["{$cdr['rate']}"]['start_call_time'])){
				 	$BILL[$cdr['calling_code']]["{$cdr['rate']}"]['first_call_time'] = $cdr['start_time'];
				 }

			} else {
				$insert = array();
				$insert['region_name'] = $cdr['region_name'];
				$insert['rate'] = $cdr['rate'];
				$insert['calls'] = 1;
				$insert['minutes'] = $cdr['minutes'];
                                $insert['charges'] = $cdr['charges'];
				$insert['first_call_time'] = $cdr['start_time'];
				$insert['last_call_time'] = $cdr['start_time'];
                                $BILL[$cdr['calling_code']]["{$cdr['rate']}"]= $insert;	
			}
		 }


		echo "\nCreating Bill Summary\n";

		//sort bill based on region name - ASC order
		$BILL = SortBill($BILL,'region_name','ASC');
	
		
		//print_r($BILL);
		//exit(0);


		//Write Summarized Bill
		fwrite($f2,"DESTINATION,RATE,CALLS,MINUTES,CHARGES,FIRST_CALL_TIME,LAST_CALL_TIME\r\n");
		foreach($BILL as $r){

			foreach($r as $c){
				$str = '';
				foreach($c as $val){
					$str .= "$val,";
				}
				$str .= "\r\n";
				fwrite($f2,$str);
			}
		}

		fclose($f1);
		fclose($f2);

		echo "\n************Done************\n";
	}

	function RateCDR($cdr,$B_RATES,$bucket_length){
			
			$number = trim($cdr['clean_number']);
			$start_time = trim(date('Y-m-d H:i:s',strtotime($cdr['start_time'])));
			$cdr['minutes'] = $minutes = round(($cdr['call_duration_int']/60),2);	

			
			$found = false;

			//slice RATES array based on buckets
			$number_slice = substr($number,0,$bucket_length);

			if($number_slice != "" && array_key_exists($number_slice,$B_RATES)){
				$BUCKET = $B_RATES[$number_slice];
				
				//search in bucket 
				//look for longest matching dial code with valid effective date
				foreach($BUCKET as $r){
	                                $calling_code = trim($r['calling_code']);
	                                $rate = floatval($r['rate']);
	                                $eff_date = date('Y-m-d H:i:s',strtotime($r['eff_date']));
	                                $region_name = $r['region_name'];


				//	echo "Matching $calling_code with $number AND $eff_date with $start_time\n";
	                                //check code and effective date
	                                if((preg_match("/^{$calling_code}\d*$/",$number)) && $start_time >= $eff_date){

						//found longest match
						$cdr['rate']    = $rate;
						$cdr['charges'] = round($minutes * $rate,3);
						$cdr['region_name'] = $region_name;
						$cdr['calling_code'] = $calling_code;

						//break since longest match found
						$found = true;
						break;
																					                                }																                    }
			} 

			//if matching dialcode is not found, mark as Unknown
			if(!$found){
					echo "\nDid not find matching dialcode for $number\n"; 
					$cdr['rate']	= 0;
					$cdr['charges'] = 0;
					$cdr['region_name'] = 'Unknown';	
			}
			
			return $cdr;	            
	}

	function CreateRateBuckets($bucket_length,$RATES){

		//output array
		$B_RATES = array();

		//loop through rates and create bucket
		foreach($RATES as $r){
			
/*
			if($r['calling_code'] == '91'){
				print_r($r);
			} else {
				continue;
			}
*/
			//extract bucket length number of digits from the dialcode
			$trimmed = substr($r['calling_code'],0,$bucket_length);
                        
			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){
                               
				$diff = $bucket_length - strlen($trimmed); 
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;

				
				for($i = $start; $i <= $end ; $i++){
					$B_RATES[$i][] = $r; 	
				}
			} else {
				$B_RATES[$trimmed][] = $r; 
			}		
		}	
	#	print_r($B_RATES);
	#	exit(0);		
		return $B_RATES;
	}

	function SortBill($BILL,$field_name,$order){
		
		//prepare field that is to be sorted
		$field = array();
		foreach($BILL as $code=>$row){
			foreach($row as $key=>$val){
				$field[$code] = $val[$field_name];
			}
		}

		//sort multi-dimensional array, based on field name in the desired order
		$result = ($order=='ASC')?array_multisort($field,SORT_ASC,$BILL):array_multisort($field,SORT_DESC,$BILL);

		if($result){
			return $BILL;
		} else {
			echo "\nCould not sort invoice\n";
			exit(0);
		}
	}




?>
