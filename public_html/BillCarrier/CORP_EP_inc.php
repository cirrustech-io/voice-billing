<?php

$arr_corp = array();

//RTT
$arr_corp[0] = '2CONNECT_AS5350B/10';
$arr_corp[1] = '2CONNECT_AS5350C/11';
$arr_corp[2] = 'REGUS_VOIP/10';

//RVIS
$arr_corp[3] = 'RVIS/10';
$arr_corp[4] = 'RVIS-FAX/10';

//TCH
$arr_corp[5] = 'TRADE_CARD_HOLDING/10';

//HDFC
$arr_corp[6] = 'HDFC_PBX/10';

//Manara Development
$arr_corp[7] = 'Manara_dev/10';
$arr_corp[8] = 'Manara_dev/11';

//Masara Advisory
$arr_corp[9] = '2CONNECT_AS5350B/15';
$arr_corp[10] = 'MASARA_ADVISORY/10';

//BFX PBX
$arr_corp[11] = '2CONNECT_AS5350C/12';
$arr_corp[12] = '2CONNECT_AS5850A/16';

//RCSI
$arr_corp[13] = '2CONNECT_AS5350B/11';

//360 Harbour + Fax
$arr_corp[14] = '2CONNECT_AS5350C/10';
$arr_corp[15] = '2CONNECT_AS5350B/12';
$arr_corp[16] = '360_FAX_2/11';
$arr_corp[17] = '360_FAX_2/12';
$arr_corp[18] = '360_FAX_2/13';
$arr_corp[19] = '360_FAX_2/14';
$arr_corp[20] = '360_FAX_2/15';
$arr_corp[21] = '360_FAX_2/16';

//Norton Rose
$arr_corp[22] = '2CONNECT_AS5350C/13';

//ACI
$arr_corp[23] ='2CONNECT_AS5350B/14';

//Ace Insurance PBX
$arr_corp[24] ='ACE_INS/10';
$arr_corp[25] ='ACE_LINKSYS/10';
$arr_corp[26] ='ACE_LINKSYS/0';

//@Bahrain
$arr_corp[27] ='AT_BAHRAIN/10';


//British Bahrain School
$arr_corp[28] ='BBS/10';


//CB Richard Ellis
$arr_corp[29] ='RICHARD_ELLIS/10';
$arr_corp[30] ='RICHARD_ELLIS/11';


//RVIS Fax
$arr_corp[31] ='RVIS-FAX/10';

//Ace Insurance Fax
$arr_corp[32] ='ACE_LINKSYS/10';

//Arcapita
$arr_corp[33]='ARCAPITA-PBX/10';
$arr_corp[34]='2CONNECT_AS5850A/11';

//BFC
$arr_corp[35]='2CONNECT_AS5850A/12';
$arr_corp[36]='2CONNECT_AS5850A/20';
$arr_corp[37]='2CONNECT_AS5850A/21';

//Naseej
$arr_corp[38]=' 2CONNECT_AS5850A/15';

//Gulf Ventures
$arr_corp[39]='Gulf-Ventures/10';

//TIBC
$arr_corp[40]='TIBC_SPLICE_PBX/10';





?>
