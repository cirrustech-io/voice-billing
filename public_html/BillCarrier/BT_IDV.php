<?php

include_once('DBAccessor.php');

$start_date = date('Y-m-d 00:00:00',strtotime('1-Jan-2015'));
$end_date = date('Y-m-d 23:59:59',strtotime('31-Jan-2015'));

$endpoint = array();
$endpoint[0][0] = 'UK_AS5400A'; $endpoint[0][1] = 60;
$endpoint[1][0] = 'UK_AS5350B'; $endpoint[1][1] = 60;

$origin = array('131','133','1344','135','136','160','1610','1616','1619','165','166','17','31','323','33','34','35','36','377','322','383','388','39','384','77','663','6996','666','6966','6967','6969','6996','6999','8008','320');

$bucket_length = 4;
$origin_bucket = CreateCodeBuckets($bucket_length,$origin);

$ep_filter = '';
foreach($endpoint as $ep){
	$ep_filter .= ($ep_filter != '')?" OR ":"";
	$ep_filter .= "(call_dest_regid = '{$ep[0]}' AND call_dest_uport = {$ep[1]})";
}

$sql = "SELECT start_time, ani, clean_number, call_duration_int
	FROM new_cdr
	WHERE start_time >= '{$start_date}' AND start_time <= '{$end_date}'
	AND ({$ep_filter}) 
	AND call_duration_int > 0;";

//echo($sql);
//exit(0);
$DB = startDB();

echo "\n*** Fetching BT IDV data between $start_date and $end_date ***\n";
$result = pg_query($sql);


if(pg_num_rows($result) == 0){
	echo "\n*** No calls found ***\n";
	exit(0);
}else{
	
	$f = fopen("Results/BT-IDV-" . date('Y-m-d-H-i-s') . ".csv",'w');
	echo "\n***" . pg_num_rows($result) . " calls found ***\n";

	echo "\n*** Processing ***\n";
	$TRAFFIC = array();
	$current = 0;
	$target = pg_num_rows($result);	
	while($line=pg_fetch_array($result, null, PGSQL_ASSOC)){
		
		 //increment cdr counter
                 $current += 1;

		$ani = $line['ani'];
		$call_duration = round(($line['call_duration_int'])/60,2);
		$dest = $line['clean_number'];
		
		$dest = substr(preg_replace('/^973/','',$dest),0,3);

		if($dest == ''){ $dest = 'NA'; }
		

		$ani_slice = substr($ani,0,$bucket_length);

		
		if(array_key_exists($ani_slice,$origin_bucket)){
			$code = $origin_bucket[$ani_slice];
		} else {
			$code = 'NA';
          		echo "\n*** Did not find matching origin for " . $ani . " ***\n";
		}
		
		

		if(array_key_exists($code,$TRAFFIC) && array_key_exists($dest,$TRAFFIC[$code])){
			$TRAFFIC[$code][$dest]['calls'] += 1;
			$TRAFFIC[$code][$dest]['duration'] += $call_duration; 
		} else {
			$TRAFFIC[$code][$dest]['calls'] = 1;
			$TRAFFIC[$code][$dest]['duration'] = $call_duration;
		}
		
		//print progress
                 echo "\r  Completed: ($current)". (int)($current*100/$target) . "%";

		

		
	}

	echo "\n*** Writing to file ***\n";
	
	fwrite($f,"ANI,DEST,CALLS,MINUTES\r\n");
	foreach($TRAFFIC as $c=>$l1){
		foreach($l1 as $d=>$l2){
			$str = "{$c},{$d},{$l2['calls']},{$l2['duration']}\r\n";
			fwrite($f,$str);
		}
	}

	fclose($f);
	
	echo "\n*** Done ***\n";
	exit(0);
}



function CreateCodeBuckets($bucket_length,$origin){
	
	//output array
	$B_CODES = array();
	
	foreach($origin as $o){
		
		$trimmed = substr($o,0,$bucket_length);		


		if(strlen($trimmed) < $bucket_length){
			$diff = $bucket_length - strlen($trimmed);
                        $start =  $trimmed * pow(10,$diff);
                        $end = $start + pow(10,$diff) - 1;

                        for($i = $start; $i <= $end ; $i++){
                        	$B_CODES[$i]= $o;
                        }
               	} else {
			$B_CODES[$trimmed]= $o;
                }
	}

	#print_r($B_CODES);
	
	return $B_CODES;
}
	
?>
