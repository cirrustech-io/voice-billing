<?

include_once('DB.php');

$DB = startDB();



echo "\nExtracting Countries..\n";



$sql = "SELECT country_code,country_name
	from country_temp";

$result = pg_query($sql);

$f = fopen('CountryInfo.csv','w');

if(pg_num_rows($result) <= 0){
	echo "\nNo Countries found\n";
	exit(0);
} else {

	while($line=pg_fetch_array($result, null, PGSQL_ASSOC)){
		foreach($line as $val){
			fwrite($f,"$val,");
		}
		fwrite($f,"\r\n");
	}
}
echo "\nDone\n"; 
fclose($f);
?>
