<?

include_once('DB.php');
include_once('CSVParser.php');

echo "\nExtracting Countries and Zones..\n";
$parser = new CsvFileParser();

$COUNTRIES = $parser->ParseFromFile('CountryInfo.csv');

if(count($COUNTRIES) <= 0){
	echo "\nNo Countries found\n";
	exit(0);
} else {

	echo "\nCreating Country Buckets\n";
	$B_COUNTRIES = CreateCountryBuckets(4,$COUNTRIES);
}


#print_r($B_COUNTRIES[8802]);

#exit(0);


/*echo "\n";
echo $B_COUNTRIES['1234']['country'];
exit(0);
echo "\n";
echo $B_COUNTRIES[$check]['zone'];
exit(0);
*/
echo "\nCorporate EP info..\n";

include_once('CORP_EP_inc.php');


#print_r($arr_corp);
#exit(0);



$DB = startDB();

echo "\nExtracting CDRs from 1 Apr11 to 30 Jun11..\n";
#echo "\nExtracting CDRs for 1 Oct08..\n";

$sql = "SELECT ani,call_party_after_src_calling_plan,clean_number,call_duration_int,call_source_regid,call_source_uport,call_dest_regid, call_dest_uport,call_error_int
	from new_cdr
	where start_time > '1-Apr-2011 00:00:00' and start_time <= '30-Jun-2011 23:59:59'";
	//where start_time > '1-Oct-2008 00:00:00' and start_time <= '31-Dec-2008 23:59:59'";
$result = pg_query($sql);

if(pg_num_rows($result) <= 0){
	echo "\nNo CDRs found\n";
	exit(0);
}


$total = pg_num_rows($result);
$current = 0;
$genius = fopen('Results/Q2-2011-Output.csv','w');


//CPS
$cps_f2f_count = 0;
$cps_f2m_count = 0;
$cps_f2g_count = 0;
$cps_f2z2_count = 0;
$cps_f2z3_count = 0;
$cps_f2z4_count = 0;
$cps_f2u_count = 0;
$cps_int = array(); 

//PPCC
$cc_f2f_count = 0;
$cc_f2m_count = 0;
$cc_f2g_count = 0;
$cc_f2z2_count = 0;
$cc_f2z3_count = 0;
$cc_f2z4_count = 0;
$cc_f2u_count = 0;
$cc_m2f_count = 0;
$cc_m2m_count = 0;
$cc_m2g_count = 0;
$cc_m2z2_count = 0;
$cc_m2z3_count = 0;
$cc_m2z4_count = 0;
$cc_m2u_count = 0;
$cc_int = array(); 

//Corp PBX
$corp_f2f_count = 0;
$corp_f2m_count = 0;
$corp_f2g_count = 0;
$corp_f2z2_count = 0;
$corp_f2z3_count = 0;
$corp_f2z4_count = 0;
$corp_f2u_count = 0;
$corp_int = array(); 


//circuit & packet switched count
$c_switch = 0;
$p_switch = 0;


echo "\nMessing with CDRs...\n";

while($line = pg_fetch_array($result,null,PGSQL_ASSOC)){

	$current ++;
	echo "Progress ({$current}/$total) " . round((($current*100)/$total),2) . "% \r";

	$ani = $line['ani'];
	$clean_number = $line['clean_number'];
	$bill_use_number = preg_replace('/^.*#/','',trim($line['call_party_after_src_calling_plan']));
	$call_duration_int = $line['call_duration_int'];
	$call_source_regid = strtoupper($line['call_source_regid']);
	$call_source_uport = $line['call_source_uport'];
	$call_dest_regid = strtoupper($line['call_dest_regid']);
	$call_dest_uport = $line['call_dest_uport'];
	$call_error_int = $line['call_error_int'];

	$source_ep = $call_source_regid . "/" . $call_source_uport;
	$dest_ep = $call_dest_regid . "/" . $call_dest_uport;
	$min =  (float)($call_duration_int/60);

	
	//skip error calls, invalid anis, invalid dialed numbers, tollfree dialed numbers
	if($call_error_int || ($ani == '') || ($bill_use_number == '') || (strlen($bill_use_number) < 4) || (substr($bill_use_number,0,3) == '800'))
		continue;



#	echo "ANI={$ani}--CLEAN={$clean_number}--BILL={$bill_use_number}--SRC={$source_ep}--DST={$dest_ep}\n";	

	//isolate country and zone
        $check = (int)substr($clean_number,0,4);
               

	
	//Calls from CPS
	if((($source_ep == "PREMIUM_PEERS/11")) && (preg_match("/^973/",$ani) || preg_match("/^17/",$ani))){
	
	 
		//international switch count
		if(preg_match('/^2CONNECT_AS/',$call_dest_regid) || preg_match('/^UK_AS/',$call_dest_regid) ){ $c_switch++; }
		else{  $p_switch++; }

		

		if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]['country']; $zone =  $B_COUNTRIES[$check]['zone'];  }
		else {	$country = 'Unknown'; $zone = 'U'; }


		//f 2 int zones
		switch($zone){
			case 'G': 
				//echo "Enter 1\n";
				$cps_f2g_count += $min;
				break;
			case 'Z2': 
				// echo "Enter 2\n";
				$cps_f2z2_count += $min;
				//echo $cps_f2z3_count;
				break;

			case 'Z3': 
				 //echo "Enter 3\n";
				$cps_f2z3_count += $min;
				//echo $cps_f2z3_count;
				break;
			case 'Z4': 
				 //echo "Enter 4\n";
				$cps_f2z4_count += $min;
				//$cps_f2z4_count;
				break;
			case 'U': 
				// echo "Enter 5\n";
				$cps_f2u_count += $min;
				break;
			default: echo "Entered into CPS switch\n";
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$cps_int)){
			$cps_int[$country] += $min ;
		} else {
			$cps_int[$country] = $min;
		}

	}


	//Fixed Calls from PPCC - ports 80 and 82
	if(((substr($call_source_regid,0,4) == 'TALK') && (($call_source_uport ==  80) || ($call_source_uport == 82) )) && (preg_match("/^9731/",$ani) || preg_match("/^1/",$ani))){

		
	#	echo "Entered CC\n";
		//f2f
		if(preg_match('/^9731/',$clean_number) || preg_match('/^1\d{7}$/',$clean_number)){
			$cc_f2f_count += $min;
			continue;
		}

		//f2m
		if( preg_match('/^9733/',$clean_number) || preg_match('/^3\d{7}$/',$clean_number)){
			$cc_f2m_count += $min;
			continue;			
		} 


		//international switch count
		if(preg_match('/^2CONNECT_AS/',$call_dest_regid) || preg_match('/^UK_AS/',$call_dest_regid) ){ $c_switch++; }
		else{  $p_switch++; }

		//isolate country and zone
		if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]['country']; $zone =  $B_COUNTRIES[$check]['zone'];  }
		else {	$country = 'Unknown'; $zone = 'U'; }

//		echo "$check--$country--$zone\n";


		//f 2 int zones
		switch($zone){
			case 'G': 
				$cc_f2g_count += $min;
				break;
			case 'Z2': 
				$cc_f2z2_count += $min;
				break;	

			case 'Z3': 
				$cc_f2z3_count += $min;
				break;
			case 'Z4': 
				$cc_f2z4_count += $min;
				break;	
			case 'U': 
				$cc_f2u_count += $min;
				break;	
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$cc_int)){
			$cc_int[$country] += $min ;
		} else {
			$cc_int[$country] = $min;
		}

	}


	//Mobile Calls from PPCC - avoiding Fastelco and Globcom wholesale
	if(((substr($call_source_regid,0,4) == 'TALK') && (($call_source_uport ==  80) || ($call_source_uport == 82) )) && (preg_match("/^9733/",$ani) || preg_match("/^3/",$ani))){

//	if((substr($call_source_regid,0,4) == 'TALK'))
//		echo "Entered CC Mobile\n";
	
		//m2f
		if(preg_match('/^9731/',$clean_number) || preg_match('/^1\d{7}$/',$clean_number)){
			$cc_m2f_count += $min;
			continue;
		}

		//m2m
		if( preg_match('/^9733/',$clean_number) || preg_match('/^3\d{7}$/',$clean_number)){
			$cc_m2m_count += $min;
			continue;			
		} 


		//international switch count
		if(preg_match('/^2CONNECT_AS/',$call_dest_regid) || preg_match('/^UK_AS/',$call_dest_regid) ){ $c_switch++; }
		else{  $p_switch++; }


		//echo $B_COUNTRIES["$check"];
		//echo "\n";
		
		//isolate country and zone
		if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]['country']; $zone =  $B_COUNTRIES[$check]['zone'];  }
		else {	$country = 'Unknown'; $zone = 'U'; }


//		echo "$check--$country--$zone\n";

		//m 2 int zones
		switch($zone){
			case 'G': 
				$cc_m2g_count += $min;
				break;
			case 'Z2': 
				$cc_m2z2_count += $min;
				break;	

			case 'Z3': 
				$cc_m2z3_count += $min;
				break;
			case 'Z4': 
				$cc_m2z4_count += $min;
				break;	
			case 'U': 
				$cc_m2u_count += $min;
				break;	
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$cc_int)){
			$cc_int[$country] += $min ;
		} else {
			$cc_int[$country] = $min;
		}

	}


	//Calls from Corp
	if(in_array($source_ep,$arr_corp)){

		#echo "\n Entered CORP\n";
		
 		//f2f
		if((preg_match('/^9731/',$clean_number)) || (preg_match('/^1\d{7}$/',$clean_number))){
			$corp_f2f_count += $min;
			continue;
		}

		//f2m
		if((preg_match('/^9733/',$clean_number)) || (preg_match('/^3\d{7}$/',$clean_number))){
			$corp_f2m_count += $min;
			continue;			
		} 

		//international switch count
		if(preg_match('/^2CONNECT_AS/',$call_dest_regid) || preg_match('/^UK_AS/',$call_dest_regid) ){ $c_switch++; }
		else{  $p_switch++; }

		//isolate country and zone
		if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]['country']; $zone =  $B_COUNTRIES[$check]['zone'];  }
		else {	$country = 'Unknown';	$zone = 'U'; }

	#	echo "$check--$country--$zone\n";

		//f 2 int zones
		switch($zone){
			case 'G': 
				$corp_f2g_count += $min;
				break;
			case 'Z2': 
				$corp_f2z2_count += $min;
				break;	

			case 'Z3': 
				$corp_f2z3_count += $min;
				break;
			case 'Z4': 
				$corp_f2z4_count += $min;
				break;	
			case 'U': 
				$corp_f2u_count += $min;
				break;	
				  
		}

		//traffic volume for each country		
		if(array_key_exists($country,$corp_int)){
			$corp_int[$country] += $min ;
		} else {
			$corp_int[$country] = $min;
		}
	}

}


//sort and truncate  international traffic arrays
arsort($cps_int); $cps_int = array_slice($cps_int,0,10,TRUE);
arsort($cc_int); $cc_int = array_slice($cc_int,0,10,TRUE);
arsort($corp_int); $corp_int = array_slice($corp_int,0,10,TRUE);

echo "\nWriting Output...\n";

//Write Output

fwrite($genius,"CPS\r\n");
fwrite($genius,"Fixed to Fixed =$cps_f2f_count\r\n");
fwrite($genius,"Fixed to Mobile=$cps_f2m_count\r\n");
fwrite($genius,"Fixed to GCC=$cps_f2g_count\r\n");
fwrite($genius,"Fixed to Zone2=$cps_f2z2_count\r\n");
fwrite($genius,"Fixed to Zone3=$cps_f2z3_count\r\n");
fwrite($genius,"Fixed to Zone4=$cps_f2z4_count\r\n");
fwrite($genius,"Fixed to Unknown=$cps_f2u_count\r\n");

fwrite($genius,"CPS International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($cps_int as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");

}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");


fwrite($genius,"PPCC\r\n");
fwrite($genius,"Fixed to Fixed =$cc_f2f_count\r\n");
fwrite($genius,"Fixed to Mobile=$cc_f2m_count\r\n");
fwrite($genius,"Fixed to GCC=$cc_f2g_count\r\n");
fwrite($genius,"Fixed to Zone2=$cc_f2z2_count\r\n");
fwrite($genius,"Fixed to Zone3=$cc_f2z3_count\r\n");
fwrite($genius,"Fixed to Zone4=$cc_f2z4_count\r\n");
fwrite($genius,"Fixed to Unknown=$cc_f2u_count\r\n");
fwrite($genius,"Mobile to Fixed =$cc_m2f_count\r\n");
fwrite($genius,"Mobile to Mobile=$cc_m2m_count\r\n");
fwrite($genius,"Mobile to GCC=$cc_m2g_count\r\n");
fwrite($genius,"Mobile to Zone2=$cc_m2z2_count\r\n");
fwrite($genius,"Mobile to Zone3=$cc_m2z3_count\r\n");
fwrite($genius,"Mobile to Zone4=$cc_m2z4_count\r\n");
fwrite($genius,"Mobile to Unknown=$cc_m2u_count\r\n");


fwrite($genius,"PPCC International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($cc_int as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");

}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"CORP\r\n");
fwrite($genius,"Fixed to Fixed =$corp_f2f_count\r\n");
fwrite($genius,"Fixed to Mobile=$corp_f2m_count\r\n");
fwrite($genius,"Fixed to GCC=$corp_f2g_count\r\n");
fwrite($genius,"Fixed to Zone2=$corp_f2z2_count\r\n");
fwrite($genius,"Fixed to Zone3=$corp_f2z3_count\r\n");
fwrite($genius,"Fixed to Zone4=$corp_f2z4_count\r\n");
fwrite($genius,"Fixed to Unknown=$corp_f2u_count\r\n");

fwrite($genius,"CORP International Destinations\r\n");
fwrite($genius,"Country,Minutes,\r\n");
foreach($corp_int as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");

}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"Switching Info\r\n");
fwrite($genius,"Packet switch=$p_switch\r\n");
fwrite($genius,"Circuit switch=$c_switch\r\n");



fclose($genius);
echo "\nDone\n";

function CreateCountryBuckets($bucket_length,$COUNTRIES){

		//output array
		$B_COUNTRIES = array();

		//loop through rates and create bucket
		foreach($COUNTRIES as $c){

			//extract bucket length number of digits from the dialcode
			$trimmed = substr($c[0],0,$bucket_length);

			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){

				$diff = $bucket_length - strlen($trimmed);
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;


				for($i = $start; $i <= $end ; $i++){
					$B_COUNTRIES[$i]['country'] = $c[1];
					$B_COUNTRIES[$i]['zone'] = $c[2];
				}
			} else {
				$B_COUNTRIES[$trimmed]['country']= $c[1];
				$B_COUNTRIES[$trimmed]['zone']= $c[2];
			}
		}

		return $B_COUNTRIES;
}










?>
