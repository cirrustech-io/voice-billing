<?

include_once('DB.php');

$DB = startDB();



echo "\nExtracting Countries..\n";



$sql = "SELECT country_code,country_name
	from country_temp
	order by country_code ASC";

$result = pg_query($sql);


if(pg_num_rows($result) <= 0){
	echo "\nNo Countries found\n";
	exit(0);
} else {

	$COUNTRIES = array();
	while($line=pg_fetch_array($result, null, PGSQL_ASSOC)){
		$COUNTRIES[] = $line;
	}

	echo "\nCreating Country Buckets\n";
	$B_COUNTRIES = CreateCountryBuckets(5,$COUNTRIES);

}

//print_r($B_COUNTRIES);
//exit(0);

echo "\nExtracting CDRs..\n";

$sql = "SELECT ani,clean_number,call_duration_int,call_error_int,isdn_cause_code
	from new_cdr
	where start_time > '1-Oct-2008 00:00:00' and start_time <= '1-Oct-2008 23:59:59'";
	//where start_time > '1-Oct-2008 00:00:00' and start_time <= '31-Dec-2008 23:59:59'";

$result = pg_query($sql);

if(pg_num_rows($result) <= 0){
	echo "\nNo CDRs found\n";
	exit(0);
}

$genius = fopen('Output-E1.csv','w');


//bahrain mobile
$mobile_count = 0;
$success_mobile_count = 0;
$mobile_isdn = array(); $mobile_isdn['empty'] = 0;
$mobile_errorint = array();

//bahrain fixed
$fixed_count = 0;
$success_fixed_count = 0;
$fixed_isdn = array(); $fixed_isdn['empty'] = 0;
$fixed_errorint = array();


//bahrain emergency
$eme_count = 0;
$success_eme_count = 0;
$eme_isdn = array(); $eme_isdn['empty'] = 0;
$eme_errorint = array();

//bahrain directory assist
$dir_count = 0;
$success_dir_count = 0;
$dir_isdn = array(); $dir_isdn['empty'] = 0;
$dir_errorint = array();


//intl calls
$intl_count = 0;
$success_intl_count = 0;
$intl_isdn = array(); $intl_isdn['empty'] = 0;
$intl_errorint = array();
$asr = array();


echo "\nMessing with CDRs...\n";

while($line = pg_fetch_array($result,null,PGSQL_ASSOC)){


	$ani = $line['ani'];
	$clean_number = $line['clean_number'];
	$call_duration_int = $line['call_duration_int'];
	$call_duration = $line['call_duration'];
	$call_error_int = $line['call_error_int'];
	$isdn_cause_code = $line['isdn_cause_code'];


	//Calls going to Bahrain Mobile
	if(preg_match("/^3\d{7}$/",$clean_number) || preg_match("/^9733\d{7}$/",$clean_number)){

		//total
		$mobile_count++;

		//success
		if($call_duration_int > 0){ $success_mobile_count++; }

		//summarize - isdn_cause_code
		if($isdn_cause_code == '') { $mobile_isdn['empty'] += 1; }
		elseif(array_key_exists($isdn_cause_code,$mobile_isdn)){ $mobile_isdn[$isdn_cause_code] += 1; }
		else{ $mobile_isdn[$isdn_cause_code] = 1; }


		//summarize call error int
		if(array_key_exists($call_error_int,$mobile_errorint)){ $mobile_errorint[$call_error_int] += 1; }
		else{ $mobile_errorint[$call_error_int] = 1; }

	 }

	 //Calls going to Bahrain Fixed
	  if(preg_match("/^1\d{7}$/",$clean_number) || preg_match("/^9731\d{7}$/",$clean_number)){

		//total
		$fixed_count++;

		//success
		if($call_duration_int > 0){ $success_fixed_count++; }

		//summarize - isdn_cause_code
		if($isdn_cause_code == '') { $fixed_isdn['empty'] += 1; }
		elseif(array_key_exists($isdn_cause_code,$fixed_isdn)){ $fixed_isdn[$isdn_cause_code] += 1; }
		else{ $fixed_isdn[$isdn_cause_code] = 1; }
	   																			                 //summarize call error int
		if(array_key_exists($call_error_int,$fixed_errorint)){ $fixed_errorint[$call_error_int] += 1; }							              else{ $fixed_errorint[$call_error_int] = 1; }
																					}

	//Calls going to Bahrain Emergency
	if(preg_match("/^199$/",$clean_number) || preg_match("/^999$/",$clean_number)){

		//total
		$eme_count++;

		//success
		if($call_duration_int > 0){ $success_eme_count++; }

		//summarize - isdn_cause_code
		if($isdn_cause_code == '') { $eme_isdn['empty'] += 1; }
		elseif(array_key_exists($isdn_cause_code,$eme_isdn)){ $eme_isdn[$isdn_cause_code] += 1; }
		else{ $eme_isdn[$isdn_cause_code] = 1; }

																			                     //summarize call error int
		if(array_key_exists($call_error_int,$eme_errorint)){ $eme_errorint[$call_error_int] += 1; }
		else{ $eme_errorint[$call_error_int] = 1; }

	}

	//Calls going to Directory Assist
	if(preg_match("/^188$/",$clean_number) || preg_match("/^181$/",$clean_number)){

			//total
			$dir_count++;

			//success
			if($call_duration_int > 0){ $success_dir_count++; }

			//summarize - isdn_cause_code
			if($isdn_cause_code == '') { $dir_isdn['empty'] += 1; }
			elseif(array_key_exists($isdn_cause_code,$dir_isdn)){ $dir_isdn[$isdn_cause_code] += 1; }
			else{ $dir_isdn[$isdn_cause_code] = 1; }

			//summarize call error int
			if(array_key_exists($call_error_int,$dir_errorint)){ $dir_errorint[$call_error_int] += 1; }
			else{ $dir_errorint[$call_error_int] = 1; }

	}



	//International Calls
	if(!(preg_match("/^973/",$clean_number)) && (preg_match("/^\d{9,}$/",$clean_number))){

			//total
			$intl_count++;

			//success
			if($call_duration_int > 0){
				$success_intl_count++;
			}

			//summarize - isdn_cause_code
			if($isdn_cause_code == '') { $intl_isdn['empty'] += 1; }
			elseif(array_key_exists($isdn_cause_code,$intl_isdn)){ $intl_isdn[$isdn_cause_code] += 1; }
			else{ $intl_isdn[$isdn_cause_code] = 1; }

			//summarize call error int
			if(array_key_exists($call_error_int,$intl_errorint)){ $intl_errorint[$call_error_int] += 1; }
			else{ $intl_errorint[$call_error_int] = 1; }



			//ASR Calculation

			//isolate country
			$check = substr($clean_number,0,5);
			if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]; }
			else {$country = 'Unknown';}

			if(array_key_exists($country,$asr)){
				$asr[$country]['total'] += 1;
				if($call_duration_int > 0){ $asr[$country]['ans'] += 1;}
			} else {
				$asr[$country]['total'] = 1;
				if($call_duration_int > 0){ $asr[$country]['ans'] = 1;}
				else { $asr[$country]['ans'] = 0;}
			}
	}


}


echo "\nWriting Output...\n";

//Write Output

fwrite($genius,"Mobile Calls\r\n");
fwrite($genius,"Total Calls=$mobile_count\r\n");
fwrite($genius,"Success Calls=$success_mobile_count\r\n");
fwrite($genius,"ISDN Cause Code Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($mobile_isdn as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}
fwrite($genius,"Error Int Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($mobile_errorint as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"Fixed Calls\r\n");
fwrite($genius,"Total Calls=$fixed_count\r\n");
fwrite($genius,"Success Calls=$success_fixed_count\r\n");
fwrite($genius,"ISDN Cause Code Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($fixed_isdn as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}
fwrite($genius,"Error Int Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($fixed_errorint as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");


fwrite($genius,"Emergency Calls\r\n");
fwrite($genius,"Total Calls=$eme_count\r\n");
fwrite($genius,"Success Calls=$success_eme_count\r\n");
fwrite($genius,"ISDN Cause Code Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($eme_isdn as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}
fwrite($genius,"Error Int Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($eme_errorint as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"Directory Assist Calls\r\n");
fwrite($genius,"Total Calls=$dir_count\r\n");
fwrite($genius,"Success Calls=$success_dir_count\r\n");
fwrite($genius,"ISDN Cause Code Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($dir_isdn as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}
fwrite($genius,"Error Int Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($dir_errorint as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");


fwrite($genius,"International Calls\r\n");
fwrite($genius,"Total Calls=$intl_count\r\n");
fwrite($genius,"Success Calls=$success_intl_count\r\n");
fwrite($genius,"Code,Call Count,\r\n");
fwrite($genius,"ISDN Cause Code Info\r\n");
foreach($intl_isdn as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}
fwrite($genius,"Error Int Info\r\n");
fwrite($genius,"Code,Call Count,\r\n");
foreach($intl_errorint as $key=>$val){
	fwrite($genius,"$key,$val,\r\n");
}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");

fwrite($genius,"ASR Info International Calls\r\n");
fwrite($genius,"Country,Total,Answered,\r\n");
foreach($asr as $country=>$val){
	$tot = $val['total'];
	$ans = $val['ans'];
	fwrite($genius,"$country,$tot,$ans\r\n");
}

fwrite($genius,"\r\n");
fwrite($genius,"\r\n");


fclose($genius);

echo "\nDone\n";




function CreateCountryBuckets($bucket_length,$COUNTRIES){

		//output array
		$B_COUNTRIES = array();

		//loop through countries and create bucket
		foreach($COUNTRIES as $c){

			//extract bucket length number of digits from the dialcode
			$trimmed = substr($c['country_code'],0,$bucket_length);

			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){

				$diff = $bucket_length - strlen($trimmed);
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;


				for($i = $start; $i <= $end ; $i++){
					$B_COUNTRIES[$i] = $c['country_name'];
				}
			} else {
				$B_COUNTRIES[$trimmed]= $c['country_name'];
			}
		}

		return $B_COUNTRIES;
}










?>
