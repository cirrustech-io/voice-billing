<?php

	include_once('DBAccessor.php');	


	$bucket_length = 3;

	$carrier_name = 'BATELCO';
	$bill_type = 'ORIGINATION';
	$endpoint = array();
	//$ip= array();
	$ignore = array();

	$filter_type = 'EXACT';
//	$filter_type = 'LIKE';

/*	
	//endpoints to ignore 
	include_once 'CORP_EP_inc.php';
	foreach($arr_corp as $ep){
		//echo "Trying with $ep\n";
		if(preg_match("/^2CONNECT_AS/",strtoupper($ep))){
			//echo "entered loop\n\n";
			$regid = substr($ep,0,strlen($ep)- strlen(substr($ep,strpos($ep,"/"))));
			$port = substr($ep,strpos($ep,"/") + 1);
			$ignore[] = array($regid,$port);
		}
	}

*/
//	print_r($ignore);
//	exit(0);

#	$endpoint[0] = array('2Connect_AS',-1);	
#	$endpoint[1] = array('2CONNECT_AS',-1);	

	$endpoint[0] = array('2CONNECT_AS5400A',7);
	$endpoint[1] = array('2CONNECT_AS5350B',7);
	$endpoint[2] = array('2CONNECT_AS5850A',7);

	$endpoint[3] = array('2CONNECT_AS5400A',10);
	$endpoint[4] = array('2CONNECT_AS5400A',12);
#	$endpoint[4] = array('2CONNECT_AS5400A',16);
	
	$endpoint[5] = array('2CONNECT_AS5350B',50);
	$endpoint[6] = array('2CONNECT_AS5400A',51);
	$endpoint[7] = array('2CONNECT_AS5400A',52);
	$endpoint[8] = array('2CONNECT_AS5350B',52);
	$endpoint[9] = array('2CONNECT_AS5400A',80);
	$endpoint[10] = array('2CONNECT_AS5350B',80);
        $endpoint[11] = array('2CONNECT_AS5850A',80);

	$endpoint[12] = array('2CONNECT_AS5850A',10);
	
	$endpoint[13] = array('test_enum_srv',10);



/*
	$ip[0] = "80.88.247.135";
	$ip[1] = "80.88.247.115";
	$ip[2] = "80.88.247.151";
*/

	$start_date = date('Y-m-d 00:00:00',strtotime('1-Apr-2013'));
	$end_date = date('Y-m-d 23:59:59',strtotime('30-Apr-2013'));

	$rated_filename = "Results/$carrier_name-$bill_type-RATED-1to30Apr13.csv";
	$bill_filename = "Results/$carrier_name-$bill_type-BILL-1to30Apr13.csv";
 
	 //1. Fetch Rates for carrier
	 //2. Create Rate Buckets
	 //3. Fetch CDRs
	 //4. Rate CDRs and Create Bill Summary

	echo "Fetching Rates for $carrier_name-$bill_type\n";
	include_once('batelco-rates-inc.php');

	if(count($batelco_rates)==0){
		echo "\nNo Rates Found for $carrier_name $bill_type\n";
		exit(0);
	} else{

		echo "\nCreating Rate Buckets\n";
		
		$B_RATES = CreateRateBuckets($bucket_length,$batelco_rates);

	}
	
	#print_r($B_RATES);
	#exit(0);
	
	echo "\nFetching CDRs for $carrier_name-$bill_type\n";
	#$C_Result = ExtractBatelcoCDRs($bill_type,$endpoint,$start_date,$end_date,$filter_type,$ignore);	
	//$C_Result = ExtractBatelcoCDRs($bill_type,$ip,$start_date,$end_date,$filter_type);
	$C_Result = ExtractCDRs($bill_type,$endpoint,$start_date,$end_date,$filter_type);
	if(pg_num_rows($C_Result) <= 0){
		echo "\nNo CDRs Found for $carrier_name $bill_type\n";
		exit(0);
	} else {

		

		$BILL = array();
		//Output Files
		$f1= fopen($rated_filename,'w');
		$f2 = fopen($bill_filename, 'w');
		
		$target = (pg_num_rows($C_Result));
		$current = 0;

		echo "\nRating CDRs for $carrier_name-$bill_type($target)\n";

		//loop through CDRs
		 while($line=pg_fetch_array($C_Result, null, PGSQL_ASSOC)){
			 
			//increment cdr counter
			$current += 1;

			$cdr= RateCDR($line,$B_RATES,$bucket_length);
			
			//print progress
			echo "\r  Completed: ($current)". (int)($current*100/$target) . "%";


			//write rated cdr
			$str = "";
			foreach($cdr as $val){
				$str .= "$val,";
			}
			$str .= "\r\n";

			fwrite($f1,$str);


			//prepare bill summary			
			if(array_key_exists($cdr['calling_code'],$BILL) && array_search($cdr['rate'],$BILL[$cdr['calling_code']])){
				 $BILL[$cdr['calling_code']]['calls'] += 1;
				 $BILL[$cdr['calling_code']]['charges'] += $cdr['charges'];
				 $BILL[$cdr['calling_code']]['minutes'] += $cdr['minutes'];
				 				 
				 if($cdr['start_time'] > ($BILL[$cdr['calling_code']]['last_call_time'])){
				 	$BILL[$cdr['calling_code']]['last_call_time'] = $cdr['start_time'];
				 } 

				 if($cdr['start_time'] < ($BILL[$cdr['calling_code']]['start_call_time'])){
				 	$BILL[$cdr['calling_code']]['first_call_time'] = $cdr['start_time'];
				 }

			} else {
				$insert = array();
				$insert['region_name'] = $cdr['region_name'];
				$insert['rate'] = $cdr['rate'];
				$insert['calls'] = 1;
				$insert['minutes'] = $cdr['minutes'];
                                $insert['charges'] = $cdr['charges'];
				$insert['first_call_time'] = $cdr['start_time'];
				$insert['last_call_time'] = $cdr['start_time'];
                                $BILL[$cdr['calling_code']] = $insert;
			}


		 }


		echo "\nCreating Bill Summary\n";

		//sort bill based on region name - ASC order
		$BILL = SortBill($BILL,'region_name','ASC');
	
		
		//Write Summarized Bill
		fwrite($f2,"DESTINATION,RATE,CALLS,MINUTES,CHARGES,FIRST_CALL_TIME,LAST_CALL_TIME\r\n");
		foreach($BILL as $c){
			$str = "";
			foreach($c as $val){
				$str .= "$val,";
			}
			$str .= "\r\n";
			fwrite($f2,$str);
		}

		fclose($f1);
		fclose($f2);

		echo "\n************Done************\n";
	}

	function RateCDR($cdr,$B_RATES,$bucket_length){
			
			#prepare number for Batelco's billing
			$number = clean_number($cdr);
			$RN = $cd['called_party_on_dst'];
		
			$start_time = trim(date('Y-m-d H:i:s',strtotime($cdr['start_time'])));
			$cdr['minutes'] = $minutes = round(($cdr['call_duration_int']/60),2);	

			
			$found = false;
			
			if(preg_match('/^800/',$number) || preg_match('/^165/',$number)){
				if(strpos($RN, 'A') != false or strpos($RN, 'B') != false )
					continue;
				$cdr['rate']    = 0;
                                $cdr['charges'] = 0;
                                $cdr['region_name'] = 'Skip';
				$cdr['calling_code'] = 'Skip';
				$found = true;		
			} else {

			
				//slice RATES array based on buckets
				$number_slice = substr($number,0,$bucket_length);

				if($number_slice != "" && array_key_exists($number_slice,$B_RATES)){
					$BUCKET = $B_RATES[$number_slice];
				
					//search in bucket 
					//look for longest matching dial code with valid effective date
					foreach($BUCKET as $r){
	                                	$calling_code = trim($r['code']);
	                          
						$rate = floatval($r['rate']);
	                                	$len = $r['len'];
	                                	$region_name = $r['region_name'];
                                        
						#Rate Calls
                                        	if((preg_match("/^{$calling_code}\d*$/",$number))){
                                               		# echo "\nMatched $calling_code with $number\n";
                                                //found longest match
                                                $cdr['rate']    = $rate;
                                                $cdr['charges'] = round($minutes * $rate,3);
                                                $cdr['region_name'] = $region_name;
                                                $cdr['calling_code'] = $calling_code;
												if($cdr['region_name']== 'BAHRAIN DIR ASSIST-181' or $cdr['region_name']== 'BAHRAIN DIR ASSIST-188')
													$cdr['charges'] = $rate;
												if(strpos($RN, 'A') != false or strpos($RN, 'B') != false ) { 
													$code = substr($RN, 0, 100);
														switch ($code) {
															case 'A01':
																$cdr['rate'] = '0.006378';
																break;
															case 'A02':
																$cdr['rate'] = '0.008101';
																break;
															case 'A03':
																$cdr['rate'] = '0.008101';
																break;
															case 'B04':
																$cdr['rate'] = '0.002711';
															default:
																$cdr['rate'] = '0.004434';
											
													}
													
													$cdr['charges'] = round($minutes * $rate,3);
																									}
                                                //break since longest match found
                                                $found = true;
                                                break;
                                        	}

					}			 															           }
			}

			//if matching dialcode is not found, mark as Unknown
			if(!$found){
					echo "\nDid not find matching dialcode for $number\n"; 
					$cdr['rate']	= 0;
					$cdr['charges'] = 0;
					$cdr['region_name'] = 'Unknown';
					$cdr['calling_code'] = 'Unknown';	
			}
			
			return $cdr;	            
	}

	function CreateRateBuckets($bucket_length,$RATES){

		//output array
		$B_RATES = array();

		//loop through rates and create bucket
		foreach($RATES as $r){

			//extract bucket length number of digits from the dialcode
			$trimmed = substr($r['code'],0,$bucket_length);
                        
			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){
                               
				$diff = $bucket_length - strlen($trimmed); 
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;

				
				for($i = $start; $i <= $end ; $i++){
					$B_RATES[$i][] = $r; 	
				}
			} else {
				$B_RATES[$trimmed][] = $r; 
			}		
		}	
		
		return $B_RATES;
	}

	function SortBill($BILL,$field_name,$order){
		

		//prepare field that is to be sorted
		$field = array();
		foreach($BILL as $key=>$row){
			$field[$key]=$row[$field_name];
		}

		//sort multi-dimensional array, based on field name in the desired order
		$result = ($order=='ASC')?array_multisort($field,SORT_ASC,$BILL):array_multisort($field,SORT_DESC,$BILL);

		if($result){
			return $BILL;
		} else {
			echo "\nCould not sort invoice\n";
			exit(0);
		}
	}


	function clean_number ($cdr) {

		#contenders
                $first = $cdr['call_party_after_src_calling_plan'];
		$second = $cdr['clean_number']; #martin's clean number from the copy batch
		$third = $cdr['called_party_on_dst'];

                $hold="";

		
		if(strlen($second) < 8){ #empty and until 7 characters, can happen if source and dest are completely different, eg call forwarding, call_transalation 
			$hold = $third; #use 'called_party_on_dst' for billing 	
		} else {
			$hold = $first; #use 'call_party_after_src_calling_plan' for billing
		}
			
		$hold = preg_replace ('/^.*#/','',$hold);     	//remove anything before #
                $hold  = preg_replace('/^973/','',$hold);      	//strip 973 if present for Bah Nat
		#$hold  = preg_replace('/^00/','',$hold);       //DONT strip 00 for international calls 


                return $hold;
        }





?>
