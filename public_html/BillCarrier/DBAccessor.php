<?php

include_once('DB.php');

/// {{{ Extract CDRs
function ExtractCDRs($bill_type,$end_point,$start_date,$end_date,$filter_type){

	$DB = startDB();

	$endpoint_clause = '';
	$duration_clause = '';
	$filter_calls_clause = '';

	if($filter_type == 'EXACT'){
		 foreach($end_point as $ep){
		 	$endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
			$endpoint_clause .= "(EP = '{$ep[0]}' AND PORT = {$ep[1]})";
		}

	}

	 if($filter_type == 'LIKE'){
		 foreach($end_point as $ep){
			$endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
			$endpoint_clause .= "(EP LIKE '{$ep[0]}%')";
		}
	}



	$duration_clause = "start_time >= '{$start_date}' AND start_time <= '{$end_date}'";

	$endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('EP','call_dest_regid', $endpoint_clause): str_replace('EP','call_source_regid', $endpoint_clause);

	if($filter_type == 'EXACT'){
		$endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('PORT','call_dest_uport', $endpoint_clause): str_replace('PORT','call_source_uport', $endpoint_clause);
	}

 	$filter_calls_clause = "call_duration_int > 0";



	$sql = "SELECT start_time,ani,called_party_on_dst,call_party_after_src_calling_plan,clean_number,call_duration_int,call_source_regid,call_source_uport,call_dest_regid, call_dest_uport ";
	$sql .= " FROM new_cdr";
	$sql .= " WHERE ($duration_clause) AND ($endpoint_clause) AND ($filter_calls_clause)";

	#echo "\n$sql\n";
	#exit(0);

	$result = pg_query($sql) or die("Could not extract CDRs");

	return $result;
}

/// }}}

/// {{{ Extract All CDRs for Reporting
function ExtractReportCDRs($bill_type,$start_date,$end_date){

        $DB = startDB();

        $endpoint_clause = '';
        $duration_clause = '';
        $filter_calls_clause = '';
/*
        if($filter_type == 'EXACT'){
                 foreach($end_point as $ep){
                        $endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
                        $endpoint_clause .= "(EP = '{$ep[0]}' AND PORT = {$ep[1]})";
                }

        }

         if($filter_type == 'LIKE'){
                 foreach($end_point as $ep){
                        $endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
                        $endpoint_clause .= "(EP LIKE '{$ep[0]}%')";
                }
        }

*/

        $duration_clause = "start_time >= '{$start_date}' AND start_time <= '{$end_date}'";
/*
        $endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('EP','call_dest_regid', $endpoint_clause): str_replace('EP','call_source_regid', $endpoint_clause);

        if($filter_type == 'EXACT'){
                $endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('PORT','call_dest_uport', $endpoint_clause): str_replace('PORT','call_source_uport', $endpoint_clause);
        }

        $filter_calls_clause = "call_duration_int > 0";

*/

        $sql = "SELECT start_time,ani,called_party_on_dst,call_party_after_src_calling_plan,clean_number,call_duration_int,call_source_regid,call_source_uport,call_dest_regid, call_dest_uport ";
        $sql .= " FROM new_cdr";
        $sql .= " WHERE ($duration_clause);"; 

	#AND ($endpoint_clause) AND ($filter_calls_clause)";

        #echo "\n$sql\n";
        #exit(0);

        $result = pg_query($sql) or die("Could not extract CDRs");

        return $result;
}

/// }}}





/// {{{ Extract CC CDRs
function Extract_CC_CDRs($bill_type,$end_point,$start_date,$end_date,$filter_type){

	$DB = startDB();

	$endpoint_clause = '';
	$duration_clause = '';
	$filter_calls_clause = '';

	if($filter_type == 'EXACT'){
		 foreach($end_point as $ep){
		 	$endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
			$endpoint_clause .= "(EP = '{$ep[0]}' AND PORT = {$ep[1]})";
		}

	}

	 if($filter_type == 'LIKE'){
		 foreach($end_point as $ep){
			$endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
			$endpoint_clause .= "(EP LIKE '{$ep[0]}%')";
		}
	}



	$duration_clause = "start_time >= '{$start_date}' AND start_time <= '{$end_date}'";

	$endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('EP','call_dest_regid', $endpoint_clause): str_replace('EP','call_source_regid', $endpoint_clause);

	if($filter_type == 'EXACT'){
		$endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('PORT','call_dest_uport', $endpoint_clause): str_replace('PORT','call_source_uport', $endpoint_clause);
	}

 	$filter_calls_clause = "call_duration_int > 0";



	$sql = "SELECT ani,clean_number,call_duration_int,call_dest_regid, call_dest_uport ";
	$sql .= " FROM new_cdr";
	$sql .= " WHERE ($duration_clause) AND ($endpoint_clause) AND ($filter_calls_clause)";

//  echo "\n$sql\n";
//  exit(0);
	$result = pg_query($sql) or die("Could not extract CDRs");

	return $result;
}

/// }}}


/// {{{ Extract Batelco CDRs
function ExtractBatelcoCDRs($bill_type,$end_point,$start_date,$end_date,$filter_type,$ignore){
//function ExtractBatelcoCDRs($bill_type,$ip,$start_date,$end_date,$filter_type){
	$DB = startDB();

	$endpoint_clause = '';
	//$ip_clause = '';
	$duration_clause = '';
	$filter_calls_clause = '';
	$ignore_clause = '';

	if($filter_type == 'EXACT'){

		 foreach($end_point as $ep){
		 	$endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
			$endpoint_clause .= "(EP = '{$ep[0]}' AND PORT = {$ep[1]})";
		}
		/*
		  foreach($ip as $i){
		  	$ip_clause .= ($ip_clause != '')?" OR ":"";
			$ip_clause .= "(IP = '{$i}')";
								                  }
*/

	}



	 if($filter_type == 'LIKE'){

		 foreach($end_point as $ep){
			$endpoint_clause .= ($endpoint_clause != '')?" OR ":"";
			$endpoint_clause .= "(EP LIKE '{$ep[0]}%')";
		}


		//Cant enter here for ip matches


	}

	$duration_clause = "start_time >= '{$start_date}' AND start_time <= '{$end_date}'";

	$endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('EP','call_dest_regid', $endpoint_clause): str_replace('EP','call_source_regid', $endpoint_clause);


	//$ip_clause = ($bill_type == 'ORIGINATION')?str_replace('IP','terminator_ip', $ip_clause): str_replace('IP','originator_ip', $ip_clause);

/* Not needed for ip matches */

	if($filter_type == 'EXACT'){
		$endpoint_clause = ($bill_type == 'ORIGINATION')?str_replace('PORT','call_dest_uport', $endpoint_clause): str_replace('PORT','call_source_uport', $endpoint_clause);
	}


	foreach($ignore as $ep){
		$ignore_clause .= ($ignore_clause != '')?" OR ":"";
		$ignore_clause .= "(EP = '{$ep[0]}' AND PORT = {$ep[1]})";
	}
/*
*/
	$ignore_clause = ($bill_type == 'ORIGINATION')?str_replace('EP','call_dest_regid', $ignore_clause): str_replace('EP','call_source_regid', $ignore_clause);

	$ignore_clause = ($bill_type == 'ORIGINATION')?str_replace('PORT','call_dest_uport', $ignore_clause): str_replace('PORT','call_source_uport', $ignore_clause);


 	$filter_calls_clause = "call_duration_int > 0";



	$sql = "SELECT start_time,ani,clean_number,call_duration_int,call_source_regid,call_source_uport,call_dest_regid, call_dest_uport ";
	$sql .= " FROM new_cdr";
	$sql .= " WHERE ($duration_clause) AND ($endpoint_clause) AND NOT($ignore_clause) AND ($filter_calls_clause)";
 // 	$sql .= " WHERE ($duration_clause) AND ($ip_clause) AND ($filter_calls_clause)";

// 	echo "\n$sql\n";
//	exit(0);
	$result = pg_query($sql) or die("Could not extract CDRs");

	return $result;
}


/// }}}

/// {{{ Extract Rates
function ExtractRates($carrier_name,$bill_type,$end_date){

	$DB= startDB();
	//exit(0);
	
	#echo "Connected to DB\n";

	$sql = "SELECT carrier_id from carrier where carrier_name = '{$carrier_name}'";

	#echo "$sql \n";
	#echo "Trying... \n";
	$result = $DB->SelectLimit($sql) or die($DB->ErrorMsg());
	
	#echo "Done sql 1\n";
	$carrier_id = $result->Fields('carrier_id');
//	exit(0);

	$carrier_id_clause = ($bill_type == 'ORIGINATION')?"carrier_id={$carrier_id}":"customer_carrier_id={$carrier_id}";


	$sql = "SELECT distinct region_name,calling_code,rate,effective_date as eff_date
		FROM rate,master
		WHERE {$carrier_id_clause}
		AND rate.master_id = master.master_id
		AND effective_date <= '{$end_date}'
		ORDER BY calling_code DESC, eff_date DESC";
	#echo "\n$sql\n";
	#exit(0);
	$result = pg_query($sql) or die("Could not extract Rates");
	#echo "Executed\n";
	return $result;

}
/// }}}


/// {{{ get LCR codes for CC
function getLCRCodesForCCTraffic(){
	$DB= startDB();
	//exit(0);

	$sql = "select calling_code as code, remark || '-' || calling_code as region_name from rsm_wholesale_hunt where calling_code <> '*' order by calling_code desc";

	$result = pg_query($sql) or die("Could not extract LCR Codes");

	return $result;

}

/// }}}



?>
