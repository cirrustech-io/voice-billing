<?php

	include_once('DBAccessor.php');	


	$bucket_length = 4;
	$carrier_name = 'CC';
        $bill_type = 'TERMINATION';
	

	$c_endpoint = array();
	$c_endpoint['3']=array('UK_AS5400A/4','UK_AS5350B/4','UK_AS5400A/80','UK_AS5350B/80','BHARTI_VoIP/80'); //Bharti
	$c_endpoint['25']=array('UK_AS5400A/14','UK_AS5400A/81'); //KPN
	$c_endpoint['1'] = array('VSNL-3/4','VSNL-7/4','VSNL_3/80','VSNL_7/80','VSNL_10/80'); //VSNL
	$c_endpoint['5'] = array('2CONNECT_AS5350B/80','2CONNECT_AS5400A/80','2Connect_AS5400A/15','2Connect_AS5350B/2','2Connect_AS5350B/23','2Connect_AS5400A/16'); //Batelco

//        print_r($c_endpoint);
//	exit(0);


	$endpoint = array();
	//$endpoint[0] = array('Talking-SIP-2',4);
	//$endpoint[1] = array('Talking-SIP-4',4);
	$endpoint[0] = array('TALKING-SIP-2',80);
        $endpoint[1] = array('TALKING-SIP-4',80);
	

	$filter_type = 'EXACT';
//	$filter_type = 'LIKE';


	$start_date = date('Y-m-d 00:00:00',strtotime('13-Apr-2009'));
	$end_date = date('Y-m-d 23:59:59',strtotime('19-Apr-2009'));

	$op_filename = "Results/$carrier_name-$bill_type-Traffic-13TO19.csv";
	$cdr_filename = "Results/$carrier_name-$bill_type-TrafficDetails-13TO19.csv";

	 //1. Fetch LCR codes for CC Traffic
	 //2. Create Code Buckets
	 //3. Fetch CDRs
	 //4. Group CDRs based on minute volume and create Summary

	echo "Fetching LCR Codes for $carrier_name-$bill_type\n";

	$lcr_codes = getLCRCodesForCCTraffic();
	
// {{{ 
	if(count($lcr_codes)==0){
		echo "\nNo LCR codes Found for $carrier_name $bill_type\n";
		exit(0);
	} else{
		 $CODES = array();
		 while($line=pg_fetch_array($lcr_codes, null, PGSQL_ASSOC)){
			 $CODES[] = $line;
		}

		echo "\nCreating Code Buckets\n";
		
		$B_CODES = CreateBuckets($bucket_length,$CODES);

	}
// }}}

//	print_r($B_CODES);
//	exit(0);

	echo "\nFetching CDRs for $carrier_name-$bill_type\n";
	$C_Result = Extract_CC_CDRs($bill_type,$endpoint,$start_date,$end_date,$filter_type);	
	if(pg_num_rows($C_Result) <= 0){
		echo "\nNo CDRs Found for $carrier_name $bill_type\n";
		exit(0);
	} else {

		

		$BILL = array();

		//Output Files
		$f1 = fopen($cdr_filename,'w');
		$f2 = fopen($op_filename, 'w');
		
		$target = (pg_num_rows($C_Result));
		$current = 0;

		echo "\nGrouping CDRs for $carrier_name-$bill_type($target)\n";

// {{{ loop through CDRs
		 while($line=pg_fetch_array($C_Result, null, PGSQL_ASSOC)){
			 
			//increment cdr counter
			$current += 1;

			$cdr= GroupCDR($line,$c_endpoint,$B_CODES,$bucket_length);
			
			//find caller type - Batelco , Zain or Fixed
			$ani_tukda = substr($cdr['ani'],0,3);

			if(preg_match("/^1/",$ani_tukda)){ $ani_type = 'Fixed-Line'; }

			elseif(preg_match("/^388/",$ani_tukda) || preg_match("/^39/",$ani_tukda) || preg_match("/^383/",$ani_tukda)){ $ani_type = 'Batelco-Mobile'; } 
			elseif(preg_match("/^36/",$ani_tukda) || preg_match("/^377/",$ani_tukda)){ $ani_type = 'Zain-Mobile'; }
			else $ani_type = 'Unknown Source';



			//print progress
			echo "\r  Completed: ($current)". (int)($current*100/$target) . "%";



			

			//print_r($BILL);
			//print_r($cdr);
			//prepare bill summary			
			
				if(array_key_exists($ani_type,$BILL) && array_key_exists($cdr['calling_code'],$BILL[$ani_type])){
					if(array_key_exists($cdr['carrier_id'],$BILL[$ani_type][$cdr['calling_code']])){
						$BILL[$ani_type][$cdr['calling_code']][$cdr['carrier_id']] += $cdr['minutes'];
					} else {
						$BILL[$ani_type][$cdr['calling_code']][$cdr['carrier_id']] = $cdr['minutes'];
					}
			
				} else {
					$insert = array();
					$insert['region_name'] = $cdr['region_name'];
					$insert[$cdr['carrier_id']] =  $cdr['minutes'];
                                	$BILL[$ani_type][$cdr['calling_code']] = $insert;
				}
			

		/*	$str = '';
			foreach($cdr as $v){
				$str .= "$v,";
			}
			$str .= "\r\n";
			fwrite($f1,$str);
		*/
		 }
// }}}
//		print_r($BILL);
//		exit(0);


		echo "\nCreating Bill Summary\n";

		//sort bill based on region name - ASC order
	//	$BILL = SortBill($BILL,'region_name','ASC');	
		foreach($BILL as $ani_type => $LLL){
			//echo "Printing $ani_type and ";
			//print_r($LLL);
			foreach($LLL as $code=>$c){
				$str = "$ani_type,$code,";
				foreach($c as $key=>$val){
					$str .= "$key,$val,";
				}
				$str .= "\r\n";
				fwrite($f2,$str);
			}
		}

		fclose($f2);

		echo "\n************Done************\n";
	
	}

	function GroupCDR($cdr,$c_endpoint,$B_CODES,$bucket_length){
			
			$number = trim($cdr['clean_number']);
			$cdr['minutes'] = $minutes = round(($cdr['call_duration_int']/60),2);
			$dest_ep = trim($cdr['call_dest_regid']) . "/" . trim($cdr['call_dest_uport']);	


			//Find terminating carrier based on dest endpoint

			$carrier_id = '-1';
			$ok = false;
		//	echo "Searching for <<<$dest_ep>>>\n";
			foreach($c_endpoint as $cid => $eps){
				foreach($eps as $ep){
					if($ep == $dest_ep){
						//echo "Found EP for carrier $cid\n\n";
						$carrier_id = "$cid";
						$ok = true;
						break;

					}
				}

				if($ok)	{ break; } 
				else 	{ continue; }
			}
			
			
			$cdr['carrier_id'] = $carrier_id;

			$found = false;
			
			//slice CODES array based on buckets
			$number_slice = substr($number,0,$bucket_length);

			if($number_slice != "" && array_key_exists($number_slice,$B_CODES)){
				$BUCKET = $B_CODES[$number_slice];
				
				//search in bucket 
				//look for longest matching dial code with valid effective date
				foreach($BUCKET as $r){
	                                $calling_code = trim($r['code']);	                           
	                                $region_name = $r['region_name'];

	                                //check code and length
	                                if(preg_match("/^{$calling_code}\d*$/",$number)){

						//found longest match
						$cdr['region_name'] = $region_name;
						$cdr['calling_code'] = $calling_code;

						//break since longest match found
						$found = true;
						break;
					}
				}
			}
			
			//if matching dialcode is not found, mark as Unknown
			if(!$found){
					//echo "\nDid not find matching dialcode for $number\n"; 
					$cdr['calling_code'] = 'ROW';
					$cdr['region_name'] = 'ROW';	
			}
			 //print_r($cdr);			
			return $cdr;	   
	
	}

	
	function CreateBuckets($bucket_length,$CODES){

		//output array
		$B_CODES = array();

		//loop through rates and create bucket
		foreach($CODES as $r){

			//extract bucket length number of digits from the dialcode
			$trimmed = substr($r['code'],0,$bucket_length);
                        
			//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
			if(strlen($trimmed) < $bucket_length){
                               
				$diff = $bucket_length - strlen($trimmed); 
				$start =  $trimmed * pow(10,$diff);
				$end = $start + pow(10,$diff) - 1;

				
				for($i = $start; $i <= $end ; $i++){
					$B_CODES[$i][] = $r; 	
				}
			} else {
				$B_CODES[$trimmed][] = $r; 
			}		
		}	
		
		return $B_CODES;
	}

	function SortBill($BILL,$field_name,$order){
		

		//prepare field that is to be sorted
		$field = array();
		foreach($BILL as $key=>$row){
			$field[$key]=$row[$field_name];
		}

		//sort multi-dimensional array, based on field name in the desired order
		$result = ($order=='ASC')?array_multisort($field,SORT_ASC,$BILL):array_multisort($field,SORT_DESC,$BILL);

		if($result){
			return $BILL;
		} else {
			echo "\nCould not sort invoice\n";
			exit(0);
		}
	}




?>
