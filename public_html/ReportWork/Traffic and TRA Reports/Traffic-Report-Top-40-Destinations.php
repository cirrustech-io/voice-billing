<?php

include_once('DB.php');
include_once('CSVParser.php');

$bucket_length = 4;
$start_date = date('Y-m-d 00:00:00',strtotime('1-Apr-2010'));
$end_date = date('Y-m-d 12:59:59',strtotime('1-Apr-2010'));

$detailed_filename = "Results/Top9DestTraffic-Detail-" . date('Y-m-d-H-i-s') . ".csv";
$summary_filename = "Results/Top9DestTraffic-Summary-" . date('Y-m-d-H-i-s') . ".csv";

echo "\nExtracting Codes For Top 9 Countries..\n";
$parser = new CsvFileParser();

$COUNTRIES = $parser->ParseFromFile('Top9CountryInfo.csv');



if(count($COUNTRIES) <= 0){
        echo "\nNo Countries found\n";
        exit(0);
} else {


	#Sort in ascending order of calling codes  before creating buckets  - done manually now
	$COUNTRIES = SortArray($COUNTRIES,1,'ASC');


        echo "\nCreating Country Buckets\n";
        $B_COUNTRIES = CreateCountryBuckets($bucket_length,$COUNTRIES);
	
	#Sort in descending order of calling codes  before creating buckets  - done manually now
        $B_COUNTRIES = Sort3DArray($B_COUNTRIES,'calling_code','DESC');


	print_r($B_COUNTRIES);
	exit(0);
}

echo "\nExtracting CDRs from {$start_date} to {$end_date}..\n";

$sql = "SELECT call_party_after_src_calling_plan,call_duration_int
        from new_cdr
        where start_time >= '{$start_date}' and start_time <= '{$end_date}'";
 
#echo "\n$sql\n";
#exit(0);

$DB = startDB();
$result = pg_query($sql);

if(pg_num_rows($result) <= 0){
        echo "\nNo CDRs found\n";
        exit(0);
}

if(pg_num_rows($result) > 0){

	$target = (pg_num_rows($result));
        $current = 0;
		 
	$SUMMARY = array();
	//Output Files
	$f1= fopen($detail_filename,'w');
	$f2 = fopen($summary_filename, 'w');


        while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
			
				$call_duration_int = $line['call_duration_int'];
				$current += 1;
				 echo "\r  Progress: ($current)". (int)($current*100/$target) . "%";

				#skip if 0 duration
				if($call_duration_int == 0){ 
					continue;	
				}
				

				#echo "\n {$line['call_party_after_src_calling_plan']} \n";
	
				# use 'call_party_after_src_calling_plan' as dialed number, retain 00 if present, clean other prefixes
				$dialed_number = preg_replace('/^.*#/','',$line['call_party_after_src_calling_plan']);
				$dialed_number = trim($dialed_number);
				
				#skip empty values and local calls - this is to identify international calls only
				if( empty($dialed_number) || preg_match('/^00973\d*$/',$dialed_number)  || !(preg_match('/^00\d*$/',$dialed_number))){
					continue;
				}
								
				
				
				$cdr = array();
				$cdr['seconds'] = $call_duration_int;
				$cdr['dialed_number'] = $dialed_number;
				
				
				#now strip 00 and proceed with longest match search
				$dialed_number = substr($dialed_number,2);
				
				#echo "$dialed_number\n";
				
				$found = false;
				$number_slice = substr($dialed_number,0,$bucket_length);
				
						
				if($number_slice != "" && array_key_exists($number_slice,$B_COUNTRIES)){
					$BUCKET = $B_COUNTRIES[$number_slice];

					//search in bucket
					//look for longest matching dial code 
					foreach($BUCKET as $c){
					
						$calling_code = trim($c['calling_code']);
						$region_name = $c['region_name'];


						//check code 
						if((preg_match("/^{$calling_code}\d*$/",$dialed_number))){
							//found longest match

							$cdr['region_name'] = $region_name;
							$cdr['calling_code'] = $calling_code;

							//break since longest match found
							$found = true;
							break;
						}  	                                                                                                                          	 		}
                	      }
				
			      if(!$found){
					$cdr['region_name'] = 'Unknown';
					$cdr['calling_code'] = 'Unknown';
			      }
				
	
				#print_r($cdr);
				#echo "\n";
				#echo "I am here";

				 //write cdr detail
				$str = "";
				foreach($cdr as $val){
						$str .= "$val,";
				}
				$str .= "\r\n";

				fwrite($f1,$str);

				 //prepare summary
				if(array_key_exists($cdr['calling_code'],$SUMMARY)){
						 $SUMMARY[$cdr['calling_code']]['calls'] += 1;
						 $SUMMARY[$cdr['calling_code']]['minutes'] +=  round(($cdr['seconds']/60),2);

				} else {
						$insert = array();
						$insert['code'] = $cdr['calling_code'];
						$insert['region_name'] = $cdr['region_name'];
						$insert['calls'] = 1;
						$insert['minutes'] = round(($cdr['seconds']/60),2);
					
						$SUMMARY[$cdr['calling_code']]= $insert;
				}

				
		}
		
		 echo "\nCreating Summary\n";


	
		//sort summary based on region name - ASC order
		$SUMMARY = SortArray($SUMMARY,'region_name','ASC');
		
		#print_r($SUMMARY);
                #exit(0);

		//Write Summarized Output
		fwrite($f2,"CALLING_CODE,REGION_NAME,CALLS,MINUTES\r\n");
		foreach($SUMMARY as $line){
			$str = "";
			foreach($line as $val){
				$str .= "$val,";
			}
			$str .= "\r\n";
			fwrite($f2,$str);	
		}

		fclose($f1);
		fclose($f2);
		
		echo "\n************Done************\n";

	


}


function CreateCountryBuckets($bucket_length,$COUNTRIES){
		
	
                //output array
                $B_COUNTRIES = array();

                //loop through country codes and create bucket
                foreach($COUNTRIES as $c){


			//Construct breakout 
			 $breakout = array();
                         $breakout['region_name'] = $c[0];
			 $breakout['calling_code'] = $c[1];	
			
                        //extract bucket length number of digits from the dialcode
                        $trimmed = substr($c[1],0,$bucket_length);
					
                        //if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
                        if(strlen($trimmed) < $bucket_length){

                                $diff = $bucket_length - strlen($trimmed);
                                $start =  $trimmed * pow(10,$diff);
                                $end = $start + pow(10,$diff) - 1;


                                for($i = $start; $i <= $end ; $i++){
					#if(!(array_key_exists($i,$B_COUNTRIES) && (array_key_exists(,$B_COUNTRIES[$i]))){			       
						$B_COUNTRIES[$i][] = $breakout;
					#}
								
                                     
                                }
                        } else {

				$B_COUNTRIES[$i][] = $breakout;
                        }
                }
		#print_r($B_COUNTRIES);
		#exit(0);
                return $B_COUNTRIES;
}


 function SortArray($ARR,$field_name,$order){

	//prepare field that is to be sorted
	$field = array();
	foreach($ARR as $code=>$row){
		
		$field[$code] = $row[$field_name];
		
	}


	#print_r($field);
	#exit(0);

	//sort multi-dimensional array, based on field name in the desired order
	$result = ($order=='ASC')?array_multisort($field,SORT_ASC,$ARR):array_multisort($field,SORT_DESC,$ARR);

	#print_r($ARR);
	#exit(0);

	if($result){
		return $ARR;
	} else {
		echo "\nCould not sort array\n";
		exit(0);
	}
}


function Sort3DArray($ARR,$field_name,$order){

        //prepare field that is to be sorted
        $field = array();
        foreach($ARR as $subARR){
	 	foreach($subARR as $row){
			$field[$row['calling_code']] = $row[$field_name];
		}
        }


       
	

        //sort multi-dimensional array, based on field name in the desired order
        $result = ($order=='ASC')?array_multisort($field,SORT_ASC,$ARR):array_multisort($field,SORT_DESC,$ARR);

        print_r($ARR);
        exit(0);

        if($result){
                return $ARR;
        } else {
                echo "\nCould not sort array\n";
                exit(0);
        }
}







?>
