
<?php

include_once('db_accessor.php');
include_once('CSVParser.php');

$parser = new CsvFileParser();
$parsedData = $parser->ParseFromFile("Sequences.csv");
echo "Sequence Number;Activation Date;Sold or not sold;Expiry Date;Status;Starting Balance;Current Balance;Last Enabling Date;Calls Charges;Calls Count;Maintenance Charges;Maintenance Count<br>";
#$output = "";
foreach($parsedData as $line)
{
$invalid=false;

if (!is_numeric($line[0]) OR strlen((string)$line[0])!= 9) {
	$invalid=true;
	//echo $invalid;
}
if($invalid == 0 ){

	$output = "";
	$sequence_number = $line[0];
	
		$cc_info = get_card_info($sequence_number);
		
        if(mssql_num_rows($cc_info) > 0){
		
		$output = mssql_fetch_assoc($cc_info);
		
		$account_number = $output['ACCOUNT'];
		//echo  $account_number;
		$account_id = $output['ACCOUNT_ID'];
		$activation_date = $output['ACTIVATION_DATE_TIME'];
		$starting_balance = floatval($output['STARTING_BALANCE'])/1000;
		$current_balance = floatval($output['BALANCE'])/1000;
		$enabled = $output['ENABLED'];
		$last_enable_date = $output['LAST_ENABLED_UPDATE_DATE_TIME'];
		//$last_enable_date = $output['LAST_ENABLE_DATE_TIME'];
		#print_r($output);
		$cc_billing = get_card_calls_info($account_id);
		$calls_info = mssql_fetch_assoc($cc_billing);
		$calls_sum = $calls_info['sum']/1000;
		$calls_count =  $calls_info['count'];	
		$cc_maintenance = get_card_maintenance_info($account_id);
		$main_info = mssql_fetch_assoc($cc_maintenance);
		//print_r($main_info);exit(0);
                $main_sum = $main_info['sum']/1000;
                $main_count =  $main_info['count'];

		$cc_expiry = get_expiry_date($sequence_number);
		$cc_ex_date = mssql_fetch_assoc($cc_expiry);
		$expiry_date = $cc_ex_date['EXPIRE_DATE'];
	//	echo $expiry_date;
		if ($calls_count == 0 OR $calls_count == NULL)
                {
                        $calls_count = 0;
                        $calls_sum = 0;
                }
		
                 if ($main_count == 0 OR $main_count == NULL)
                {
                        $main_count = 0;
                        $main_sum = 0;
                }

		$not_found = false;
	}
		 else {
			$not_found = true;
		 		}

		$valid = check_validity($sequence_number);
                //echo "am here";
		
		if($valid > 0){
			$validity = "Sold";
		} else {
			$validity = "Not Sold";
		}
		
		if($enabled == 0){
                        $status = "Disabled";
                } else {
                        $status = "Enabled";
                }
	
	$screen_message = "";

	$delimiter = ";";
	$quote ='"';
	
	if($not_found == false) {
	
	$output = $sequence_number .$delimiter.$activation_date .$delimiter.$validity .$delimiter.$expiry_date .$delimiter.$status .$delimiter. $starting_balance .$delimiter. $current_balance .$delimiter.$last_enable_date .$delimiter.$calls_sum .$delimiter.$calls_count.$delimiter.$main_sum .$delimiter.$main_count."<br>";
		
	}
	
	elseif ($not_found == true){
		$output =  $sequence_number . $delimiter . "record not found<br>";
	}
/*	
	echo "Sequnce Number : <b>".$sequence_number .$delimiter;
	echo "Activation Date : <b>" . $activation_date . $delimiter;
	echo "Sold : <b>" . $validity .$delimiter;
	echo "Expiry Date : <b>" . $expiry_date .$delimiter;
	echo "Enabled : <b>" . $status . $delimiter;
	echo "Starting Balance : <b>". $starting_balance ." BD". $delimiter;
	echo "Current Balance : <b>" . $current_balance ." BD". $delimiter;
	echo "Last Enabling Date : <b>" . $last_enable_date . $delimiter;
	echo "Calls Total Amount : <b>".$calls_sum ." BD". $delimiter;
	echo "Number of Calls : <b>". $calls_count . $delimiter;
	echo "Maintenance Total Amount : <b>" .$main_sum ." BD" . $delimiter;
	echo "Maintenace Count : <b>". $main_count . $delimiter;
*/
	
	
}
else{ 
	echo $line[0].";"."Number is invalid<br>";
}
echo $output;
}
/*
$myFile = "testFile.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh, $output);
fclose($fh);
*/
?>

