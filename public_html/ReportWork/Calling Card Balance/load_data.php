<?php

	include_once('CSVParser.php');
	include_once('db_accessor.php');
	
	$parser = new CsvFileParser();
	$parsedData = $parser->ParseFromFile("Calling Card Sheet-11.csv");
	
	foreach($parsedData as $line){
		$order_date = $line[0];
		$order_description = $line[1];
		$starting_sequence = $line[2];
		$ending_sequence = $line[3];
		$cards_count = $line[4];
		$account_group_id = $line[5];
		#insert_order($order_date,$order_description,$starting_sequence,$ending_sequence,$cards_count);
		insert_order($order_date,$order_description,$starting_sequence,$ending_sequence,$cards_count,$account_group_id);
	}
	
	echo "Inserted data from Calling Card Sheet.csv";
	
	#check();
	echo "<br>Done";

?>
