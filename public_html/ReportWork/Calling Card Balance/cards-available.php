<?php
include_once('db_accessor.php');

$filename = 'Sequences.csv';
$text = file($filename);

$order_date = array();
$starting_seq = array();
$ending_seq = array();
$acc_grp_id = array();


foreach($text as $value)
{
	$temp = explode(';',$value);
	array_push ($order_date,rtrim($temp[0]));
	array_push ($starting_seq,$temp[1]);
	array_push ($ending_seq,rtrim($temp[2]));
	array_push ($acc_grp_id,rtrim($temp[3]));
	
}
/*echo '<pre>';
print_r($starting_seq);
echo '</pre>';
echo '<pre>';
print_r($ending_seq);
echo '</pre>';*/

$count = count($starting_seq);

$output = "";
for ($i = 0; $i <= $count ; $i++) {
	
    $result = query_database($starting_seq[$i],$ending_seq[$i],$acc_grp_id[$i]);
    $order_info = mssql_fetch_assoc($result);
    $cards_count = $order_info['count_cards'];
    $cards_balance = $order_info['total_balance'];
    
    $output = $output.$order_date[$i].";".$starting_seq[$i].";".$ending_seq[$i].";".$cards_count.";".$cards_balance."\r\n";
}

$fp = fopen('myfile.txt', 'w');

fwrite($fp, $output);

fclose($fp); 

?>
