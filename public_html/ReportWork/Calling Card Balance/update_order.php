<?php
	#echo "HMMMM";
	

	include_once('db_accessor.php');
	
	

	$cc_result = fetch_cc_details();
	
	
	if(pg_num_rows($cc_result) > 0){
	
		
		while($line = pg_fetch_array($cc_result, NULL,PGSQL_ASSOC)){
			#print_r($line);
			#echo "<br>";
			$id = intval($line['id']);
			$start_sequence = intval($line['starting_sequence']);
			$end_sequence = intval($line['ending_sequence']);
			$account_group_id =  intval($line['account_group_id']);
			
			if($start_sequence > $end_sequence) { echo "\nSkipping CC Order Id {$id}. Start > End\n"; continue; }
	
			echo "\nAccessing IVR for Order Id {$id} \n";

			#$ivr_result = read_ivr_details($start_sequence,$end_sequence);
			$ivr_result = read_ivr_details_temp($start_sequence,$end_sequence,$account_group_id);
			#$id = 871;
			#$ivr_result = read_ivr_details('106104879','106109878',35);

			if(mssql_num_rows($ivr_result) > 0){
				
				$enabled_count = 0;
				$enabled_balance = 0;
				$disabled_count = 0;
				$disabled_balance = 0;
				$card_account_group = '';
				$batch_id = 0;
				$batch_name = '';
				while($output = mssql_fetch_array($ivr_result,MSSQL_ASSOC)){
					 $card_account_group = $output['ACCOUNT_GROUP'];
					 $batch_id = $output['BATCH_ID'];
					 $batch_name = $output['BATCH_NAME'];
					 $status = intval($output['STATUS']);
					 switch($status){
						case 0:
							$disabled_count = $output['NO_OF_CARDS'];
							$disabled_balance = $output['AVAILABLE_BALANCE'];
							break;
						case 1:
							$enabled_count = $output['NO_OF_CARDS'];
							$enabled_balance = $output['AVAILABLE_BALANCE'];
							break;
					 }
				}
				
				#$update_result = update_order_temp($id,$enabled_count,$enabled_balance,$disabled_count,$disabled_balance,$card_account_group,$batch_id,$batch_name);
				$update_result = update_order($id,$enabled_count,$enabled_balance,$disabled_count,$disabled_balance,$card_account_group,$batch_id,$batch_name); 
				#print_r($update_result);
				#exit(0); 
				#if(!$update_result){ echo "\nCould not successfully update Order Id {$id}.\n"; continue; }	
				
			} else {
				echo "\nCould not read info from IVR for Order Id {$id}.\n"; #continue;
			}
			echo "\nSuccessfully updated Order Id {$id}.\n";
	
		}
		
	} else {
		echo "\nCould not fetch CC Details.\n";
	}
	

	echo "\nDone\n";
	

?>
