<?php

	#DB Accessor
	
	include("db-inc.php");
	require_once('includes/functions.inc.php');

	#function insert_order($order_date,$order_description,$starting_sequence,$ending_sequence,$cards_count){
	function insert_order($order_date,$order_description,$starting_sequence,$ending_sequence,$cards_count,$account_group_id){
			connectPG();
			
			$sql = "INSERT INTO callingcard_audit(order_date,order_description,starting_sequence,ending_sequence,cards_count,account_group_id)
					VALUES (" 
					.	GetSQLValueString($order_date,'date') . ","
					.	GetSQLValueString($order_description,'text') . ","
					.	GetSQLValueString($starting_sequence,'text') . ","
					.	GetSQLValueString($ending_sequence,'text') . ","
					.	GetSQLValueString($cards_count,'int') . "," 
					.       GetSQLValueString($account_group_id,'int')

					. ");";
			
			$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			pg_close();
	}

	function fetch_cc_details(){
		connectPG();
	/*	
		if(!($conn = pg_connect("host=80.88.242.8 dbname=db_order user=alicia password=fatty"))){
			echo "\nError while trying to connect to the Report DB\n";
			exit(0);
		}
	*/	

		
		$sql = "SELECT * from callingcard_audit";

		$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
		#echo "\nReached here\n";
		
		pg_close();
		
		return $result;
	}
	
	#function read_ivr_details($start_sequence,$end_sequence){
	function read_ivr_details($start_sequence,$end_sequence,$account_group_id){
	
		if(!($conn = mssql_connect('192.168.96.3','alicia.dsouza','w0rk$tuff'))){
			echo mssql_get_last_message();
			echo "\nFatal Error while trying to connect\n";
		} elseif(!(mssql_select_db('TEL_DATA',$conn))){
			echo "\nFatal Error while trying to bind database\n";
		}
		
			
		
		#connectSQL();	
		
		$sql = "SELECT AG.ACCOUNT_GROUP, B.BATCH as BATCH_NAME,A.BATCH_ID AS BATCH_ID, A.ENABLED as STATUS,count(*) as NO_OF_CARDS,sum(A.BALANCE)/1000 as AVAILABLE_BALANCE 
				from ACCOUNTS as A 
				, BATCHES as B
				, ACCOUNT_GROUPS as AG
				where A.SEQUENCE_NUMBER >= " . GetSQLValueString($start_sequence,'int')
				. " and A.SEQUENCE_NUMBER <= " . GetSQLValueString($end_sequence,'int')
				. " and B.BATCH_ID = A.BATCH_ID
				and AG.ACCOUNT_GROUP_ID = A.ACCOUNT_GROUP_ID
				and A.ACCOUNT_GROUP_ID = " . GetSQLValueString($account_group_id,'int')	
				. " GROUP BY AG.ACCOUNT_GROUP, B.BATCH,A.BATCH_ID,A.ENABLED
				;";
		#echo "\n$sql\n";
		#exit(0);
		$result = mssql_query($sql,$conn) or die ('Query failed: ' . mssql_get_last_message());
	 	#$result = mssql_query($sql) or die ('Query failed: ' . mssql_get_last_message());
		return $result;
	}
	
	function update_order($id,$enabled_count,$enabled_balance,$disabled_count,$disabled_balance,$card_account_group,$batch_id,$batch_name){
			
			connectPG();
			
			$sql = "UPDATE callingcard_audit
					SET enabled_count =" 	. GetSQLValueString($enabled_count,'int') 
					. ",enabled_balance =" 	. GetSQLValueString($enabled_balance,'double') 
					. ",disabled_count =" 	. GetSQLValueString($disabled_count,'int') 
					. ",disabled_balance =" 	. GetSQLValueString($disabled_balance,'double') 
					. ",card_account_group =" 	. GetSQLValueString($card_account_group,'text') 
					. ",batch_id =" 	. GetSQLValueString($batch_id,'int')
					. ",batch_name =" 	. GetSQLValueString($batch_name,'text')
					. " WHERE id = " . GetSQLValueString($id,'int')
					. ";";
					
		#	echo "\n$sql\n";
			#exit(0);
			$result = pg_query($sql) or die ('Query failed: ' . pg_last_error());
			pg_close();
	}
	
?>
