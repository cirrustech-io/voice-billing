<?php

//Aditional Functions
require_once('includes/functions.inc.php');

function getRatesCoolness($cat,$who,$when,$where){
	
	//Connection statement
	include('Connections/DB.php');
	
	$sql = "Select carrier_id from carrier where carrier_name='2CONNECT'";
	$twoconn = $DB->SelectLimit($sql) or die($DB->ErrorMsg());
	
	switch($cat){
		case "CS":
			$supp = $who;
			$cust = $twoconn->Fields('carrier_id');
			break;
		case "CC":
			$supp = $twoconn->Fields('carrier_id');
			$cust = $who;
			break;			
	}
	
	$when = GetSQLValueString($when,"date");
	$cust =	GetSQLValueString($cust,"int");
	$supp = GetSQLValueString($supp,"int");
	
	
	$sql = "select r.rate_id,r.master_id,m.calling_code,m.region_name,r.carrier_id,c1.carrier_name as supplier,c2.carrier_name as customer,r.rate,r.effective_date 
			from rate as r, carrier as c1, carrier as c2, master as m
			where rate_id in
				(select rate_id from 
				(select max(rate_id) as rate_id,master_id,carrier_id,customer_carrier_id,effective_date 
				from rate 
				where (master_id,carrier_id,customer_carrier_id,effective_date) in 
					(select master_id,carrier_id,customer_carrier_id,max(effective_date) 
					from rate where effective_date<=$when";
	
	switch($who){
		case 'ALL':
				if($cat == "CS"){
					$sql .= " and customer_carrier_id=$cust";
				}
				else {
					$sql .= " and carrier_id=$supp";
				}
				break;
		default:
				$sql .= " and carrier_id=$supp and customer_carrier_id=$cust";			
	}
	
	switch($where){
		case 'ALL':
			break;
		default:
			$sql .= " and master_id in (select master_id from master where calling_code like '$where%')";
	}
	
	
	
	$sql .= " group by carrier_id,customer_carrier_id,master_id order by master_id,carrier_id,customer_carrier_id)  
				group by master_id,carrier_id,customer_carrier_id,effective_date) as x)
					and m.master_id=r.master_id 
					and c1.carrier_id=r.carrier_id
					and c2.carrier_id=r.customer_carrier_id	
					order by calling_code asc,supplier asc";
	
	$rows =  pg_query($sql) or die('Query failed: ' . pg_last_error());
	return $rows;
	
}

function getAllLiveCarriers(){
	
	//Connection statement
	include('Connections/DB.php');

	$getAllCarriersSQL = "SELECT * FROM carrier where Type in ('C','P') order by carrier_name";
	$carrier = $DB->SelectLimit($getAllCarriersSQL) or die($DB->ErrorMsg());

	return $carrier;
}

function getDestinations(){
	//Connection statement
	include('Connections/DB.php');

	$sql = "SELECT * FROM country order by country_name";
	$result = $DB->SelectLimit($sql) or die($DB->ErrorMsg());

	return $result;
}

function enterLiveRates($username,$password){
	//Connection statement
	include('Connections/DB.php');

	$sql = "SELECT * FROM liverate_login where username='$username' and password='$password'";
	$result = $DB->SelectLimit($sql) or die($DB->ErrorMsg());

	return $result;
}
?>