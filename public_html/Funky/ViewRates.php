<?php

	include_once('DBAccessor.php');
	
	session_start();
	
	if (empty($_SESSION['user_name'])) { session_write_close(); header("Location: Login.php"); exit; }	

	// build the form action
	$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

	//read carriers and destinations
	$rows_carriers = getAllLiveCarriers();
	$rows_destinations = getDestinations();

	if($rows_carriers->EOF || $rows_destinations->EOF){
		echo "Oh-oh billing db in walker is acting funny. Developer defends code. Please contact ext. 101";
	}

	if ((isset($_POST["doodoo"])) && ($_POST["doodoo"] == "weewee")) {
		$p1 = $_POST['category_list'];
		$p2 = $_POST['carrier_list'];
		$p3 = $_POST['date_field'];
		$p4 = $_POST['destination_list'];
		 session_write_close(); 
		$goto= "Location: ViewRatesResult.php?p1='".$p1."'&p2='".$p2."'&p3='".$p3."'&p4='".$p4."'";
		header($goto);

	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>View Rates Request</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="STYLESHEET" type="text/css" href='calendar-blue2.css'/>
	<script type="text/javascript" src="calendar.js"></script>
	<script type="text/javascript" src="lang/calendar-en.js"></script>
	<script type="text/javascript" src="calendar-setup.js"></script>

	<style>
		input.btn
		{
			background-color:#000000;
		   	border:1px solid;
		   	border-top-color:#8FC4E8;
		   	border-left-color:#8FC4E8;
		   	border-right-color:#8FC4E8;
		   	border-bottom-color:#8FC4E8;
		   	cursor:pointer;
			cursor:hand;
			font-family:calibri;
			font-size:12pt;
			color:white;
		}

		input
		{
			font-family:calibri;
			font-size:12pt;
			color:black;
		}

		body
		{
			font-family:calibri;
			font-size:12pt;
			color:black;

		}
	</style>
</head>

<body>
	<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
	<table border=0>
		<tr><td><img src="images/slogan.gif"/></td></tr>
		<tr><td><img src="images/bar.gif"/></td></tr>
		<tr>
	 	<table bgcolor="Silver" cellpadding="5" cellspacing="0"">
	 		<tr>
		 		<td>Select category</td>
		 		<td>
					<select name="category_list" id="category_list">
						<option id="CC" value="CC" selected>Customer Rates</option>
						<option id="CS" value="CS">Supplier Rates</option>
					</select>
		 		</td>
	 		</tr>

	 		<tr>
		 		<td>Select carrier</td>
				<td>
					<select name="carrier_list" id="carrier_list">
					<?php
						$rows_carriers->MoveFirst();
						while(!$rows_carriers->EOF){
							$carrier_id = $rows_carriers->Fields('carrier_id');
							$carrier_name = $rows_carriers->Fields('carrier_name');
							echo "<option id=$carrier_id value=$carrier_id>";
							echo $rows_carriers->Fields('carrier_name');
							echo "</option>";
							$rows_carriers->MoveNext();
						}
					?>
						<option id="ALL" value="ALL">ALL</option>
		 			</select>
		 		</td>
	  		</tr>

	  		<tr>
		 		<td>Select destination</td>
				<td>
					<select name="destination_list" id="destination_list">
					<?php
						$rows_destinations->MoveFirst();
						while(!$rows_destinations->EOF){
							$country_code = $rows_destinations->Fields('country_code');
							$country_name = $rows_destinations->Fields('country_name');
							echo "<option id=$country_code value=$country_code>";
							echo "$country_name--$country_code";
							echo "</option>";
							$rows_destinations->MoveNext();
						}
					?>
						<option id=ALL value=ALL selected>ALL</option>
		 			</select>
		 		</td>
	  		</tr>

	  		<tr>
	  			<td>Effective date</td>
	  			<td><input type="text" id="date_field" name="date_field" readonly size="10" value=<?php echo date('m/d/Y'); ?> /><button id="trigger" class="btn">...</button>
	  			</td>
	  		</tr>

	  		<tr>
	  			<td colspan="2" align="center">
	  				<br>
	  				<input type="hidden" name="doodoo" value="weewee"/>
	  				<input type="submit" name="Submit" value="Get Rates" class="btn"/>
	  			</td>
	  		</tr>
		</table>
		</tr>		
		<tr><td><img src="images/lower_bar.gif"/></td></tr>
		<tr><td><a href='logout.php'>Logout</a></td></tr>
	</table>
	</form>

	<script type="text/javascript">
		  Calendar.setup(
		    {
		      inputField  : "date_field",   // ID of the input field
		      ifFormat    : "%m/%d/%Y",    // the date format
		      button      : "trigger"       // ID of the button
		    }
		  );
	</script>

</body>
</html>
