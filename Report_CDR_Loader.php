<?php

	include_once('DBAccessor.php');


		 echo "\n***Fetch search filter criteria***\n";
                $criteria_result =  getPopulateCriteria();



                if(!isset($criteria_result)){
                                echo "ERROR: Could not read criteria details \n";
                                exit(0);
                }

                if(!(pg_num_rows($criteria_result) > 0)){
                                echo "ERROR: No Criteria Found \n";
                                exit(0);
                }

                $criteria_filter = array();
                for($i = 0 ; $i < pg_num_rows($criteria_result); $i++){
                        $criteria_filter[] = array('D' => pg_fetch_result($criteria_result,$i,0),
                                                'E' => pg_fetch_result($criteria_result,$i,1));
                }




	while(true){
		


		echo "\n***MAX Destination CDR File Id***\n";
		$result =  getDestMaxCDRFileId(); 
		
		if(!(isset($result)) || !(pg_num_rows($result) > 0)){
			echo "ERROR: Could not read max destination cdr file id\n";
			exit(0);
		}
		
		$max_file_id = pg_fetch_result($result,0,0);
		echo "{$max_file_id}\n";
		
		echo "\nLooking for new files\n";
		$result =  getSourceCDRFileIds($max_file_id);
		
		if(!(isset($result))){
			echo "ERROR: Could not read source cdr file ids\n";
			exit(0);
		}
		
		if(!(pg_num_rows($result) > 0)){
			echo "No New Files Found\n";
			go_to_sleep(1800);
			continue;
		}
		
		$files_found = pg_num_rows($result);
		
		echo "\n***{$files_found} new files found***\n";
		for($i = 0 ; $i < $files_found; $i++){
			$source_file_id = pg_fetch_result($result,$i,0);	
			echo "Querying for File Id:{$source_file_id} \n";
		
			$cdr_result = getCDRS($source_file_id);
			
			if(!isset($cdr_result)){
				echo "ERROR: Could not read from source table \n";
				exit(0);
			}
			
			if(!(pg_num_rows($cdr_result) > 0)){
				echo "No records found for File Id: {$source_file_id}\n";
			}
			
			if(populate_dest_table($cdr_result,$criteria_filter)){
				echo "\n***Updating control***\n\n";
				update_control($source_file_id);
				
			} else {
				echo "\nERROR: Could not complete task for File Id: {$source_file_id} \n";
				echo "\n***ABORT***\n";
				exit(0);
			}
			go_to_sleep(2);
		
		}
		
		#go_to_sleep(1800);
		go_to_sleep(10);
	
	}
	
	function go_to_sleep($till){
		echo "Sleeping";
		
		for($i=$till;$i>0;$i--){
			echo "\rSleeping {$i}     ";
			sleep(1);
		}
		echo "\r                        \r";
	}
	
	
	function populate_dest_table($cdr_result,$criteria_filter){
		
		echo "\n Completed";
		$current = 0;
		$total = pg_num_rows($cdr_result);
		while($line = pg_fetch_array($cdr_result,null,PGSQL_ASSOC)){
			if(match_found($line,$criteria_filter)){
					insert_report_new_cdr($line);
			}

			$current++; 
			echo "\rCompleted " . intval(($current/$total)*100) . "%";
				

		}
		echo "\r                           \r";
		
		return true;
		
	}
	
	function match_found($line,$criteria_filter){
		
		$s_regid = $line['call_source_regid'];
		$s_port =  $line['call_source_uport'];
		
		$d_regid = $line['call_dest_regid'];
		$d_port =  $line['call_dest_uport'];
	
			
		
		foreach($criteria_filter as $cf){
			$direction = trim($cf['D']);
			$endpoint = trim($cf['E']);
		
			#echo "Looking for $endpoint NOW\n";	
			if (is_int(strpos($endpoint, ':'))) {
				$eps = explode(':',$endpoint); //multiple eps
				foreach($eps as $endpoint){
						 $ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
						 $port = substr($endpoint,strpos($endpoint,"/") + 1);			
						 if(do_the_check($direction,$ep,$port,$s_regid,$s_port,$d_regid,$d_port)){  return true; }
				}

			} else {
					$ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
					$port = substr($endpoint,strpos($endpoint,"/") + 1);
					if(do_the_check($direction,$ep,$port,$s_regid,$s_port,$d_regid,$d_port)){ return true; }
			}
			#go_to_sleep(1);

		}
		return false;
		
		
	}
	
	
	function do_the_check($direction,$ep,$port,$s_regid,$s_port,$d_regid,$d_port){
		#echo "Here 4\n";
		#echo "$direction,$ep,$port,$s_regid,$s_port,$d_regid,$d_port\n";
		switch($direction){
			case 'C':
				if(trim($ep) == trim($s_regid) && trim($port) == trim($s_port)) return true;
				break;
			case 'S':
				if(trim($ep) == trim($d_regid) && trim($port) == trim($d_port)) return true;
				break;
		}
		return false;
		
		
	}
	



?>
