-- Table: payment_method

-- DROP TABLE payment_method;

CREATE TABLE payment_method
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(250),
  def smallint DEFAULT 0,
  CONSTRAINT payment_method_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE payment_method OWNER TO postgres;
GRANT ALL ON TABLE payment_method TO postgres;
GRANT SELECT ON TABLE payment_method TO web_order;




