-- Table: billing_media

-- DROP TABLE billing_media;

CREATE TABLE billing_media
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(250),
  def smallint DEFAULT 0,
  CONSTRAINT billing_media_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE billing_media OWNER TO postgres;
GRANT ALL ON TABLE billing_media TO postgres;
GRANT SELECT ON TABLE billing_media TO web_order;




