-- Table: service_topology

-- DROP TABLE service_topology;

CREATE TABLE service_topology
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(200),
  def smallint DEFAULT 0,
  CONSTRAINT service_topology_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE service_topology OWNER TO postgres;
GRANT ALL ON TABLE service_topology TO postgres;
GRANT SELECT ON TABLE service_topology TO web_order;




