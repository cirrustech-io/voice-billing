-- Table: order_type

-- DROP TABLE order_type;

CREATE TABLE order_type
(
  id serial NOT NULL,
  title character varying(100),
  description character varying(250),
  def smallint DEFAULT 0,
  CONSTRAINT order_type_pkey PRIMARY KEY (id)
) 
WITHOUT OIDS;
ALTER TABLE order_type OWNER TO postgres;
GRANT ALL ON TABLE order_type TO postgres;
GRANT SELECT ON TABLE order_type TO web_order;




