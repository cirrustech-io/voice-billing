<?php
/*
 * Performs valid form type validation by encryption and decryption
 * using the global $site_sessionname variable value
 * This is to provide security for the online forms. 
 */
 
global $site_sessionname;

$action = isset( $action ) ? $action : false;
$data = isset( $data ) ? $data : false;
$key = isset( $key ) ? $key : false;
$secureKey = '[[secureKey]]';

// Do not proceed if no action is defined.
if( $action == false || $data == false || $key == false ) return false;

$validForms = array( 
	'contest' => "The message for contest",
	'leadSubmission' => "The message for submission",
	'newsletter_subscribe' => "The Message for NewsLetter",
	'newsletter_unsubscribe' => "Newsletter unsubscribing message"
);

if( $action == 'encode' ){
	return encryptInput( $input );
}elseif( $action == 'verify' ){
	return decryptInput( $input, $validForms ); ;
}else{ return false; )

/*
 * Returns the MD5 hash of any valid form type as defined in the array $list
 */
function encryptInput( $type, $list ){
	if( array_key_exists( $type, $list ) ){
		return md5( $site_sessionname . $type );
	}else{
		return false;
	}
}

/*
 * Verifies any MD5 has of a valid form type in the array $list
 * Returns the key value of the form type
 */
function decryptInput( $data, $list ){
	$flag = -1;
	foreach( $list as $key => $value ){
		if( $data == encrypt( $key ) ){
			return $key;
		}
	}
	return false;
}

?>
