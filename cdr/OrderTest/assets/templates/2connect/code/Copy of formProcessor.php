<?php
/*
 * Process web forms
 */
global $site_sessionname;

$validForms = array( 
	'contest' => "The message for contest",
	'leadSubmission' => "The message for submission",
	'newsletter_subscribe' => "The Message for NewsLetter",
	'newsletter_unsubscribe' => "Newsletter unsubscribing message"
);


 // Do not proceed if criteria are not met is defined.
if( 
	!isset( $_POST ) 
	|| !isset( $_POST['x2c01'] ) 
	|| !isset( $_POST['x2c02'] ) 
	|| $_POST['x2c01'] != base64_encode( crypt( $_SESSION['modx.session.created.time'], '$1$' . md5($site_sessionname ) )  ) 
	|| !checkValidForm( 'decrypt', $_POST['x2c02'], $validForms, $site_sessionname ) 
	) 
	{	
		echo '<div class="error">---ERROR---</div>';
		return false;
	}

	
$secureKey = $_POST['x2c01'];
$formType = decryptInput( $_POST['x2c02'], $validForms, $site_sessionname );
$data = $_POST;

switch( $formType ){
	case 'contest':
		echo x2c_contest( $data );
		break;
	case 'leadSubmission':
		echo x2c_leadSubmission( $data );
		break;
	case 'newsletter_subscribe':
		echo x2c_newsletterUnsubscribe( $data );
		break;
	case 'newsletter_unsubscribe':
		echo x2c_newsletterUnsubscribe( $data );
		break;
	default:
		break;
}

/*
 * Performs valid form type validation by encryption and decryption
 * using the global $site_sessionname variable value
 * This is to provide security for the online forms. 
 */
 
function checkValidForm( $action, $input, $list, $session ){
	if( $action == 'encrypt' ){
		return encryptInput( $input, $list, $session );
	}elseif( $action == 'decrypt' ){
		return decryptInput( $input, $list, $session );
	}else{ return false; }
}

/*
 * Returns the MD5 hash of any valid form type as defined in the array $list
 */
function encryptInput( $data, $list, $session ){
	if( array_key_exists( $data, $list ) ){
		return md5( $session . $data );
	}else{
		return false;
	}
}

/*
 * Verifies any MD5 has of a valid form type in the array $list
 * Returns the key value of the form type
 */
function decryptInput( $data, $list, $session ){
	foreach( $list as $key => $value ){
		if( $data == encryptInput( $key, $list, $session ) ){
			return $key;
		}
	}
	return false;
}

/*
 *  Process a Contest Form
 */

function x2c_contest( $formInput ){
	$errors = array();
	$output = '';
	
	// echo print_r( $formInput );
	$input = array(
		'name' => $modx->db->escape( $formInput['name'] ),
		'email' => $modx->db->escape( $formInput['email'] ),
		'city' => $modx->db->escape( $formInput['city'] ),
		'mobile' => $modx->db->escape( $formInput['mobile'] )
	);
	
	print_r( $input );
	
	
	foreach( $input as $field => $value ){
		checkFieldValidity( $field, $value, $errors );
	}
	
	// print_r( $errors );
		
	if( count( $errors ) > 0 ){
		foreach( $errors as $key => $value ){
			$output .= $value . ", ";
		}
		$output .= substr( $output, 0, strlen($output)-2 ) . ".";
		$output .= '<div class="draw_error">' . $output . '</div>';
	}else{
		/*
		 * Write database code here.
		 */
		
		$name = $input['name'];
		$email = $input['email'];
		$mobile = $input['mobile'];
		$city = $input['city'];
		
		$entry_sql = "INSERT INTO contest_entries VALUES( null, '$name', '$email', $mobile, '$city', 0, NOW() );";
		$entry_result = $modx->db->query( $entry_sql );
		if( $entry_result == 1 ){
			$output = '<div class="draw_success">ACCEPTED.</div>';
		}else{
			$output = '<div class="draw_error">DB PROBLEM (contact admin).</div>';
		}
	}
	return $output;
}
// End of Contest form

function x2c_leadSubmission( $formInput ) {}

function x2c_newsletterSubscribe( $formInput ){}

function x2c_newsletterUnsubscribe( $formInput ){}

function checkFieldValidity( $field, $value, &$errors )
{
	switch( $field )
	{
		case 'name':
			if( $value=='' ){ array_push( $errors, "Invalid Name" ); }
			break;
			
		case 'email':
			$pattern = '[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)';
			ereg( $pattern, $value, $regxp_result );
			if( $value=='' || $regxp_result == null ){ array_push( $errors, "Invalid Email" ); }
			break;
			
		case 'city':
			break;
			
		case 'mobile':
			if( $value == '' || strlen( $value )!=8 || ( substr( $value, 0, 2 )!='39' && substr( $value, 0, 2 )!='36' ) ){
				array_push( $errors, "Invalid Mobile Number" );
			}
			break;
			
		default:
			break;
	}
	return true;
}

function sendTextMessage( $phone, $name, $message ){
	// Send an SMS message
}

?>