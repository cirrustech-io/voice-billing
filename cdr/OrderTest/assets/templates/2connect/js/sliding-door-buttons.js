var TwoConnect = {

	start: function(){
		if( $('myajaxmenu') ) TwoConnect.headerLinks();
		if( $('footer') ) TwoConnect.footerLinks();
	},

	footerLinks: function(){
		if( $('footer') ){
			var fLinks = new slidingDoorButtons( 'footer', {buttonHeight:25, buttonFinalPosition: -100, rightImageWidth:11, activeClass:'current'} );
		}
	}, // end of footer links modification

	
	/* Modify the top navigation bar */
	headerLinks: function(){
		
		var ajaxMenu = $$('ul#myajaxmenu li a');
		ajaxMenu.each( function(el,i){
			
			var overFx = new Fx.Styles( el, {duration: 250, wait: false});
			
			if( !el.getParent().hasClass('current') )
			{
				el.addEvent( 'mouseenter', function(el){
					overFx.start({
						'border-top-width': 5
					});
				});
				
				el.addEvent( 'mouseleave', function(el){
					overFx.start({
						'border-top-width': 0
					});
				});
			}
		});
	}//end of header links modification
};

var slidingDoorButtons = new Class({
	options: {
		buttonHeight: 34,				// in pixels
		buttonTotalStops: 4,			// number of slides-1 ( not currently in use )
		buttonFinalPosition: -136,		// in pixels
		buttonCurrentPosition: 0,		// dynamically changing
		rightImageWidth: 6,				// Width of Right Image
		duration: 50,					// time in ms
		periodicalOver: '',				// empty at initialization
		periodicalOut: '',				// empty at initialization
		activeClass: 'active'			// class for the active button
	},

	initialize: function( id, options ){
		this.setOptions( options );
		this.id = id;
		this.buttons = $$('#' + id + ' li a');
		
		this.buttons.each( function(element, index){
			// Create options for each element. This will be helpful in storing the current position of the background shift
			element.options = $merge( this.options );
			
			// Initialize the Fx.Style for each element
			this.assignFx( element );
			
			// Add events to each element
			element.addEvents({
				
				// Start the fading IN transition effect for the element
				'mouseenter': function(){
					$clear(element.options.periodicalOut);
					element.options.periodicalOver = this.fadeIn.periodical(element.options.duration, this, element);
					// this.fadeIn( element );
				}.bind(this), // end of 'mouseenter'
				
				// Start the fading OUT effect for the element
				'mouseleave': function(){
					$clear(element.options.periodicalOver);
					element.options.periodicalOut = this.fadeOut.periodical(element.options.duration, this, element);
					// this.fadeOut( element );
				}.bind(this), // end of 'mouseleave'
				
				// When used in combination with MooTabs this will smoothly transition the button to its initial state
				'click': function(){
					this.fadeOutAll( index );
				}.bind(this) // end of 'click'
				
			}, this);
			
		}, this);
		// end each
	},
	
	// Assign the Fx.Style to the elements 
	assignFx: function( element ){
		element.fxLink = ( window.ie )?( new Fx.Style( element, 'background-position-y', {duration: 0} ) ):( new Fx.Style( element, 'background-position', {duration: 0} ) );
		element.fxList = ( window.ie )?( element.fxList = new Fx.Style( element.getParent(), 'background-position-y', {duration: 0}) ):( new Fx.Style( element.getParent(), 'background-position', {duration: 0}) );
	},
	
	// Perform the Fade IN effect on the element
	fadeIn: function( element ){
		if( element.options.buttonCurrentPosition > element.options.buttonFinalPosition && !element.getParent().hasClass( this.options.activeClass ) )
		{
			element.$tmp.buttonNewPosition = element.options.buttonCurrentPosition-element.options.buttonHeight;
			element.$tmp.fxA = element.fxLink.start( ( window.ie )?( element.$tmp.buttonNewPosition ):( (element.getSize().size.x-element.options.rightImageWidth) + ' ' + ( element.$tmp.buttonNewPosition ) ) );
			element.$tmp.fxL = element.fxList.start( (window.ie )?( element.$tmp.buttonNewPosition ):( '0 ' + ( element.$tmp.buttonNewPosition ) ) );
			element.options.buttonCurrentPosition = element.$tmp.buttonNewPosition;
		}
		else
		{
			$clear(element.options.periodicalOver);
		}
	},
	
	// Perform the Fade OUT effect on the element
	fadeOut: function( element){
		if( element.options.buttonCurrentPosition < 0 && !element.getParent().hasClass( this.options.activeClass ))
		{
			element.$tmp.buttonNewPosition = element.options.buttonCurrentPosition+element.options.buttonHeight;
			element.$tmp.fxA = element.fxLink.start( ( window.ie )?( element.$tmp.buttonNewPosition ):( (element.getSize().size.x-element.options.rightImageWidth) + ' ' + ( element.$tmp.buttonNewPosition ) ) );
			element.$tmp.fxL = element.fxList.start( ( window.ie )?( element.$tmp.buttonNewPosition ):( '0 ' + ( element.$tmp.buttonNewPosition ) ) );
			element.options.buttonCurrentPosition = element.$tmp.buttonNewPosition;
		}
		else
		{
			$clear(element.options.periodicalOut);
		}
	},
	
	// To be used with MooTabs. Deactivate the current tab smoothly.
	fadeOutAll: function( index ){
		this.buttons.each( function(e,i){
			if(index!=i){ e.periodicalOut = this.fadeOut.periodical(this.duration, this, e); }
		}, this);
	}

});	// end of class definition

// implement certain classes in the base class
slidingDoorButtons.implement(new Options, new Events);

/*
window.addEvent( 'domready', function(){ 
	var fLinks = ( $('footer') )
					?( new slidingDoorButtons( 'footer', {buttonHeight:25, buttonFinalPosition: -100, rightImageWidth:11, activeClass:'current'} ) )
					:'';
});
*/

window.addEvent('domready', TwoConnect.start);