<?php
	
	$page_control = array();

		
	#title,html-tag,type,id/name,size,text value/default-value/default-index,read-only(0/1),action,class,populate-field,mandatory(0/1),visible(0/1),tip 

		
	{{{ #login_page
	$ctrl_login = array();
	$ctrl_login[] = array('User Name','input','text','txt_username',50,'',0,'','',null,1,1,null);
	$ctrl_login[] = array('Password','input','password','txt_password',50,'',0,'','',null,1,1,null);
	$page_control['login']['No_Title'] = $ctrl_login;
	unset($ctrl_login);
	
	$ctrl_login = array();	
	$ctrl_login[] = array('Submit Btn','input','submit','btn_login',25,'Login',0,'','custombutton',null,0,1,null);
	$ctrl_login[] = array('Hidden Val','input','hidden','hdn_posted',25,'yes',1,'','',null,0,0,null);
	$page_control['login']['Buttons'] = $ctrl_login; 
	unset($ctrl_login);
	
	}}}
	
	{{{ #cust_details_page	
	
	$ctrl_cust = array();
	$ctrl_cust['sp4'] = array('Order Type',					'select','order_id',					'cmb_ordertype',		50,0,0,'','',															'order_type',1,1,null);
	$ctrl_cust[] = array('Order Type Tip',					'span','span',						    'span_order_type_tip',250,'Use Order Type dropdown list to review/modify existing orders',0,'','',null,0,1,null);
	$ctrl_cust['sp5'] = array('Existing Orders',			'select','special',						'cmb_orderlist',		50,0,0,'','',															'order_list',0,0,null);
	$ctrl_cust[] = array('Order Date',						'input','text',							'txt_orderdate',		50,'',1,'order_date','',												'date',1,1,null);
	$ctrl_cust[] = array('Reqd. Service Activation Date','input','text',							'txt_requireddate',		50,'',1,'required_date','',												'date',1,1,null);
	$ctrl_cust[] = array('Agg Date Tip',					'span','span',						    'span_agreement_date_tip',250,'Required service activation date is subject to confirmation by 2Connect',0,'','',					null,0,1,null);
	$ctrl_cust[] = array('Agreement Number',				'input','text',							'txt_agreementnumber',	50,'',0,'','',															'agreement_number',0,1,null);
	$ctrl_cust[] = array('Agreement Date',					'input','text',							'txt_agreementdate',	50,'',1,'agreement_date','',											'date',0,1,null);
	#$ctrl_cust[] = array('2Connect Account Executive',		'select','account_executive_id',		'cmb_accexecutive',		50,0,0,'','',															'account_executive',1,1,null);
	$ctrl_cust[] = array('2Connect Account Executive',		'input','text',							'txt_accexecutive',		50,'',0,'','',						 									'account_executive',1,1,null);
	$ctrl_cust[] = array('Purchase Order Number',			'input','text',							'txt_ponumber',			50,'',0,'','',															'p_o_number',0,1,null);
	$page_control['cust_info']['Order Details'] = $ctrl_cust;
	
	unset($ctrl_cust);
	
	
	$ctrl_cust = array();
	$ctrl_cust[] = array('Load Default Btn',				'button','button',						'btn_default',			25,'Autofill Customer Details',0,'loadDefault','',				null,0,1,null);
	$ctrl_cust[] = array('Company Legal Name',				'input','text',							'txt_companylegalname',	50,'',0,'','',						'company_legal_name',1,1,null);
	$ctrl_cust[] = array('Address',							'input','text',							'txt_companyaddress',	50,'',0,'','',						'address',1,1,null);
	$ctrl_cust[] = array('Building',						'input','text',							'txt_companybuilding',	50,'',0,'','',						'building',0,1,null);
	$ctrl_cust[] = array('Town/City',						'input','text',							'txt_companytowncity',	50,'',0,'','',						'town_city',1,1,null);
	$ctrl_cust[] = array('State/Province',						'input','text',						'txt_companyprovince',	50,'',0,'','',					'province',0,1,null);
	$ctrl_cust[] = array('Zip/Postal Code',						'input','text',							'txt_companypostcode',	50,'',0,'','',						'postcode',0,1,null);
	$ctrl_cust[] = array('Country',							'select','country_id',					'cmb_companycountry',	50,0,0,'','',					    'country',0,1,null);
	$ctrl_cust[] = array('Contact Name',					'input','text',							'txt_contactname',		50,'',0,'','',						'contact_name',1,1,null);
	$ctrl_cust[] = array('Title',							'input','text',							'txt_contacttitle',		50,'',0,'','',						'contact_title',0,1,null);
	$ctrl_cust[] = array('Email',							'input','text',							'txt_contactemail',		50,'',0,'','',						'contact_email',1,1,null);
	$ctrl_cust[] = array('Telephone',						'input','text',							'txt_contacttelephone',	50,'',0,'','',						'contact_telephone',1,1,null);
	$ctrl_cust[] = array('Mobile',							'input','text',							'txt_contactmobile',	50,'',0,'','',						'contact_mobile',0,1,null);
	$ctrl_cust[] = array('Fax',								'input','text',							'txt_contactfax',		50,'',0,'','',						'contact_fax',1,1,null);
	$ctrl_cust[] = array('Technical Contact',				'input','text',							'txt_techcontact',		50,'',0,'','',						'tech_contact_name',0,1,null);
	$ctrl_cust[] = array('Tech. Contact Email',				'input','text',							'txt_techemail',		50,'',0,'','',						'tech_contact_email',0,1,null);
	$ctrl_cust[] = array('Tech. Contact Telephone',			'input','text',							'txt_techcontacttelephone',	50,'',0,'','',					'tech_contact_telephone',0,1,null);
	$page_control['cust_info']['Contact Details'] = $ctrl_cust;
	unset($ctrl_cust);
	
	
	$ctrl_cust = array();	
	$ctrl_cust[] = array('Company Name',					'input','text',							'txt_billcompanyname',		50,'',0,'','',					'bill_company_name',1,1,null);
	$ctrl_cust[] = array('Address',							'input','text',							'txt_billaddress',			50,'',0,'','',					'bill_address',1,1,null);
	$ctrl_cust[] = array('Building',						'input','text',							'txt_billbuilding',			50,'',0,'','',					'bill_building',0,1,null);
	$ctrl_cust[] = array('Town/City',						'input','text',							'txt_billtowncity',			50,'',0,'','',					'bill_town_city',1,1,null);
	$ctrl_cust[] = array('State/Province',					'input','text',							'txt_billprovince',			50,'',0,'','',					'bill_province',0,1,null);
	$ctrl_cust[] = array('Zip/Postal Code',					'input','text',							'txt_billpostcode',			50,'',0,'','',					'bill_postcode',0,1,null);
	$ctrl_cust[] = array('Country',							'select','bill_country_id',				'cmb_billcountry',			50,0,0,'','',					'country',0,1,null);
	$ctrl_cust[] = array('Contact Name',					'input','text',							'txt_billcontactname',		50,'',0,'','',					'bill_contact_name',0,1,null);
	$ctrl_cust[] = array('Title',							'input','text',							'txt_billcontacttitle',		50,'',0,'','',					'bill_title',0,1,null);
	$ctrl_cust[] = array('Email',							'input','text',							'txt_billcontactemail',		50,'',0,'','',					'bill_email',0,1,null);
	$ctrl_cust[] = array('Telephone',						'input','text',							'txt_billcontacttel',		50,'',0,'','',					'bill_telephone',0,1,null);
	$ctrl_cust[] = array('Mobile',							'input','text',							'txt_billcontactmobile',	50,'',0,'','',					'bill_contact_mobile',0,1,null);
	$ctrl_cust[] = array('Fax',								'input','text',							'txt_billcontactfax',		50,'',0,'','',					'bill_contact_fax',0,1,null);
	$ctrl_cust[] = array('VAT Number',						'input','text',							'txt_billvatnumber',		50,'',0,'','',					'vat_number',0,1,null);
	$ctrl_cust[] = array('Billing Cycle',					'input','text',							'txt_billingcycle',			50,'Monthly',1,'','',			'payment_frequency',1,1,null);
	$ctrl_cust[] = array('Initial Contract Term',			'select','initial_contract_term_id',	'cmb_initcontractterm',		50,0,0,'','',					'contract_term',1,1,null);
	$ctrl_cust[] = array('Billing Media',					'select','billing_media_id',			'cmb_billingmedia',			50,0,0,'','',					'billing_media',0,1,null);
	$ctrl_cust[] = array('Billing Currency',				'select','billing_currency_id',			'cmb_billingcurrency',		50,0,0,'','',					'billing_currency',1,1,null);
	$ctrl_cust[] = array('Payment Method',					'select','payment_method_id',			'cmb_paymentmethod',		50,0,0,'','',					'payment_method',0,1,null);
	$ctrl_cust[] = array('Pay Method Tip',					'span','span',						    'span_pay_method_tip',250,'Cheque payment is only available for Bahrain-based billing adddress.',0,'','',null,0,1,null);
	$page_control['cust_info']['Billing Details'] = $ctrl_cust;
	unset($ctrl_cust);
	
	
	$ctrl_cust = array();	
	$ctrl_cust[] = array('Submit Btn',						'input','submit',						'btn_save',					25,'Save Customer Details',0,'','',								null,0,1,null);
	$ctrl_cust[] = array('Clear Btn',						'button','button',						'btn_clear',				25,'Clear Section',0,'clearFields','',				null,0,1,null);
	$ctrl_cust[] = array('Clear Form Btn',					'button','button',					    'btn_clearform',			25,'Cancel Order',0,'clearOrder','',					        null,0,1,null);
	$ctrl_cust[] = array('Hidden Val1',						'input','hidden',						'hdn_posted',				25,'yes',1,'','',												null,0,0,null);
	$ctrl_cust[] = array('Hidden Val2',						'input','hidden',						'hdn_clear',				25,'no',1,'','',												null,0,0,null);
	$page_control['cust_info']['Buttons'] = $ctrl_cust;
	unset($ctrl_cust);
	
	
	}}}
		
	{{{ #service_details_page
	$ctrl_serv_dtl = array();
	
	$ctrl_serv_dtl[] = array('Topology',					'select','topology_id',					'cmb_topology',				50,0,0,'','',					'service_topology',1,1,null);
	
	#Used inside load server details
	$ctrl_serv_dtl['sp1'] = array('Type of site',			'select','type_of_site_id',				'cmb_typesite',				50,0,0,'','',					'site_type_detail',0,0,null);
	$ctrl_serv_dtl['sp2'] = array('Site Status',			'select','site_status_id',				'cmb_sitestatus',			50,0,0,'','',					'site_status',0,0,null);
	
	$ctrl_serv_dtl[] = array('Multi-port Service',			'select','multiport',					'cmb_multiport',			50,0,0,'','',					'yes_no',0,1,null);
	$ctrl_serv_dtl[] = array('Internet Access Bundle',		'select','access_bundle',				'cmb_accessbundle',			50,0,0,'','',					'yes_no',0,1,null);
	$ctrl_serv_dtl[] = array('CoS Active on Service',		'select','cos',							'cmb_cos',					50,0,0,'','',					'yes_no',0,1,null);
	$ctrl_serv_dtl[] = array('Bandwidth',					'select','bandwidth_id',				'cmb_bandwidth',			50,0,0,'','',					'bandwidth_detail',0,1,null);
	
	#Used inside load server details
	$ctrl_serv_dtl['sp3'] = array('Bandwidth Details',		'input','text',							'txt_bdetail',				100,'',0,'','',					'bandwidth_description',0,0,null);
	
	
	$page_control['service_detail']['No_Title1'] = $ctrl_serv_dtl;	
	unset($ctrl_serv_dtl);
	
	
	$ctrl_serv_dtl = array();	
	$ctrl_serv_dtl[] = array('Company Name',				'input','text',							'txt_acompany',				50,'',0,'','',					'a_company_name',1,1,null);
	
	
	$ctrl_serv_dtl[] = array('Address',						'input','text',							'txt_aaddress',				50,'',0,'','',					'a_address',1,1,null);
	$ctrl_serv_dtl[] = array('Building',					'input','text',							'txt_abuilding',			50,'',0,'','',					'a_building',0,1,null);
	$ctrl_serv_dtl[] = array('Area',						'input','text',							'txt_aarea',				50,'',0,'','',					'a_area',0,1,null);
	$ctrl_serv_dtl[] = array('Road',						'input','text',							'txt_aroad',				50,'',0,'','',					'a_road',0,1,null);
	$ctrl_serv_dtl[] = array('Block',						'input','text',							'txt_ablock',				50,'',0,'','',					'a_block',0,1,null);
	$ctrl_serv_dtl[] = array('Town/City',					'input','text',							'txt_atowncity',			50,'',0,'','',					'a_towncity',1,1,null);
	$ctrl_serv_dtl[] = array('State/Province',				'input','text',							'txt_aprovince',			50,'',0,'','',					'a_province',0,1,null);
	$ctrl_serv_dtl[] = array('Zip/Postal Code',				'input','text',							'txt_apobox',				50,'',0,'','',					'a_pobox',0,1,null);
	$ctrl_serv_dtl[] = array('Country',						'select','a_country_id',	    		'cmb_acountry',				50,0,0,'','',					'country',0,1,null);
	
	$ctrl_serv_dtl[] = array('Telehouse',				    'input','checkbox',					    'chk_atelehouse',			50,'',0,'','',					'a_telehouse',0,1,'');
	
	$ctrl_serv_dtl[] = array('Suite',				        'input','text',					        'txt_asuite',				50,'',0,'','',					'a_suite',0,1,null);
	$ctrl_serv_dtl[] = array('Row',				            'input','text',					        'txt_arow',					50,'',0,'','',					'a_row',0,1,null);
	$ctrl_serv_dtl[] = array('Rack',				        'input','text',					        'txt_arack',				50,'',0,'','',					'a_rack',0,1,null);
	$ctrl_serv_dtl[] = array('Vert',				        'input','text',					        'txt_avert',				50,'',0,'','',					'a_vert',0,1,null);
	
	$page_control['service_detail']['Address - A End'] = $ctrl_serv_dtl; 
	unset($ctrl_serv_dtl);
	
	$ctrl_serv_dtl = array();	
	$ctrl_serv_dtl[] = array('Company Name',				'input','text',							'txt_bcompany',				50,'',0,'','',					'b_company_name',1,1,null);
	$ctrl_serv_dtl[] = array('Address',						'input','text',							'txt_baddress',				50,'',0,'','',					'b_address',1,1,null);
	$ctrl_serv_dtl[] = array('Building',					'input','text',							'txt_bbuilding',			50,'',0,'','',					'b_building',0,1,null);
	$ctrl_serv_dtl[] = array('Area',						'input','text',							'txt_barea',				50,'',0,'','',					'b_area',0,1,null);
	$ctrl_serv_dtl[] = array('Road',						'input','text',							'txt_broad',				50,'',0,'','',					'b_road',0,1,null);
	$ctrl_serv_dtl[] = array('Block',						'input','text',							'txt_bblock',				50,'',0,'','',					'b_block',0,1,null);
	$ctrl_serv_dtl[] = array('Town/City',					'input','text',							'txt_btowncity',			50,'',0,'','',					'b_towncity',1,1,null);
	$ctrl_serv_dtl[] = array('State/Province',				'input','text',							'txt_bprovince',			50,'',0,'','',					'b_province',0,1,null);
	$ctrl_serv_dtl[] = array('Zip/Postal Code',				'input','text',							'txt_bpobox',				50,'',0,'','',					'b_pobox',0,1,null);
	$ctrl_serv_dtl[] = array('Country',						'select','b_country_id',				'cmb_bcountry',				50,0,0,'','',					'country',0,1,null);
	$ctrl_serv_dtl[] = array('Telehouse',				    'input','checkbox',					    'chk_btelehouse',			50,'',0,'','',					'b_telehouse',0,1,'');
	
	$ctrl_serv_dtl[] = array('Suite',				        'input','text',					        'txt_bsuite',				50,'',0,'','',					'b_suite',0,1,null);
	$ctrl_serv_dtl[] = array('Row',				            'input','text',					        'txt_brow',					50,'',0,'','',					'b_row',0,1,null);
	$ctrl_serv_dtl[] = array('Rack',				        'input','text',					        'txt_brack',				50,'',0,'','',					'b_rack',0,1,null);
	$ctrl_serv_dtl[] = array('Vert',				        'input','text',					        'txt_bvert',				50,'',0,'','',					'b_vert',0,1,null);
	
	$page_control['service_detail']['Address - B End'] = $ctrl_serv_dtl; 
	unset($ctrl_serv_dtl);
	
	$ctrl_serv_dtl = array();	
	$ctrl_serv_dtl[] = array('Pricing',						'label','label',						'label_pricing_details',	250,'Subject to confirmation by 2Connect.',0,'','',					null,0,1,null);
	$page_control['service_detail']['No_Title2'] = $ctrl_serv_dtl; 
	unset($ctrl_serv_dtl);
	
	$ctrl_serv_dtl = array();
	$ctrl_serv_dtl[] = array('Additional Details',			'textarea','textarea',					'txt_additionaldetails',	50,'',0,'','',														'additional_information',0,1,null);
	$page_control['service_detail']['No_Title3'] = $ctrl_serv_dtl; 
	unset($ctrl_serv_dtl);
	
	
	
	$ctrl_serv_dtl = array();	
	$ctrl_serv_dtl[] = array('Spacer1',							'input','text',	    				'text_spacer',				50,0,0,'','',				    'spacer1',0,0,null);
	$ctrl_serv_dtl[] = array('Spacer2',							'input','text',	    				'text_spacer',				50,0,0,'','',				    'spacer2',0,0,null);
	$ctrl_serv_dtl[] = array('Hidden Val1',						'input','hidden',					'hdn_posted',				25,'yes',1,'','',				null,0,0,null);
	$ctrl_serv_dtl[] = array('Hidden Val2',						'input','hidden',					'hdn_clear',				25,'no',1,'','',												null,0,0,null);
	$ctrl_serv_dtl[] = array('Submit Btn',						'input','submit',					'btn_save',					25,'Save Service Details',0,'','',				null,0,1,null);
	$ctrl_serv_dtl[] = array('Clear Btn',						'button','button',					'btn_clear',				25,'Clear Section',0,'clearFields','',null,0,1,null);
	$ctrl_serv_dtl[] = array('Clear Form Btn',					'button','button',					'btn_clearform',			25,'Cancel Order',0,'clearOrder','',					        null,0,1,null);
	$page_control['service_detail']['Buttons'] = $ctrl_serv_dtl;
	unset($ctrl_serv_dtl);
	


	
	}}}
	
{{{ #order_summary
	$ctrl_summary = array();
	$ctrl_summary[] = array('Order Details',					'span','span',						'span_order_details',			250,'',0,'','',					'summary',0,1,null);
	$page_control['order_summary']['No_Title1'] = $ctrl_summary;
	unset($ctrl_summary);
		
	$ctrl_summary = array();	
	$ctrl_summary[] = array('Print Btn',						'button','button',					'btn_print',					50,'Print Order',0,'return print_order','',  null,0,1,null);
	$ctrl_summary[] = array('Submit Btn',						'button','button',					'btn_submit',					50,'Submit Order to 2Connect',0,'','',  null,0,1,null);
	$ctrl_summary[] = array('Hidden Order No',					'input','hidden',					'hdn_order_no',					25,1,1,'','',							            'service_order_no',0,0,null);
	$ctrl_summary[] = array('Hidden Val',						'input','hidden',					'hdn_posted',					25,'yes',1,'','',						             null,0,0,null);
	$page_control['order_summary']['Buttons'] = $ctrl_summary; 
	unset($ctrl_summary);
	
	}}}	
	
	{{{ #print_order
	$ctrl_printorder = array();
	$ctrl_printorder[] = array('Order Details',					'span','span',						'span_order_details',			250,'',0,'','',					'summary',0,1,null);
	$page_control['print_order']['No_Title1'] = $ctrl_printorder;
	unset($ctrl_printorder);
		
	
	}}}	
	
	{{{ #instructions
	$ctrl_instructions = array();
	$ctrl_instructions[] = array('Instructions',					'span','span',					'span_instructions',			250,'',0,'','',					'instructions',0,1,null);
	$ctrl_instructions[] = array('Start Btn',						   'input','submit',			     'btn_start',					25,'Proceed',0,'','',				null,0,1,null);
	$ctrl_instructions[] = array('Hidden Val',						'input','hidden',					'hdn_posted',					25,'yes',1,'','',						             null,0,0,null);
	$page_control['instructions']['No_Title1'] = $ctrl_instructions;
	unset($ctrl_instructions);
	
	
	}}}	
	
	
	
	

	
	
	
	




?>