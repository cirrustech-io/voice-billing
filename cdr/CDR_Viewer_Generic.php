<?php

	include_once('DBAccessor.php');

	//check if the session is still alive
	session_start();
	if (empty($_SESSION['user_name'])) {
		session_write_close(); header("Location: Login.php"); exit(0);
	} else {


		$p_arr = array();
		$form_elements = array();
		$dir_values = array('O'=>'Outgoing','I'=>'Incoming');
		$limit_values = array(50=>50,100=>100,250=>250,500=>500,1000=>1000);

		$error_msg = '*** NexTone CDRs are retrieved with a latency of 45 mins-1 hour<br>You may view records upto 6 months prior to current date';
		$output = '';
		$date=date("Y-m-d_H-i-s");
		$file="Output_$date.csv";
		
		$cdr_table = '';
		$partition_select = "";
		$carrier_select = "";
		$direction_select = "";
		$sdate_text = "";
		$shour_select = "";
		$sminute_select = "";
		$ssecond_select = "";
		$edate_text = "";
		$ehour_select = "";
		$eminute_select = "";
		$esecond_select = "";
		$limit_select = "";
		$error_chkbox = "";
		$reporttype_select = "";


		$subject = (isset($_POST['hdn_subject']))?$_POST['hdn_subject']:'NA';
		$partition = (isset($_POST['partition_name_field']))?$_POST['partition_name_field']:'';
		$direction = (isset($_POST['direction_field']))?$_POST['direction_field']:'O';
		$ep = (isset($_POST['carrier_field']))?$_POST['carrier_field']:'';
		$sdate = (isset($_POST['date_field']))?$_POST['date_field']:date('Y/m/d');
		$shour = (isset($_POST['time_shour_field']))?$_POST['time_shour_field']:'0';
		$sminute = (isset($_POST['time_sminute_field']))?$_POST['time_sminute_field']:'0';
		$ssecond = (isset($_POST['time_ssecond_field']))?$_POST['time_ssecond_field']:'0';
		$edate = (isset($_POST['date_field2']))?$_POST['date_field2']:date('Y/m/d');
		$ehour = (isset($_POST['time_ehour_field']))?$_POST['time_ehour_field']:'23';
		$eminute = (isset($_POST['time_eminute_field']))?$_POST['time_eminute_field']:'59';
		$esecond = (isset($_POST['time_esecond_field']))?$_POST['time_esecond_field']:'59';
		$limit = (isset($_POST['cdrlimit_field']))?$_POST['cdrlimit_field']:'';
		$error = (isset($_POST['err_ok_field']))?$_POST['err_ok_field']:'';
		$report_type = (isset($_POST['reporttype_field']))?$_POST['reporttype_field']:'';
		$subject = (isset($_POST['hdn_subject']))?$_POST['hdn_subject']:'';
		
		$partition_details = getPartitionDetails();
		if(pg_num_rows($partition_details) > 0){
			
			for($i=0;$i<pg_num_rows($partition_details);$i++){
				$pid= trim(pg_result($partition_details,$i,0));
				$pname = trim(pg_result($partition_details,$i,1));
				$carrier = trim(pg_result($partition_details,$i,2));
				$endpoint = trim(pg_result($partition_details,$i,3));

				$p_arr[$pid] = (array_key_exists($pid,$p_arr))?"{$p_arr[$pid]}||{$carrier};{$endpoint}":"{$pid}||{$pname}||{$carrier};{$endpoint}";
			}
		}  else {
			$error_msg = 'DB Error';
		}

		#print_r($p_arr);
		#exit(0);

		$form_elements = initializeForm($p_arr,$dir_values,$limit_values,$partition,$direction,$ep,$sdate,$shour,$sminute,$ssecond,$edate,$ehour,$eminute,$esecond,$limit,$error,$report_type);



		if(count($form_elements) > 0){
			$partition_select = $form_elements[0];
			$carrier_select = $form_elements[1];
			$direction_select = $form_elements[2];
			$sdate_text = $form_elements[3];
			$shour_select = $form_elements[4];
			$sminute_select = $form_elements[5];
			$ssecond_select = $form_elements[6];
			$edate_text = $form_elements[7];
			$ehour_select = $form_elements[8];
			$eminute_select = $form_elements[9];
			$esecond_select = $form_elements[10];
			$limit_select = $form_elements[11];
			$error_chkbox = $form_elements[12];
			$reporttype_select = $form_elements[13];
		} else {
			$error_msg = 'Could not initialize form';
		}

	}

	// build the form action
	$form_action = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");



	if ((isset($_POST["posted"])) && ($_POST["posted"] == "yay")){


		$sdate = date('Y/m/d H:i:s',strtotime("$sdate $shour:$sminute:$ssecond"));
		$edate = date('Y/m/d H:i:s',strtotime("$edate $ehour:$eminute:$esecond"));

		$cdr_table = '';
		switch($report_type){
			case 'S': $cdr_table = createSummary($subject,$direction,$ep,$sdate,$edate,$error_msg,$output);
				break;
			case 'D':  $cdr_table = createDetail($subject,$direction,$ep,$sdate,$edate,$limit,$error,$error_msg,$output);
				break;
		}
		
		
		$ofile = fopen("output/$file",'w');
		fwrite($ofile,$output);

	} 

	function createSummary($subject,$direction,$ep,$sdate,$edate,&$error_msg,&$output){
		$summary = fetchSummary($direction,$ep,$sdate,$edate);
		$cdr_table = "<table id='results'>\n";
		$cdr_table .= "<tr class='header'>
						<td>Name</td>
						<td>Total Calls</td>
						<td>Error Calls</td>
						<td>Completed Calls</td>
						<td>Minutes</td>
						<td>Avg Call Duration</td>
						<td>ASR</td>
						<td>First Call Time</td>
						<td>Last Call Time</td>			
					  </tr >\n";
		$output = '';
		$output .= "TRAFFIC SUMMARY - {$subject} - From {$sdate} To {$edate}\r\n";
		$output .= "Name,Total Calls,Error Calls,Completed Calls,Minutes,Avg Call Duration,ASR,First Call Time, Last Call Time\r\n";
		
		if(pg_num_rows($summary) > 0){
			$total = 0;
			$error = 0;
			$completed = 0;
			$minutes = 0;
			$avg_duration = 0;
			$asr = 0;
			$first_call_time = '';
			$last_call_time = '';
			while($line = pg_fetch_array($summary, null, PGSQL_ASSOC)){

					$start_time = $line['start_time'];
					$call_duration = $line['call_duration'];
					$error_int = $line['call_error_int'];
			
					$total++;
					$first_call_time = (($first_call_time == '') || ($start_time < $first_call_time))?$start_time:$first_call_time; 
					$last_call_time = (($last_call_time == '') || ($start_time > $last_call_time))?$start_time:$last_call_time;
					
					$error += ($error_int <> 0)?1:0;
			
					$minutes += (trim($call_duration) <> '00:00:00' )?round((hms2sec(trim($call_duration)))/60,2):0;
		
			}
			
			
			$completed = $total - $error;
			$avg_duration = round($minutes/$completed,2);
			$asr = round(($completed/$total)*100,2);
		
			$row = "";
			$row .= "<tr>";
			$row .= "<td>{$subject}</td>";
			$row .= "<td>{$total}</td>";
			$row .= "<td>{$error}</td>";
			$row .= "<td>{$completed}</td>";
			$row .= "<td>{$minutes}</td>";
			$row .= "<td>{$avg_duration}</td>";
			$row .= "<td>{$asr}</td>";
			$row .= "<td>{$first_call_time}</td>";
			$row .= "<td>{$last_call_time}</td>";
			$row .= "</tr>";
			$cdr_table .= "$row\n";
			$output .= "{$subject},{$total},{$error},{$completed},{$minutes},{$avg_duration},{$asr},{$first_call_time},{$last_call_time},\r\n";
			
		} else {
				$error_msg = 'No results found';
				$output = "{$error_msg}\r\n";
		}

		$cdr_table .= "</table>\n";
		return $cdr_table;
		
	}
		
	function createDetail($subject,$direction,$ep,$sdate,$edate,$limit,$error,&$error_msg,&$output){
		
		$cdrs = fetchCDR($direction,$ep,$sdate,$edate,$limit,$error);
		
		$cdr_table = "<table id='results'>\n";
		$cdr_table .= "<tr class='header'>
						<td>Start Time</td>
						<td>Hunt</td>
						<td>ANI</td>
						<td>Dialed At Source</td>
						<td>Dialed At Dest</td>
						<!--<td>Cleaned</td>-->
						<td>Source IP</td>
						<td>Source EP</td>
						<td>Dest IP</td>
						<td>Dest EP</td>
						<td>Duration</td>
						<!--<td>Call Error Int</td>-->
						<td>Call Error Str</td>
						<td>ISDN Cause Code</td>
						
					  </tr >\n";
		$output = '';
		$output .= "CALL DETAIL SUMMARY - {$subject} - From {$sdate} To {$edate}\r\n";
		$output .= "Start Time,Hunt,ANI,Dialed At Source,Dialed At Dest,Source IP,Source EP,Dest IP,Dest EP,Duration,Call Error Str,ISDN Cause Code\r\n";
		
		if(pg_num_rows($cdrs) > 0){
			while($line = pg_fetch_array($cdrs, null, PGSQL_ASSOC)){

							$row = "";
							$row .= "<tr>";
							$row .= "<td>{$line['start_time']}</td>";
							$row .= "<td>{$line['hunting_attempts']}</td>";
							$row .= "<td>{$line['ani']}</td>";
							$row .= "<td>{$line['called_party_on_src']}</td>";
							$row .= "<td>{$line['called_party_on_dst']}</td>";
							$row .= "<!--<td>{$line['clean_number']}</td>-->";
							$row .= "<td>{$line['originator_ip']}</td>";
							$row .= "<td>{$line['call_source_regid']}/{$line['call_source_uport']}</td>";
							$row .= "<td>{$line['terminator_ip']}</td>";
							$row .= "<td>{$line['call_dest_regid']}/{$line['call_dest_uport']}</td>";
							$row .= "<td>{$line['call_duration']}</td>";
							$row .= "<!--<td>{$line['call_error_int']}</td>-->";
							$row .= "<td>{$line['call_error_str']}</td>";
							$row .= "<td>{$line['isdn_cause_code']}</td>";

					  		$row .= "</tr>";
					  		$cdr_table .= "$row\n";
							$output .= "{$line['start_time']},{$line['hunting_attempts']},{$line['ani']},{$line['called_party_on_src']},{$line['called_party_on_dst']},";
							$output .= "{$line['originator_ip']},{$line['call_source_regid']}/{$line['call_source_uport']},";
							$output .= "{$line['terminator_ip']},{$line['call_dest_regid']}/{$line['call_dest_uport']},";
							$output .= "{$line['call_duration']},{$line['call_error_str']},{$line['isdn_cause_code']}\r\n";

			}
		} else {
				$error_msg = 'No results found';
				$output .= "{$error_msg}\r\n";
		}

		$cdr_table .= "</table>\n";
		return $cdr_table;
	}

	function initializeForm($p_arr,$dir_values,$limit_values,$partition,$direction,$endpoint,$sdate,$shour,$sminute,$ssecond,$edate,$ehour,$eminute,$esecond,$limit,$error,$report_type){
		$partition_select = "";
		$carrier_select = "";
		$direction_select = "";
		$sdate_text = "";
		$shour_select = "";
		$sminute_select = "";
		$ssecond_select = "";
		$edate_text = "";
		$ehour_select = "";
		$eminute_select = "";
		$esecond_select = "";
		$reporttype_select = "";
		$limit_select = "";
		$error_chkbox = "";


		$flag_reload = false;

		$partition_select = "<select name='partition_name_field' id='partition_name_field'  onchange=PopulateCarrierList(document.getElementById('carrier_field').value)>\n";
		foreach($p_arr as $key=>$val){
			$data = explode("||",$val);
			
			if($partition == ''){
				$partition_select .= "<option value={$val} selected>{$data[1]}</option>\n";	
			} else {
				$partition_select .= ($partition==$val)?"<option value={$val} selected>{$data[1]}</option>\n":"<option value={$val}>{$data[1]}</option>\n";
			}
			if($partition == $val){
				$flag_reload = true;
				$endpoint_data = $data;
			}
		}
		$partition_select .= "</select>";

		if(!$flag_reload){
			$endpoint_data = $data;
		}

		/*
		echo "Partition = $partition<br>";
		print_r($endpoint_data);
		exit(0);
		*/

		$carrier_select .= "<select name='carrier_field' id='carrier_field'>\n";
		for($i=2;$i<count($endpoint_data);$i++){
			$eps = explode(";",$endpoint_data[$i]);
			$carrier_select .= ($endpoint==$eps[1])?"<option value={$eps[1]} selected>$eps[0]</option>\n":"<option value={$eps[1]}>$eps[0]</option>\n";
		}
		$carrier_select .= "</select>";

		$direction_select .= "<select id='direction_field' name='direction_field'>\n";
		foreach($dir_values as $key=>$val){
			$direction_select .= ($direction==$key)?"<option value={$key} selected>{$val}</option>":"<option value={$key}>{$val}</option>";
		}
		$direction_select .= "</select>";


		$sdate_text = "<input type='text' id='date_field' name='date_field' readonly size='10' value= {$sdate} /><button id='trigger' class='btn'>...</button>";

		$shour_select .= "<select id='time_shour_field' name='time_shour_field'>";
		for($i=0;$i<24;$i++){
			$shour_select .= ($shour==$i)?"<option value=$i selected>$i</option>\n":"<option value=$i>$i</option>\n";
		}
		$shour_select .= "</select>";


		$sminute_select	.= "<select id='time_sminute_field' name='time_sminute_field'>";
		for($i=0;$i<60;$i++){
			$sminute_select	.= ($sminute==$i)?"<option value=$i selected>$i</option>\n":"<option value=$i>$i</option>\n";
		}
		$sminute_select	.= "</select>";

		$ssecond_select .= "<select id='time_ssecond_field' name='time_ssecond_field'>";
		for($i=0;$i<60;$i++){
			$ssecond_select .= ($ssecond==$i)? "<option value=$i selected>$i</option>\n":"<option value=$i>$i</option>\n";
		}
		$ssecond_select .= "</select>";


		$edate_text = "<input type='text' id='date_field2' name='date_field2' readonly size='10' value={$edate} /><button id='trigger2' class='btn'>...</button>";

		$ehour_select .= "<select id='time_ehour_field' name='time_ehour_field'>";
		for($i=0;$i<24;$i++){
			$ehour_select .= ($ehour==$i)?"<option value=$i selected>$i</option>\n":"<option value=$i>$i</option>\n";
		}
		$ehour_select .= "</select>";


		$eminute_select	.= "<select id='time_eminute_field' name='time_eminute_field'>";
		for($i=0;$i<60;$i++){
			$eminute_select	.= ($eminute==$i)?"<option value=$i selected>$i</option>\n":"<option value=$i>$i</option>\n";
		}
		$eminute_select	.= "</select>";

		$esecond_select .= "<select id='time_esecond_field' name='time_esecond_field'>";
		for($i=0;$i<60;$i++){
			$esecond_select .= ($esecond==$i)?"<option value=$i selected>$i</option>\n":"<option value=$i>$i</option>\n";
		}
		$esecond_select .= "</select>";


		$reporttype_select .= "<select id='reporttype_field' name='reporttype_field' onChange='cmbReportTypeChanged()'>";
		$reporttype_select .= "<option value = 'S' ";
		$reporttype_select .= ($report_type == 'S')?" selected": ""; 
		$reporttype_select .= ">Summary</option>";
		$reporttype_select .= "<option value = 'D' ";
		$reporttype_select .= ($report_type == 'D')?" selected": "";
		$reporttype_select .= ">Detail</option>";
		$reporttype_select .= "</select>";
	 	
		#echo "$reporttype_select";
		#exit(0);	
		
		$limit_select .= "<select id='cdrlimit_field' name='cdrlimit_field'>";
 		foreach($limit_values as $key=>$val){
			$limit_select .= ($limit==$key)?"<option value={$key} selected>{$val}</option>":"<option value={$key} selected>{$val}</option>";
		}
	 	$limit_select .= "</select>";

		$error_chkbox .= ($error=='ok')?"<input type='checkbox' name='err_ok_field'  id='err_ok_field' value='ok' checked>":"<input type='checkbox' name='err_ok_field'  id='err_ok_field' value='ok'>";

		$form_elements = array();
		$form_elements[0]=$partition_select;
		$form_elements[1]=$carrier_select;
		$form_elements[2]=$direction_select;
		$form_elements[3]=$sdate_text;
		$form_elements[4]=$shour_select;
		$form_elements[5]=$sminute_select;
		$form_elements[6]=$ssecond_select;
		$form_elements[7]=$edate_text;
		$form_elements[8]=$ehour_select;
		$form_elements[9]=$eminute_select;
		$form_elements[10]=$esecond_select;
		$form_elements[11]=$limit_select;
		$form_elements[12]=$error_chkbox;
		$form_elements[13] =$reporttype_select;

		return $form_elements;
	}

    function hms2sec ($hms) {
 
		list($h, $m, $s) = explode (":", $hms);
		$seconds = 0;
		$seconds += (intval($h) * 3600);
		$seconds += (intval($m) * 60);
		$seconds += (intval($s));
		return $seconds;

    }

?>


<HTML>
<head>
	<title>Billing - Custom Report Viewer</title>
	<link rel="STYLESHEET" type="text/css" href='calendar-blue2.css'/>
	<script type="text/javascript" src="calendar.js"></script>
	<script type="text/javascript" src="lang/calendar-en.js"></script>
	<script type="text/javascript" src="calendar-setup.js"></script>




	<script type="text/javascript">
			function doOnBodyLoad(){
				
				rtselect= document.getElementById('reporttype_field');
				v = rtselect.options[rtselect.selectedIndex ].value;
                                if(v == 'S'){
					document.getElementById('div_lc_title').className = 'hide';
					document.getElementById('div_lc_ctrl').className = 'hide';
					document.getElementById('div_e_title').className = 'hide';
					document.getElementById('div_e_ctrl').className = 'hide';
				}
				if(v == 'D'){
					document.getElementById('div_lc_title').className = 'show';
                    document.getElementById('div_lc_ctrl').className = 'show';
					document.getElementById('div_e_title').className = 'show';
					document.getElementById('div_e_ctrl').className = 'show';

				}
				
				reloadSubject();
				
			
			}
			
			function reloadSubject(){
				cselect = document.getElementById('carrier_field');

				document.getElementById('hdn_subject').value = cselect.options[cselect.selectedIndex].text;
			}

			function cmbReportTypeChanged(){
				doOnBodyLoad();
			}
			function PopulateCarrierList(val){
			

				cselectbox = document.getElementById('carrier_field');

				for(i=cselectbox.options.length-1;i>=0;i--)
				{
					cselectbox.remove(i);
				}

				pselectbox = document.getElementById('partition_name_field');
				valuesArray = pselectbox.options[pselectbox.selectedIndex].value.split("||");

				var flag = false;
				idx = 0;
				var opt = document.createElement("option");
				for(i=2;i<valuesArray.length;i++)
				{
					optionArray = valuesArray[i].split(";");

					var myOption = opt.cloneNode(false);
					myOption.appendChild(document.createTextNode(optionArray[0]));
				  	myOption.value = optionArray[1];
				  	cselectbox.appendChild(myOption);
				}

				for(i=0;i<cselectbox.options.length;i++){
					if(cselectbox.options[i].value == val){
						idx = i;
					}
				}

				cselectbox.selectedIndex = idx;
				
				reloadSubject();


			}
	</script>
</head>
<body onLoad='doOnBodyLoad()' >
<form id="v_form" name="v_form" method="POST" action="<?php echo $form_action ?>" enctype="multipart/form-data">
	<?php include('Menu_Header.php');?>
	<table id="mainMenu">

	<tr>
			<td>Partition:</td>
			<td> <?php echo "$partition_select"; ?></td>

			<td>Cust / Supp:</td>
			<td> <?php echo "$carrier_select\n"; ?></td>

			<td>Direction:</td>
			<td> <?php echo "$direction_select\n"; ?></td>
	</tr>


	<tr>
		<td>Start Time:</td>
		<td>
			<?php
				echo "$sdate_text\n";
				echo "$shour_select\n";
				echo "$sminute_select\n";
				echo "$ssecond_select\n";
			?>
		</td>
		<td>End Time:</td>
		<td><?php
			echo "$edate_text\n";
			echo "$ehour_select\n";
			echo "$eminute_select\n";
			echo "$esecond_select\n";
			?>
		</td>
		<td></td>
		<td></td>
	 </tr>



	 <tr>
	 		<td>Report Type:</td>
	 		<td> <?php echo "$reporttype_select\n"; ?> </td>

	 		<td><div id='div_e_title' class='show'>Error Calls:</div></td>
	 		<td><div id='div_e_ctrl' class='show'><?php echo "$error_chkbox\n"; ?></div></td>
	 		
			<td><div id='div_lc_title' class='show'>Limit CDRs:</div></td>
	 		<td><div id='div_lc_ctrl' class='show'><?php echo "$limit_select\n"; ?> </div></td>
	 </tr>
	 <tr>
	 	<td>
	 		<br>
			<input type="hidden" name="posted" value="yay"/>
			<?php echo "<input type='hidden' id='hdn_subject' name='hdn_subject' value={$subject} />"; ?>
			<input type="submit" name="submit" value="Query" class="btn"/>
	  	</td>
	 </tr>

	</table>

	<?php include('bar.php'); ?>
	<?php if($cdr_table <> ""){
				echo "<input type='button' value='Download Result' class='btn' onClick=\"window.open('Download_Attacher.php?file={$file}');\" />";
		  }
	?>
	<?php echo $cdr_table ?>

	<br/>

	<span id="messageText"><?php echo "$error_msg" ?></span>
	<script type="text/javascript">
					  Calendar.setup(
					    {
					      inputField  : "date_field",   // ID of the input field
					      ifFormat    : "%Y/%m/%d",    // the date format
					      button      : "trigger"       // ID of the button
					    }
					  );

					  Calendar.setup(
					  			    {
					  			      inputField  : "date_field2",   // ID of the input field
					  			      ifFormat    : "%Y/%m/%d",    // the date format
					  			      button      : "trigger2"       // ID of the button
					  			    }
					  );
	</script>
<form>


</body>


</HTML>
