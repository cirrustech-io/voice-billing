<?php
	echo "<h3>This website has been temporarily taken down for maintenance. - Alicia </h3>";
	exit(0);

	
?>

<?php

	
	include('DBAccessor.php');
	$ip_address = $_SERVER['REMOTE_ADDR'];

	session_start();
	$_SESSION['user_name'] = "";

	// build the form action
	$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

	$error_message = "";


	if ((isset($_POST["posted"])) && ($_POST["posted"] == "yay")) {
		$username = trim($_POST['txt_username']);
		$password = trim($_POST['txt_password']);


		//checks if the username and password are correct
		$login_result=checkPassword($username,$password);

		if($login_result->EOF)
			{
				session_write_close();
				$error_message = "<p id=\"errorText\">Login failed. Please try again.</p>";
			}
			else
			{
				$_SESSION['user_name'] = $_POST['txt_username'];
				session_write_close();
				header("Location: CDR_Viewer_Generic.php");
				exit(0);
			}
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>CDR Viewer Login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
	<?php include('Menu_Header.php');?>
	<form action="<?php echo $editFormAction ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
	<table border=0>
		<tr>
	 	<table id="mainMenu">
	 		<tr>
		 		<td>User Name</td>
		 		<td>
					<input type="text" id="txt_username" name="txt_username" class= "txt" />
		 		</td>
	 		</tr>
	 		<tr>
		 		<td>Password</td>
		 		<td>
					<input type="password" id="txt_password" name="txt_password" class="txt" />
		 		</td>
	 		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<br>
	  				<input type="hidden" name="posted" value="yay"/>
	  				<input type="submit" name="Submit" value="Login" class="btn"/>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td colspan="2" align="center">
	  				<font size="-1" color="Maroon"><i><?php echo $error_message ?></i></font>
	  			</td>
	  		</tr>
		</table>
		</tr>
	</table>
	</form>
<br/>

<?php include('Page_Footer.php');?>
</body>
</html>
